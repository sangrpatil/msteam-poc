from utility.mongo_dao import insert,update,get_one as get, get_by_id
import datetime
from utility.session_manager import get as get_context, modify as update_context
from flask_babel import gettext
import pytz
import bson
"""
Function: Log conversation from the bot and the user
user_log logs user messages
bot_log logs bot messages
update is used by both user_log and bot_log to update MongoDB
"""
class ConversationLogger():

    def __init__(self,session_id, bot_id):
        self.session_id = session_id
        #from main import globalBotName

        if not get_by_id('status',session_id):
            insert('status',{"_id":bson.ObjectId(self.session_id),"date_created":pytz.utc.localize(datetime.datetime.utcnow()),"has_ended":False, "bot_id":bot_id})
            session = get_context(session_id)
            session['session_id'] = session_id
            session['session_exist'] = True
            update_context(session_id,session)

    def update(self,state):
        try:
            print('CURRENT SESSION - ', get_by_id('status',self.session_id)['history'], type(get_by_id('status',self.session_id)['history']))
            current_state = get_by_id('status',self.session_id)['current_state']
            history = get_by_id('status',self.session_id)['history']
            print('STATE - ', state, ' HISTORY - ', history)
            history.append(state)
            print('HISTORY -' , history)
            update('status', self.session_id, {'current_state': state, 'prior_state': current_state,'history':history,"date_created": pytz.utc.localize(datetime.datetime.utcnow())})
        except Exception as e:
            print('UPDATE EXCEPTION - ', e)
            history_list = []
            history_list.append(state)
            update('status', self.session_id, {'current_state': state,'history':history_list,"date_created": pytz.utc.localize(datetime.datetime.utcnow())})


    def updateIntentFoundCounter(self, message):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['intentFound'])
            current_index = current_count
            type_message_index = "intentFound." + str(current_index) + "." + "intentFound"
            time_index = "intentFound." + str(current_index) + "." + "time"
            # time = datetime.datetime.now()  # .strftime("%Y-%m-%d %H:%M")
            time = pytz.utc.localize(datetime.datetime.utcnow())
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})
        except:
            current_index = 0
            type_message_index = "intentFound." + str(current_index) + "." + "intentFound"
            time_index = "intentFound." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})

    def updateIntentNotFound(self, message):
        print('updating intent not found')

        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['intentNotFound'])
            current_index = current_count
            type_message_index = "intentNotFound." + str(current_index) + "." + "intentNotFound"
            time_index = "intentNotFound." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})
        except:
            current_index  = 0
            type_message_index = "intentNotFound." + str(current_index) + "." + "intentNotFound"
            time_index = "intentNotFound." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            update('status', self.session_id, {type_message_index: message})
            update('status', self.session_id, {time_index: time})

    def user_log(self, user_msg, ASRFlag, endSurveyFlag,guidedRestartFlag):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['conversations'])
            current_index = current_count
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index =  "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())#.strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            error = False
            update('status', self.session_id, {user_msg_index: user_msg})
            update('status', self.session_id, {type_message_index: 'user message'})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {fuzzy_index: error})
            update('status', self.session_id, {asr_index: ASRFlag})
            update('status', self.session_id, {end_survey_index: endSurveyFlag})
            update('status', self.session_id, {restartGuidedIndex: guidedRestartFlag})
            if str(endSurveyFlag) == 'True':
                print(type(user_msg), user_msg)
                if str(user_msg) in '12345':
                    update('status', self.session_id, {'Rating': user_msg})
                elif user_msg.lower().strip() != gettext('yes') and user_msg.lower().strip() != gettext('no'):
                    update('status', self.session_id, {'Comments': user_msg})
        except Exception as e:
            print('Exception: ',str(e))
            current_index = 0
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index = "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            error = False
            update('status', self.session_id, {user_msg_index: user_msg})
            update('status', self.session_id, {type_message_index: 'user message'})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {fuzzy_index: error})
            update('status', self.session_id, {asr_index: ASRFlag})
            update('status', self.session_id, {end_survey_index: endSurveyFlag})
            update('status', self.session_id, {restartGuidedIndex: guidedRestartFlag})
            update('status', self.session_id, {'Rating': "None"})
            update('status', self.session_id, {'Comments': "None"})

    def bot_log(self, bot_msg, ASRFlag, endSurveyFlag,guidedRestartFlag):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['conversations'])
            current_index = current_count
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index = "conversations." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext("Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error=True
            else:
                error=False
            update('status', self.session_id, {str(bot_msg_index): bot_msg})
            update('status', self.session_id, {str(type_message_index): 'bot message'})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {fuzzy_index: error})
            update('status', self.session_id, {asr_index: ASRFlag})
            update('status', self.session_id, {end_survey_index: endSurveyFlag})
            update('status', self.session_id, {restartGuidedIndex: guidedRestartFlag})
        except Exception as e:
            print('Exception: ',str(e))
            current_index = 0
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index =  "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            time = pytz.utc.localize(datetime.datetime.utcnow())#.strftime("%Y-%m-%d %H:%M")
            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext("Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error=True
            else:
                error=False
            update('status', self.session_id, {str(bot_msg_index): bot_msg})
            update('status', self.session_id, {str(type_message_index): 'bot message'})
            update('status', self.session_id, {time_index: time})
            update('status', self.session_id, {fuzzy_index: error})
            update('status', self.session_id, {asr_index: ASRFlag})
            update('status', self.session_id, {end_survey_index: endSurveyFlag})
            update('status', self.session_id, {restartGuidedIndex: guidedRestartFlag})