import datetime
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import logging
logger = logging.getLogger()
def send_email(report_filename, emails):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    # to_addrs = 'Jigar.Desai2@pfizer.com'
    #to_addrs = ['abhinav.mishra@pfizer.com','robert.xu@pfizer.com']
    # to_addrs = ['ruth.darcy@pfizer.com','Elaine.Dock@pfizer.com','Erum.Farooqui@pfizer.com','Alicia.Gupta@pfizer.com','Metrics.4wmr1ekjft3bao0v@u.box.com','SaiBharath.Lella@pfizer.com','abhinav.mishra@pfizer.com','robert.xu@pfizer.com']
    to_addrs = emails
    from_addr = 'gcmaproduction@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "GCMA Bot Report for " + str(today_date)
        body = "Hello " + ",\n\nThank you for using GCMA. Please find attached the Report for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)
        # with open(report_filename, "r",
        #           encoding='utf8') as fil:
        #     part = MIMEApplication(
        #         fil.read(),
        #         Name=basename(report_filename)
        #     )
        #
        # part['Content-Disposition'] = 'attachment; filename="%s"' % basename(report_filename)
        # msg.attach(part)

    except Exception as e:
        print('ERROR: Unexpected error: Could not form the message string for email. - {}'.format(e))
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.".format(e))
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        # server.login(username, password)
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        print('ERROR: Unexpected error: Could not send email. - {}'.format(e))
        logger.error("ERROR: Unexpected error: Could not send email-{}.".format(e))
        return ('Failure')

