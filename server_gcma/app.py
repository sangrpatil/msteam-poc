from flask import Flask, render_template, request,redirect, session,url_for
from flask import Flask,jsonify,Markup, session, Response
import os
from routes.admin import admin_blueprint
from routes.bot import bot_blueprint
from routes.user import user_blueprint
from routes.validate import validate_blueprint
from routes.metrics import metrics_blueprint
from routes.notifications import notifications_blueprint, listen_notification, g_notifications
from utility.logger import logger
from utility.mongo_dao import get_collection
from flask_babel import Babel, gettext
from flask_cors import CORS
import bson
import configparser
import threading

app = Flask(__name__)
app.config['SECRET_KEY'] = 'D3l01TT3'


babel = Babel(app)
CORS(app)
# Blueprints
app.register_blueprint(admin_blueprint)
app.register_blueprint(bot_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(validate_blueprint)
app.register_blueprint(metrics_blueprint)
app.register_blueprint(notifications_blueprint)

if __name__ == "__main__":

    # Set the notification fields
    #cursor = get_collection('notifications')
    #try:
    #    for documents in cursor:
    #
    #        bot_id = documents['_id']
    #        emails = documents['emails']
    #        notification_day = documents['notification_day']
    #        notification_time = documents['notification_time']
    #        notification_frequency = documents['notification_frequency']
    #        sent = documents['sent']
    #        # g_notification global variable imported from notification.py
    #        g_notifications[bson.ObjectId(bot_id)] = {"emails":emails,"notification_day":notification_day,"notification_time":notification_time,"notification_frequency":notification_frequency,"sent":sent}
    #        pass
    #except Exception as e:
    #    print('Error while setting global notification variable = {}'.format(e))

    #print('Current Global Notification Variable in app.py = {}'.format(g_notifications))
    notify_thread = threading.Thread(target=listen_notification, args=[])
    notify_thread.start()


    app_host = os.environ.get('APP_HOST') or '10.44.96.41'
    app_port = os.environ.get('APP_PORT') or '8085'
    logger.info('Application will be started at http://%s:%s',app_host,app_port)
    # app.run(host='0.0.0.0', port=8000, debug=True,threaded=True)
    app.run(host=str(app_host), port=int(app_port))

