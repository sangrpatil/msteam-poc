from flask import Blueprint, request, jsonify
from utility.mongo_dao import get_all, insert
user_blueprint = Blueprint('user', __name__, template_folder='templates')


@user_blueprint.route('/api/user/create', methods=['POST'])
def user_create():
    message = request.json
    existing_user = get_all('users','username',message['username'])
    if len(existing_user)>0:
        return jsonify(
            Status='Failed',
            Message="User creation failed"
        )
    else:
        insert('users',{"username":message['username'],'password':message['password']})
        return jsonify(
            Status='Success',
            Message="User created successfully"
        )
