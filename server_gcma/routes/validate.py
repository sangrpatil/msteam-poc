from flask import Blueprint, request, jsonify, render_template
from utility.mongo_dao import insert, get_by_id, get_one
validate_blueprint = Blueprint('validate', __name__, template_folder='templates')
import bson
import os
import datetime

@validate_blueprint.route('/validate/<string:type>/<string:id>',methods=['GET'])
def validate(type,id):
    # type = str(request.args.get('type'))
    # id = str(request.args.get('bot_id'))
    if type == 'session':
        if not (bson.objectid.ObjectId.is_valid(id) and get_by_id('context',id)):
            return jsonify(result=1,msg='Existing Session ID is invalid')
        else:
            try:
                current_status = get_by_id('status', id)
                # a cookie will not be used if it is older than 1 minute
                # logic - if window is open and user clicks on it AND over the allotted time, it will still continue
                # if the window is open and the user refreshses AND over allotted time, it will restart
                current_state = current_status['date_created']
                diff = datetime.datetime.now() - current_state
                timediff = divmod(diff.days * 86400 + diff.seconds, 60)
                timeout = os.environ.get('SESSION_TIMEOUT') or 1

                ended_flag = current_status['has_ended']
                print('---CHECKING HAS ENDED---', ended_flag)
                if timediff[0] > int(timeout) or ended_flag == True:
                    # existing_session = False
                    return jsonify(result=1,msg='Existing Session is False')
                else:
                    # existing_session = True
                    return jsonify(result=0,msg='Success')
            except Exception as e:
                print('TIME EXCEPTION - ', e)
                # existing_session = False
                return jsonify(result=1,msg='Existing Session is False')
    elif type == 'bot':
        print('bot validate check')
        if not (bson.objectid.ObjectId.is_valid(id) and get_by_id('bot', id)):
            return jsonify(result=1,msg='Bot ID is invalid')
        print('bot id is fine')
        return jsonify(result=0,msg='Success')
    else:
        return jsonify(result=1,msg='Must send bot id or session id')
