from flask import Blueprint, render_template, request, jsonify, session, Markup, send_from_directory
from utility.logger import logger
from utility.generate_reports_json import generateReports
from utility.generate_reports_excel import generateReportsWeekly
import json
import os
metrics_blueprint = Blueprint('metrics', __name__, template_folder='templates')
from utility.mail_pfizer import send_email

# Return metrics of all bots
@metrics_blueprint.route('/api/metrics', methods=['GET'])
def metrics_all_bots():
    if request.method == 'GET':
        # Call the function with not bot_id passed
        returnedjson = generateReports()
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        # return returnedjson
        pass

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>', methods=['GET'])
def metrics_bot(bot_id):
    if request.method == 'GET':
        #Call the function with bot_id passed
        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>/download', methods=['GET'])
def metrics_download(bot_id):
    if request.method == 'GET':
        try:
            filename1 = generateReportsWeekly(bot_id)
            file_name = str(bot_id) + '.xlsx'
            path = os.path.abspath("static/reports/")
            print('FILE PATH IS - ', path, 'NAME IS - ', file_name)
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/metrics/<bot_id>/ytd', methods=['GET'])
def metrics_ytd_bot(bot_id):
    if request.method == 'GET':
        try:
            #Call the function with bot_id passed
            emails = [{"email": "robert.xu@pfizer.com"},{"email": "SaiBharath.Lella@pfizer.com"}]
            #emails = [{"email":"robert.xu@pfizer.com"},{"email":"Laura.Poiger@pfizer.com"},{"email":"janine.welen@pfizer.com"},{"email":"Claudia.Pintus@pfizer.com"},{"email":"Steven.Ring@pfizer.com"},{"email":"chatbot-hilfefuermich@buddybrand.de"}]
            #if bot_id == '5bd1293790d3800001d5a503':
            #    bot_name = 'PACO_MBC'
            #else:
            #    bot_name = 'PACO_RCC'
            #current_moment = datetime.now()

            file_loc = generateReportsWeekly(bot_id)
            print('file loc is ', file_loc)
            email_list = []
            for email_name in emails:
                email_list.append(email_name['email'])
            try:
                sent_status = send_email(file_loc, email_list)
            except Exception as e:
                print('email send error - ', e)
                return jsonify(status = str(e))
            if sent_status == 'Success':
                return jsonify(
                    status = "Success"
                )
            else:
                return jsonify(
                    status="Failure"
                )
        except Exception as e:
            return jsonify(
                status = str(e)
            )

