# Mail
# Save this in mail.py inside utility
# A python module to send emails
import smtplib
from abc import ABCMeta
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

from utility.logger import logger


class Mail:
    __metaclass__ = ABCMeta

    def __init__(self, smtp_host, smtp_port, username, password):
        self.username = username
        self.password = password
        self.smtp_host = smtp_host
        self.smtp_port = smtp_port
        self.COMMASPACE = ', '

    # REF https://gist.github.com/rdempsey/22afd43f8d777b78ef22

    def send(self, email_addresses, subject, message, attach_location='', from_name='Admin'):
        outer = MIMEMultipart()
        outer['Subject'] = subject
        outer['To'] = self.COMMASPACE.join(email_addresses)
        outer['From'] = from_name + '<' + self.username + '>'  # "Your name <Your email>
        body = message
        outer.attach(MIMEText(body, 'plain'))
        outer.attach(MIMEText(body, 'html'))
        # handling attachments
        if attach_location == '':
            pass
        else:
            with open(attach_location, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=basename(attach_location)
                )
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(attach_location)
            outer.attach(part)
            pass

        composed = outer.as_string()

        # Send the email
        try:
            with smtplib.SMTP(self.smtp_host, self.smtp_port) as s:
                s.ehlo()
                s.starttls()
                s.ehlo()
                # s.login(self.username, self.password)
                s.sendmail(self.username, email_addresses, composed)
                s.close()
            return 1, 'Email sent successfully'
        except Exception as e:
            logger.error("Mail error: ", str(e))
            return 0, 'Failed to send email - ' + str(e)

        # Decide whether 0 for success or the other way
