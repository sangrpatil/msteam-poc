from datetime import datetime
from datetime import timedelta

import dateutil.parser
import numpy as np
import pytz
import xlsxwriter
from dateutil import tz
from utility.mongo_dao import *

to_zone = tz.gettz('UTC-5')
host = os.environ.get('MONGO_HOST') or 'localhost'
port = os.environ.get('MONGO_PORT') or '27017'
username = os.environ.get('MONGO_USERNAME') or 'admin'
password = os.environ.get('MONGO_PASSWORD') or 'admin'
database = os.environ.get('MONGO_DB') or 'admin'

client = MongoClient('mongodb://' + username + ':' + password + '@' + host + ':' + str(port) + '/admin')


def get_dashboard(start, end, bot_id=None, bot_name='', download=False):
    try:
        start_date, end_date = start_end(start, end)
        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        surveyKeyList = []
        surveyValueList = {'Yes': [], 'No': []}
        latencyKeyList = []
        latencyValueList = []
        ##################### Number of Questions By Day ####################
        data = get_DashData(start, end, bot_id)

        # end_date = end_date  # + timedelta(days=-1)
        try:
           num_days=(end_date-start_date).days
           date_list= [(start_date + timedelta(i)).strftime("%Y-%m-%d") for i in range(num_days)]

        except Exception as e:
            date_list = [str(start_date).split(' ')[0]]
        survey_val_list_yes = []
        survey_val_list_no = []
        date_val_list = []
        if data == "No Data" and download:
            response = {"dateKeyList": dateKeyList,
                        "dateValuesList": dateValuesList,
                        "timeKeyList": timeKeyList,
                        "timeValueList": timeValueList,
                        "latencyKeyList": latencyKeyList,
                        "latencyValueList": latencyValueList,
                        "surveyKeyList": surveyKeyList,
                        "surveyValueList": surveyValueList,
                        "intentFoundList": [],
                        "intentNotFoundList": [],
                        "num_convo": []}
            return response
        elif data == "No Data":
            final_json = {
                "bot_name": bot_name,
                "graph_data": [
                    {
                        "graph_name": "Number of Conversations",
                        "x_values": dateKeyList,
                        "y_values": dateValuesList,
                        "x_axis_title": "Dates",
                        "y_axis_title": "Number of Conversations"
                    },
                    {
                        "graph_name": "Number of Conversations Trend",
                        "x_values": timeKeyList,
                        "y_values": timeValueList,
                        "x_axis_title": "Time of Day(EST)",
                        "y_axis_title": "Number of Conversations"
                    },
                    {
                        "graph_name": "Survey Response Summary",

                        "bar1": {
                            'x': surveyKeyList,
                            'y': surveyValueList['Yes'],
                            'name': 'Yes',
                            'type': 'bar'
                        },
                        "bar2": {
                            'x': surveyKeyList,
                            'y': surveyValueList['No'],
                            'name': 'No',
                            'type': 'bar'
                        },
                        "x_axis_title": "Yes/No",
                        "y_axis_title": "Number of Responses"
                    },
                    {
                        "graph_name": "Overall Bot Latency Trend",
                        "x_values": latencyKeyList,
                        "y_values": latencyValueList,
                        "x_axis_title": "Time of Day(EST)",
                        "y_axis_title": "Average Response Latency (sec)"
                    }

                ]
            }
            return final_json

        for key, value in sorted(data["dateDict"].items()):  # data["dateDict"].items()
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)
        ######################## Peak Engagement Hours #########################

        for key, value in sorted(data["timeDict"].items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)
        ################################ User Satisfaction Distribution #################

        surveyKeyList = list(data["surveyDict"].keys())
        surveyValueList = {'Yes': [], 'No': []}
        for key, value in (data["surveyDict"].items()):
            surveyValueList['Yes'].append(value.count('Yes') + value.count('yes'))
            surveyValueList['No'].append(value.count('No') + value.count('no'))
        surveyKeyList = surveyKeyList[::-1]
        surveyValueList['Yes'] = surveyValueList['Yes'][::-1]
        surveyValueList['No'] = surveyValueList['No'][::-1]
        ########################## LATENCY GRAPH ##############################

        for key, value in sorted(data["latencyDict"].items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum'] / value['numConvos']

                    if finalLatencyAvg < 0:
                        finalLatencyAvg = 0
                    if finalLatencyAvg > 1:
                        finalLatencyAvg = 1 / finalLatencyAvg
                else:
                    finalLatencyAvg = 0
           # finalLatencyAvg= '%.4f' % finalLatencyAvg
            latencyValueList.append(round(finalLatencyAvg,4))
        ##########################################################
        for i in date_list:
            if i in dateKeyList:
                date_val_list.append(dateValuesList[dateKeyList.index(i)])
            else:
                date_val_list.append(0)
            if i in surveyKeyList:
                indx = surveyKeyList.index(i)
                survey_val_list_yes.append(surveyValueList['Yes'][indx])
                survey_val_list_no.append(surveyValueList['No'][indx])
            else:
                survey_val_list_yes.append(0)
                survey_val_list_no.append(0)
        dateKeyList = date_list
        surveyKeyList = date_list
        dateValuesList = date_val_list
        surveyValueList['Yes'] = survey_val_list_yes
        surveyValueList['No'] = survey_val_list_no
        final_json = {
            "bot_name": bot_name,
            "graph_data": [
                {
                    "graph_name": "Number of Conversations",
                    "x_values": dateKeyList,
                    "y_values": dateValuesList,
                    "x_axis_title": "Dates",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "Number of Conversations Trend",
                    "x_values": timeKeyList,
                    "y_values": timeValueList,
                    "x_axis_title": "Time of Day(EST)",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "Survey Response Summary",

                    "bar1": {
                        'x': surveyKeyList,
                        'y': surveyValueList['Yes'],
                        'name': 'Yes',
                        'type': 'bar'
                    },
                    "bar2": {
                        'x': surveyKeyList,
                        'y': surveyValueList['No'],
                        'name': 'No',
                        'type': 'bar'
                    },
                    "x_axis_title": "Dates",
                    "y_axis_title": "Number of Responses"
                },
                {
                    "graph_name": "Overall Bot Latency Trend",
                    "x_values": latencyKeyList,
                    "y_values": latencyValueList,
                    "x_axis_title": "Time of Day(EST)",
                    "y_axis_title": "Average Response Latency (sec)"
                }

            ]
        }
        if download == True:
            return {"dateKeyList": dateKeyList,
                    "dateValuesList": dateValuesList,
                    "timeKeyList": timeKeyList,
                    "timeValueList": timeValueList,
                    "latencyKeyList": latencyKeyList,
                    "latencyValueList": latencyValueList,
                    "surveyKeyList": surveyKeyList,
                    "surveyValueList": surveyValueList,
                    "intentFoundList": data["intfound"],
                    "intentNotFoundList": data["intnotfound"],
                    "num_convo": data["numconvo"]}

        return final_json
    except Exception as e:
        return "Failure"


def get_intent_found(start, end, bot_id, offset=0, next=50):
    try:
        bot_id = str(bot_id)
        start_date, end_date = start_end(start, end)
        res = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentFound"
                    },
                    "id": "$_id",
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },

            {
                "$project": {"row": {
                    "ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.userQuestion",
                    "time": "$wordsAsKeyValuePairs.v.time",

                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "EMAIL ID": "$wordsAsKeyValuePairs.v.email",
                    # "SESSION ID": "$wordsAsKeyValuePairs.v.taskId",
                    "QUERY TYPE": "$wordsAsKeyValuePairs.v.queryType",
                    "TASK ID": "$wordsAsKeyValuePairs.v.taskId",
                    'TASK TEXT': {"$ifNull": ["$wordsAsKeyValuePairs.v.intentFound", "None"]}

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},
            {"$project": {
                "_id": 0,
                "total": "$myCount",
                "intent": {"$slice": ["$convo", offset, next]}}}

        ])

        result = [i for i in res]
        count = 0
        id_dic = {}
        for j in result[0]["intent"]:
            j['ID'] = str(j['ID']).replace('ObjectId(', '').replace(')', '')
            if j['ID'] not in id_dic:
                id_dic[j['ID']] = []

            hr, mn = j['TIME'].split(':')

            j['TIME'] = hr + ':' + mn
        count = len(id_dic)
        return result[0]["intent"], result[0]["total"]
    except Exception as e:
        print(e)
        return [], 0



def get_intent_NotFound(start, end, bot_id, offset=0, next=50):
    try:
        bot_id = str(bot_id)
        start_date, end_date = start_end(start, end)
        res = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentNotFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentNotFound"
                    },
                    "id": "$_id",
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                    "ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.intentNotFound",
                    "time": "$wordsAsKeyValuePairs.v.time",
                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "EMAIL ID": "$wordsAsKeyValuePairs.v.email",
                    "QUERY TYPE": "$wordsAsKeyValuePairs.v.queryType",
                    "TASK ID": "$wordsAsKeyValuePairs.v.taskId",

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},
            {"$project": {
                "_id": 0,
                "total": "$myCount",
                "intent": {"$slice": ["$convo", offset, next]}}}

        ])

        result = [i for i in res]
        count = 0
        id_dic = {}
        for j in result[0]["intent"]:
            j['ID'] = str(j['ID']).replace('ObjectId(', '').replace(')', '')
            if j['ID'] not in id_dic:
                id_dic[j['ID']] = []
            hr, mn = j['TIME'].split(':')
            j['TIME'] = hr + ':' + mn
        count = len(id_dic)
        return result[0]["intent"], result[0]["total"]
    except Exception as e:
        print(e)
        return [], 0


def get_ConvoLog(start, end, bot_id, offset=0, next=50):
    try:
        bot_id = str(bot_id)
        start_date, end_date = start_end(start, end)
        res = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"conversations": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$conversations"
                    },

                    "id": "$_id"

                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },

            {
                "$project": {"row": {
                    "ID": "$id",
                    "FROM": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                                 "bot message"]}, "then": "ChatBot", "else": "User"}}
                    ,
                    "MESSAGE": "$wordsAsKeyValuePairs.v.message",
                    "time": "$wordsAsKeyValuePairs.v.time",

                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "Survey": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.endSurveyFlag",
                                                 True]},
                                  "then": "$wordsAsKeyValuePairs.v.message", "else": None}},

                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},

                }}},

            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},
            {"$project": {
                "_id": 0,
                "total": "$myCount",
                "intent": {"$slice": ["$convo", offset, next]}}}

        ])

        result = [i for i in res]
        count = 0
        id_dic = {}
        for j in result[0]["intent"]:
            j['ID'] = str(j['ID']).replace('ObjectId(', '').replace(')', '')
            if j['ID'] not in id_dic:
                id_dic[j['ID']] = []

            hr, mn = j['TIME'].split(':')

            j['TIME'] = hr + ':' + mn
        count = len(id_dic)
        return result[0]["intent"], result[0]["total"]
    except Exception as e:
        print(e)
        return [], 0


def get_DashData(start, end, bot_id, group=None):
    try:
        bot_id = str(bot_id)
        start_date, end_date = start_end(start, end)
        resintentnot = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentNotFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentNotFound"
                    },
                    "id": "$_id",
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                    "ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.intentNotFound",

                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "EMAIL ID": "$wordsAsKeyValuePairs.v.email",
                    "SESSION ID": "$wordsAsKeyValuePairs.v.TaskId",
                    "QUERY TYPE": "$wordsAsKeyValuePairs.v.QueryType",
                    "TASK ID": "$wordsAsKeyValuePairs.v.TaskId",

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

        ])
        resintent = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentFound"
                    },
                    "id": "$_id",
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                    "ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.userQuestion",
                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "EMAIL ID": "$wordsAsKeyValuePairs.v.email",
                    "SESSION ID": "$wordsAsKeyValuePairs.v.TaskId",
                    "QUERY TYPE": "$wordsAsKeyValuePairs.v.QueryType",
                    "TASK ID": "$wordsAsKeyValuePairs.v.TaskId",
                    'TASK TEXT': {"$ifNull": ["$wordsAsKeyValuePairs.v.intentFound", "None"]},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

        ])
        resconvo = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"conversations": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$conversations"
                    },

                    "id": "$_id"

                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                    "ID": "$id",
                    "FROM": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                                 "bot message"]}, "then": "ChatBot", "else": "User"}}
                    ,
                    "MESSAGE": "$wordsAsKeyValuePairs.v.message",
                    "time": "$wordsAsKeyValuePairs.v.time",

                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},
                    "Survey": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.endSurveyFlag",
                                                 True]},
                                  "then": "$wordsAsKeyValuePairs.v.message", "else": None}},

                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}}

                }}},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

        ])
        try:
            intnotfound = [i for i in resintentnot][0]["convo"]
        except Exception as e:
            print(e)
            intnotfound = []
        result = intnotfound  # [i for i in resintentnot][0]["convo"]
        try:
            intfound = [i for i in resintent][0]["convo"]
        except Exception as e:
            print(e)
            intfound = []
        result.extend(intfound)

        convo_result = [i for i in resconvo]
        count = 0
        id_dic = {}
        try:
            for j in convo_result[0]["convo"]:

                if j['ID'] not in id_dic:
                    j['ID'] = str(j['ID']).replace('ObjectId(', '').replace(')', '')
                    id_dic[j['ID']] = []
                    # j['ID'] = id_dic[j['ID']]
                # else:
                #     count += 1
                #     id_dic[j['ID']] = count
                #     j['ID'] = id_dic[j['ID']]
            count = len(id_dic)
            convo = [i for i in convo_result][0]["convo"]
        except Exception as e:
            convo = []
            return "No Data"
        dateDict = {}
        timeDict = {}
        latencyDict = {}
        surveyDict = {}
        for k in range(0, 24):
            timeDict[k] = []
            latencyDict[k] = []

        # convo=[i for i in result1][0]["convo"]
        botJustSentMessage = True
        userReplyTime = utc_to_est(datetime.utcnow())
        for j in convo:
            hour = int(j["TIME"].split(":")[0])
            if j["DATE"] in dateDict:
                dateDict[j["DATE"]].append(j["ID"])
            else:
                dateDict[j["DATE"]] = [j["ID"]]
            if j["DATE"] in surveyDict:
                surveyDict[j["DATE"]].append(j["Survey"])
            else:
                surveyDict[j["DATE"]] = [j["Survey"]]
            if hour in timeDict:
                timeDict[hour].append(j["ID"])
            else:
                timeDict[hour] = [j["ID"]]
            if hour in latencyDict:
                if latencyDict[hour] == []:
                    # only want to calculate "bot message timestamp" - "user message timestamp"
                    if j['FROM'] == 'User':
                        userReplyTime = j['time']
                        userReplyTime = utc_to_est(userReplyTime)
                    latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                elif j['FROM'] == 'User':
                    userReplyTime = j['time']
                    botJustSentMessage = False
                elif j['FROM'] == 'ChatBot' and botJustSentMessage == False:
                    latency = abs((j['time'] - userReplyTime)).total_seconds()
                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                    latencySum = latencyDict[hour]['latencySum'] + latency
                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                    botJustSentMessage = True

        return {"dateDict": dateDict,
                "timeDict": timeDict,
                "surveyDict": surveyDict,
                "latencyDict": latencyDict,
                "intnotfound": intnotfound,
                "intfound": intfound,
                "numconvo": count
                }
    except Exception as e:
        print(e)
        return "Failure"


def generateReports_convo(start, end, bot_id=None):
    fileTitle = 'static/reports/Chatbot_Report_convo' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)

    convoLog = workbook.add_worksheet('ConversationLog')
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    headers = ['ID', 'FROM', 'MESSAGE', 'DATE', 'TIME']

    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:I1')
    # master formatting style

    tempconvodata, num_convo = convo_complete(start, end, bot_id)
    conversation = tempconvodata[0]['convo']
    count = 1
    for cnv in conversation:
        dataRow = [
            str(cnv['ID']).replace('ObjectId(', '').replace(')', ''),
            cnv['FROM'],
            str(cnv['MESSAGE']),
            cnv['DATE'],
            cnv['TIME']]
        convoLog.write_row(count, 0, dataRow)
        count += 1

    workbook.close()
    return fileTitle


def convo_complete(start, end, bot_id=None):
    bot_id = str(bot_id)
    start_date, end_date = start_end(start, end)
    res = client[database]["status"].aggregate([
        {
            "$match": {"$and": [

                {"bot_id": {"$eq": bot_id}},
                {"conversations": {
                    "$exists": "true"
                }}]
            }

        },
        {
            "$project": {
                "wordsAsKeyValuePairs": {
                    "$objectToArray": "$conversations"
                },

                "id": "$_id",

            }
        },
        {
            "$unwind": "$wordsAsKeyValuePairs"
        },
        {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
        {
            "$match": {"$and": [

                {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
            }

        },
        {
            "$project": {"row": {
                "ID": "$id",
                "FROM": {
                    "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                             "bot message"]}, "then": "ChatBot", "else": "User"}}
                ,
                "MESSAGE": "$wordsAsKeyValuePairs.v.message",

                "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},

                "TIME": {"$dateToString": {"format": "%H:%M:%S", "date": '$wordsAsKeyValuePairs.v.time'}},

                "time": "$wordsAsKeyValuePairs.v.time",
                "DATE_TIME": "$wordsAsKeyValuePairs.v.time",

            }
            }},

        {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

    ])
    result = [i for i in res]
    count = 0
    id_dic = {}
    try:
        for j in result[0]["convo"]:
            if j['ID'] in id_dic:
                id_dic[j['ID']] = []

        count = len(id_dic)
        return result, count
    except Exception as e:
        return [], 0


def intent_complete(start, end, bot_id=None):
    try:
        bot_id = str(bot_id)
        start_date, end_date = start_end(start, end)
        resintnot = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentNotFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentNotFound"
                    },
                    "id": "$_id",

                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},

            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                    "ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.intentNotFound",
                    "time": "$wordsAsKeyValuePairs.v.time",

                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "EMAIL ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.email", "None"]},
                    "QUERY TYPE": {"$ifNull": ["$wordsAsKeyValuePairs.v.queryType", "None"]},
                    "TASK ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.taskId", "None"]},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}}

        ])

        resintfound = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"intentFound": {
                        "$exists": "true"
                    }}]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$intentFound"
                    },
                    "id": "$_id"
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {"row": {
                    "ID": "$id",
                    "USER QUESTION": "$wordsAsKeyValuePairs.v.userQuestion",
                    "time": "$wordsAsKeyValuePairs.v.time",

                    "DATE": {"$dateToString": {"format": "%Y-%m-%d", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "TIME": {"$dateToString": {"format": "%H:%M", "date": '$wordsAsKeyValuePairs.v.time'}},

                    "EMAIL ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.email", "None"]},
                    # "SESSION ID": "$wordsAsKeyValuePairs.v.taskId",
                    "QUERY TYPE": {"$ifNull": ["$wordsAsKeyValuePairs.v.queryType", "None"]},
                    "TASK ID": {"$ifNull": ["$wordsAsKeyValuePairs.v.taskId", "None"]},
                    'TASK TEXT': {"$ifNull": ["$wordsAsKeyValuePairs.v.intentFound", "None"]},

                }
                }},
            {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

        ])

        result1 = [i for i in resintfound]
        count = 0
        id_dic1 = {}
        if result1 == []:
            intentfound = []
        else:
            for j in result1[0]["convo"]:
                if j['ID'] not in id_dic1:
                    id_dic1[j['ID']] = []

            count = len(id_dic1)
            intentfound = result1[0]["convo"]
        result = [i for i in resintnot]
        count = 0
        id_dic2 = {}
        if result == []:
            intentnotfound = []
        else:
            for j in result[0]["convo"]:
                if j['ID'] not in id_dic2:
                    id_dic2[j['ID']] = []
            count = len(id_dic2)
            intentnotfound = result[0]["convo"]

        return {"intentnotfound": intentnotfound, "intentfound": intentfound}
    except Exception as e:
        print(e)
        return {"intentnotfound": [], "intentfound": []}


def graph_download(bot_id, bot_name, start, end):
    response = get_dashboard(start, end, bot_id, bot_name, True)
    fileTitle = 'static/reports/Chatbot_Report_Graph' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'

    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)

    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    dashboard = workbook.add_worksheet('Dashboard')
    dataSheet = workbook.add_worksheet('DataSheet')
    surveySheet = workbook.add_worksheet('SurveySheet')
    convoByDay = workbook.add_worksheet('NumberOfConversationsByDay')
    convoByHour = workbook.add_worksheet('NumberOfConversationsByHour')
    botlatencyByHour = workbook.add_worksheet('BotLatencyByHour')

    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    title = 'Chatbot Dashboard: ' + datetime.today().strftime('%m-%d-%Y')
    formatting.set_font_size(20)
    dashboard.merge_range('A1:S2', title, formatting)
    formatting.set_font_size(11)

    totalsum = []
    dashboard.set_column(1, 1, 50)
    surveySheet.write_row('A1:D1', ['Date', 'Yes', 'No', 'Total survey for the day'], formatting)
    surveySheet.write_column('A2', response['surveyKeyList'])
    surveySheet.write_column('B2', response['surveyValueList']['Yes'])
    surveySheet.write_column('C2', response['surveyValueList']['No'])
    totalsum = np.array(response['surveyValueList']['Yes']) + np.array(response['surveyValueList']['No'])
    surveySheet.write_column('D2', totalsum)
    convoByDay.write_row('A1:B1', ['Date', 'Number'], formatting)
    convoByDay.write_column('A2', response['dateKeyList'])
    convoByDay.write_column('B2', response['dateValuesList'])
    convoByHour.write_row('A1:B1', ['Hour', 'Number'], formatting)
    convoByHour.write_column('A2', response['timeKeyList'])
    convoByHour.write_column('B2', response['timeValueList'])
    botlatencyByHour.write_row('A1:B1', ['Hour', 'Latency(Sec)'], formatting)
    botlatencyByHour.write_column('A2', response['latencyKeyList'])
    botlatencyByHour.write_column('B2', response['latencyValueList'])
    # initialize charts
    barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
    lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
    lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})
    barGraph_Survey = workbook.add_chart({'type': 'column'})

    dataSheet.write_row(0, 0, response['dateKeyList'])
    dataSheet.write_row(1, 0, response['dateValuesList'])
    dataSheet.write_row(2, 0, response['timeKeyList'])
    dataSheet.write_row(3, 0, response['timeValueList'])
    dataSheet.write_row(6, 0, response['latencyKeyList'])
    dataSheet.write_row(7, 0, response['latencyValueList'])
    dataSheet.write_row(8, 0, response['surveyKeyList'])
    dataSheet.write_row(10, 0, response['surveyValueList']['Yes'])
    dataSheet.write_row(12, 0, response['surveyValueList']['No'])
    barGraph_ConvByDay.add_series({
        'name': 'Number of Conversations',
        'categories': ['DataSheet', 0, 0, 0, len(response['dateKeyList']) - 1],
        'values': ['DataSheet', 1, 0, 1, len(response['dateKeyList']) - 1]
    })

    barGraph_ConvByDay.set_x_axis({'name': 'Dates'})
    barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
    barGraph_ConvByDay.set_legend({'none': True})

    lineGraph_ConvByHour.add_series({
        'name': 'Number of Conversations Trend',
        'categories': '=DataSheet!$A$3:$Y$3',
        'values': '=DataSheet!$A$4:$Y$4'
    })
    lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day(EST)'})
    lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
    lineGraph_ConvByHour.set_legend({'none': True})

    lineGraph_LatencyByHour.add_series({
        'name': 'Overall Bot Latency Trend',
        'categories': ['DataSheet', 6, 0, 6, len(response['latencyKeyList']) - 1],
        'values': ['DataSheet', 7, 0, 7, len(response['latencyValueList']) - 1]
    })

    lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day(EST)'})
    lineGraph_LatencyByHour.set_y_axis({'name': 'Average Response Latency (sec)'})
    lineGraph_LatencyByHour.set_legend({'none': True})

    barGraph_Survey.add_series({
        'name': 'Yes',
        'categories': ['SurveySheet', 1, 0, len(response['surveyKeyList']), 0],
        'values': ['SurveySheet', 1, 1, len(response['surveyKeyList']), 1]
    })
    barGraph_Survey.add_series({
        'name': 'No',
        'categories': ['SurveySheet', 1, 0, len(response['surveyKeyList']), 0],
        'values': ['SurveySheet', 1, 2, len(response['surveyKeyList']), 2]
    })
    barGraph_Survey.set_x_axis({'name': 'Dates'})
    barGraph_Survey.set_y_axis({'name': 'Number of Responses'})
    barGraph_Survey.set_title({'name': 'Survey Response Summary'})
    # barGraph_Survey.set_legend({'none': True})
    # insert the charts into the dashboard
    dashboard.insert_chart('B8', barGraph_ConvByDay)
    dashboard.insert_chart('G8', lineGraph_ConvByHour)
    dashboard.insert_chart('B25', lineGraph_LatencyByHour)
    dashboard.insert_chart('G25', barGraph_Survey)

    # Hide the datasheet used to create the charts
    dataSheet.hide()
    workbook.close()
    return fileTitle


def generateReports_intentfound(start, end, bot_id=None):
    fileTitle = 'static/reports/Chatbot_Report_intentfound' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    intentFound = workbook.add_worksheet('Intents Found')

    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    intent = intent_complete(start, end, bot_id)
    intentFoundList = intent["intentfound"]
    """
    Create sheets for intent found and intent not found
    """

    # user_id and secret_key in same order
    headersIntent = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID']
    headersIntentF = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID', 'TASK TEXT']

    # create sheet for intent found
    intentFound.write_row('A1', headersIntentF, cell_format=formatting)
    intentFound.set_column(0, 7, 20)
    intentFound.autofilter('A1:D1')
    h = 1
    for item in intentFoundList:
        intentFound.write_row(h, 0, [str(item['ID']).replace('ObjectId(', '').replace(')', ''),
                                     item['USER QUESTION'],
                                     item['EMAIL ID'],
                                     item['DATE'],
                                     item['TIME'],
                                     item['QUERY TYPE'],
                                     item['TASK ID'],
                                     item['TASK TEXT']])
        h += 1

    workbook.close()
    return fileTitle


def generateReports_intentnotfound(start, end, bot_id=None):
    fileTitle = 'static/reports/Chatbot_Report_intentnotfound' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    intentNotFound = workbook.add_worksheet('Intent Not Found')

    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    intent = intent_complete(start, end, bot_id)
    intentNotFoundList = intent["intentnotfound"]

    """
    Create sheets for intent found and intent not found
    """

    # user_id and secret_key in same order
    headersIntent = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID']
    # #create sheet for intent not found
    intentNotFound.write_row('A1', headersIntent, cell_format=formatting)
    intentNotFound.set_column(0, 7, 20)
    intentNotFound.autofilter('A1:D1')
    h = 1

    for item in intentNotFoundList:
        intentNotFound.write_row(h, 0, [str(item['ID']).replace('ObjectId(', '').replace(')', ''),
                                        item['USER QUESTION'],
                                        item['EMAIL ID'],
                                        item['DATE'],
                                        item['TIME'],
                                        item['QUERY TYPE'],
                                        item['TASK ID']])
        h += 1

    return fileTitle


def generateReports_intent(start, end, bot_id=None):
    fileTitle = 'static/reports/Chatbot_Report_intent' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    intentNotFound = workbook.add_worksheet('Intent Not Found')
    intentFound = workbook.add_worksheet('IntentFound')

    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    intent = intent_complete(start, end, bot_id)
    intentNotFoundList = intent["intentnotfound"]
    intentFoundList = intent["intentfound"]
    """
    Create sheets for intent found and intent not found
    """

    # user_id and secret_key in same order
    headersIntent = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID']
    headersIntentF = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID', 'TASK TEXT']

    # #create sheet for intent
    intentFound.write_row('A1', headersIntentF, cell_format=formatting)
    intentFound.set_column(0, 7, 20)
    intentFound.autofilter('A1:D1')
    l = 1

    for item in intentFoundList:
        intentFound.write_row(l, 0, [str(item['ID']).replace('ObjectId(', '').replace(')', ''),
                                     item['USER QUESTION'],
                                     item['EMAIL ID'],
                                     item['DATE'],
                                     item['TIME'],
                                     item['QUERY TYPE'],
                                     item['TASK ID'],
                                     item['TASK TEXT']])
        l += 1
    intentNotFound.write_row('A1', headersIntent, cell_format=formatting)
    intentNotFound.set_column(0, 7, 20)
    intentNotFound.autofilter('A1:D1')
    h = 1

    for item in intentNotFoundList:
        intentNotFound.write_row(h, 0, [str(item['ID']).replace('ObjectId(', '').replace(')', ''),
                                        item['USER QUESTION'],
                                        item['EMAIL ID'],
                                        item['DATE'],
                                        item['TIME'],
                                        item['QUERY TYPE'],
                                        item['TASK ID']])
        h += 1

    return fileTitle


def generateReport(bot_id, bot_name, start, end, report=None):
    response = get_dashboard(start, end, bot_id, bot_name, True)
    if report == None:
        fileTitle = 'static/reports/Chatbot_Report_All' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    else:
        fileTitle = 'static/reports/Chatbot_Report_' + str(report) + datetime.today().strftime('%m-%d-%Y') + '.xlsx'

    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)

    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    dashboard = workbook.add_worksheet('Dashboard')
    dataSheet = workbook.add_worksheet('DataSheet')
    surveySheet = workbook.add_worksheet('SurveySheet')
    convoByDay = workbook.add_worksheet('NumberOfConversationsByDay')
    convoByHour = workbook.add_worksheet('NumberOfConversationsByHour')
    botlatencyByHour = workbook.add_worksheet('BotLatencyByHour')
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    title = 'Chatbot Dashboard: ' + datetime.today().strftime('%m-%d-%Y')
    formatting.set_font_size(20)
    dashboard.merge_range('A1:S2', title, formatting)
    formatting.set_font_size(11)

    totalsum = []
    dashboard.set_column(1, 1, 50)
    surveySheet.write_row('A1:D1', ['Date', 'Yes', 'No', 'Total survey for the day'], formatting)
    surveySheet.write_column('A2', response['surveyKeyList'])
    surveySheet.write_column('B2', response['surveyValueList']['Yes'])
    surveySheet.write_column('C2', response['surveyValueList']['No'])
    totalsum = np.array(response['surveyValueList']['Yes']) + np.array(response['surveyValueList']['No'])
    surveySheet.write_column('D2', totalsum)
    convoByDay.write_row('A1:B1', ['Date', 'Number'], formatting)
    convoByDay.write_column('A2', response['dateKeyList'])
    convoByDay.write_column('B2', response['dateValuesList'])
    convoByHour.write_row('A1:B1', ['Hour', 'Number'], formatting)
    convoByHour.write_column('A2', response['timeKeyList'])
    convoByHour.write_column('B2', response['timeValueList'])
    botlatencyByHour.write_row('A1:B1', ['Hour', 'Latency(Sec)'], formatting)
    botlatencyByHour.write_column('A2', response['latencyKeyList'])
    botlatencyByHour.write_column('B2', response['latencyValueList'])
    # initialize charts
    barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
    lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
    lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})
    barGraph_Survey = workbook.add_chart({'type': 'column'})

    dataSheet.write_row(0, 0, response['dateKeyList'])
    dataSheet.write_row(1, 0, response['dateValuesList'])
    dataSheet.write_row(2, 0, response['timeKeyList'])
    dataSheet.write_row(3, 0, response['timeValueList'])
    dataSheet.write_row(6, 0, response['latencyKeyList'])
    dataSheet.write_row(7, 0, response['latencyValueList'])
    dataSheet.write_row(8, 0, response['surveyKeyList'])
    dataSheet.write_row(10, 0, response['surveyValueList']['Yes'])
    dataSheet.write_row(12, 0, response['surveyValueList']['No'])
    barGraph_ConvByDay.add_series({
        'name': 'Number of Conversations',
        'categories': ['DataSheet', 0, 0, 0, len(response['dateKeyList']) - 1],
        'values': ['DataSheet', 1, 0, 1, len(response['dateKeyList']) - 1]
    })

    barGraph_ConvByDay.set_x_axis({'name': 'Dates'})
    barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
    barGraph_ConvByDay.set_legend({'none': True})

    lineGraph_ConvByHour.add_series({
        'name': 'Number of Conversations Trend',  # 'Peak Engagement Hours',
        'categories': '=DataSheet!$A$3:$Y$3',
        'values': '=DataSheet!$A$4:$Y$4'
    })
    lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day(EST)'})
    lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
    lineGraph_ConvByHour.set_legend({'none': True})

    lineGraph_LatencyByHour.add_series({
        'name': 'Overall Bot Latency Trend',  # 'Bot Response Latency',
        'categories': ['DataSheet', 6, 0, 6, len(response['latencyKeyList']) - 1],
        'values': ['DataSheet', 7, 0, 7, len(response['latencyValueList']) - 1]
    })

    lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day(EST)'})
    lineGraph_LatencyByHour.set_y_axis({'name': 'Average Response Latency (sec)'})
    lineGraph_LatencyByHour.set_legend({'none': True})

    barGraph_Survey.add_series({
        'name': 'Yes',
        'categories': ['SurveySheet', 1, 0, len(response['surveyKeyList']), 0],
        'values': ['SurveySheet', 1, 1, len(response['surveyKeyList']), 1]
    })
    barGraph_Survey.add_series({
        'name': 'No',
        'categories': ['SurveySheet', 1, 0, len(response['surveyKeyList']), 0],
        'values': ['SurveySheet', 1, 2, len(response['surveyKeyList']), 2]
    })
    barGraph_Survey.set_x_axis({'name': 'Dates'})
    barGraph_Survey.set_y_axis({'name': 'Number of Responses'})
    barGraph_Survey.set_title({'name': 'Survey Response Summary'})
    # insert the charts into the dashboard
    dashboard.insert_chart('B8', barGraph_ConvByDay)
    dashboard.insert_chart('G8', lineGraph_ConvByHour)
    dashboard.insert_chart('B25', lineGraph_LatencyByHour)
    dashboard.insert_chart('G25', barGraph_Survey)
    # Intent and convolog
    intentNotFound = workbook.add_worksheet('Intent Not Found')
    intentFound = workbook.add_worksheet('IntentFound')
    convoLog = workbook.add_worksheet('ConversationLog')
    intent = intent_complete(start, end, bot_id)
    intentNotFoundList = intent["intentnotfound"]
    intentFoundList = intent["intentfound"]
    headersIntentF = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID', 'TASK TEXT']
    headersIntent = ['ID', 'USER QUESTION', 'EMAIL ID', 'DATE', 'TIME', 'QUERY TYPE', 'TASK ID']
    # #create sheet for intent
    intentFound.write_row('A1', headersIntentF, cell_format=formatting)
    intentFound.set_column(0, 7, 20)
    intentFound.autofilter('A1:D1')
    l = 1

    try:
        for item in intentFoundList:
            intentFound.write_row(l, 0, [str(item['ID']).replace('ObjectId(', '').replace(')', ''),
                                         item['USER QUESTION'],
                                         item['EMAIL ID'],
                                         item['DATE'],
                                         item['TIME'],
                                         item['QUERY TYPE'],
                                         item['TASK ID'],
                                         item['TASK TEXT']])
            l += 1
    except Exception as e:
        print("Exception", e)

    intentNotFound.write_row('A1', headersIntent, cell_format=formatting)
    intentNotFound.set_column(0, 7, 20)
    intentNotFound.autofilter('A1:D1')
    h = 1

    for item in intentNotFoundList:
        intentNotFound.write_row(h, 0, [str(item['ID']).replace('ObjectId(', '').replace(')', ''),
                                        item['USER QUESTION'],
                                        item['EMAIL ID'],
                                        item['DATE'],
                                        item['TIME'],
                                        item['QUERY TYPE'],
                                        item['TASK ID']])
        h += 1
    # Hide the datasheet used to create the charts
    headers = ['ID', 'FROM', 'MESSAGE', 'DATE', 'TIME']

    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:I1')
    # master formatting style

    tempconvodata, num_convo = convo_complete(start, end, bot_id)
    if tempconvodata == []:
        conversation = []
    else:
        conversation = tempconvodata[0]['convo']
    count = 1
    for cnv in conversation:
        dataRow = [
            str(cnv['ID']).replace('ObjectId(', '').replace(')', ''),
            cnv['FROM'],
            str(cnv['MESSAGE']),
            cnv['DATE'],
            cnv['TIME']]
        convoLog.write_row(count, 0, dataRow)
        count += 1
    dataSheet.hide()
    workbook.close()
    return fileTitle


def utc_to_est(utc_time):
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('UTC-5')
    utc = utc_time.replace(tzinfo=from_zone)
    # Convert utc time zone to chinese local time. Chinese local time = UTC+8
    china_local = utc.astimezone(to_zone)
    return china_local


def start_end(start, end):
    if end == '':
        next_day=(datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d") # this will create date as 12 am (YYYY-MM-DD 00:00:00) of next day,
        end_date = datetime.strptime(next_day,"%Y-%m-%d")

    else:
        end_date = datetime.strptime(end, '%Y-%m-%d')+timedelta(days=1)

    if start == '':
        start_date =end_date + timedelta(days=-30)
    else:
        start_date= datetime.strptime(start, '%Y-%m-%d')

    return start_date,end_date