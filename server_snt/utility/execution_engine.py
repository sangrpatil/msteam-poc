import configparser
import datetime
import os

import random
import re
from operator import itemgetter

import configparser
from flask_babel import gettext, refresh
from nltk.tokenize import  word_tokenize
import nltk
nltk.download('punkt')

import numpy as np
from flask_babel import gettext
from fuzzywuzzy import fuzz
from utility.TTS import synthesize_text
from utility.conversation_logger import ConversationLogger
from utility.elasticsearch_query import SearchES
from utility.gcp_places import retrieve_location
from utility.logger import logger
from utility.mongo_dao import get_by_id, update
from utility.session_manager import get as get_context, modify as update_context

"""
Has three main functions: init, execute, resume
The rest are utility functions
@init: initializes the session with an id and starts logging
@execute: executes bot tasks, i.e. displaying a message or buttons
@resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
"""

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))

hybrid_intent_not_found = 'None of the above'


class ExecutionEngine():
    def __init__(self, session_id, bot_id):
        """
        Initializes execution session
        :param session_id:
        :param bot_id:
        """
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id, self.session)

    def get_task_definition(self, search_key, search_value, bot_id):
        """
        This method queries bot collection with given key and value and return corresponding task

        :param search_key: "Field Name" corresponding to key in Bot's mapping data in database
        :param search_value: "Value" corresponding to field name provided in search_key
        :param bot_id: "Bot's Id"
        :return: "Task Definition" corresponding to the search_key and search_value  provided
        """
        state_return = 'None'
        state_definition_all = get_by_id('bot', bot_id)['mapping']
        for state in state_definition_all:
            if search_key in state.keys():
                if state[search_key] == search_value:
                    state_return = state
                    break
        return state_return

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False,
                       existing_json={}, menuSwitch=None, validateemailsnt=None):
        """
        This method builds all type of responses to be send to the UI based on the input parameters.

        :param type: string, hyperlink, list, images, video
        :param static_text: text to be displayed on message bubble
        :param interaction:
        :param interaction_elements:
        :param is_multi_message: True if more than one message
        :param existing_json: existing json in which we append next message
        :param menuSwitch: Boolean to differentiate between Main menu and Next Main Menu
        :param validateemailsnt: value for validating email
        :return: json response
        """
        guidedRestart = False
        current_task = ""
        if self.session['restartFlag'] == True and self.session['ES_UsedFlag'] == False:
            guidedRestart = True
        static_text_final = static_text
        if isinstance(interaction_elements, list):
            static_text_final += "\n".join(interaction_elements)
        else:
            static_text_final += " " + str(interaction_elements)

        if static_text == '':
            pass
        else:
            self.conv_log.bot_log(static_text_final, self.session['ASRFlag'], self.session['endSurveyFlag'],
                                  guidedRestart, self.session['user_id'])
        # sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        static_text = static_text.replace("__NAME__", self.session['name'])
        disable_response = False
        if "button" in interaction:
            disable_response = True
        if self.session['ASRFlag'] == True:
            static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
            tts_text = synthesize_text(static_text1)
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False
        if is_multi_message == True:
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio": tts_text,
                "disable_response": disable_response
            })
            existing_json['is_multi'] = True
            return existing_json
        else:
            json_return = {
                "is_multi": False,
                "message": [{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": disable_response,
                    "menuSwitch": menuSwitch,
                    "validateemailsnt": validateemailsnt

                }]
            }
            return json_return

    def execute(self, task_definition):
        """
        This method executes interaction types
        :param task_definition: Task to be executed
        :return: json response
        """
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            self.session['disable_input'] = False
        session_id = update_context(self.session_id, self.session)

        # check what the information about the current state is.
        # depending on what the interaction type is for the current state is, execute accordingly

        try:
            self.conv_log.update(task_definition['Task Name'])  # To update the current state in status
            response_json = ''

            task_text = task_definition['Task Text']
            if task_text == '""':
                task_text = ''

            interaction = task_definition['Interaction Type']
            answers = task_definition['Answers']
            interaction_values = task_definition['Interaction Values']
            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text)  # , dbData)

            # if it is url, then show the hyperlink to the user
            elif interaction == 'url':
                priorstate = get_by_id('status', self.session_id)['prior_state']
                if priorstate == hybrid_intent_not_found or task_definition['Task Name'] == 'HelpLink':
                    task_definition['Contact URL'] = \
                    self.get_task_definition('Task Name', self.session['menuSelected'], self.session['bot_id'])[
                        'Contact URL']
                    response_json = self.url_execute(task_definition, task_text)  # , dbData)
                    response_json['message'][0]['disable_response'] = True
                else:
                    response_json = self.url_execute(task_definition, task_text)  # , dbData)
            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text)  # , dbData)

            elif interaction == 'text_q':
                response_json = self.text_execute(task_definition, answers)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                response_json = self.button_execute(task_definition, task_text)  # , dbData, interactionObj)

            # if it is a zipcode input, give the user an input to type in a zipcode
            elif interaction == 'zipcode':
                response_json = self.zip_execute(task_definition, task_text)

            # if it is a _____ input, proceed to the next task
            elif interaction == 'nextTask':
                if task_definition['Task Name'] == 'Start':
                    self.session['Next Task Name'] = 'CloseBot'
                    update_context(self.session_id, self.session)
                response_json = self.next_task_execute(task_definition, task_text)


            elif interaction in ['productStatusHandler', 'priceHandler', 'customerHandler',
                                 'productInformationHandler']:
                message = self.session['message']
                response_json = self.custom_sheet_execute(message, interaction, task_definition)

            elif interaction == 'video':
                response_json = self.media_execute(task_definition, task_text, interaction, interaction_values)

            elif interaction == 'images':
                response_json = self.media_execute(task_definition, task_text, interaction, interaction_values)
        except Exception as e:
            logger.error('Execute Method Error - ', str(e))
            response_json = 'Sorry we weren''t able to understand your intent. - EXECUTE Error'

        return response_json

    def resume(self, message, ASR, bot_id):
        """
        All the intermediate messages from UI gets into this method
        :param message: message from UI
        :param ASR: Audio to Speech Recognition boolean
        :param bot_id: Bot's Id
        :return: response Json
        """
        print(message)
        self.session['message'] = message
        # todo - should I make the returns uniform
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            if message.lower().strip() == 'nein' and self.session['disable_input'] == True:
                self.session['disable_input'] = True
            else:
                self.session['disable_input'] = False
        self.session['ASRFlag'] = ASR
        logger.debug('THE ASR VALUE IS = ', self.session['ASRFlag'], ASR)
        self.session['message'] = message
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.session['ES_UsedFlag'] = False
        update_context(self.session_id, self.session)
        if message.split(':')[0] == 'Next Task Name':
            self.conv_log.user_log('User clicked the link', self.session['ASRFlag'], self.session['endSurveyFlag'],
                                   guidedRestartFlag=False, user_id=self.session['user_id'])
        else:
            self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'],
                                   guidedRestartFlag=False, user_id=self.session['user_id'])
        try:
            # first check if the message is small talk
            small_talk_response = 0  # self.small_talk_module(message)
            keyword_response = self.keyword_handler(message, bot_id)
            if small_talk_response != 0:
                return small_talk_response

            elif keyword_response != 0:
                return keyword_response
            # next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restart_execute(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagFAQ'] is True:
                return self.restart_execute_FAQ(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagNextTask'] is True:
                return self.restart_next_task_execute(message, self.session_id, self.session, bot_id)
            elif self.session['endSurveyFlag'] is True:
                return self.survey_execute(message)
            # next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                return self.fuzzy_execute(message, bot_id)
            # next check if we're expecting a response from a hybrid guided
            elif self.session['guidedFlag'] is True:
                return self.guided_flag_execute(message, bot_id)
            # if it was none of the above, then it is a normal message
            else:
                # language and analyzer
                es_analyzer = get_by_id('bot', bot_id)['es_analyzer']

                # first determine what the state is in the conversation, and what states can lead from here
                current_state = get_by_id('status', self.session_id)['current_state']
                survey_question = app_config.get('GENERAL', 'exit_bot')
                if message.lower().strip() in survey_question:
                    self.session['endSurveyFlag'] = True
                    update_context(self.session_id, self.session)
                    return self.survey_execute(message)
                if current_state == 'Main Menu':
                    self.session['menuSelected'] = message
                    update_context(self.session_id, self.session)
                state_definition = self.get_task_definition('Task Name', current_state, bot_id)

                action_type = state_definition['Action Type']
                next_task_ids = state_definition['Next Task IDs']
                next_possible_states_json = []
                next_possible_states_names = []
                next_possible_task_text = []
                try:
                    for id in next_task_ids:
                        state_option = self.get_task_definition('Task ID', id, bot_id)
                        next_possible_states_json.append(state_option)
                    for state_json in next_possible_states_json:
                        next_possible_states_names.append(state_json['Task Name'])
                        next_possible_task_text.append(state_json['Task Text'])
                except Exception as e:
                    logger.error("Execution engine error: ", str(e))
                    next_possible_states_json = self.get_task_definition('Is Start', 'true', bot_id)
                    next_possible_states_names = next_possible_states_json['Task Name']
                # if the action type is code, take the user's message and apply it to the code
                if 'Next Task Name' == message.split(':')[0]:
                    task_def = self.get_task_definition('Task Name', message.split(':')[1], self.session['bot_id'])
                    response = self.execute(task_def)
                    return response
                if action_type == 'code':  # todo
                    self.codeExecute(message, state_definition)
                # if the action if FAQ, then take the user's message and query elasticsearch with it
                elif action_type == 'FAQ':
                    response = self.faq_execute(message, bot_id, self.session['ASRFlag'], es_analyzer)
                    return response
                # if the action is hybrid guided, take the user's message and check the hybrid guided process
                elif action_type == 'hybrid_guided':
                    response = self.hybrid_guided_execute(message, next_possible_states_names, bot_id,
                                                          next_possible_task_text)
                    print('hybrid response:',response)
                    return response
                # if the action is query, query the user's message against a database using DAO.py
                elif action_type == 'query':  # todo
                    self.queryExecute(message, current_state)
                # if the action type is navigate, we are navigating from one branch to another in the conversation
                elif action_type == 'navigate':
                    if message == "Enter New Question" and current_state == 'Next Main Menu':
                        query_task = self.get_task_definition('Task Name', self.session['menuSelected'],
                                                              self.session['bot_id'])
                        response = self.execute(query_task)
                    else:
                        response = self.navigate_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action type is location, we are taking in the user's zip code input
                elif action_type == 'location':
                    response = self.location_execute(message)
                    return response
                elif action_type == 'input':
                    response = self.input_execute(message, next_possible_states_names)
                    return response

        except Exception as e:

            logger.error(str(e))  # logger.info(string)
            return 'Sorry, we weren''t able to understand your intent - RESUME Error'

    #####################################################################################################################
    #####################################################################################################################
    """
    Execute Functions
    """

    #####################################################################################################################
    #####################################################################################################################

    def next_task_execute(self, task_definition, task_text):
        """
        Executes next state of the current state
        :param task_definition: current task definition
        :param task_text: Task Text
        :return: response json
        """
        currentState = task_definition['Task Name']
        nextState = task_definition['Next Task IDs'][0]
        validateemailsnt = False
        message = re.sub(r'<>', currentState, task_text)
        logger.debug('next state - ', nextState, 'bot ID - ', self.session['bot_id'])

        response = self.build_response("string", message, "text", "")
        if currentState == 'Start':
            validateemailsnt = 'email'

        next_task_def = self.get_task_definition("Task ID", str(int(float(nextState))), self.session['bot_id'])
        response_json_2 = self.execute(next_task_def)

        response['is_multi'] = True
        response_json_2['message'][0]["validateemailsnt"] = validateemailsnt
        response['message'].extend(response_json_2['message'])
        return response

    def text_execute(self, task_definition, task_text):
        """
        Executes normal text response from Task Name
        :param task_definition: current task's definition
        :param task_text: Task Text
        :return: response json
        """
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            response_task_text = self.build_response('string', task_text, 'text', '')
            if task_definition['Task Name'] == 'CloseBot':
                response_task_text['message'][0]["disable_response"] = True
            return response_task_text

    def button_execute(self, task_definition, task_text):
        """
        Executes button response to be send

        :param task_definition: current task's Definition
        :param task_text: Task Text
        :return: response json
        """
        menuSwitch = False
        currentState = task_definition['Task Name']
        task_text = re.sub(r'<>', currentState, task_text)
        if currentState == "Main Menu":
            menuSwitch = True
        if task_definition['Interaction Fetch From DB'] != "True":
            interaction_values = task_definition['Interaction Values']
            task_text = self.build_response('list', task_text, 'button_vertical', interaction_values,
                                            menuSwitch=menuSwitch)
            return task_text

    # if interaction is none, check if query database or not
    # just show text and then restart
    def none_execute(self, task_definition, task_text):
        """

        :param task_definition: current task's definition
        :param task_text: Task Text
        :return: response json
        """
        # global results
        static_values = task_definition['Interaction Values']
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)

            response = self.build_response("string", message, "text", "")
            response_json_2 = self.restart(response)
            return response_json_2

    def url_execute(self, task_definition, task_text):
        """
        if interaction is url, check if query database or not
        then display url

        :param task_definition: current task's definition
        :param task_text: Task Text
        :return: response json
        """
        interaction_urls = task_definition['Contact URL'].split(";")  # task_definition['Interaction Values']

        response_urls = ""

        # check interaction url pattern and form the url response
        for url in interaction_urls:
            if "|" in url:
                url_text, url_hyperlink = url.split("|")
                link = url_text.split(' ', 1)
                url_text = link[1]
                url_text, url_hyperlink = url_text.strip(), url_hyperlink.strip()
            else:
                if task_definition['Task Name'] == 'NotConsent':
                    link = str(task_text).split(" ", 1)
                else:
                    link = ('Link:', self.session['menuSelected'])
                response_urls += str(link[0])

                url_text, url_hyperlink = link[1], url.strip()
            url_template = "<a href=\"{0}\" target=\"_blank\">{1}</a>".format(url_hyperlink, url_text)
            response_urls += url_template

        # check for <URL> in task text and replace it with response url
        if "<URL>" in task_text:
            response_text = re.sub(r'<URL>', response_urls, task_text)
            response_text = re.sub(r'<br/>', "", response_text)
        else:
            response_text = response_urls

        if task_definition['Interaction Fetch From DB'].lower() != "true":
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)
            # response = self.build_response("hyperlink", response_text, "text", task_text)
            response = self.build_response("hyperlink", str(link[0]) + '|' + url_text, "url", url_hyperlink)
            nexttaskname = get_context(self.session_id)['Next Task Name']

            if nexttaskname=='CloseBot':
                response['message'][-1]["showNext"] = False
                response['message'][-1]["Next Task Name"] = nexttaskname
            else:
                response['message'][-1]["showNext"] = True
                nexttaskname=self.get_task_definition('Task Name',nexttaskname,self.session['bot_id'])
                response['message'].extend(self.execute(nexttaskname)['message'])


            return response

    def zip_execute(self, task_definition, task_text):
        """

        :param task_definition: current task's definition
        :param task_text: Task Text
        :return: response json
        """
        response = self.build_response("string", task_text, "location_zip", "")
        return response

    #####################################################################################################################
    #####################################################################################################################
    """
    Resume Functions
    """

    #####################################################################################################################
    #####################################################################################################################

    def small_talk_fuzzy(self, msg, search_list):
        """

        :param msg: user message
        :param search_list: list to be searhed for
        :return: response ratio
        """
        for item in search_list:
            if fuzz.ratio(msg, item) > 65:
                return 1
        return 0

    def small_talk_module(self, msg):
        """
        Checks if message corresponds to small talk the responds correspondingly
        :param msg: user message
        :return: json response
        """
        msg = re.sub(r'[^\w\s]', '', msg)
        greetings = [gettext('hello'), gettext('hi'), gettext('what''s up'), gettext('good afternoon'),
                     gettext('good morning'), gettext('good day'), gettext('good evening')]
        question_questions = [gettext('I would like to ask a question'), gettext('I want to ask you a question'),
                              gettext('I have a doubt'), gettext('I want to ask you something'),
                              gettext('I want to ask you questions'), gettext('I want to consult you')]
        inquiries = [gettext('how are you'), gettext('how are you doing'), gettext('how is your day')]
        greeting_response = [gettext('Hi there! How can I help you')]
        question_question_responses = [gettext('With pleasure, tell me how I can help you.')]
        inquiriesResponse = [gettext('I\'m doing well! How can I help you today?'),
                             gettext('I\'m feeling great! How can I help you today?')]
        if msg.lower().strip() in greetings or self.small_talk_fuzzy(msg, greetings) == 1:
            response = self.build_response("string", random.choice(greeting_response), "text", "")
            return response
        elif msg.lower().strip() in question_questions:
            response = self.build_response("string", random.choice(question_question_responses), "text", "")
            return response
        elif msg.lower().strip() in inquiries:
            response = self.build_response("string", random.choice(inquiriesResponse), "text", "")
            return response
        else:
            return 0

    def keyword_handler(self, message, bot_id):
        """
        Handles bot keywords like help, restart, go back etc.

        :param message: User message
        :param bot_id: Bot's Id
        :return: response json
        """

        help_config = app_config.get('GENERAL', 'help_keywords')
        restart_config = app_config.get('GENERAL', 'restart_keywords')
        goback_config = app_config.get('GENERAL', 'goback_keywords')

        help_keywords = help_config
        restart_keywords = restart_config
        goback_keywords = goback_config

        if message.lower().strip() in help_keywords:
            help_task = self.get_task_definition("Task Name", "help", bot_id)
            help_text = help_task['Task Text']

            response = self.build_response("string", help_text, "text", "")

            next_state = help_task['Next Task IDs']

            if next_state == '' or next_state == [] or next_state == [""]:
                return response

            next_state_def = self.get_task_definition('Task ID', next_state[0], bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

        elif message.lower().strip() in restart_keywords:
            restart = self.get_task_definition("Is Start", "true", bot_id)
            self.session['custStatus'] = 'Done'
            self.session['productInformationHandler'] = 'Done'
            self.session['priceStatus'] = 'Done'
            self.session['prodStatus'] = 'Done'
            print('')
            self.session['switch'] = False
            update_context(self.session_id, self.session)
            return self.execute(restart)
        elif message.lower().strip() in goback_keywords:
            current_state = get_by_id('status', self.session_id)['current_state']
            prior_state = get_by_id('status', self.session_id)['prior_state']
            history = get_by_id('status', self.session_id)['history']
            history = history[:-1]  # pop out the last element
            if (len(history) == 0):
                return self.build_response("string", gettext("Sorry, there is nothing to go back to."), "text", "")
            previous_state = history[-1]  # get the last element
            history = history[:-1]  # pop out again because will be replaced in conversation logger
            update('status', self.session_id,
                   {'current_state': previous_state, 'prior_state': current_state, 'history': history,
                    "date_created": datetime.datetime.now()})
            previous_state_json = self.get_task_definition('Task Name', previous_state, bot_id)
            return self.execute(previous_state_json)
        else:
            return 0

    def survey_execute(self, message):
        """
        Executes survey question and responses are handled.

        :param message: User message
        :return: response json
        """

        if message.strip().lower() not in ['yes', 'no']:
            surveytext = self.get_task_definition('Task Name', 'Survey', self.session['bot_id'])['Task Text']
            response = self.build_response("list", surveytext, "button_vertical", [gettext("Yes"), gettext("No")])
            update_context(self.session_id, self.session_id)
            return response
        else:
            response = self.execute(self.get_task_definition('Task Name', 'CloseBot', self.session['bot_id']))

            return response

    def survey_create(self, state, json):
        """
        Creates Survey questions
        :param state: survey state
        :param json:
        :return: response json
        """
        self.session['endSurveyFlag'] = True
        update_context(self.session_id, self.session)
        if state == 1:
            self.session['surveyFlagNum'] = True
            update_context(self.session_id, self.session)
            options = [1, 2, 3, 4, 5]
            response = self.build_response("list", gettext(
                'On a scale of 1-5, how satisfied were you with your service today?'), "button_horizontal", options,
                                           True, json)
            return response
        elif state == 2:
            self.session['surveyFlagComm'] = True
            update_context(self.session_id, self.session)
            response = self.build_response("list", gettext('Would you like to leave any comments?'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def fuzzy_search(self, nextStates, message):
        """
        Use Fuzzywuzzy to check for fuzzysearch options for Navigate options
        if the match is > 45% similar, return as option for user to pick from
        :param nextStates: next possible states of current state
        :param message: user message
        :return:
        """
        options = []
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]
        if ratio > int(os.environ.get('FUZZY_RATIO') or '45'):
            # check if task is a navigate task, if yes, check message value, see if pass
            response = self.build_response("list", gettext('We couldn\'t find that. Do you mean: ') + suggestionOutput,
                                           "button_vertical", [gettext("Yes"), gettext("No")])
            self.session['fuzzyFlag'] = True
            update_context(self.session_id, self.session)
            return response
        else:
            response = self.build_response("string",
                                           gettext('Sorry we don\'t have an answer for that. Could you try again?'),
                                           "text", "")
            return response

    def fuzzy_execute(self, message, bot_id):
        """
        If FuzzyFlag is True, then execute this
        check if the fuzzysearch came from a query or code
        if not, then check if the suggestion picked comes from a next state
        if not what they are looking for, return to start

        :param message: User message
        :param bot_id:
        :return:
        """
        if str(message).lower() == gettext('yes'):
            self.session['fuzzyFlag'] = False
            get_next_state = self.get_task_definition('Task Name', self.session['suggestion'], bot_id)
            response = self.execute(get_next_state)
            update_context(self.session_id, self.session)
            return response
            pass
        elif message.lower() == gettext('no'):
            self.session['fuzzyFlag'] = False
            update_context(self.session_id, self.session)
            next_state = self.get_task_definition('Is Start', 'true',
                                                  bot_id)  # self.get_task_definition("Is Start","true", bot_id)
            response = self.execute(next_state)
            return response
        else:
            response = self.build_response("list", gettext('Please choose yes or no.'), "button_horizontal",
                                           [gettext("Yes"), gettext("No")])
            return response

    def input_execute(self, message, next_possible_state_names):
        """
        After executing current state allows use to type in text box area
        :param message: current message
        :param next_possible_state_names: next possible states of current state
        :return: response json
        """
        self.session['email'] = message
        if self.session['name'] == 'None':  # todo store these in conversation logger
            self.session['name'] = message
        elif 'contact' not in self.session.keys():
            self.session['contact'] = message
        elif 'profession' not in self.session.keys():
            self.session['profession'] = message
        else:
            pass  # add something here later if all fields are input
        update_context(self.session_id, self.session)
        get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
        response = self.execute(get_next_state)
        return response

    def navigate_execute(self, message, next_possible_states_names, bot_id):
        """

        :param message:
        :param next_possible_states_names:
        :param bot_id:
        :return:
        """
        found = False
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                get_next_state = self.get_task_definition('Task Name', state, bot_id)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True

                    response = self.execute(get_next_state)
                    return response

        if found == False:
            # do Fuzzy search here
            response = self.fuzzy_search(next_possible_states_names, message)
            return response
            pass

    def restart_execute(self, message, session_id, session, bot_id):
        """

        :param message:
        :param session_id:
        :param session:
        :param bot_id:
        :return:
        """

        if message.strip().lower() == gettext('yes'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            get_start = self.get_task_definition("Is Start", "true", bot_id)
            start_response = self.execute(get_start)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon!'),
                                                 "text", "", False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def restart_execute_FAQ(self, message, session_id, session, bot_id):
        """

        :param message:
        :param session_id:
        :param session:
        :param bot_id:
        :return:
        """

        if message.strip().lower() == gettext('yes'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            start_response = self.build_response("string", gettext("What other questions can I help you with?"), "text",
                                                 "", False)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon'),
                                                 "text", "", False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def restart_next_task_execute(self, message, session_id, session, bot_id):
        """

        :param message:
        :param session_id:
        :param session:
        :param bot_id:
        :return:
        """

        if message.strip().lower() == gettext('yes'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            current_state = get_by_id('status', self.session_id)['current_state']

            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = state_definition['Next Task IDs'][0]
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            start_response = self.execute(next_state_def)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for using the Pfizer Bot!'), "text", "",
                                                 False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def hybrid_guided_execute(self, message, next_possible_states_names, bot_id, next_possible_task_text=None):
        """
        # if action type is hybrid, execute this
    # first check if the message is a 1:1 match with taskNames
    # if it is, execute the corresponding state
    # if it is not, check message against keywords. For each keyword matching in the message, add a point
    # return taskNames with highest keyword scores for using to pick from
        :param message:
        :param next_possible_states_names:
        :param bot_id:
        :param next_possible_task_text:
        :return:
        """

        # global guidedFlag
        if next_possible_task_text != None:
            next_possible_states = next_possible_task_text
            query = 'Task Text'
        else:
            next_possible_states = next_possible_states_names
            query = 'Task Name'
        if "switch" in self.session.keys() and self.session['switch'] == True:

            for state in next_possible_states:
                if query == 'Task Name':
                    stateCompare = re.sub(r'<.*>', '', state)
                else:
                    stateCompare = state
                if message.strip().lower() == stateCompare.strip().lower():
                    # get_next_state = get('state', 'state_name', state)
                    # get_next_state = self.get_task_definition('Task Name',state, bot_id)
                    get_next_state = self.get_task_definition(query, state, bot_id)
                    if 'None' in str(get_next_state):
                        pass
                    else:
                        found = True
                        self.session['taskId'] = get_next_state['Task ID']
                        update_context(self.session_id, self.session)
                        self.conv_log.updateIntentFoundCounter(message)
                        interaction = get_next_state['Interaction Type']
                        response = self.execute(get_next_state)

                        self.session['switch'] = False
                        update_context(self.session_id, self.session)
                        response = self.restart_next_task(response)
                        return response

                elif message == hybrid_intent_not_found:
                    self.session['Next Task Name'] = 'Next Main Menu'
                    notFoundState = self.get_task_definition("Task Name", hybrid_intent_not_found,
                                                             self.session['bot_id'])

                    response = self.execute(notFoundState)
                    # response = self.restart_next_task(response)
                    self.session['switch'] = False
                    self.session['Next Task Name'] = 'CloseBot'
                    self.session['taskId'] = 'None of the Above'
                    # self.get_task_definition('Task Name',
                    #                                 self.session['menuSelected'],
                    #                                 self.session['bot_id'])['Task ID']
                    update_context(self.session_id, self.session)
                    self.conv_log.updateIntentNotFound(self.session['userinput'])
                    return response

        else:

            found = False

            if found == False:  # so here instead of a fuzzysearch, do the keyword search
                stateScores = []
                newScoresWithAllStatesTuple = []
                finalStates = []
                possibleNonAliasStates = []
                # for state in next_possible_states_names:
                for state in next_possible_states:
                    score = 0
                    stateinfo = self.get_task_definition(query, state, bot_id)
                    self.session['userinput'] = message
                    update_context(self.session_id, self.session)
                    alias = stateinfo['Alias']
                    if alias.lower() == 'false':
                        keywords = stateinfo['Keywords']
                        if len(keywords) > 0:
                            words_msg=[i.lower().strip() for i in word_tokenize(message)]
                            for key in keywords:
                                key_token=[i.lower().strip() for i in word_tokenize(key)]
                                for i in range(len(words_msg)):
                                    if words_msg[i:i + len(key_token)] == key_token and key!='':

                                #if key.lower().strip() in words_msg and key != '':
                                        score += 1
                            if score != 0:
                                stateScores.append(score)
                                possibleNonAliasStates.append(state)
                                newScoresWithAllStatesTuple.append((score, state))

                # pick the state names with the highest scores, return those as options
                if stateScores == []:
                    self.session['Next Task Name'] = 'Next Main Menu'
                    self.session['taskId'] = \
                    self.get_task_definition('Task Name', self.session['menuSelected'], self.session['bot_id'])[
                        'Task ID']
                    update_context(self.session_id, self.session)
                    self.conv_log.updateIntentNotFound(message)
                    response = self.execute(self.get_task_definition('Task Name', 'NoMatch', self.session['bot_id']))
                    return response
                else:
                    highScore = max(stateScores)
                    if highScore != 0:
                        for i in np.arange(0, len(stateScores)):
                            if stateScores[i] == highScore:
                                finalStates.append(possibleNonAliasStates[i])

                    if len(finalStates) != 0:
                        finalStates.append(gettext(hybrid_intent_not_found))
                        newScoresWithAllStatesTuple.sort()
                        newScoresWithAllStatesTuple = newScoresWithAllStatesTuple[::-1]
                        newFinalStates = [i[1] for i in newScoresWithAllStatesTuple]
                        newFinalStates.append(gettext(hybrid_intent_not_found))
                        response = self.build_response("list", gettext(
                            'Please select the option that best matches your question.'), "button_vertical",
                                                       newFinalStates)
                        self.session['hybrid_user_intent'] = message
                        session_id = update_context(self.session_id, self.session)
                        self.session['switch'] = True
                        update_context(self.session_id, self.session)
                        return response
                    else:
                        response = self.build_response("string", gettext('No matching Questions were located'), "text",
                                                       "")
                        return response
                    # except Exception as e:
                    #     logger.error("State Score list is empty ", str(e))
                    #     response = self.build_response("string", gettext('No matching Questions were located'), "text", "")
                    #     return response

    def guided_flag_execute(self, message, bot_id):
        """
         # if expecting a response from Hybrid guided, check here if it leads to a valid state
        :param message:
        :param bot_id:
        :return:
        """
        get_next_state = self.get_task_definition('Task Name', message, bot_id)
        if str(get_next_state) != 'None':
            response = self.execute(get_next_state)
            self.session['taskId'] = get_next_state['Task ID']
            self.session['guidedFlag'] = False
            session_id = update_context(self.session_id, self.session)
            self.conv_log.updateIntentFoundCounter(self.session['hybrid_user_intent'])
            return response
        elif message == gettext(hybrid_intent_not_found):
            self.conv_log.updateIntentNotFound(self.session['hybrid_user_intent'])
            response = self.build_response("string", gettext(
                'I\'m sorry none of those options were helpful. Would you like to try rephrasing your question?'),
                                           "text", "")
            response_json_2 = self.restart(response)
            self.session['guidedFlag'] = False
            update_context(self.session_id, self.session)
            return response_json_2
        else:
            response = self.build_response("string", gettext('Please choose one of the options'), "text", "")
            return response

    def faq_execute(self, message, bot_id, asr_flag, es_analyzer):
        """

        :param message:
        :param bot_id:
        :param asr_flag:
        :param es_analyzer:
        :return:
        """
        search = SearchES(self.session_id, bot_id)
        self.session['ES_UsedFlag'] = True
        update_context(self.session_id, self.session)
        response_flag, response = search.execute(message, bot_id, asr_flag, es_analyzer)
        if response_flag == 0:
            self.session['ASRFlag'] = False
            response_json_2 = self.restart_FAQ(response)
            update_context(self.session_id, self.session)
            return response_json_2
        # self.restart()
        elif response_flag == 1:
            # display the ES answer
            logger.debug('RESPONSE TO RETURN FROM FAQ EXECUTE - ', response)
            return response
        elif response_flag == 2:
            state = self.get_task_definition('Is Start', 'true', bot_id)
            response = self.execute(state)
            return response
        pass

    def location_execute(self, message):
        """

        :param message:
        :return:
        """
        try:
            # message should be a 5 digit zip code
            pharmacy_data = retrieve_location(message, "pharmacy", 5)
            if pharmacy_data['status'] == 'Success':
                pharmacy_list = pharmacy_data['message']
                response = self.build_response("list", gettext("Please select any of the stores to view it on a map"),
                                               "location_maps", pharmacy_list)  # [[name, address],[name, address]]

            else:
                response = self.build_response("string", gettext(
                    "Sorry, we do not have pharmacies within 50 miles of your area."), "text", "")

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = state_definition['Next Task IDs'][0]
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response
        except Exception as e:
            logger.error("Location Execute Error:", str(e))

    def restart(self, json):
        """
        # ask the user if they would like to restart - send button and set global restartflag to True
        :param json:
        :return:
        """
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlag'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Error: ", str(e))

    def restart_FAQ(self, json):
        """
        # ask the user if they would like to restart - send button and set global restartflag to True
        :param json:
        :return:
        """
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlagFAQ'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart FAQ Error: ", str(e))

    def restart_next_task(self, json):
        """

        :param json:
        :return:
        """
        try:
            nextmainmenu = self.get_task_definition('Task Name', "Next Main Menu", self.session['bot_id'])
            response = self.build_response("list", nextmainmenu['Task Text'], "button_vertical",
                                           nextmainmenu['Interaction Values'], True, json)
            update('status', self.session_id, {'current_state': "Next Main Menu"})
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Next Task Error: ", str(e))

    def media_execute(self, task_definition, task_text, interaction, interaction_values):
        """

        :param task_definition:
        :param task_text:
        :param interaction:
        :param interaction_values:
        :return:
        """
        if interaction == 'video':
            response = self.build_response('video', task_text, 'video', interaction_values)
        if interaction == 'images':
            if type(interaction_values) == list and len(interaction_values) > 1:
                response = self.build_response("string", task_text, "text", "")
                for i in interaction_values:
                    text, url = i.split('|')
                    response = self.build_response('images', text, 'images', url,
                                                   is_multi_message=True, existing_json=response)

            else:
                response = self.build_response('images', task_text, 'images', interaction_values)
        return response
