from utility.de_coupled_report import *
from utility.de_coupled_report import *
from utility.mongo_dao import get_by_id
import configparser

app_config = configparser.ConfigParser()


# app_config.read_file(open(r'../config/app_settings.ini'))

def generateReportsDaily(bot_id=None):
    start = (datetime.now()+timedelta(days=-1)).strftime("%Y-%m-%d")
    end = datetime.now().strftime("%Y-%m-%d")
    try:
        bot_name = get_by_id('bot', bot_id).get('bot name')
        filename1 = generateReport(bot_id, bot_name, start, start, 'Daily')
        file_name = 'Chatbot_Report_Daily' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
        filetitle = "static/reports/" + file_name
        return filetitle
    except Exception as e:
        return jsonify(
            status='Failure',
            message=e
        )


def generateReportsWeekly(bot_id=None):
    end = str(datetime.now()+timedelta(days=-1)).split(' ')[0]
    start = str(datetime.now() + timedelta(days=-7)).split(' ')[0]
    try:
        bot_name = get_by_id('bot', bot_id).get('bot name')
        filename1 = generateReport(bot_id, bot_name, start, end, 'Weekly')
        filetitle = 'static/reports/Chatbot_Report_Weekly' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
        return filetitle
    except Exception as e:
        return jsonify(
            status='Failure',
            message=e
        )
