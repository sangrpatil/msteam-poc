import datetime
import logging
import smtplib
from datetime import timedelta
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logger = logging.getLogger()


def send_email(report_filename, emails, mailtype):
    today_date = (datetime.datetime.today()+timedelta(days=-1)).strftime('%Y-%m-%d')
    weekbeforedate = (datetime.datetime.today() + timedelta(days=-7)).strftime('%Y-%m-%d')

    to_addrs = emails
    from_addr = 'snt@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        if mailtype == "daily":
            msg["Subject"] = "S&T Query Assistant – Report for " + str(today_date)
            body = "Attached please find the S&T Query Assistant Report for  " + str(today_date) + \
                   ". \n \nS&T Query Assistant: [Query.pfizer.com]    "
        else:
            msg["Subject"] = "S&T Query Assistant – Report from " + str(weekbeforedate) + " to " + str(today_date)
            body = "Attached please find the S&T Query Assistant Report for  " + str(weekbeforedate) + " to " + str(
                today_date) \
                   + ". \n \nS&T Query Assistant: [Query.pfizer.com]    "

        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename.split('/')[-1])
        msg.attach(part)


    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.".format(e))
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.".format(e))
        return ('Failure')
