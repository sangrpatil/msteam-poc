import xlrd
import json
import datetime
from utility.logger import logger


def getColNames(sheet):
   try:
      rowSize = sheet.row_len(0)
      colValues = sheet.row_values(0, 0, rowSize )
      columnNames = []

      for value in colValues:
         columnNames.append(value)

      return columnNames
   except Exception as e:
      logger.error("excel parser error: ", str(e))

def getRowData(row, columnNames):
   rowData = {}
   counter = 0
   try:
      for cell in row:
         # check if it is of date type print in iso format
         if cell.ctype==xlrd.XL_CELL_DATE:
            rowData[columnNames[counter].lower().replace(' ', '_')] = datetime.datetime(*xlrd.xldate_as_tuple(cell.value,0)).isoformat()
         else:
            rowData[columnNames[counter]] = str(cell.value)
         counter +=1

      return rowData
   except Exception as e:
      logger.error("excel parser error: ", str(e))

def getSheetData(sheet, columnNames):
   nRows = sheet.nrows
   sheetData = []
   counter = 1
   try:
      for idx in range(1, nRows):
         row = sheet.row(idx)
         rowData = getRowData(row, columnNames)
         sheetData.append(rowData)

      return sheetData
   except Exception as e:
      logger.error("excel parser error: ", str(e))

def getWorkBookData(workbook):
   nsheets = workbook.nsheets
   counter = 0
   workbookdata = {}
   try:
      for idx in range(0, nsheets):
         worksheet = workbook.sheet_by_index(idx)
         columnNames = getColNames(worksheet)
         sheetdata = getSheetData(worksheet, columnNames)
         workbookdata[worksheet.name.lower().replace(' ', '_')] = sheetdata

      return workbookdata
   except Exception as e:
      logger.error("excel parser error: ", str(e))


def convert(file_location):
   try:
       workbook = xlrd.open_workbook(file_location)
       workbookdata = getWorkBookData(workbook)
       return json.dumps(workbookdata)
   except Exception as e:
      logger.error("excel parser error:  = {}".format(e))


# convert(r'C:\Users\roxu\Documents\Python\PfizerAI_COE\Chatbot_JSON_Test_052218.xlsx')
