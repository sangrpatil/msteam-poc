from flask_socketio import SocketIO, send, emit
from flask_babel import gettext
from elasticsearch import Elasticsearch
import json
from utility.conversation_logger import ConversationLogger
from utility.TTS import synthesize_text
from utility.session_manager import get as get_context, modify as update_context
import os
import re
import math
import configparser
from utility.logger import logger

#GLobal variables

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))

elasticsearch_host = os.environ.get('ES_HOST') or 'localhost'
elasticsearch_port = os.environ.get('ES_PORT') or '9200'
elasticsearch_threshold = os.environ.get('ES_THRESHOLD') or '5.0'

class SearchES():

    def __init__(self, session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id,bot_id)


    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json={}):
        try:
            #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
            self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], guidedRestartFlag=False, user_id=None)
            if self.session['ASRFlag'] == True:
                static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
                tts_text = synthesize_text(static_text1)
                asr_use = True
                self.session['ASRFlag'] = False
                update_context(self.session_id, self.session)
            else:
                tts_text = ""
                asr_use = False
            if is_multi_message == True:
                existing_json['message'].append({
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio":tts_text,
                    "disable_response": self.session['disable_input']
                })
                existing_json['is_multi'] = True
                return existing_json
            else:
                json_return = {
                    "is_multi":False,
                    "message":[{
                        "interaction elements": interaction_elements,
                        "text": static_text,
                        "type": type,
                        "interaction": interaction,
                        "tts": asr_use,
                        "tts audio": tts_text,
                        "disable_response": self.session['disable_input']
                    }]
                }
                return json_return
        except Exception as e:
            logger.error("Elastic search Build response error: ", str(e))

    #based off feedback, update weight of elasticsearch answers
    def update_votes(self,score_answer, botName):
        try:
            updated_score_answer = score_answer
            new_source = json.loads(json.dumps(updated_score_answer[2]))

            l = {"doc": {"qid": updated_score_answer[1], "q": new_source['q'], "a": updated_score_answer[3],
                         "votes": int(new_source['votes']) + 1}}


            es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])
            es.update(index=str(botName).lower(), doc_type=str(botName).lower(), id=score_answer[1],body=l)
        except Exception as e:
            logger.error("Elastic search Build response error: ", str(e))

    #search for answer within english elasticsearch
    def search_es(self,question,botName):
        try:
            es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

            data = {
                "query": {
                    "function_score": {
                        "query": {

                            "match": {
                                'q': question
                            }
                        },
                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p"
                        }
                    }
                }
            }
            response = es.search(index=str(botName).lower(), body=data)
            score = ''
            answer = ''
            source = ''
            id = ''
            score_answer = []
            for items in response['hits']['hits']:
                score = items['_score']
                source = items['_source']
                id = items['_id']
                answer = source['a']
                votes = source['votes']
                if ((score) / (math.log10(votes + 2))) < float(elasticsearch_threshold):
                    print('This answer score is below threshold - Skipping this answer = {}'.format(answer))
                    continue
                else:
                    print('APPENDING THIS ANSWER = {}'.format(answer))
                    score_answer.append([score, id, source, answer, votes])
            return score_answer
        except Exception as e:
            logger.error("Elastic search searches error: ", str(e))

    #search for answers within Spanish elasticsearch
    def search_es_non_english(self, question, botName, elasticsearch_analyzer):

        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

        data = {
            "query": {
                "function_score": {

                    "query": {

                        "multi_match": {
                            "type": "phrase",
                            "query": question,
                            "fields": ["q"]

                        }
                    },

                    "field_value_factor": {
                        "field": "votes",
                        "modifier": "log2p"
                    }

                }
            }
        }
        response = es.search(index=str(botName).lower(), body=data)
        score = ''
        answer = ''
        source = ''
        id = ''
        print('RESPONSE FROM ELASTICSEARCH = {}'.format(response))
        exact_match = True
        if response['hits']['total'] == 0:
            exact_match = False
            print('No Exact match found. Going to execute Best Match')
            data = {
                "query": {
                    "function_score": {

                        "query": {

                            "multi_match": {
                                "type": "best_fields",
                                "query": question,
                                "fields": ["q"]

                            }
                        },

                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p"
                        }

                    }
                }
            }
            response = es.search(index=str(botName).lower(), body=data)
            print('RESPONSE FROM ELASTICSEARCH AFTER BEST FIELDS MATCH= {}'.format(response))

        # score_answer is - [score,id,source,answer]
        score_answer = []
        for items in response['hits']['hits']:
            score = items['_score']
            source = items['_source']
            id = items['_id']
            answer = source['a']
            votes = source['votes']
            original_score = ((score) / (math.log10(votes + 2)))
            if exact_match == False:
                if ((score) / (math.log10(votes + 2))) < float(elasticsearch_threshold):
                    continue
                else:
                    score_answer.append([score,id, source, answer, votes])
            else:
                score_answer.append([score, id, source, answer, votes])

        return (score_answer)


    #take users message and determine what to do with it
    #will either be feedback yes/no or will be a message to query
    def execute(self,message,BotName, ASR, es_analyzer): #message,bot_id, self.session['ASRFlag'],es_analyzer

        if message == "Go Back":
            return 2, ''
        if self.session['feedbackFlag_ES'] == True:
            if message.lower().strip() == gettext('yes'):
                updated_score_answer = self.session['es_reponse'][0]

                self.update_votes(updated_score_answer,BotName)

                response = self.build_response("string",gettext('Thank you! Your feedback has been added.'),"text","")
                self.session['suggestCounter_ES'] = 0
                self.session['feedbackFlag_ES'] = False
                update_context(self.session_id, self.session)
                self.intentFoundRecord(self.session['currentQuestion'])
                return 0, response
            elif message.lower().strip() == gettext('no'):
                self.session['suggestCounter_ES'] += 1
                update_context(self.session_id, self.session)
                if self.session['suggestCounter_ES'] < 2:
                    try:
                        if self.session['es_reponse']:
                            del self.session['es_reponse'][0]
                            response = self.build_response("string",self.session['es_reponse'][0][3],"text","")
                            response_json_2 = self.feedback(response)
                            self.session['feedbackFlag_ES'] = True
                            update_context(self.session_id, self.session)
                            return 1, response_json_2
                            # return code,message
                    except Exception as e:
                        logger.error("Elasticsearch Exception error: ", str(e))
                        self.session['feedbackFlag_ES'] = False
                        self.intentNotFoundRecord(self.session['currentQuestion'])
                        self.session['suggestCounter_ES'] = 0
                        update_context(self.session_id, self.session)
                        response_json = self.build_response("string",gettext('Sorry we weren''t able to find what you were looking for. Your feedback has been noted.'),"text","")
                        return 0, response_json
                else:
                    self.intentNotFoundRecord(self.session['currentQuestion'])
                    self.session['suggestCounter_ES'] = 0
                    self.session['feedbackFlag_ES'] = False
                    update_context(self.session_id, self.session)
                    response_json = self.build_response("string",gettext('Sorry we weren''t able to find what you were looking for. Your feedback has been noted.'),"text","")
                    return 0, response_json
            else:
                self.session['currentQuestion'] = message
                self.session['feedbackFlag_ES'] = False
                update_context(self.session_id, self.session)
                if es_analyzer == 'English':
                    response_json = self.checkResponse(message, BotName)
                    return 1, response_json
                else:
                    response_json = self.checkResponse_non_english(message, BotName, es_analyzer)
                    return 1, response_json

        else:
            self.session['currentQuestion'] = message
            update_context(self.session_id, self.session)
            if es_analyzer == 'English':
                response_query = self.checkResponse(message, BotName)
                return 1, response_query
            else:
                response_query = self.checkResponse_non_english(message, BotName, es_analyzer)
                return 1, response_query


    #send message and send TTS audio
    def send_msg(self, msg):
        # global ASRFlag
        endSurveyFlag = False
        self.conv_log.bot_log(msg, self.session['ASRFlag'], endSurveyFlag)
        emit('message', msg)

    #check feedback, whether or not user was satisfied with the answer returned by elasticsearch
    def feedback(self, input_json):
        json = self.build_response("list",gettext('Was this helpful?'),"button_horizontal",[gettext('Yes'),gettext('No')],True,input_json)
        self.session['feedbackFlag_ES'] = True
        update_context(self.session_id, self.session)
        return json

    #if the user is actually
    def checkResponse(self, message, BotName):
        self.session['es_reponse'] = self.search_es(message, BotName)
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        if len(self.session['es_reponse']) > 0:
            response_json = self.build_response("string",self.session['es_reponse'][0][3],"text","")
            response_json_2 = self.feedback(response_json)

            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)
            return response_json_2

        else:
            response_json = self.build_response("string",gettext('Sorry, I can''t find an answer for that. Could you try again?'),"text","")
            self.intentNotFoundRecord(message)
            return response_json

    def checkResponse_non_english(self, message, BotName, es_analyzer):

        self.session['es_reponse'] = self.search_es_non_english(message, BotName, es_analyzer)
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        if len(self.session['es_reponse']) > 0:


            response_json = self.build_response("string", self.session['es_reponse'][0][3], "text", "")
            response_json_2 = self.feedback(response_json)

            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)
            return response_json_2
        else:
            response_json = self.build_response("string", gettext('Sorry, I can''t find an answer for that. Could you try again?'),"text","")

            self.intentNotFoundRecord(message)
            return response_json
    def intentNotFoundRecord(self, msg):
        self.conv_log.updateIntentNotFound(msg, self.session["user_id"])
        pass
    def intentFoundRecord(self, msg):
        self.conv_log.updateIntentFoundCounter(msg, self.session["user_id"])