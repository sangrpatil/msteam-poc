import os

import xlsxwriter
from utility.mongo_dao import *


def bot_excel(bot_id):
    bot = get_by_id('bot', bot_id)

    file_name = 'static/reports/' + str(bot_id) + '.xlsx'
    lst = []
    header = [j for j in bot['mapping'][0].keys()]
    file_path = os.path.dirname(os.path.dirname(__file__))
    file_path = os.path.join(file_path, "static", "reports")
    file_path = os.path.join(file_path, str(bot_id) + '.xlsx')
    with open(file_path, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=file_path)
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    bot_sheet = workbook.add_worksheet('bot_sheet')
    bot_sheet.write_row('A1', header, cell_format=formatting)

    j = 0
    for i in bot['mapping']:
        j += 1
        row = []
        for name in header:
            if isinstance(i[name], (list)):
                row.append(str(';'.join(map(str, i[name]))))
            else:
                row.append(str(i[name]))
        bot_sheet.write_row(j, 0, row)

    workbook.close()
    return file_name
