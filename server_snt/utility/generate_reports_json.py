import datetime
import configparser
import datetime
import re
from datetime import datetime
from dateutil import tz
from utility.mongo_dao import get_collection, get_by_id

app_config = configparser.ConfigParser()


# app_config.read_file(open(r'../config/app_settings.ini'))


def generateReports(bot_id=None):  # todo pass bot ID
    cursor = get_collection('status')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    ID = 0
    numRated = 0
    sumScore = 0
    dateDict = {}
    timeDict = {}
    latencyDict = {}
    botAnsweredDict = {}
    intentFoundList = []
    intentNotFoundList = []
    final_conv_log = []
    found_bot_id = False
    bot_name = ''

    # create hour dict for conversations over time of day and latency
    for k in range(0, 25):
        timeDict[k] = []
        latencyDict[k] = []
    for document in cursor:
        print('---CHECKING JSON DOC CURSOR---')
        # determine the converation's rating and comments, if any
        try:

            if bot_id == None:
                pass
            elif str(document['bot_id']) == str(bot_id):

                bot_name = get_by_id('bot', bot_id)['bot name']
                found_bot_id = True

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.utcnow()

                # look for user input intents that could be successfully answered (user said, "Yes" this was helpful)
                try:
                    intentFoundDocument = document['intentFound']
                    if intentFoundDocument != 'None':
                        for key, value in intentFoundDocument.items():
                            # this is for checking if the MOST granular group is within access - for intent not found page
                            li = []
                            if value['group'][-1] in group_list:
                                print('in here if the group matches group list')
                                utc_time = value['time']
                                li.append(utc_time)
                                chinese_local = utc_to_chinese(utc_time)
                                chinese_local = datetime.strftime(chinese_local, "%Y-%m-%d %H:%M:%S")
                                date, min_hour = chinese_local.split(' ')
                                min_hour = min_hour[:5]
                                try:
                                    adminResponse = value['adminResponse']
                                except:
                                    adminResponse = 'No'

                                if adminResponse == 'No':
                                    try:
                                        category = [value['group']]
                                    except:
                                        category = ['None']

                                    if adminResponse == 'No':
                                        SLA_time = str(
                                            int(round((userReplyTime - utc_time).total_seconds() / 3600, 0))) + ' hours'
                                    else:
                                        SLA_time = 'N/A'

                                    try:
                                        sessionID = value['sesssionID']
                                    except:
                                        sessionID = ''

                                    try:
                                        uniqueID = value['uniqueID']
                                    except:
                                        uniqueID = sessionID + 'ID'

                                    if date in dateDict:
                                        dateDict[date].append(uniqueID)
                                    else:
                                        dateDict[date] = [uniqueID]

                                    # intentFoundList.append([value['intentFound'],ID, date, min_hour])
                                    intentFoundList.append({"USER QUESTION": value['intentFound'],
                                                            "QUESTION ID": ID,
                                                            "DATE": date,
                                                            "USER ID": value['userid'],
                                                            "KEYWORDS": value['keywords'],
                                                            "TIME (Asia/Shanghai)": min_hour,
                                                            "SLA REMAINING TIME": str(SLA_time),
                                                            "ADMIN RESPONSE": adminResponse,
                                                            "SESSION ID": sessionID,
                                                            "CATEGORY": category,
                                                            "UNIQUE ID": uniqueID
                                                            })  # look for questions answered by bot and questions answered by admin
                                '''
                            botAnsweredDict = {"group A":#,"group B":#}
                            questionResponseDict = {"Answered":{"Bot":#,"Admin":#},"Unanswered":{"Bot":#,"Admin":#}}
                            '''
                            if value['group'][0] in group_list:
                                if value['group'][0] in botAnsweredDict:
                                    botAnsweredDict[value['group'][0]] += 1
                                else:
                                    botAnsweredDict[value['group'][0]] = 1
                                questionResponseDict['Answered']['Bot'] += 1
                            print(li)
                except Exception as e:
                    print('Error intent found update of ', e)

                # look for user input intents that couldn't be found (user said "No" not helpful, or bot said "sorry can't find it")
                try:
                    intentNotFoundDocument = document['intentNotFound']
                    if intentNotFoundDocument != 'None':
                        for key, value in intentNotFoundDocument.items():
                            if value['adminResponse'] == 'No':
                                utc_time = value['time']
                                chinese_local = utc_to_chinese(utc_time)
                                chinese_local = datetime.strftime(chinese_local, "%Y-%m-%d %H:%M:%S")
                                date, min_hour = chinese_local.split(' ')
                                min_hour = min_hour[:5]
                                if value['adminResponse'] == 'No':
                                    SLA_time = str(
                                        int(round((userReplyTime - utc_time).total_seconds() / 3600, 0))) + ' hours'
                                else:
                                    SLA_time = 'N/A'
                                try:
                                    category = [value['category']]
                                except:
                                    category = ['None']

                                if date in dateDict:
                                    dateDict[date].append(uniqueID)
                                else:
                                    dateDict[date] = [uniqueID]
                                # intentNotFoundList.append([value['intentNotFound'],ID, date, min_hour])
                                intentNotFoundList.append({"USER QUESTION": value['intentNotFound'],
                                                           "QUESTION ID": ID,
                                                           "DATE": date,
                                                           "TIME": min_hour,
                                                           "SESSION ID": value['sesssionID'],
                                                           "UNIQUE ID": value['uniqueID'],
                                                           "CATEGORY": category
                                                           })

                                questionResponseDict['Unanswered']['Bot'] += 1
                                if value['adminResponse'] == 'No':
                                    questionResponseDict['Unanswered']['Admin'] += 1
                                elif value['adminResponse'] == 'Yes':
                                    questionResponseDict['Answered']['Admin'] += 1
                except Exception as e:
                    pass

                # now iterate through each individual conversation log
                try:
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        chinese_local = utc_to_chinese(time)
                        date, min_hour = str(chinese_local).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)
                        # create bar graph information for dates vs #conversations
                        # if date in dateDict:
                        #     dateDict[date].append(ID)
                        # else:
                        #     dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = utc_to_chinese(time)

                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = utc_to_chinese(time)
                                print(userReplyTime)
                                print(time)

                                # userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'

                        final_conv_log.append({"ID": ID,
                                               "FROM": type,
                                               "MESSAGE": message,

                                               "COMMENTS": comments,
                                               "SURVEY RESPONSES": Survey,
                                               "DATE": date,
                                               "TIME": min_hour_sec
                                               })

                except Exception as e:
                    print(e)

            else:
                continue
        except Exception as e:
            print('Error in document cursor is ', e)
            pass

    """
    Create sheets for intent found and intent not found
    """

    try:
        if found_bot_id == False:
            return 'Failure'
    except:
        pass

    try:
        averageScore = sumScore / numRated
    except:
        averageScore = 'N/A'

    # initialize lists for data sheet
    dateKeyList = []
    dateValuesList = []
    timeKeyList = []
    timeValueList = []
    ratingKeyList = []
    ratingValueList = []
    latencyKeyList = []
    latencyValueList = []
    ratingCountKeyList = []
    ratingCountValueList = []
    botAnswerKeyList = []
    botAnswerValueList = []
    questionAnswerKeyList = []
    questionAnswerValueList = []

    # create the necessary lists for populating the charts
    for key, value in botAnsweredDict.items():
        botAnswerKeyList.append(key)
        botAnswerValueList.append(value)

    for key, value in questionResponseDict.items():
        questionAnswerKeyList.append(key)
        questionAnswerValueList.append(value)

    for key, value in sorted(latencyDict.items()):
        if key == 12:
            key = '12 pm'
        elif key == 24 or key == 0:
            key = '12 am'
        elif (key - 12) > 1:
            key = key - 12
            key = str(key) + ' pm'
        else:
            key = str(key) + ' am'
        latencyKeyList.append(key)
        if value == []:
            finalLatencyAvg = 0
        else:
            if value['numConvos'] != 0:
                finalLatencyAvg = value['latencySum'] / value['numConvos']
            else:
                finalLatencyAvg = 0
        latencyValueList.append(finalLatencyAvg)

    for key, value in sorted(dateDict.items()):
        dateKeyList.append(key)
        value = len(set(value))
        dateValuesList.append(value)

    for key, value in sorted(timeDict.items()):
        if key == 12:
            key = '12 pm'
        elif key == 24 or key == 0:
            key = '12 am'
        elif (key - 12) > 1:
            key = key - 12
            key = str(key) + ' pm'
        else:
            key = str(key) + ' am'

        timeKeyList.append(key)
        value = len(set(value))
        timeValueList.append(value)
    try:
        final_conv_log = final_conv_log[-300:]
        intentNotFoundList[:50]
        intentFoundList[:50]
    except Exception as e:
        pass
    final_json = {
        "bot_name": bot_name,
        "chat_log": final_conv_log,
        "intent_not_found": intentNotFoundList,
        "intent_found": intentFoundList,
        "graph_data": [
            {
                "graph_name": "Number of Questions By Day",
                "x_values": dateKeyList,
                "y_values": dateValuesList,
                "x_axis_title": "Days",
                "y_axis_title": "Number of Questions"
            },
            {
                "graph_name": "Peak Engagement Hours",
                "x_values": timeKeyList,
                "y_values": timeValueList,
                "x_axis_title": "Time of Day (Asia/Shanghai)",
                "y_axis_title": "Number of Sessions"
            },
            {
                "graph_name": "User Satisfaction",
                "x_values": [],  # ratingKeyList,
                "y_values": [],  # ratingValueList,
                "x_axis_title": "Days",
                "y_axis_title": "Average Satisfaction Rating"
            },
            {
                "graph_name": "Bot Response Latency",
                "x_values": latencyKeyList,
                "y_values": latencyValueList,
                "x_axis_title": "Time of Day (Asia/Shanghai)",
                "y_axis_title": "Average Response Latency (sec)"
            },

        ]
    }

    return final_json


def utc_to_chinese(utc_time):
    from_zone = tz.gettz('UTC')
    to_zone = tz.gettz('Asia/Shanghai')
    utc = utc_time.replace(tzinfo=from_zone)
    # Convert utc time zone to chinese local time. Chinese local time = UTC+8
    china_local = utc.astimezone(to_zone)
    return china_local
