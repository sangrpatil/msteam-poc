import os

from flask import Blueprint, request, jsonify, send_from_directory
from utility.de_coupled_report import *
from utility.generate_reports_json import generateReports
from utility.logger import logger
from utility.mongo_dao import *

metrics_blueprint = Blueprint('metrics', __name__, template_folder='templates')


# Return metrics of all bots
@metrics_blueprint.route('/metrics', methods=['GET'])
def metrics_all_bots():
    """
    Reads information of all the existing bots
    :return: json data of all bot
    """
    if request.method == 'GET':
        # Call the function with not bot_id passed
        returnedjson = generateReports()
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        # return returnedjson
        pass


# Return metrics of a particular bot
@metrics_blueprint.route('/metrics/<bot_id>', methods=['GET', 'POST'])
def metrics_bot(bot_id):
    """
    Reads info related to specific bot id
    :param bot_id: Object Id of bot
    :return: json data
    """
    if request.method == 'POST':
        # Call the function with bot_id passed

        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )

    if request.method == 'GET':
        # Call the function with bot_id passed
        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


'''
Should take a POST request input of:
1. user_id 
2. page number 
'''


@metrics_blueprint.route('/metrics/<bot_id>/convolog', methods=['GET', 'POST'])
def metrics_convolog_bot(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        # Call the function with bot_id passed
        # todo - page_number = request.json['page_number']
        convo_log = get_ConvoLog()
        # todo - slice the returned JSON by the page number.
        return jsonify(convlog=convo_log)


@metrics_blueprint.route('/metrics/<bot_id>/download', methods=['GET', 'POST'])
def metrics_download(bot_id):
    """
    Return metrics of a particular bot
    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            bot_name = ''
            filename1 = generateReport(bot_id, bot_name, start, end)
            file_name = 'Chatbot_Report_All' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/metrics/<bot_id>/convo', methods=['POST'])
def metrics_convo(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        # Call the function with bot_id passed
        offset = request.json['offset']
        next = request.json['next']
        start = request.json['start_date']
        end = request.json['end_date']
        try:
            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.error(e)
            print('Bot info read error - ', e)

        try:
            returnedjson, total = get_ConvoLog(start, end, bot_id, offset, next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    msg='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/metrics/<bot_id>/intentfound', methods=['POST'])
def metrics_intentfound(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        # Call the function with bot_id passed
        offset = request.json['offset']
        next = request.json['next']
        start = request.json['start_date']
        end = request.json['end_date']
        try:

            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.error(e)
            print('bot info read error - ', e)

        try:
            returnedjson, total = get_intent_found(start, end, bot_id, offset, next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/metrics/<bot_id>/intentnotfound', methods=['POST'])
def metrics_intentnotfound(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        # Call the function with bot_id passed
        offset = request.json['offset']
        next = request.json['next']
        start = request.json['start_date']
        end = request.json['end_date']
        try:
            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.error(e)
            print('Bot info read error - ', e)

        try:
            returnedjson, total = get_intent_NotFound(start, end, bot_id, offset, next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name

                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/metrics/<bot_id>/graph', methods=['POST'])
def metrics_graph_data(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.error(e)
            print('user info read error - ', e)

        try:
            returnedjson = get_dashboard(start, end, bot_id, bot_name)
            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    bot_name=returnedjson['bot_name'],
                    graph_data=returnedjson['graph_data'])


        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/metrics/<bot_id>/convo/download', methods=['POST'])
def metrics_download_convo(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = generateReports_convo(start, end, bot_id)
            file_name = 'Chatbot_Report_convo' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'

            path = os.path.abspath("static/reports/")

            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/metrics/<bot_id>/intent/download', methods=['POST'])
def metrics_download_intent(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = generateReports_intent(start, end, bot_id)
            file_name = 'Chatbot_Report_intent' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/metrics/<bot_id>/graph/download', methods=['POST'])
def metrics_download_graph(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = graph_download(bot_id, '', start, end)
            file_name = 'Chatbot_Report_Graph' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/metrics/<bot_id>/intentfound/download', methods=['POST'])
def metrics_download_intentfound(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = generateReports_intentfound(start, end, bot_id)
            file_name = 'Chatbot_Report_intentfound' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/metrics/<bot_id>/intentnotfound/download', methods=['POST'])
def metrics_download_intentnotfound(bot_id):
    """

    :param bot_id:
    :return:
    """
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = generateReports_intentnotfound(start, end, bot_id)
            file_name = 'Chatbot_Report_intentnotfound' + datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")

            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure
