import calendar
import json
import logging
from datetime import *
import threading
import bson
from flask import Blueprint, request, jsonify, json, Response
from routes.validate import validate
from utility.generate_reports_excel import generateReportsWeekly, generateReportsDaily
from utility.mail_pfizer import send_email
from utility.mongo_dao import insert, update, delete

notifications_blueprint = Blueprint('notifications', __name__, template_folder='templates')
g_notifications = {}
mailtype = ""
logger = logging.getLogger()


@notifications_blueprint.route('/scheduler/<bot_id>/create', methods=['POST'])
def set_notification(bot_id):
    """
    This function creates notification entry in notifications collection for weekly and
    daily report's schedule mailing list, time and day.

    :param bot_id: Id of the Bot ,type: string
    :return: Success or Failure message
    """
    # Validate bot_id
    try:
        response_validate = validate('bot', bot_id)
        logger.info('REPONSE from Crete Notification = {}'.format(Response(response_validate)))
        logger.debug('RESPONSE Type - ', type(response_validate))

        response_json = response_validate.get_data(as_text=True)
        response_json = json.loads(response_json)
        response_status = response_json['result']
    except Exception as e:
        logger.error(str(e))
    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:
        try:
            # Schedule notifications in memory
            configuration = request.get_json(force=True)

            # Save it to mongoDB
            insert('notifications',
                   {"emails": configuration['emails'],
                    "bot_id": bot_id,
                    "notification_time": configuration['notification_time'],
                    "notification_day": configuration['notification_day'],
                    "notification_frequency": configuration['notification_frequency'],
                    "sent": False})

            global g_notifications
            print('Current Global Notification Variable in notification.py = {}'.format(g_notifications))
            g_notifications[bson.ObjectId(bot_id)] = {"emails": configuration['emails'],
                                                      "bot_id": bot_id,
                                                      "notification_time": configuration['notification_time'],
                                                      "notification_day": configuration['notification_day'],
                                                      "notification_frequency": configuration['notification_frequency'],
                                                      "sent": False}

            return jsonify(
                status="Success",
                msg="Notification Set"
            )
        except Exception as e:
            return jsonify(
                status="Failed",
                msg=e
            )


@notifications_blueprint.route('/scheduler/<bot_id>', methods=['GET'])
def get_notification(bot_id):
    """
    This function returns all the scheduler informations stored in database i.e
    notifications collection.

    :param bot_id:
    :return: Success(With schedule details) or Failure (With error message)
    """
    # Validate bot_id
    try:
        response_validate = validate('bot', bot_id)
        response_json = response_validate.get_data(as_text=True)
        response_json = json.loads(response_json)
        logger.info('REPONSE from Crete Notification = {}'.format(response_json))
        response_status = response_json['result']

        if response_status == 1:
            return jsonify(
                status="Failed",
                msg=response_json['msg']
            )
        else:

            global g_notifications
            return jsonify(status="Success", msg=g_notifications[bson.ObjectId(bot_id)])
    except Exception as e:
        logger.exception(str(e))


@notifications_blueprint.route('/scheduler/<bot_id>/delete', methods=['GET'])
def delete_notification(bot_id):
    """
    This function deletes notifications data with given bot id.

    :param bot_id: Bot's Id
    :return: Success or Failure status with message
    """

    # Validate bot_id
    try:
        response_validate = validate('bot', bot_id)
        response_json = response_validate.get_data(as_text=True)
        response_json = json.loads(response_json)
        logger.debug('REPONSE from Crete Notification = {}'.format(response_json))

        response_status = response_json['result']
    except Exception as e:
        logger.error(str(e))
    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:
        try:
            # Delete it from memory
            global g_notifications
            g_notifications.pop(bson.ObjectId(bot_id))
            # Delete it to mongoDB
            delete('notifications', bot_id)
            return jsonify(
                status="Success",
                msg="Notification deleted"
            )
        except Exception as e:
            logger.error("notification error: ", str(e))
            return jsonify(
                status="Failed",
                msg=e
            )


@notifications_blueprint.route('/scheduler/<bot_id>/update', methods=['POST'])
def update_notification(bot_id):
    """
    This function updates the existing notificatio's data.

    :param bot_id: Bot's Id
    :return: Success or Failure with message
    """
    # Validate bot_id
    try:
        response_validate = validate('bot', bot_id)
        response_json = response_validate.get_data(as_text=True)
        response_json = json.loads(response_json)
        logger.debug('REPONSE from Crete Notification = {}'.format(response_json))
        response_status = response_json['result']
    except Exception as e:
        logger.error(str(e))
    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:
        try:
            # Schedule notifications in memory
            configuration = request.get_json(force=True)
            # Schedule notifications in memory
            global g_notifications
            g_notifications[bson.ObjectId(bot_id)] = {"emails": configuration['emails'],
                                                      "bot_id": bot_id,
                                                      "notification_time": configuration['notification_time'],
                                                      "notification_day": configuration['notification_day'],
                                                      "notification_frequency": configuration['notification_frequency'],
                                                      "sent": False}
            # Save it to mongoDB
            update('notifications', bot_id, {"emails": configuration['emails'],
                                             "bot_id": bot_id,
                                             "notification_time": configuration['notification_time'],
                                             "notification_day": configuration['notification_day'],
                                             "notification_frequency": configuration['notification_frequency'],
                                             "sent": False})
            return jsonify(
                status="Success",
                msg="Notification Updated"
            )
        except Exception as e:
            logger.error("notification error: ", str(e))
            return jsonify(
                status="Failed",
                msg=e
            )
    pass


def send_notification_weekly(bot_id, emails, weekly_daily=False):
    """
    This function prepares and sends weekly mail by checking time.

    :param bot_id: Bot's Id
    :param emails: list of emails
    :param weekly_daily: Boolean to check if the reporting is for both weekly and daily at same time
    :return: None
    """
    global g_notifications
    notification = g_notifications[bot_id]
    mailtype = "weekly"
    if notification['sent'] == False:
        try:
            logger.debug('sending notification for bot', bot_id, 'to users ', emails)
            # Prepare notification (Generate your reports and store it in some common location)
            file_loc = generateReportsWeekly(bot_id)

            email_list = []
            for email_name in emails:
                email_list.append(email_name['email'])
            try:
                send_email(file_loc, email_list, mailtype)
            except Exception as e:
                logger.error('email send error - ', e)
            if not weekly_daily:
                g_notifications[bot_id]['sent'] = True

        except Exception as e:
            logger.error('SEND NOTIFICATION ERROR - ', e)
            if not weekly_daily:
                g_notifications[bot_id]['sent'] = True

    pass


def send_notification_daily(bot_id, emails):
    """
    Function to prepare and send notification
    Sends daily mail by checking time.

    :param bot_id: Bot's Id
    :param emails:
    :return:
    """
    global g_notifications
    notification = g_notifications[bot_id]
    mailtype = "daily"
    if notification['sent'] == False:
        try:
            logger.debug('sending notification for bot', bot_id, 'to users ', emails)
            # Prepare notification (Generate your reports and store it in some common location)
            file_loc = generateReportsDaily(bot_id)

            email_list = []
            for email_name in emails:
                email_list.append(email_name['email'])
            try:
                send_email(file_loc, email_list, mailtype)
            except Exception as e:
                logger.error('email send error - ', e)
            g_notifications[bot_id]['sent'] = True
        except Exception as e:
            logger.error('SEND NOTIFICATION ERROR - ', e)
            g_notifications[bot_id]['sent'] = True


def listen_notification():
    """
    Asynchronous function to listen for notifications
    :return: None
    """
    global g_notifications
    logger.debug('Current Global Notification Variable in listen notification.py = {}'.format(g_notifications))
    while True:
        try:
            for bot_id, notification in g_notifications.items():
                current_day = calendar.day_name[date.today().weekday()]
                current_time = datetime.now().strftime('%H:%M')
                notification_day = notification['notification_day']
                notification_time = notification['notification_time']
                notification_frequency = notification['notification_frequency']
                emails = notification['emails']
                if (
                        notification_frequency.lower() == 'weekly' and current_day == notification_day and current_time == notification_time):
                    send_notification_weekly(bot_id, emails)
                elif (notification_frequency.lower() == 'daily' and current_time == notification_time):
                    send_notification_daily(bot_id, emails)
                elif notification_frequency.lower() == 'weekly&daily':
                    if (current_day == notification_day and current_time == notification_time):
                        send_notification_weekly(bot_id, emails, True)
                        send_notification_daily(bot_id, emails)
                    elif current_time == notification_time:  # Check for daily report
                        send_notification_daily(bot_id, emails)
                    else:
                        g_notifications[bot_id]['sent'] = False


                else:
                    # Update the notification sent to False
                    g_notifications[bot_id]['sent'] = False
        except Exception as e:
            logger.error(str(e))
notify_thread = threading.Thread(target=listen_notification, args=[])
