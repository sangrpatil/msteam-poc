from flask import Blueprint, render_template, request, session, Markup
from utility.logger import logger
from utility.mongo_authentication import MongoAuthentication

admin_blueprint = Blueprint('admin', __name__, template_folder='templates')
auth = MongoAuthentication()


@admin_blueprint.route('/admin/')
def admin_index():
    return render_template('admin.html')


@admin_blueprint.route('/admin/login', methods=['GET', 'POST'])
def admin_login():
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        try:
            username = request.form.get('username')
            password = request.form.get('password')
            is_valid, message = auth.login(username, password)
            if is_valid == 0:
                session['logged_in'] = True
                return render_template('admin.html')
            else:
                return render_template('login.html', message=Markup(
                    '<div class="alert alert-danger" role="alert"><b>' + message + '</b></div>'))
        except Exception as e:
            logger.error(str(e))
            message = 'Some error occurred'
            return render_template('login.html', message=Markup(
                '<div class="alert alert-danger" role="alert"><b>' + message + '</b></div>'))
