from flask import Blueprint, request, jsonify
from utility.mongo_dao import get_all, insert, get_collection, get_by_id, update, delete, get_one

user_blueprint = Blueprint('user', __name__, template_folder='templates')


@user_blueprint.route('/user/create', methods=['POST'])
def user_create():
    """
    Creates user
    :return: Success or Failure response
    """
    message = request.json
    existing_user = get_all('user', 'username', message['username'])
    if len(existing_user) > 0:
        return jsonify(
            status='Failed',
            msg="User creation failed"
        )
    else:
        insert('user',
               {"username": message['username'], 'password': message['password'], "lastlogin": "Never Logged in!"})
        return jsonify(
            status='Success',
            msg="User created successfully"
        )


@user_blueprint.route('/user/read', methods=['POST'])
def user_read():
    """
    Gets all user data from database
    :return: user list
    """
    message = request.json
    users = get_collection("user")
    user_list = []
    if "sessionObj" in message.keys():
        del message['sessionObj']
    for index, user in enumerate(users):
        usr = {}
        usr['_id'] = str(user['_id']).replace('ObjectId(', '').replace(')', '')
        usr['username'] = str(user['username'])
        usr['lastlogin'] = str(user['lastlogin'])
        # usr['email'] = str(user.get('email'))
        usr['password'] = str(user['password'])
        if 'superuser' in user:
            usr['superuser'] = str(user['superuser'])
        user_list.append(usr)

    return jsonify(
        status="Success",
        msg=user_list)


@user_blueprint.route('/user/update', methods=['POST'])
def user_update():
    """
    Updates existing user information
    :return: Success or Failure response
    """
    message = request.json
    existing_user = get_by_id('user', message['_id'])
    print(existing_user)
    if "sessionObj" in message.keys():
        del message['sessionObj']
    if len(existing_user) > 0:
        id = message['_id']
        del message['_id']
        if "lastlogin" in message.keys():
            del message['lastlogin']

        updated_user = update('user', id, message)
        return jsonify(
            status='Success',
            msg="User updated successfully"
        )
    else:
        return jsonify(
            status='Failed',
            msg="User updation failed"
        )


@user_blueprint.route('/user/delete', methods=['POST'])
def user_delete():
    """
    Deletes existing user
    :return: Success or Failure response
    """
    message = request.json
    existing_user = get_by_id('user', message['_id'])
    user = get_one('bot', 'lock_by', message['_id'])
    print(user)
    # This below code is to remove the lock over cct if the user is deleted from admin ui
    if user is not None:
        if user['lock_by'] == message['_id']:
            user['_id'] = str(user['_id']).replace('ObjectId(', '').replace(')', '')
            update('bot', user['_id'], {'lock_by': ''})
    if len(existing_user) > 0:
        delete('user', message['_id'])
        return jsonify(
            status='Success',
            msg="User deleted successfully"
        )
    else:
        return jsonify(
            status='Failed',
            msg="No record found"
        )


@user_blueprint.route('/user/specific', methods=['POST'])
def user_specific_data():
    """
    Reads specific user's info based on ID
    :return: user info
    """
    message = request.json
    print(message)
    user = get_by_id('user', message['_id'])
    usr = {}
    usr['_id'] = str(user.get('_id')).replace('ObjectId(', '').replace(')', '')
    usr['username'] = str(user.get('username'))
    usr['lastlogin'] = str(user.get('lastlogin'))
    usr['email'] = str(user.get('email'))
    usr['password'] = str(user.get('password'))
    if 'superuser' in user:
        usr['superuser'] = str(user['superuser'])
    return jsonify(
        status="Success",
        msg=usr)


@user_blueprint.route('/user/unlock', methods=['POST'])
def user_unlock():
    """

    :return:
    """
    message = request.json
    update('bot', message['bot_id'], {'lock_by': ''})
    return jsonify(
        status="Success",
        msg="You have unlocked the CCT for modification"

    )


@user_blueprint.route('/user/lock', methods=['POST'])
def user_lock():
    """

    :return:
    """
    message = request.json
    user = get_by_id('bot', message['bot_id'])

    if user['lock_by'] == "":
        update('bot', message['bot_id'], {'lock_by': message['user_id']})
        return jsonify(
            status="Success",
            msg="You have locked the CCT for modification"

        )
    else:
        return jsonify(
            status="Failed",
            msg="Already locked"

        )
