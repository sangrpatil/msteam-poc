from flask import Blueprint, request, jsonify, render_template, session, redirect, Markup
from utility.mongo_dao import insert, get_by_id, get_one, update, get_collection
from utility.bot_engine import validate_bot, store_bot, update_bot
from utility.logger import logger
from utility.execution_engine import ExecutionEngine
from utility.session_manager import get as get_context, set as set_context, modify as update_context
import time
import datetime
bot_blueprint = Blueprint('bot', __name__, template_folder='templates')
import bson
import json
from utility.excel_parser import convert
import os
from flask_babel import refresh, gettext
from flask import current_app as app


@bot_blueprint.route('/check')
def check():
    return jsonify(
        status='Running'
    )

@bot_blueprint.route('/login', methods = ['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    elif request.method == 'POST':
        admin_username = os.environ.get('ADMIN_USERNAME') or 'admin'
        admin_password = os.environ.get('ADMIN_PASSWORD') or 'admin'

        logindata = request.json
        if logindata == None or logindata == 'None':
            username = request.form.get('username')
            password = request.form.get('password')
        else:
            username = logindata['username']
            password = logindata['password']
        logger.info("Username received = {}".format(username))

        now = datetime.datetime.now()
        dic = {"lastlogin": str(now.strftime("%Y-%m-%d %H:%M"))}
        try:
            user = get_one('user', 'username', username)
        except Exception as e:
            logger.exception("Bot create exception: ", str(e))

        if not (user == None) and password == user.get('password'):
            session['loggedIn'] = True
            session['username'] = username

            print(now.strftime("%Y-%m-%d %H:%M"), "***")
            id = get_one('user', 'username', username).get('_id')
            update('user', id, dic)
            print("session---", type(session), session)
            logger.info('Following User logged in successfully = {} at {}'.format(username, now))
            return jsonify(
                status='Success',
                msg=str(id).replace('ObjectId(', '').replace(')', '')
            )
        else:
            logger.info('Login Failed for username = {} at {}'.format(username, now))
            session['loggedIn'] = False

            return jsonify(
                status='Failure',
                msg='User login failed'
            )


@bot_blueprint.route('/logout', methods = ['GET'])
def logout():
    session['loggedIn'] = False
    return render_template('login.html')

@bot_blueprint.route('/bot/create',methods=['GET','POST'])
def bot_create():
    session['update_login_attempt'] = False
    if request.method == 'GET':
        try:
            if session['loggedIn'] == True:
                return render_template('create_bot_excel.html')
            else:
                return redirect('/login')
        except Exception as e:
            logger.error("Bot error: ", str(e))
            return redirect('/login')
    elif request.method == 'POST':
        try:
            f = request.files['file']
            f.save(r'./mapping/' + f.filename)
            configuration = {"bot name":"GCOoD Bot","language":"en","es_analyzer":"English","mapping":[]} #todo un-hardcode this
            mapping_result = json.loads(convert(r'./mapping/' + f.filename))
            for key, value in mapping_result.items():
                configuration["mapping"] = value
        except Exception as e:
            logger.error("Bot error: ", str(e))
            configuration = request.get_json(force=True)
        is_valid,message = validate_bot(configuration)
        if is_valid == 0:
            is_saved, bot_id, message = store_bot(configuration)
            if is_saved == 0:
                return jsonify(
                    status='Success',
                    id=bot_id,
                    msg='Bot created successfully'
                )
            else:
                return jsonify(
                    status='Error',
                    msg=message
                )
        else:
            return jsonify(
                status='Error',
                msg=message
            )


@bot_blueprint.route('/bot/<bot_id>/update',methods=['GET','POST'])
def bot_update(bot_id):
    if request.method == 'GET':
        session['update_login_attempt'] = True
        session['update_bot_id'] = bot_id
        try:
            if session['loggedIn'] == True:
                return render_template('update_bot.html', bot_id=bot_id)
            else:
                return redirect('/login')
        except Exception as e:
            logger.error("Bot error: ", str(e))
            return redirect('/login')
    elif request.method == 'POST':
        try:
            f = request.files['file']
            f.save(r'./mapping/' + f.filename)
            configuration = {"bot name":"GCOoD Bot","language":"en","es_analyzer":"English","mapping":[]} #todo un-hardcode this
            mapping_result = json.loads(convert(r'./mapping/' + f.filename))
            for key, value in mapping_result.items():
                configuration["mapping"] = value
        except Exception as e:
            logger.error("Bot error: ", str(e))
            configuration = request.get_json(force=True)

        #modify configuration
        new_mapping = []
        try:
            if isinstance(configuration['mapping'][0]['Task Text'],(list,)):
                for item in configuration['mapping']:
                    task_text_list = item['Task Text']
                    if len(item['Task Text']) == 1:
                        item['Task Text'] = item['Task Text'][0]
                        new_mapping.append(item)
                    else:
                        for text in task_text_list:
                            item_copy = item.copy()
                            item_copy['Task Text'] = text
                            new_mapping.append(item_copy)
                configuration['mapping'] = new_mapping
        except Exception as e:
            logger.error("Bot error: ", str(e))
            return jsonify(
                status='Error',
                msg=str(e)
            )
        is_valid, message = validate_bot(configuration)
        if is_valid == 0:
            is_saved,bot_id,message = update_bot(configuration,bot_id)
            success_msg = 'Your bot has been updated!'
            if is_saved == 0:
                return jsonify(
                    status='Success',
                    id=bot_id,
                    msg='Bot updated successfully'
                )
            else:
                return jsonify(
                    status='Error',
                    msg=message
                )

        else:
            return jsonify(
                status='Error',
                msg=message
            )

@bot_blueprint.route('/bot/<bot_id>',methods=['GET','POST'])
def bot_read(bot_id):
    if request.method == 'GET':
        is_valid,configuration, message = read_bot(bot_id)
        configuration['_id'] = str(configuration['_id']).replace('ObjectId(','').replace(')','')
        new_mapping = []
        if is_valid == 0:
            last_id = 0
            task_text_dict = {}
            for item in configuration['mapping']:
                try:
                    if last_id < int(float(item['Task ID'])):
                        last_id = int(float(item['Task ID']))
                except Exception as e:
                    logger.error("Bot error: ", str(e))
                    pass
                if item['Task ID'] == '':
                    item['Task ID'] = '0'
                if item['Task ID'] in task_text_dict:
                    task_text_dict[item['Task ID']].append(item['Task Text'])
                else:
                    task_text_dict[item['Task ID']] = [item['Task Text']]
            seen_ids = []
            for map in configuration['mapping']:
                if map['Task ID'] == '':
                    map['Task ID'] = '0'
                if map['Task ID'] in seen_ids:
                    pass
                else:
                    seen_ids.append(map['Task ID'])
                    map['Task Text'] = task_text_dict[map['Task ID']]
                    new_mapping.append(map)

            configuration['max_id'] = last_id
            configuration['mapping'] = new_mapping

            return jsonify(
                status='Success',
                configuration=configuration,
                msg=message
            )
        else:
            return jsonify(
                status='Failure',
                configuration='',
                msg=message
            )

@bot_blueprint.route('/bot',methods=['GET'])
def bot_read_all():
    if request.method == 'GET':
        try:
            bots = get_collection("bot")
            bot_list = []
            for index, bot in enumerate(bots):
                bot['_id'] = str(bot['_id']).replace('ObjectId(','').replace(')','')
                new_mapping = []
                last_id = 0
                task_text_dict = {}
                for item in bot['mapping']:
                    try:
                        if last_id < int(float(item['Task ID'])):
                            last_id = int(float(item['Task ID']))
                    except Exception as e:
                        logger.error("Bot error: ", str(e))
                        pass
                    if item['Task ID'] == '':
                        item['Task ID'] = '0'
                    if item['Task ID'] in task_text_dict:
                        task_text_dict[item['Task ID']].append(item['Task Text'])
                    else:
                        task_text_dict[item['Task ID']] = [item['Task Text']]
                seen_ids = []
                for map in bot['mapping']:
                    if map['Task ID'] == '':
                        map['Task ID'] = '0'
                    if map['Task ID'] in seen_ids:
                        pass
                    else:
                        seen_ids.append(map['Task ID'])
                        map['Task Text'] = task_text_dict[map['Task ID']]
                        new_mapping.append(map)

                bot['max_id'] = last_id
                bot['mapping'] = new_mapping
                bot_list.append(bot)

            return jsonify(
                bots= bot_list
            )
        except Exception as e:
            logger.error("Bot error: ", str(e))
            return jsonify(
                status='Failure',
                msg=e
            )

@bot_blueprint.route('/bot/<bot_id>/delete',methods=['GET','POST'])
def bot_delete(bot_id):
    if request.method == 'GET':
        try:
            is_valid, message = delete_bot(bot_id)
        except Exception as e:
            logger.error("Bot error: ", str(e))
        if is_valid == 0:
            return jsonify(
                status='Success',
                msg=message
            )
        else:
            return jsonify(
                status='Failure',
                msg=message
            )



################# Chat Events ####################
@bot_blueprint.route('/bot/<bot_id>/init',methods=['GET'])
def bot_init(bot_id):
    logger.info('BOT ID: ',bot_id)
    try:
        if len(get_by_id('bot',bot_id))>0:
            logger.debug('Valid bot id - %s',bot_id)
            session = {}
            session['on_connect_start'] = True
            session['suggestion'] = ''
            session['fuzzyFlag'] = False
            session['restartFlag'] = False
            session['restartFlagFAQ'] = False
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session['codeFlag'] = False
            session['guidedFlag'] = False
            session['audioSentFlag'] = False
            session['ASRFlag'] = False
            session['results'] = ''
            session['selectColumnQ'] = ''
            session['filterColumnQ'] = ''
            session['filterValueQ'] = ''
            session['surveyFlagNum'] = False
            session['surveyFlagComm'] = False
            session['endSurveyFlag'] = False
            session['feedbackFlag_ES'] = False
            session['suggestCounter_ES'] = 0
            session['audioSentFlag_ES'] = False
            session['bot_id'] = bot_id
            session['name'] = 'None'
            session['disable_input'] = False
            session['survey_taken'] = False
            session['email'] = request.args.get("email_id")
            session['user_id'] = request.args.get("user_id")
            session['created_on'] = time.time()  # todo time zone
            session_id = set_context(session)
            return jsonify(
                status='Success',
                session_id=session_id
            )

        else:
            return jsonify(
                status='Failure',
                msg='Invalid bot id'
            )

    except Exception as e:
        logger.error(str(e))
        return jsonify(
            status='Failure',
            msg='Invalid bot id'
        )


@bot_blueprint.route('/bot/<bot_id>/<session_id>/end',methods=['POST'])
def bot_end(bot_id,session_id):
    try:
        if (bson.objectid.ObjectId.is_valid(bot_id) and get_by_id('bot',bot_id)):
            if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context',session_id)):
                update('status', session_id, {'has_ended': True})
                return jsonify(
                    status='Success',
                    msg='Session ended'
                )
            else:
                return jsonify(
                    status='Failure',
                    msg='Not a valid Session ID'
                )
        else:
            return jsonify(
                status='Failure',
                msg='Not a valid Bot ID'
            )
    except Exception as e:
        logger.error("Bot error: ", str(e))

@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat',methods=['POST'])
@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat/<user_id>', methods=['POST'])
def bot_chat(bot_id,session_id, user_id='None'):
    print("In chat")
    bot_data=get_by_id('bot',bot_id)
    if (bson.objectid.ObjectId.is_valid(bot_id) and bot_data):#get_by_id('bot',bot_id)):
        if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context',session_id)):
            current_status = get_by_id('status', session_id)

            app.config['BABEL_DEFAULT_LOCALE'] =bot_data['language'] #get_by_id('bot', bot_id)['language']
            refresh()
            message_request = request.get_json(force=True)
            message = message_request['message']
            current_state = message_request['state']
            asr_flag = message_request['ASR']

            session = get_context(session_id)

            if not session['user_id'] and message_request['user_id']:
                session['user_id'] = message_request['user_id']

            if session['on_connect_start'] == True:
                execution_engine = ExecutionEngine(session_id, bot_id)
                state_definition = bot_data['mapping'] #get_by_id('bot', bot_id)['mapping']

                for state in state_definition:
                    for key, value in state.items():
                        if key == 'Is Start' and value == 'true':
                            response = execution_engine.execute(state)
                            return jsonify(
                                status='Success',
                                result=response,
                                current_state=get_by_id("status",session_id)["current_state"]
                            )
            else:
                #if condition - if msg is empty and on connet start is FALSE then just re-execute the current state
                #if we refresh, then just re-execute the current state.
                #pop out the last state in history because it will be added back in via conversation logger
                if message == '':
                    execution_engine = ExecutionEngine(session_id, bot_id)
                    current_state = get_by_id("status", session_id)["current_state"]
                    state_definition = bot_data['mapping'] #get_by_id('bot', bot_id)['mapping']
                    for state in state_definition:
                        for key, value in state.items():
                            if key == 'Task Name' and value == current_state:
                                history = get_by_id('status', session_id)['history']
                                history = history[:-1]  # pop out the last element
                                update('status', session_id, {'history': history})
                                response = execution_engine.execute(state)
                                return jsonify(
                                    status='Success',
                                    result=response,
                                    current_state=get_by_id("status", session_id)["current_state"]
                                )
                else:
                    execution_engine = ExecutionEngine(session_id, bot_id)
                    response = execution_engine.resume(message, asr_flag, bot_id)
                    if response is None:
                        return jsonify(
                            status='Failure',
                            result=response,
                            current_state=get_by_id("status", session_id)["current_state"]
                        )
                    else:
                        return jsonify(
                            status='Success',
                            result=response,
                            current_state = get_by_id("status",session_id)["current_state"]
                        )
        else:
            return jsonify(
                status='Failure',
                result='Invalid session id'
            )
    else:
        return jsonify(
            status='Failure',
            result='Invalid bot id'
        )
