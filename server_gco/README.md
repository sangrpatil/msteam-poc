# Project Title

Chatbot Accelerator Version 2.0

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

MongoDB - https://docs.mongodb.com/getting-started/shell/installation/
ElasticSearch - https://www.elastic.co/downloads/elasticsearch
Python 3.6.3 - https://www.python.org/downloads/release/python-363/


### Installing

(Non Docker Route):
Install MongoDB, Elasticsearch, and Python from their respective websites. Run MongoDB and ElasticSearch, and then run the python main.py file.

(Docker Route):
See Docker Deployment steps

## Deployment

(Non Docker Route)
Just run...

```
python main.py
```

..and that's it!

(Docker Route):

1. Build command
```
make build
```

2. Start all containers
```
docker-compose up
```

3. Alternatively, to start in swarm mode, run this command instead of command #2
```
docker stack deploy --compose-file docker-compose.yaml pfizer-chatbot
```

## Versioning

We use Bitbucket for versioning

## Authors

Niranjan Sonachalam

Abhinav Mishra

Robert Xu

