import xlsxwriter
import datetime
from utility.logger import logger
import re, os
import configparser
from utility.mongo_dao import get_all, get_collection, get_by_id
import pytz


app_config = configparser.ConfigParser()
# app_config.read_file(open(r'../config/app_settings.ini'))



def generateReportsDaily(bot_id = None):

    cursor = get_collection('status')
    if bot_id == None:
        bot_name = 'All_Bots'

    else:
        bot_name = get_by_id('bot', bot_id)['bot name']
    if bot_name == '':
        bot_name = 'Bot'
    fileTitle = bot_name + '_Chatbot_Report_Daily_' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    file_uri = os.path.join(directory_path, 'reports', fileTitle)

    workbook = xlsxwriter.Workbook(filename=file_uri)
    convoLog = workbook.add_worksheet('ConversationLog')


    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['ID', 'FROM', 'MESSAGE', 'RATING', 'DATE', 'TIME (EST)']
    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:I1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    headers = ['ID', 'FROM', 'MESSAGE', 'RATING', 'DATE', 'TIME (EST)']
    ID = 0
    numRated = 0
    sumScore = 0
    number_conv_counter = 0
    dateDict = {}
    timeDict = {}
    ratingsDict = {}
    latencyDict = {}
    intentFoundList = []
    intentNotFoundList = []
    final_conv_log = []

    final_conv_log.append(headers)
    found_bot_id = False

    # create hour dict for conversations over time of day and latency
    for k in range(0, 24):
        timeDict[k] = []
        latencyDict[k] = []
    ratingHistogramDict = {'No Rating': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
    for document in cursor:
        try:
            # determine the converation's rating and comments, if any

            if bot_id == None:
                number_conv_counter += 1
                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated += 1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except  Exception as e:
                    logger.error("Generate report excel error: ", str(e))

                    rating = 'None'
                    ratingHistogramDict['No Rating'] += 1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report excel error: ", str(e))
                    comments = 'None'

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    print('bot ID is none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time'].astimezone(pytz.timezone('US/EASTERN'))

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID: int(rating)}

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'

                        dataRow = [ID, type, message, rating, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ", str(e))

            elif str(document['bot_id']) == str(bot_id):
                number_conv_counter += 1
                found_bot_id = True

                try:
                    rating = document['Rating']
                    numRated += 1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report excel error: ", str(e))
                    rating = 'None'
                    ratingHistogramDict['No Rating'] += 1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report excel error: ", str(e))
                    comments = 'None'

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()


                # now iterate through each individual conversation log
                try:
                    print('bot ID is not none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time'].astimezone(pytz.timezone('US/EASTERN'))

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID: int(rating)}

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'

                        dataRow = [ID, type, message, rating, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ", str(e))

            else:
                continue
        except Exception as e:
            logger.error("Generate Report Excel DOCUMENT CURSOR ERROR - : ", str(e))

    """
    Create sheets for intent found and intent not found
    """
    # #create header that will be used for "Intent Found" and "Intent not Found"


    try:
        if found_bot_id == False:
            return 'Failure'
    except Exception as e:
        logger.error("Generate Report Excel error: ", str(e))
        pass

    # Hide the datasheet used to create the charts

    workbook.close()
    return fileTitle

def generateReportsWeekly(bot_id = None): #todo pass bot ID
    try:
        logger.info('---CHECKING BOT ID ---', bot_id)
        cursor = get_collection('status')
        if bot_id == None:
            bot_name = 'All_Bots'
        else:
            bot_name = get_by_id('bot', bot_id)['bot name']
            # cursor = get_by_id('status', bot_id)

        if bot_name == '':
            bot_name = 'Bot'
        fileTitle = bot_name + '_Chatbot_Report_Weekly_' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
        file_path = os.path.dirname(__file__)
        directory_path = os.path.dirname(file_path)
        file_uri = os.path.join(directory_path, 'reports', fileTitle)


        workbook = xlsxwriter.Workbook(filename=file_uri)
        dashboard = workbook.add_worksheet('Dashboard')
        convoLog = workbook.add_worksheet('ConversationLog')
        dataSheet = workbook.add_worksheet('DataSheet')

        # master formatting style
        formatting = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': 'white',
            'fg_color': '#007DC6'})

        # write column headers for conversation data log
        headers = ['ID', 'FROM', 'MESSAGE', 'RATING', 'DATE', 'TIME (EST)']
        convoLog.write_row('A1',headers, cell_format=formatting)
        convoLog.set_column(0, 8, 20)
        convoLog.autofilter('A1:I1')

        #Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
        i = 1
        headers = ['ID', 'FROM', 'MESSAGE', 'RATING', 'DATE', 'TIME (EST)']
        ID = 0
        numRated = 0
        sumScore = 0
        number_conv_counter = 0
        dateDict = {}
        timeDict = {}
        ratingsDict = {}
        latencyDict = {}
        final_conv_log = []

        final_conv_log.append(headers)

        #create hour dict for conversations over time of day and latency
        for k in range(0,25):
            timeDict[k] = []
            latencyDict[k] = []
        ratingHistogramDict ={'No Rating':0,'1':0,'2':0,'3':0,'4':0,'5':0}
        for document in cursor:
            logger.info('---CHECKING BOT ID---', str(bot_id))
            try:
                #determine the converation's rating and comments, if any

                if bot_id == None:
                    number_conv_counter+=1
                    # found_bot_id = True
                    try:
                        rating = document['Rating']
                        numRated +=1
                        sumScore += int(rating)
                        ratingHistogramDict[rating] += 1
                    except Exception as e:
                        logger.error("Generate report excel error: ", str(e))
                        rating = 'None'
                        ratingHistogramDict['No Rating'] +=1
                    try:
                        comments = document['Comments']
                    except Exception as e:
                        logger.error("Generate report excel error: ", str(e))
                        comments = 'None'

                    #initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                    ID += 1
                    userReplyTime  = datetime.datetime.now()

                    #now iterate through each individual conversation log
                    try:
                        conversations = document['conversations']

                        botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                        for item in conversations:
                            message = conversations[item]['message']
                            type = conversations[item]['type']
                            time = conversations[item]['time'].astimezone(pytz.timezone('US/EASTERN'))

                            #process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            #process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            #process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            #create bar graph information for dates vs #conversations
                            if date in dateDict:
                                dateDict[date].append(ID)
                            else:
                                dateDict[date] = [ID]

                            #create line graph information for # conversations by hour of day
                            if hour in timeDict:
                                timeDict[hour].append(ID)
                            else:
                                timeDict[hour] = [ID]

                            #create line graph info for latency by hour of day
                            if hour in latencyDict:
                                if latencyDict[hour] == []:
                                    # only want to calculate "bot message timestamp" - "user message timestamp"
                                    if conversations[item]['type'] == 'user message':
                                        userReplyTime = time
                                    latencyDict[hour] = {'numConvos': 0, 'latencySum':0}  # not time
                                elif conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                    botJustSentMessage = False
                                else:
                                    if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                        latency = (time - userReplyTime).total_seconds()
                                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                                        latencySum = latencyDict[hour]['latencySum'] + latency
                                        latencyDict[hour] = {'numConvos':numConvosLat,'latencySum': latencySum}
                                        botJustSentMessage = True

                            #rating over days
                            if rating != 'None':
                                if date in ratingsDict:
                                    if ID in ratingsDict[date]:
                                        pass
                                    else:
                                        ratingsDict[date][ID] = int(rating)
                                else:
                                    ratingsDict[date] = {ID:int(rating)}

                            #check if ASR was used or not
                            if str(conversations[item]['ASR']) == 'False':
                                ASR = 'No'
                            else:
                                ASR = 'Yes'

                            if str(conversations[item]['endSurveyFlag']) == 'False':
                                Survey = 'No'
                            else:
                                Survey = 'Yes'
                            final_conv_log.append([ID,type, message, ASR, rating, comments,Survey, date, min_hour_sec])

                            dataRow = [ID,type, message, rating, date, min_hour_sec]
                            convoLog.write_row(i,0, dataRow)
                            i += 1
                    except Exception as e:
                        logger.error("Generate Report Excel error: ", str(e))

                elif str(document['bot_id']) == str(bot_id):
                    number_conv_counter+=1
                    # found_bot_id = True
                    try:
                        rating = document['Rating']
                        numRated +=1
                        sumScore += int(rating)
                        ratingHistogramDict[rating] += 1
                    except Exception as e:
                        logger.error("Generate report excel error: ", str(e))
                        rating = 'None'
                        ratingHistogramDict['No Rating'] +=1
                    try:
                        comments = document['Comments']
                    except Exception as e:
                        logger.error("Generate report excel error: ", str(e))
                        comments = 'None'

                    #initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                    ID += 1
                    userReplyTime  = datetime.datetime.now()

                    #now iterate through each individual conversation log
                    try:
                        conversations = document['conversations']

                        botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                        for item in conversations:
                            message = conversations[item]['message']
                            type = conversations[item]['type']
                            time = conversations[item]['time'].astimezone(pytz.timezone('US/EASTERN'))

                            #process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            #process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            #process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            #create bar graph information for dates vs #conversations
                            if date in dateDict:
                                dateDict[date].append(ID)
                            else:
                                dateDict[date] = [ID]

                            #create line graph information for # conversations by hour of day
                            if hour in timeDict:
                                timeDict[hour].append(ID)
                            else:
                                timeDict[hour] = [ID]

                            #create line graph info for latency by hour of day
                            if hour in latencyDict:
                                if latencyDict[hour] == []:
                                    # only want to calculate "bot message timestamp" - "user message timestamp"
                                    if conversations[item]['type'] == 'user message':
                                        userReplyTime = time
                                    latencyDict[hour] = {'numConvos': 0, 'latencySum':0}  # not time
                                elif conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                    botJustSentMessage = False
                                else:
                                    if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                        latency = (time - userReplyTime).total_seconds()
                                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                                        latencySum = latencyDict[hour]['latencySum'] + latency
                                        latencyDict[hour] = {'numConvos':numConvosLat,'latencySum': latencySum}
                                        botJustSentMessage = True

                            #rating over days
                            if rating != 'None':
                                if date in ratingsDict:
                                    if ID in ratingsDict[date]:
                                        pass
                                    else:
                                        ratingsDict[date][ID] = int(rating)
                                else:
                                    ratingsDict[date] = {ID:int(rating)}

                            #check if ASR was used or not
                            if str(conversations[item]['ASR']) == 'False':
                                ASR = 'No'
                            else:
                                ASR = 'Yes'

                            if str(conversations[item]['endSurveyFlag']) == 'False':
                                Survey = 'No'
                            else:
                                Survey = 'Yes'

                            dataRow = [ID,type, message, rating, date, min_hour_sec]
                            convoLog.write_row(i,0, dataRow)
                            i += 1
                    except Exception as e:
                        logger.error("Generate Report Excel error: ", str(e))

                else:
                    continue
            except Exception as e:
                logger.error('DOCUMENT CURSOR ERROR - ', e)
    except Exception as e:
        logger.error('Weekly Notifications Failure of  - ', e)
    try:

        #format main dashboard
        title = 'Chatbot Dashboard: ' + datetime.datetime.today().strftime('%m-%d-%Y')
        formatting.set_font_size(20)
        dashboard.merge_range('A1:S2', title, formatting)
        formatting.set_font_size(11)

        #create summary data table
        dashboard.write(3,1,'Summary Data')
        dashboard.merge_range('B4:C4', 'Summary Data', formatting)
        dashboard.write_row(4,1,['Number of Conversations', number_conv_counter])

        try:
            averageScore = sumScore/numRated
        except Exception as e:
            logger.error("Generate report excel error: ", str(e))
            averageScore = 'N/A'
        dashboard.write_row(5, 1, ['Average Satisfaction Rating', averageScore])
        # dashboard.write_row(6, 1, ['Number of Questions Users Rated as Helpful', len(intentFoundList)])
        # dashboard.write_row(7, 1, ['Number of Questions Rated as Unhelpful', len(intentNotFoundList)])
        dashboard.set_column(1, 1, 50)

        # initialize charts
        barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
        lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
        lineGraph_RatingByDay = workbook.add_chart({'type': 'line'})
        lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})
        barGraph_RatingCount = workbook.add_chart({'type': 'column'})

        #initialize lists for data sheet
        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        ratingKeyList = []
        ratingValueList = []
        latencyKeyList = []
        latencyValueList = []
        ratingCountKeyList = []
        ratingCountValueList = []

        #create the necessary lists for populating the charts
        for key, value in sorted(ratingHistogramDict.items()):
            ratingCountKeyList.append(key)
            ratingCountValueList.append(value)

        for key, value in sorted(ratingsDict.items()):
            ratingKeyList.append(key)
            counter = 0
            ratingSum = 0
            for ratingKey, ratingValue in value.items():
                counter += 1
                ratingSum += ratingValue
            ratingValueList.append(ratingSum / counter)



        for key,value in sorted(latencyDict.items()):
            if key == 12:
                key = '12 pm'
            elif key ==24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum']/value['numConvos']
                else:
                    finalLatencyAvg =0
            latencyValueList.append(finalLatencyAvg)

        for key,value in sorted(dateDict.items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)

        for key,value in sorted(timeDict.items()):
            if key == 12:
                key = '12 pm'
            elif key ==24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)


        dataSheet.write_row(0,0,dateKeyList)
        dataSheet.write_row(1,0, dateValuesList)
        dataSheet.write_row(2, 0, timeKeyList)
        dataSheet.write_row(3, 0, timeValueList)
        dataSheet.write_row(4, 0, ratingKeyList)
        dataSheet.write_row(5, 0, ratingValueList)
        dataSheet.write_row(6, 0, latencyKeyList)
        dataSheet.write_row(7, 0, latencyValueList)
        dataSheet.write_row(8, 0, ratingCountKeyList)
        dataSheet.write_row(9, 0, ratingCountValueList)


        # create the charts and format them


        barGraph_ConvByDay.add_series({
            'name':'Number of Conversations by Day',
            'categories':['DataSheet',0,0,0,len(dateKeyList)-1],
            'values': ['DataSheet',1,0,1,len(dateKeyList)-1]
        })

        barGraph_ConvByDay.set_x_axis({'name': 'Days'})
        barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
        barGraph_ConvByDay.set_legend({'none': True})

        lineGraph_ConvByHour.add_series({
            'name': 'Peak Engagement Hours',
            'categories': '=DataSheet!$A$3:$Y$3',
            'values': '=DataSheet!$A$4:$Y$4'
        })
        lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day (EST)'})
        lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
        lineGraph_ConvByHour.set_legend({'none': True})

        lineGraph_RatingByDay.add_series({
            'name': 'User Satisfaction',
            'categories': ['DataSheet', 4, 0, 4, len(ratingKeyList) - 1],
            'values': ['DataSheet', 5, 0, 5, len(ratingValueList) - 1]
        })

        lineGraph_RatingByDay.set_x_axis({'name': 'Days'})
        lineGraph_RatingByDay.set_y_axis({'name': 'Average Satisfaction Rating', 'min': 0, 'max': 5})
        lineGraph_RatingByDay.set_legend({'none': True})


        lineGraph_LatencyByHour.add_series({
            'name': 'Bot Response Latency',
            'categories': ['DataSheet', 6, 0, 6, len(latencyKeyList) - 1],
            'values': ['DataSheet', 7, 0, 7, len(latencyValueList) - 1]
        })

        lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day (EST)'})
        lineGraph_LatencyByHour.set_y_axis({'name': 'Aversage Response Latency (sec)'})
        lineGraph_LatencyByHour.set_legend({'none': True})

        barGraph_RatingCount.add_series({
            'name': 'User Satisfaction Distribution',
            'categories': ['DataSheet', 8, 0, 8, len(ratingCountKeyList) - 1],
            'values': ['DataSheet', 9, 0, 9, len(ratingCountValueList) - 1]
        })

        barGraph_RatingCount.set_x_axis({'name': 'Rating Score'})
        barGraph_RatingCount.set_y_axis({'name': 'Number of Ratings'})
        barGraph_RatingCount.set_legend({'none': True})

        #insert the charts into the dashboard
        dashboard.insert_chart('B11', barGraph_ConvByDay)
        dashboard.insert_chart('F11', lineGraph_ConvByHour)
        dashboard.insert_chart('B27', lineGraph_RatingByDay)
        dashboard.insert_chart('F27', lineGraph_LatencyByHour)
        dashboard.insert_chart('B43', barGraph_RatingCount)


        #Hide the datasheet used to create the charts
        dataSheet.hide()
        workbook.close()
        print('we here?')
        print(fileTitle)
        return fileTitle
    except Exception as e:
        logger.error('Weekly Sheet Generation Failure due to  - ', e)
        return 'Failure due to ' + str(e)
