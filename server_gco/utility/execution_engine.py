from utility.mongo_dao import get_by_id, update
from utility.conversation_logger import ConversationLogger
from fuzzywuzzy import fuzz
from operator import itemgetter
import re
import os
import datetime
import configparser
from flask_babel import gettext
from utility.session_manager import get as get_context, modify as update_context
from utility.logger import logger


"""
Has three main functions: init, execute, resume
The rest are utility functions
@init: initializes the session with an id and starts logging
@execute: executes bot tasks, i.e. displaying a message or buttons
@resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
"""

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))


hybrid_intent_not_found = 'I am not interested in any of these options'
class ExecutionEngine():
    def __init__(self,session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id)

    """
    Function: executes bot actions by giving user prompts (buttons/text/query outputs)
    This will check whether the output should be text, buttons, etc
    @:param self
    @:param task_definition: this is the current state json
    """
    def get_task_definition(self, search_key, search_value, bot_id):
        state_return = 'None'
        state_definition_all = get_by_id('bot', bot_id)['mapping']
        for state in state_definition_all:
            if search_key in state.keys():
                if state[search_key] == search_value:
                    state_return = state
                    break
        return state_return

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json={}):
        guidedRestart = False
        if self.session['restartFlag'] == True and self.session['ES_UsedFlag'] == False:
            guidedRestart = True
        self.conv_log.bot_log(static_text,self.session['ASRFlag'],self.session['endSurveyFlag'],guidedRestart, self.session['user_id'])
        #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        static_text = static_text.replace("__NAME__",self.session['name'])
        disable_response = False
        if "button" in interaction:
            disable_response = True
        if self.session['ASRFlag'] == True:
            static_text1 = re.sub(r'<[^<]+?>',' ',static_text)
            print('STATIC TEXT - ', static_text)
            # tts_text = synthesize_text(static_text1)
            tts_text = ''
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False
        if is_multi_message == True:
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio":tts_text,
                "disable_response":disable_response
            })
            existing_json['is_multi'] = True
            return existing_json
        else:
            json_return = {
                "is_multi":False,
                "message":[{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": disable_response
                }]
            }
            return json_return

    def execute(self, task_definition):
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False
        self.session['restartFlagNextTask'] = False
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            self.session['disable_input'] = False
        session_id = update_context(self.session_id,self.session)

        # check what the information about the current state is.
        # depending on what the interaction type is for the current state is, execute accordingly

        try:
            self.conv_log.update(task_definition)  # To update the current state in status
            #self.session['botName'] = task_definition['bot_name']
            response_json = ''

            task_text = task_definition['Task Text']
            interaction = task_definition['Interaction Type']

            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text)#, dbData)

            # if it is url, then show the hyperlink to the user
            elif interaction == 'url':
                response_json = self.url_execute(task_definition, task_text)#, dbData)

            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text)#, dbData)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                response_json = self.button_execute(task_definition, task_text)#, dbData, interactionObj)

            # if it is a _____ input, proceed to the next task
            elif interaction == 'nextTask':
                response_json = self.next_task_execute(task_definition, task_text)

        except Exception as e:
            logger.error('Execute Method Error - ', str(e))
            response_json = 'Sorry we weren''t able to understand your intent. - EXECUTE Error'

        return response_json

    def resume(self, message, ASR, bot_id):
        message =str(message)
        self.session['message'] = message
        #todo - should I make the returns uniform
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            if message.lower().strip() == 'nein' and self.session['disable_input'] == True:
                self.session['disable_input'] = True
            else:
                self.session['disable_input'] = False
        self.session['ASRFlag'] = ASR
        logger.debug('THE ASR VALUE IS = ', self.session['ASRFlag'], ASR)
        self.session['message'] = message
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.session['ES_UsedFlag'] = False
        update_context(self.session_id,self.session)

        self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'],guidedRestartFlag=False, user_id=self.session['user_id'])
        try:
            keyword_response = self.keyword_handler(message, bot_id)

            if keyword_response != 0:
                return keyword_response

            # next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restart_execute(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagNextTask'] is True:
                return self.restart_next_task_execute(message, self.session_id, self.session, bot_id)
            elif self.session['endSurveyFlag'] is True:
                # if self.session['survey_taken'] is True:
                return self.survey_execute(message)
                # else:
                #     response_first = self.build_response("string", gettext(
                #         'Thank you for your visit. If you have any additional questions please feel free to reach out to us at <a target="_blank" href="mailto:GCOonDemand@pfizer.com">GCOonDemand@pfizer.com</a> '),
                #                                          "text", "",
                #                                          False)
                #     return self.survey_create(1,response_first)
            # next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                return self.fuzzy_execute(message,bot_id)

            # if it was none of the above, then it is a normal message
            else:
                # language and analyzer
                es_analyzer = get_by_id('bot', bot_id)['es_analyzer']

                # first determine what the state is in the conversation, and what states can lead from here
                current_state = get_by_id('status', self.session_id)['task_id']

                state_definition = self.get_task_definition('Task ID',current_state,bot_id)
                print("***", state_definition)
                action_type = state_definition['Action Type']
                next_task_ids = state_definition['Next Task IDs']
                next_possible_states_json = []
                next_possible_states_names = []
                try:
                    for id in next_task_ids:
                        state_option = self.get_task_definition('Task ID', id, bot_id)
                        next_possible_states_json.append(state_option)
                    for state_json in next_possible_states_json:
                        next_possible_states_names.append(state_json['Task Name'])
                except Exception as e:
                    logger.error("Execution engine error: ", str(e))
                    next_possible_states_json = self.get_task_definition('Is Start', 'true', bot_id)
                    next_possible_states_names = next_possible_states_json['Task Name']
                # if the action type is code, take the user's message and apply it to the code

                # if the action type is navigate, we are navigating from one branch to another in the conversation
                if action_type == 'navigate':
                    response = self.navigate_execute(message,next_task_ids, bot_id)
                    return response

        except Exception as e:

            logger.error(str(e)) #logger.info(string)
            return 'Sorry, we weren''t able to understand your intent - RESUME Error'



    #####################################################################################################################
    #####################################################################################################################
    """
    Execute Functions
    """
    #####################################################################################################################
    #####################################################################################################################

    def next_task_execute(self, task_definition, task_text):
        currentState = task_definition['Task Name']
        nextState = task_definition['Next Task IDs'][0]
        message = re.sub(r'<>', currentState, task_text)
        logger.debug('next state - ', nextState, 'bot ID - ', self.session['bot_id'])

        response = self.build_response("string", message, "text", "")


        next_task_def = self.get_task_definition("Task ID", nextState, self.session['bot_id'])
        response_json_2 = self.execute(next_task_def)

        response['is_multi'] = True
        response['message'].append(response_json_2['message'][0])
        return response

    def text_execute(self, task_definition, task_text):
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            response_task_text = self.build_response('string',task_text,'text','')
            # response_task_text = self.survey_create(1, response_task_text)
            response_task_text = self.restart_next_task(response_task_text)
            self.session['restartFlagNextTask'] = True
            # self.session['endSurveyFlag'] == True
            return response_task_text

    def button_execute(self, task_definition, task_text):  # type, static_text, interaction, interaction_elements
        currentState = task_definition['Task Name']
        task_text = re.sub(r'<>', currentState, task_text)

        if task_definition['Interaction Fetch From DB'] != "True":
            interaction_values = task_definition['Interaction Values']
            task_text = self.build_response('list', task_text, 'button_vertical', interaction_values)
            return task_text

    # if interaction is none, check if query database or not
    # just show text and then restart
    def none_execute(self, task_definition, task_text): #, dbData):
        # global results
        static_values = task_definition['Interaction Values']
        if task_definition['Interaction Fetch From DB'].lower() != "true":  # print from flashmessage if interaction = none
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)

            response = self.build_response("string",message,"text","")
            response_json_2 = self.restart(response)
            return response_json_2


    # if interaction is url, check if query database or not
    # then display url and restart
    def url_execute(self, task_definition, task_text):  # , dbData):
        interaction_url = task_definition['Interaction Values'][0]
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)
            response = self.build_response("hyperlink", gettext(message), "url", interaction_url)

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = str(int(float(state_definition['Next Task IDs'][0])))

            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response



    #####################################################################################################################
    #####################################################################################################################
    """
    Resume Functions
    """
    #####################################################################################################################
    #####################################################################################################################

    def keyword_handler(self, message, bot_id):

        help_config = app_config.get('GENERAL', 'help_keywords')
        restart_config = app_config.get('GENERAL', 'restart_keywords')
        goback_config = app_config.get('GENERAL', 'goback_keywords')
        reestablish_connection_config = app_config.get('GENERAL', 'reestablish_connection_keywords')

        help_keywords = help_config
        restart_keywords = restart_config
        goback_keywords = goback_config
        reestablish_connection_keywords = reestablish_connection_config

        if message.lower().strip() in help_keywords:
            help_task = self.get_task_definition("Task Name","help",bot_id)
            help_text = help_task['Task Text']
            update('status', self.session_id, {'has_ended': False})
            response = self.build_response("string", help_text, "text", "")


            next_state = help_task['Next Task IDs']

            if next_state == '' or next_state == [] or next_state == [""]:
                return response

            next_state_def = self.get_task_definition('Task ID', next_state[0], bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

        elif message.lower().strip() in restart_keywords:
            restart = self.get_task_definition("Is Start","true",bot_id)
            update('status', self.session_id, {'has_ended': False})
            return self.execute(restart)
        elif message.lower().strip() in goback_keywords:
            current_state = get_by_id('status', self.session_id)['current_state']
            prior_state = get_by_id('status', self.session_id)['prior_state']
            history = get_by_id('status', self.session_id)['history']
            history = history[:-1] #pop out the last element
            if (len(history) == 0):
                return self.build_response("string", gettext("Sorry, there is nothing to go back to."), "text", "")
            previous_state = history[-1] #get the last element
            history = history[:-1] #pop out again because will be replaced in conversation logger
            update('status', self.session_id, {'current_state': previous_state, 'prior_state': current_state, 'history': history,"date_created": datetime.datetime.now()})
            previous_state_json = self.get_task_definition('Task Name', previous_state, bot_id)
            return self.execute(previous_state_json)

        elif message.lower().strip() in reestablish_connection_keywords:
            # fetching current state
            current_state = get_by_id('status', self.session_id)['current_state']
            has_ended = get_by_id('status', self.session_id)['has_ended']
            # fetching json for current state
            current_task = self.get_task_definition('Task Name', current_state, bot_id)

            if self.session['surveyFlagNum'] is True:
                response_first = self.build_response("string", gettext(
                    'Thank you for your visit. If you have any additional questions please feel free to reach out to us at <a target="_blank" href="mailto:GCOonDemand@pfizer.com">GCOonDemand@pfizer.com</a> '),
                                                     "text", "",
                                                     False)
                return self.survey_create(1,{"message":[]})
            elif has_ended is False:
                response = self.execute(current_task)
                return response
        else:
            return 0


    def survey_execute(self, message):
        if self.session['surveyFlagNum'] is True:
            if message in '12345' or message.lower().strip() == 'reestablish_connection':
                # log survey
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.build_response("string", gettext('Thank you for your feedback. For more information, reach out to us <a target="_blank" href="mailto:dl-gcoondemand@pfizer.com">here</a>'), "text", '')            # response = self.restart_next_task(response)
                return response
            else:
                response = self.build_response("string",gettext('Please choose an option 1-5.'),"text","")
                response_json_2 = self.survey_create(1,response)
                return response_json_2

    def survey_create(self,state,json):

        self.session['endSurveyFlag'] = True
        update_context(self.session_id, self.session)
        if state ==1:
            self.session['surveyFlagNum'] = True
            update_context(self.session_id, self.session)
            options = [1,2,3,4,5]
            response = self.build_response("list",gettext('Did you find this answer helpful? On a Scale from 1 – 5, 5 being outstanding and 1 being not helpful at all, how helpful was this answer?'),"button_horizontal",options,True,json)
            return response

    # use Fuzzywuzzy to check for fuzzysearch options for Navigate options
    # if the match is > 45% similar, return as option for user to pick from
    def fuzzy_search(self, nextStates, message):
        options = []
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]
        if ratio > int(os.environ.get('FUZZY_RATIO') or '45'):
            # check if task is a navigate task, if yes, check message value, see if pass
            response = self.build_response("list",gettext('We couldn\'t find that. Do you mean: ') + suggestionOutput,"button_vertical",[gettext("Yes"), gettext("No")])
            self.session['fuzzyFlag'] = True
            update_context(self.session_id, self.session)
            return response
        else:
            response = self.build_response("string",gettext('Sorry we don\'t have an answer for that. Could you try again?'),"text","")
            return response

    # If FuzzyFlag is True, then execute this
    # check if the fuzzysearch came from a query or code
    # if not, then check if the suggestion picked comes from a next state
    # if not what they are looking for, return to start
    def fuzzy_execute(self, message, bot_id):
        if str(message).lower() == gettext('yes'):
            self.session['fuzzyFlag'] = False
            get_next_state = self.get_task_definition('Task Name',self.session['suggestion'],bot_id)
            response = self.execute(get_next_state)
            update_context(self.session_id,self.session)
            return response
            pass
        elif message.lower() == gettext('no'):
            self.session['fuzzyFlag'] = False
            update_context(self.session_id,self.session)
            next_state = self.get_task_definition('Is Start','true',bot_id) #self.get_task_definition("Is Start","true", bot_id)
            response = self.execute(next_state)
            return response
        else:
            response = self.build_response("list",gettext('Please choose yes or no.'),"button_horizontal",[gettext("Yes"), gettext("No")])
            return response


    def navigate_execute(self, message, next_possible_states_names, bot_id):
        found = False
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            get_next_state = self.get_task_definition('Task ID', state, bot_id)
            next_state = get_next_state['Task Name']
            if message.strip().lower() == next_state.strip().lower():
                # get_next_state = self.get_task_definition('Task Name', state, bot_id)
                # print('NAVIGATE NEXT STATE - ', get_next_state)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True

                    response = self.execute(get_next_state)
                    return response

        if found == False:
            # do Fuzzy search here
            response = self.fuzzy_search(next_possible_states_names, message)
            return response
            pass

    def restart_execute(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            get_start = self.get_task_definition("Is Start","true", bot_id)
            start_response = self.execute(get_start)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon!'), "text", "", False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'), "button_horizontal",[gettext("Yes"), gettext("No")])
            return response

    def restart_next_task_execute(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            current_state = get_by_id('status', self.session_id)['current_state']

            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = str(int(float(state_definition['Next Task IDs'][0])))
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            start_response = self.execute(next_state_def)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thank you for your visit. If you have any additional questions please feel free to reach out to us at <a target="_blank" href="mailto:GCOonDemand@pfizer.com">GCOonDemand@pfizer.com</a> '), "text", "",
                                                 False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def restart(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"),"button_horizontal",[gettext("Yes"),gettext("No")], True, json)
            self.session['restartFlag'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Error: ",str(e))

    def restart_next_task(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlagNextTask'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Next Task Error: ",str(e))







