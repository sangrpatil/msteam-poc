from utility.mongo_dao import insert,update, get_by_id
import datetime
from utility.session_manager import get as get_context, modify as update_context
from flask_babel import gettext
import pytz
import bson
from utility.logger import logger
"""
Function: Log conversation from the bot and the user
user_log logs user messages
bot_log logs bot messages
update is used by both user_log and bot_log to update MongoDB
"""
class ConversationLogger():

    def __init__(self,session_id, bot_id):
        self.session_id = session_id
        #from main import globalBotName
        try:
            if not get_by_id('status',session_id):
                insert('status',{"_id":bson.ObjectId(self.session_id),"date_created":pytz.utc.localize(datetime.datetime.utcnow()),"has_ended":False, "bot_id":bot_id})
                session = get_context(session_id)
                session['session_id'] = session_id
                session['session_exist'] = True
                update_context(session_id,session)
        except Exception as e:
            logger.error("Error during convo log Init:", str(e))

    def update(self, task_definition):
        state = task_definition['Task Name']
        task_id = task_definition['Task ID']
        try:
            logger.debug('CURRENT SESSION - ', get_by_id('status',self.session_id)['history'], type(get_by_id('status',self.session_id)['history']))
            current_state = get_by_id('status',self.session_id)['current_state']
            history = get_by_id('status',self.session_id)['history']
            logger.debug('STATE - ', state, ' HISTORY - ', history)
            history.append(state)
            update('status', self.session_id, {'current_state': state,
                                               'prior_state': current_state,
                                               'task_id': task_id,
                                               'history':history,
                                               "date_created": pytz.utc.localize(datetime.datetime.utcnow())})
        except Exception as e:
            logger.exception('UPDATE EXCEPTION - ', e)
            history_list = []
            history_list.append(state)
            update('status', self.session_id, {'current_state': state,
                                               'task_id': task_id,
                                               'history':history_list,
                                               "date_created": pytz.utc.localize(datetime.datetime.utcnow())})


    def user_log(self, user_msg, ASRFlag, endSurveyFlag,guidedRestartFlag, user_id=None):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['conversations'])
            current_index = current_count
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index =  "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())#.strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            user_id_row = "conversations." + str(current_index) + "." + "user_id"
            error = False
            update('status', self.session_id, {user_msg_index: user_msg,
                                               type_message_index: 'user message',
                                               time_index: time,
                                               fuzzy_index: error,
                                               asr_index: ASRFlag,
                                               end_survey_index: endSurveyFlag,
                                               restartGuidedIndex: guidedRestartFlag,
                                               user_id_row: user_id
                                               })

            if str(endSurveyFlag) == 'True':
                print(type(user_msg), user_msg)
                if str(user_msg) in '12345':
                    update('status', self.session_id, {'Rating': user_msg})
        except Exception as e:
            logger.exception('Exception: ',str(e))
            current_index = 0
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index = "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            user_id_row = "conversations." + str(current_index) + "." + "user_id"
            error = False
            update('status', self.session_id, {user_msg_index: user_msg,
                                               type_message_index: 'user message',
                                               time_index: time,
                                               fuzzy_index: error,
                                               asr_index: ASRFlag,
                                               end_survey_index: endSurveyFlag,
                                               restartGuidedIndex: guidedRestartFlag,
                                               'Rating': "None",
                                               'Comments': "None",
                                               user_id_row: user_id
                                               })


    def bot_log(self, bot_msg, ASRFlag, endSurveyFlag,guidedRestartFlag, user_id=None):
        current_doc = get_by_id('status', self.session_id)
        try:
            current_count = len(current_doc['conversations'])
            current_index = current_count
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index = "conversations." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            user_id_row = "conversations." + str(current_index) + "." + "user_id"
            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext("Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error=True
            else:
                error=False
            update('status', self.session_id, {str(bot_msg_index): bot_msg,
                                               str(type_message_index): 'bot message',
                                               time_index: time,
                                               fuzzy_index: error,
                                               asr_index: ASRFlag,
                                               end_survey_index: endSurveyFlag,
                                               restartGuidedIndex: guidedRestartFlag,
                                               user_id_row: user_id
                                               })

        except Exception as e:
            logger.exception('Exception: ',str(e))
            current_index = 0
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index =  "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            user_id_row = "conversations." + str(current_index) + "." + "user_id"
            time = pytz.utc.localize(datetime.datetime.utcnow())
            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext("Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error=True
            else:
                error=False
            update('status', self.session_id, {str(bot_msg_index): bot_msg,
                                               str(type_message_index): 'bot message',
                                               time_index: time,
                                               fuzzy_index: error,
                                               asr_index: ASRFlag,
                                               end_survey_index: endSurveyFlag,
                                               restartGuidedIndex: guidedRestartFlag,
                                               user_id_row: user_id})
