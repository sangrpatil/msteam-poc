import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import os
import logging
#logger = logging.getLogger()
def send_email(report_filename, emails):
    print("Inside send email")
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'gcoondemand@pfizer.com'

    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    file_uri = os.path.join(directory_path, 'reports', report_filename)

    try:
        print("Forming email")
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "GCOoD Bot Report for " + str(today_date)
        body = "Hello " + ",\n\nThank you for using GCO chatbot. Please find attached the Report for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)
    
        part = MIMEBase('application', "octet-stream")
        print("Opening file", file_uri)
        part.set_payload(open(file_uri, "rb").read())
        print("Closing file", file_uri)
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)
        print("succes in forming mail")

    except Exception as e:
      #  logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.".format(e))
        print("failure in forming mail")
        return ('Failure')

    try:
        print("sending email")
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        print("Success in sending mail")
        return ('Success')
    except Exception as e:
       # logger.error("ERROR: Unexpected error: Could not send email-{}.".format(e))
        print("failure in sending mail")
        return ('Failure')

#send_email('GCOoD Bot_Chatbot_Report_Weekly_10-17-2019.xlsx', ['rajat.shah@pfizer.com'])
