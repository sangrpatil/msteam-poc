# Script to create admin user
# Should be integrated to main
import os
from utility.mongo_authentication import MongoAuthentication

if __name__ == "__main__":
    auth = MongoAuthentication()
    username = os.environ.get('ADMIN_USERNAME') or 'admin'
    password = os.environ.get('ADMIN_PASSWORD') or 'pass'
    status, message = auth.register(username,password)
    print(message)
