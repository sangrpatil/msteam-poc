from flask import Flask, jsonify, Markup, session, Response, request
import requests, json

app = Flask(__name__)


@app.route("/api/message", methods=["POST", "GET"])
def message():
    try:
        res = request.json
        print(res)
        # response_text = {
        #     "type": "message",
        #     "from": {
        #         "id": res["recipient"]["id"],
        #         "name": res["recipient"]["name"]
        #     },
        #     "conversation": {
        #         "id": res["conversation"]["id"],
        #         # "name": "conversation's name"
        #     },
        #     "recipient": {
        #         "id": res["from"]["id"],
        #         "name": res["from"]["name"]
        #     },
        #     "text": "Hello <a href='https://www.google.com'> Link to Google</a>",
        #     "replyToId": res["id"]
        # }

        response_text = {
            "type": "message",
            "from": {
                "id": res["recipient"]["id"],
                "name": res["recipient"]["name"]
            },
            "conversation": {
                "id": res["conversation"]["id"],
                # "name": "conversation's name"
            },
            "recipient": {
                "id": res["from"]["id"],
                # "name": res["from"]["name"]
            },
            # "text": "Hello, Welcome to Test Bot!!",
            "replyToId": res["id"],
            "attachments": [
                {
                    "contentType": "application/vnd.microsoft.card.hero",
                    "content": {
                        "text": "Hero card for buttons",
                        "buttons": [
                            {
                                "type": "messageBack",
                                "title": "First button",
                                "value": {"button_click": "first_button"}
                            },
                            {
                                "type": "messageBack",
                                "title": "Second button",
                                "value": {"button_click": "second_button"}
                            }
                        ]
                    }
                }
            ]
        }

        # get the token for authorization
        url_token = "https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token"

        url_token_header = {
                "Content-Type": "application/x-www-form-urlencoded"
        }

        body = {
            "grant_type" :"client_credentials",
            "client_id":"3b1e063d-7506-4b22-9a4b-c62b1dfc52c5",
            "client_secret":"MqpTejpO4G/ndJwH-b@l9kS:zu90.jOG",
            "scope":"https://api.botframework.com/.default"
        }

        response_token = requests.post(url_token, data=body, headers=url_token_header).json()

        # sending response to the bot
        response_header = {
            "Authorization": response_token["token_type"] + " " + response_token["access_token"],
            "Content-Type": "application/json"
        }
        url = res["serviceUrl"] + "v3/conversations/" + res["conversation"]["id"] + "/activities/" + res["id"]
        response = requests.post(url, data=json.dumps(response_text), headers=response_header)
        return Response("200", response.text)

    except Exception as e:
        print(e)


if __name__ == "__main__":
    app.run(host="localhost", port=int(5050))
    pass
