# Spanish translations for PROJECT.
# Copyright (C) 2018 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2018-05-22 17:36-0500\n"
"PO-Revision-Date: 2018-05-22 17:39-0500\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: es\n"
"Language-Team: es <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.3\n"

#: templates/chat.html:37
msgid "Message"
msgstr ""

#: templates/chat.html:45
msgid "Hit enter to send message"
msgstr ""

#: utility/conversation_logger.py:74 utility/execution_engine_backup.py:283
#: utility/execution_engine_backup.py:318
#: utility/execution_engine_backup.py:347
msgid "yes"
msgstr ""

#: utility/conversation_logger.py:74 utility/execution_engine_backup.py:290
#: utility/execution_engine_backup.py:321
#: utility/execution_engine_backup.py:377
msgid "no"
msgstr ""

#: utility/conversation_logger.py:107 utility/conversation_logger.py:127
#: utility/execution_engine_backup.py:224
#: utility/execution_engine_backup.py:225
msgid "We couldn't find that. Do you mean: "
msgstr ""

#: utility/conversation_logger.py:107 utility/conversation_logger.py:127
#: utility/execution_engine_backup.py:228
#: utility/execution_engine_backup.py:229
msgid "Sorry we don't have an answer for that. Could you try again?"
msgstr ""

#: utility/execution_engine_backup.py:223
#: utility/execution_engine_backup.py:225
#: utility/execution_engine_backup.py:236
#: utility/execution_engine_backup.py:238
#: utility/execution_engine_backup.py:270
#: utility/execution_engine_backup.py:272
#: utility/execution_engine_backup.py:299
#: utility/execution_engine_backup.py:301
#: utility/execution_engine_backup.py:390
#: utility/execution_engine_backup.py:392
msgid "Yes"
msgstr ""

#: utility/execution_engine_backup.py:223
#: utility/execution_engine_backup.py:225
#: utility/execution_engine_backup.py:236
#: utility/execution_engine_backup.py:238
#: utility/execution_engine_backup.py:270
#: utility/execution_engine_backup.py:272
#: utility/execution_engine_backup.py:299
#: utility/execution_engine_backup.py:301
#: utility/execution_engine_backup.py:390
#: utility/execution_engine_backup.py:392
msgid "No"
msgstr ""

#: utility/execution_engine_backup.py:237
#: utility/execution_engine_backup.py:238
msgid "Would you like to continue?"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:24
msgid "hello"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:24
msgid "hi"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:24
msgid "whats up"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:24
msgid "good afternoon"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:25
msgid "good morning"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:25
msgid "good day"
msgstr ""

#: utility/execution_engine_backup.py:243 utility/resume_utilities.py:25
msgid "good evening"
msgstr ""

#: utility/execution_engine_backup.py:244 utility/resume_utilities.py:26
msgid "I would like to ask a question"
msgstr ""

#: utility/execution_engine_backup.py:244 utility/resume_utilities.py:26
msgid "I want to ask you a question"
msgstr ""

#: utility/execution_engine_backup.py:244 utility/resume_utilities.py:27
msgid "I have a doubt"
msgstr ""

#: utility/execution_engine_backup.py:244 utility/resume_utilities.py:27
msgid "I want to ask you something"
msgstr ""

#: utility/execution_engine_backup.py:244 utility/resume_utilities.py:28
msgid "I want to ask you questions"
msgstr ""

#: utility/execution_engine_backup.py:244 utility/resume_utilities.py:28
msgid "I want to consult you"
msgstr ""

#: utility/execution_engine_backup.py:245 utility/resume_utilities.py:29
msgid "how are you"
msgstr ""

#: utility/execution_engine_backup.py:245 utility/resume_utilities.py:29
msgid "how are you doing"
msgstr ""

#: utility/execution_engine_backup.py:245 utility/resume_utilities.py:29
msgid "how is your day"
msgstr ""

#: utility/execution_engine_backup.py:246 utility/resume_utilities.py:30
msgid "Hi there! How can I help you"
msgstr ""

#: utility/execution_engine_backup.py:247 utility/resume_utilities.py:31
msgid "With pleasure, tell me how I can help you."
msgstr ""

#: utility/execution_engine_backup.py:248 utility/resume_utilities.py:32
msgid "Im doing well! How can I help you today?"
msgstr ""

#: utility/execution_engine_backup.py:248 utility/resume_utilities.py:33
msgid "Im feeling great! How can I help you today?"
msgstr ""

#: utility/execution_engine_backup.py:266
#: utility/execution_engine_backup.py:267
msgid "On a scale of 1-5, how satisfied were you with your service today?"
msgstr ""

#: utility/execution_engine_backup.py:271
#: utility/execution_engine_backup.py:272
msgid "Would you like to leave any comments?"
msgstr ""

#: utility/execution_engine_backup.py:293
#: utility/execution_engine_backup.py:294
msgid "Thanks for using the Pfizer Bot!"
msgstr ""

#: utility/execution_engine_backup.py:300
#: utility/execution_engine_backup.py:301
msgid "Would you like to continue? Please choose yes or no."
msgstr ""

#: utility/execution_engine_backup.py:314
#: utility/execution_engine_backup.py:315
msgid "Please choose an option 1-5."
msgstr ""

#: utility/execution_engine_backup.py:319
#: utility/execution_engine_backup.py:320
msgid "Please leave your comment below:"
msgstr ""

#: utility/execution_engine_backup.py:325
#: utility/execution_engine_backup.py:326
#: utility/execution_engine_backup.py:332
#: utility/execution_engine_backup.py:333
msgid "Thank you for your participation!"
msgstr ""

#: utility/execution_engine_backup.py:380
#: utility/execution_engine_backup.py:381
msgid "Sorry about that. Lets try again from the start."
msgstr ""

#: utility/execution_engine_backup.py:391
#: utility/execution_engine_backup.py:392
msgid "Please choose yes or no."
msgstr ""

#: utility/execution_engine_backup.py:402
#: utility/execution_engine_backup.py:403
msgid "Please choose one of the options"
msgstr ""

#: utility/execution_engine_backup.py:491
#: utility/execution_engine_backup.py:494
msgid "Are you interested in any of these options?"
msgstr ""

#: utility/execution_engine_backup.py:498
#: utility/execution_engine_backup.py:499
msgid "Sorry, we may not have information for that. Can you try again?"
msgstr ""

#: utility/execution_engine_backup.py:569
#: utility/execution_engine_backup.py:599
#: utility/execution_engine_backup.py:618
msgid ""
"Sorry, we currently do not have access to this data. Please try again "
"later!"
msgstr ""

#: utility/execution_engine_backup.py:586
#: utility/execution_engine_backup.py:587
msgid "Click here to go to the link"
msgstr ""

#: utility/execution_engine_backup.py:662
msgid "Which category are you interested in?"
msgstr ""

#: utility/execution_engine_backup.py:670
msgid "Which category would you like to filter on?"
msgstr ""

#: utility/execution_engine_backup.py:677
msgid "Which of these do you want to know about?"
msgstr ""

#: utility/execution_engine_backup.py:682
msgid "Here is the information regarding "
msgstr ""

#: utility/execution_engine_backup.py:693
#: utility/execution_engine_backup.py:710
msgid ""
"Sorry we currently do not have access to this data. Please try again "
"later!"
msgstr ""

