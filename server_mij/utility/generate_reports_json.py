import configparser
import datetime
import re

from utility.logger import logger
from utility.mongo_dao import get_collection, get_by_id

app_config = configparser.ConfigParser()


# app_config.read_file(open(r'../config/app_settings.ini'))


def generateReports(bot_id=None):  # todo pass bot ID
    try:
        cursor = get_collection('status')  # todo - read for only particular bot
        # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
        i = 1
        ID = 0
        dateDict = {}
        timeDict = {}
        latencyDict = {}
        final_conv_log = []

        found_bot_id = False
        bot_name = ''
        # create hour dict for conversations over time of day and latency
        for k in range(0, 24):
            timeDict[k] = []
            latencyDict[k] = []

        for document in cursor:
            # determine the converation's rating and comments, if any

            if len(document["conversations"]) == 1:
                continue

            if bot_id == None:
                bot_name = 'None'
                found_bot_id = True

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        final_conv_log.append({"CONVERSATION ID": ID,
                                               "FROM": type,
                                               "MESSAGE": message,
                                               "DATE": date,
                                               "TIME (JST)": min_hour_sec
                                               })

                except Exception as e:
                    logger.error("Generate Repor Json error: ", str(e))

            elif str(document['bot_id']) == str(bot_id):
                bot_name = get_by_id('bot', bot_id)['bot name']

                found_bot_id = True

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        final_conv_log.append({"CONVERSATION ID": ID,
                                               "FROM": type,
                                               "MESSAGE": message,
                                               "DATE": date,
                                               "TIME (JST)": min_hour_sec
                                               })

                except Exception as e:
                    logger.error("Generate Report Json error: ", str(e))

            else:
                continue

        """
        Create sheets
        """

        try:
            if found_bot_id == False:
                return 'Failure'
        except Exception as e:
            logger.error("Generate report json error: ", str(e))
            pass

        # initialize lists for data sheet
        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        latencyKeyList = []
        latencyValueList = []

        for key, value in sorted(latencyDict.items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum'] / value['numConvos']
                else:
                    finalLatencyAvg = 0
            latencyValueList.append(finalLatencyAvg)

        for key, value in sorted(dateDict.items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)

        for key, value in sorted(timeDict.items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)

        final_json = {
            "bot_name": bot_name,
            "chat_log": final_conv_log,
            "graph_data": [
                {
                    "graph_name": "Number of Conversations By Day",
                    "x_values": dateKeyList,
                    "y_values": dateValuesList,
                    "x_axis_title": "Days",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "Peak Engagement Hours",
                    "x_values": timeKeyList,
                    "y_values": timeValueList,
                    "x_axis_title": "Time of Day (JST)",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "Bot Response Latency",
                    "x_values": latencyKeyList,
                    "y_values": latencyValueList,
                    "x_axis_title": "Time of Day (JST)",
                    "y_axis_title": "Average Response Latency (sec)"
                }
            ]
        }

        return final_json
    except Exception as e:
        logger.error("Generate Report Json error: ", str(e))
