import datetime
import os
import time
from abc import ABCMeta

from pymongo import MongoClient
from werkzeug.security import generate_password_hash, check_password_hash

from utility.logger import logger


############Each function will return a code and a message
# 0 for failure
# 1 for success


class MongoAuthentication:
    __metaclass__ = ABCMeta

    def __init__(self):

        host = os.environ.get('MONGO_HOST') or 'localhost'
        port = os.environ.get('MONGO_PORT') or '27017'
        username = os.environ.get('MONGO_USERNAME') or 'admin'
        password = os.environ.get('MONGO_PASSWORD') or 'admin'
        self.database = os.environ.get('MONGO_DB') or 'admin'
        self.collection = 'users'
        self.client = MongoClient('mongodb://' + username + ':' + password + '@' + host + ':' + str(port) + '/admin')
        self.type = "MongoDB Authentication"

    def register(self, username, password):
        try:
            username_in_db = self.client[self.database][self.collection].find({'username': username})
            if username_in_db.count() > 0:
                return 1, 'An account already exists for this username'
            else:
                now = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                new_user = {"username": username, "password": generate_password_hash(password), "created_on": now}
                self.client[self.database][self.collection].insert(new_user)
                return 0, 'User created successfully'
        except Exception as e:
            logger.error("Mongo Authentication register error: ", str(e))

    def login(self, username, password):
        try:
            user_in_db = self.client[self.database][self.collection].find({'username': username})
            if user_in_db.count() > 0:
                user_in_db_password = user_in_db[0]['password']
                if check_password_hash(user_in_db_password, password):
                    return 0, 'Logged in successfully'
                else:
                    return 1, 'Incorrect password'
            else:
                return 1, 'Username not found'
        except Exception as e:
            logger.error("Mongo Authentication login error: ", str(e))
