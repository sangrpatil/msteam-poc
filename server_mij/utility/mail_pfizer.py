import datetime
import logging
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logger = logging.getLogger()


def send_email(report_filename, emails):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'mij@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "MIJ Bot Report for " + str(today_date)
        body = "Hello " + ",\n\nThank you for using MIJ. Please find attached the Report for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)


    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.".format(e))
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.".format(e))
        return ('Failure')
