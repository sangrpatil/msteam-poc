import configparser
import datetime
import os
import random
import re
from operator import itemgetter

import numpy as np
from flask_babel import gettext
from fuzzywuzzy import fuzz

from utility.TTS import synthesize_text
from utility.conversation_logger import ConversationLogger
from utility.elasticsearch_query import SearchES
from utility.gcp_places import retrieve_location
from utility.logger import logger
from utility.mongo_dao import get_by_id, update, get_all_prod
from utility.session_manager import get as get_context, modify as update_context

"""
Has three main functions: init, execute, resume
The rest are utility functions
@init: initializes the session with an id and starts logging
@execute: executes bot tasks, i.e. displaying a message or buttons
@resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
"""

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))
bot_timeout_states = r"同意する"

hybrid_intent_not_found = 'I am not interested in any of these options'


class ExecutionEngine():
    def __init__(self, session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id)

    """
    Function: executes bot actions by giving user prompts (buttons/text/query outputs)
    This will check whether the output should be text, buttons, etc
    @:param self
    @:param task_definition: this is the current state json
    """

    def get_task_definition(self, search_key, search_value, bot_id):
        state_return = 'None'
        state_definition_all = get_by_id('bot', bot_id)['mapping']
        for state in state_definition_all:
            if search_key in state.keys():
                if state[search_key] == search_value:
                    state_return = state
                    break
        return state_return

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False,
                       existing_json={}):
        guidedRestart = False
        if self.session['restartFlag'] == True and self.session['ES_UsedFlag'] == False:
            guidedRestart = True
        self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], guidedRestart, self.session['user_id'])
        # sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        static_text = static_text.replace("__NAME__", self.session['name'])
        disable_response = False
        if "button" in interaction:
            disable_response = True
        if self.session['ASRFlag'] == True:
            static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
            print('STATIC TEXT - ', static_text)
            tts_text = synthesize_text(static_text1)
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False
        if is_multi_message == True:
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio": tts_text,
                "disable_response": disable_response
            })
            existing_json['is_multi'] = True
            return existing_json
        else:
            json_return = {
                "is_multi": False,
                "message": [{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": disable_response
                }]
            }
            return json_return

    def execute(self, task_definition):
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            self.session['disable_input'] = False
        session_id = update_context(self.session_id, self.session)

        # check what the information about the current state is.
        # depending on what the interaction type is for the current state is, execute accordingly

        try:
            self.conv_log.update(task_definition['Task Name'])  # To update the current state in status
            # self.session['botName'] = task_definition['bot_name']
            response_json = ''

            task_text = task_definition['Task Text']
            interaction = task_definition['Interaction Type']

            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text)  # , dbData)

            # if it is url, then show the hyperlink to the user
            elif interaction == 'url':
                response_json = self.url_execute(task_definition, task_text)  # , dbData)

            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text)  # , dbData)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                response_json = self.button_execute(task_definition, task_text)  # , dbData, interactionObj)

            # if it is a zipcode input, give the user an input to type in a zipcode
            elif interaction == 'zipcode':
                response_json = self.zip_execute(task_definition, task_text)

            # if it is a _____ input, proceed to the next task
            elif interaction == 'nextTask':
                response_json = self.next_task_execute(task_definition, task_text)

            elif interaction in ['productStatusHandler', 'priceHandler', 'customerHandler',
                                 'productInformationHandler']:
                message = self.session['message']
                response_json = self.custom_sheet_execute(message, interaction, task_definition)

            # if interaction is a closeBot, close the chat window
            elif interaction == 'closeBot':
                response_json = self.closeBot_execute(task_definition, task_text)  # , dbData)
        except Exception as e:
            logger.error('Execute Method Error - ', str(e))
            response_json = 'Sorry we weren''t able to understand your intent. - EXECUTE Error'

        return response_json

    def resume(self, message, ASR, bot_id):
        self.session['message'] = message
        # todo - should I make the returns uniform
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            if message.lower().strip() == 'nein' and self.session['disable_input'] == True:
                self.session['disable_input'] = True
            else:
                self.session['disable_input'] = False
        self.session['ASRFlag'] = ASR
        logger.debug('THE ASR VALUE IS = ', self.session['ASRFlag'], ASR)
        self.session['message'] = message
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.session['ES_UsedFlag'] = False

        # check if disclaimer is accepted and message has value defined in bot_timeout_return_state
        bot_timeout_return_state = app_config.get('GENERAL', 'bot_timeout_return_state')
        if (not self.session['disclaimer_accept']) and (
                message in bot_timeout_return_state or message in bot_timeout_states):
            self.session['disclaimer_accept'] = True
        update_context(self.session_id, self.session)

        self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'], guidedRestartFlag=False, user_id=self.session['user_id'])
        try:
            # first check if the message is small talk
            small_talk_response = self.small_talk_module(message)
            keyword_response = self.keyword_handler(message, bot_id)
            if small_talk_response != 0:
                return small_talk_response

            elif keyword_response != 0:
                return keyword_response
            # next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restart_execute(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagFAQ'] is True:
                return self.restart_execute_FAQ(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagNextTask'] is True:
                return self.restart_next_task_execute(message, self.session_id, self.session, bot_id)
            elif self.session['endSurveyFlag'] is True:
                return self.survey_execute(message)
            # next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                return self.fuzzy_execute(message, bot_id)
            # next check if we're expecting a response from a hybrid guided
            elif self.session['guidedFlag'] is True:
                return self.guided_flag_execute(message, bot_id)
            # if it was none of the above, then it is a normal message
            else:
                # language and analyzer
                es_analyzer = get_by_id('bot', bot_id)['es_analyzer']

                # first determine what the state is in the conversation, and what states can lead from here
                current_state = get_by_id('status', self.session_id)['current_state']

                state_definition = self.get_task_definition('Task Name', current_state, bot_id)
                if current_state in ['Accept', 'Main Navigation - Repeat', '同意する', '同意しない<6003>', 'Decline<6003>']:
                    task_data = self.get_task_definition('Task Name', 'Accept', bot_id)
                    if task_data == 'None':
                        task_data = self.get_task_definition('Task Name', '同意する', bot_id)
                    accept_data = task_data['Product Store Data'][task_data['Interaction Values'].index(message)]
                    self.session['collection_info'] = accept_data.split("|")
                    update_context(self.session_id, self.session)

                action_type = state_definition['Action Type']
                next_task_ids = state_definition['Next Task IDs']
                next_possible_states_json = []
                next_possible_states_names = []

                try:
                    for id in next_task_ids:
                        state_option = self.get_task_definition('Task ID', str(int(float(id))), bot_id)
                        next_possible_states_json.append(state_option)

                    for state_json in next_possible_states_json:
                        next_possible_states_names.append(state_json['Task Name'])
                except Exception as e:
                    logger.error("Execution engine error: ", str(e))
                    next_possible_states_json = self.get_task_definition('Is Start', 'true', bot_id)
                    next_possible_states_names = next_possible_states_json['Task Name']
                # if the action type is code, take the user's message and apply it to the code
                if action_type == 'code':  # todo
                    self.codeExecute(message, state_definition)
                # if the action if FAQ, then take the user's message and query elasticsearch with it
                elif action_type == 'FAQ':
                    response = self.faq_execute(message, bot_id, self.session['ASRFlag'], es_analyzer)
                    return response
                # if the action is hybrid guided, take the user's message and check the hybrid guided process
                elif action_type == 'hybridGuided':
                    response = self.hybrid_guided_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action is query, query the user's message against a database using DAO.py
                elif action_type == 'query':  # todo
                    self.queryExecute(message, current_state)
                # if the action type is navigate, we are navigating from one branch to another in the conversation
                elif action_type == 'navigate':  # and character_execute==False:
                    response = self.navigate_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action type is location, we are taking in the user's zip code input
                elif action_type == 'navigate_character':  # or character_execute==True:
                    response = self.navigate_character_execute(message,
                                                               self.get_task_definition('Task Name', current_state,
                                                                                        self.session['bot_id']))
                    return response
                # if the action type is location, we are taking in the user's zip code input
                elif action_type == 'location':
                    response = self.location_execute(message)
                    return response
                elif action_type == 'input':
                    response = self.input_execute(message, next_possible_states_names)
                    return response

        except Exception as e:

            logger.error(str(e))  # logger.info(string)
            return 'Sorry, we weren''t able to understand your intent - RESUME Error'

    #####################################################################################################################
    #####################################################################################################################
    """
    Execute Functions
    """

    #####################################################################################################################
    #####################################################################################################################

    def next_task_execute(self, task_definition, task_text):
        currentState = task_definition['Task Name']
        nextState = task_definition['Next Task IDs'][0]
        message = re.sub(r'<>', currentState, task_text)
        logger.debug('next state - ', nextState, 'bot ID - ', self.session['bot_id'])

        response = self.build_response("string", message, "text", "")

        next_task_def = self.get_task_definition("Task ID", nextState, self.session['bot_id'])
        response_json_2 = self.execute(next_task_def)

        response['is_multi'] = True
        response['message'].append(response_json_2['message'][0])
        return response

    def text_execute(self, task_definition, task_text):
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            response_task_text = self.build_response('string', task_text, 'text', '')
            return response_task_text

    def button_execute(self, task_definition, task_text):  # type, static_text, interaction, interaction_elements
        currentState = task_definition['Task Name']
        task_text = re.sub(r'<>', currentState, task_text)

        if task_definition['Interaction Fetch From DB'] != "True":
            interaction_values = task_definition['Interaction Values']
            task_text = self.build_response('list', task_text, 'button_vertical', interaction_values)
            return task_text

    # if interaction is none, check if query database or not
    # just show text and then restart
    def none_execute(self, task_definition, task_text):  # , dbData):
        # global results
        static_values = task_definition['Interaction Values']
        if task_definition[
            'Interaction Fetch From DB'].lower() != "true":  # print from flashmessage if interaction = none
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)

            response = self.build_response("string", message, "text", "")
            response_json_2 = self.restart(response)
            return response_json_2

    # if interaction is url, check if query database or not
    # then display url and restart
    def url_execute(self, task_definition, task_text):  # , dbData):
        interaction_urls = task_definition['Interaction Values']
        response_urls = ""

        # check interaction url pattern and form the url response
        for url in interaction_urls:
            if "|" in url:
                url_text, url_hyperlink = url.split("|")
                url_text, url_hyperlink = url_text.strip(), url_hyperlink.strip()
            else:
                url_text, url_hyperlink = gettext("Click Here"), url.strip()
            url_template = "<br/><a href=\"{0}\" target=\"_blank\" class=\"anchortag\">{1}</a>".format(url_hyperlink,
                                                                                                       url_text)
            response_urls += url_template

        # check for <URL> in task text and replace it with response url
        if "<URL>" in task_text:
            response_text = re.sub(r'<URL>', response_urls, task_text)
            response_text = re.sub(r'<br/>', "", response_text)
        else:
            response_text = task_text + response_urls

        if 'Interaction Fetch From DB' not in task_definition:
            response = self.build_response("hyperlink", response_text, "text", task_text)
            current_state = 'Main Navigation - Repeat'
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            # check if action_type "delayNavigate" is present in the current state
            action_type = task_definition['Action Type']
            if "delayNavigate" in action_type:
                response['message'][0] = self.delay_navigate_execute(action_type, response['message'][0])
            response_json_2 = self.execute(state_definition)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

        elif task_definition['Interaction Fetch From DB'].lower() != "true":
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)
            response = self.build_response("hyperlink", response_text, "text", task_text)

            # check if action_type "delayNavigate" is present in the current state
            action_type = task_definition['Action Type']
            if "delayNavigate" in action_type:
                response['message'][0] = self.delay_navigate_execute(action_type, response['message'][0])

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = str(int(float(state_definition['Next Task IDs'][0])))
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response

    def zip_execute(self, task_definition, task_text):
        response = self.build_response("string", task_text, "location_zip", "")
        return response

    def closeBot_execute(self, task_definition, task_text):
        response = self.build_response("closeBot", task_text, "text", "")
        return response

    def delay_navigate_execute(self, action_type, response):
        """
        Method will add the navigation_delay attribute to the response with supplied time in seconds with action type
        :param action_type: action type with delay time in seconds passed inside <>
        :param response: response dictionary in which navigation_delay need to be sent.
        :return: response dictionary
        """
        delay_in_seconds = re.findall(r"delayNavigate<\s*(\d+)\s*>", action_type)[0].strip()
        response.update({"navigation_delay": delay_in_seconds})
        return response

    #####################################################################################################################
    #####################################################################################################################
    """
    Resume Functions
    """

    #####################################################################################################################
    #####################################################################################################################

    def small_talk_fuzzy(self, msg, search_list):
        for item in search_list:
            if fuzz.ratio(msg, item) > 65:
                return 1
        return 0

    def small_talk_module(self, msg):
        msg = re.sub(r'[^\w\s]', '', msg)
        greetings = [gettext('hello'), gettext('hi'), gettext('what''s up'), gettext('good afternoon'),
                     gettext('good morning'), gettext('good day'), gettext('good evening')]
        question_questions = [gettext('I would like to ask a question'), gettext('I want to ask you a question'),
                              gettext('I have a doubt'), gettext('I want to ask you something'),
                              gettext('I want to ask you questions'), gettext('I want to consult you')]
        inquiries = [gettext('how are you'), gettext('how are you doing'), gettext('how is your day')]
        greeting_response = [gettext('Hi there! How can I help you')]
        question_question_responses = [gettext('With pleasure, tell me how I can help you.')]
        inquiriesResponse = [gettext('I\'m doing well! How can I help you today?'),
                             gettext('I\'m feeling great! How can I help you today?')]
        if msg.lower().strip() in greetings or self.small_talk_fuzzy(msg, greetings) == 1:
            response = self.build_response("string", random.choice(greeting_response), "text", "")
            return response
        elif msg.lower().strip() in question_questions:
            response = self.build_response("string", random.choice(question_question_responses), "text", "")
            return response
        elif msg.lower().strip() in inquiries:
            response = self.build_response("string", random.choice(inquiriesResponse), "text", "")
            return response
        else:
            return 0

    def keyword_handler(self, message, bot_id):

        help_config = app_config.get('GENERAL', 'help_keywords')
        restart_config = app_config.get('GENERAL', 'restart_keywords')
        goback_config = app_config.get('GENERAL', 'goback_keywords')
        bot_timeout_config = app_config.get('GENERAL', 'bot_timeout_keywords')
        reestablish_connection_config = app_config.get('GENERAL', 'reestablish_connection_keywords')

        help_keywords = help_config
        restart_keywords = restart_config
        goback_keywords = goback_config
        bot_timeout_keywords = bot_timeout_config
        reestablish_connection_keywords = reestablish_connection_config

        if len(message.lower().strip()) == 1:
            return 0
        if message.lower().strip() in help_keywords:
            help_task = self.get_task_definition("Task Name", "help", bot_id)
            help_text = help_task['Task Text']

            response = self.build_response("string", help_text, "text", "")

            next_state = help_task['Next Task IDs']

            if next_state == '' or next_state == [] or next_state == [""]:
                return response

            next_state_def = self.get_task_definition('Task ID', next_state[0], bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

        elif message.lower().strip() in restart_keywords:
            restart = self.get_task_definition("Is Start", "true", bot_id)
            self.session['custStatus'] = 'Done'
            self.session['productInformationHandler'] = 'Done'
            self.session['priceStatus'] = 'Done'
            self.session['prodStatus'] = 'Done'
            self.session['disclaimer_accept'] = False
            self.session["product_search"] = False
            update_context(self.session_id, self.session)
            return self.execute(restart)

        elif message.lower().strip() in goback_keywords:
            current_state = get_by_id('status', self.session_id)['current_state']
            prior_state = get_by_id('status', self.session_id)['prior_state']
            history = get_by_id('status', self.session_id)['history']
            history = history[:-1]  # pop out the last element
            if (len(history) == 0):
                return self.build_response("string", gettext("Sorry, there is nothing to go back to."), "text", "")
            previous_state = history[-1]  # get the last element
            history = history[:-1]  # pop out again because will be replaced in conversation logger
            update('status', self.session_id,
                   {'current_state': previous_state, 'prior_state': current_state, 'history': history,
                    "date_created": datetime.datetime.now()})
            previous_state_json = self.get_task_definition('Task Name', previous_state, bot_id)
            return self.execute(previous_state_json)

        elif message.lower().strip() in bot_timeout_keywords:
            bot_timeout_return_state = [app_config.get('GENERAL', 'bot_timeout_return_state')]
            bot_timeout_return_state.extend([bot_timeout_states])
            if self.session['disclaimer_accept']:
                for state in bot_timeout_return_state:
                    task = self.get_task_definition("Task Name", state, bot_id)
                    if task != 'None':
                        break
            else:
                task = self.get_task_definition("Is Start", "true", bot_id)
            update('status', self.session_id, {"current_state": task['Task Name']})
            self.session["product_search"] = False
            update_context(self.session_id, self.session)
            return self.execute(task)

        elif message.lower().strip() in reestablish_connection_keywords:
            # fetching current state
            current_state = get_by_id('status', self.session_id)['current_state']

            # setting up product search flag to False
            self.session["product_search"] = False
            update_context(self.session_id, self.session)

            # fetching json for current state
            current_task = self.get_task_definition('Task Name', current_state, bot_id)

            if current_task['Action Type'] == 'navigate_character' and (
                    'ワ' in current_state.lower().strip() or 'wa' in current_state.lower().strip()):
                response = self.navigate_character_execute(current_state, current_task)
                update('status', self.session_id, {"current_state": current_task['Task Name']})
                self.session['Wa'] = True
                update_context(self.session_id, self.session)
            else:
                self.session['Wa'] = False
                update_context(self.session_id, self.session)
                response = self.execute(current_task)

                # check if possible next state is 'navigate_character' then set the interaction as button_horizontal
                possible_next_state = self.get_task_definition('Task ID', str(
                    int(float(current_task['Next Task IDs'][0]))), bot_id)
                if possible_next_state["Action Type"] == "navigate_character":
                    response["message"][0]["interaction"] = "button_horizontal"
            return response
        else:
            return 0

    def survey_execute(self, message):

        if self.session['surveyFlagNum'] is True:
            if message in '12345':
                # log survey
                self.session['endSurveyFlag'] = True
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.survey_create(2, '')
                return response
            else:
                response = self.build_response("string", gettext('Please choose an option 1-5.'), "text", "")
                response_json_2 = self.survey_create(1, response)
                return response_json_2
        elif self.session['surveyFlagComm'] is True:
            if message.strip().lower() == gettext('yes'):
                response = self.build_response("string", gettext('Please leave your comment below:'), "text", "")
                return response
            elif message.strip().lower() == gettext('no'):
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.build_response("string", gettext('Thank you for your participation!'), "text", "")
                return response

            else:
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.build_response("string", gettext('Thank you for your participation!'), "text", "")
                return response

    def survey_create(self, state, json):

        self.session['endSurveyFlag'] = True
        update_context(self.session_id, self.session)
        if state == 1:
            self.session['surveyFlagNum'] = True
            update_context(self.session_id, self.session)
            options = [1, 2, 3, 4, 5]
            response = self.build_response("list", gettext(
                'On a scale of 1-5, how satisfied were you with your service today?'), "button_horizontal", options,
                                           True, json)
            return response
        elif state == 2:
            self.session['surveyFlagComm'] = True
            update_context(self.session_id, self.session)
            response = self.build_response("list", gettext('Would you like to leave any comments?'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    # use Fuzzywuzzy to check for fuzzysearch options for Navigate options
    # if the match is > 45% similar, return as option for user to pick from
    def fuzzy_search(self, nextStates, message):
        options = []
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]
        if ratio > int(os.environ.get('FUZZY_RATIO') or '45'):
            # check if task is a navigate task, if yes, check message value, see if pass
            response = self.build_response("list", gettext('We couldn\'t find that. Do you mean: ') + suggestionOutput,
                                           "button_vertical", [gettext("Yes"), gettext("No")])
            self.session['fuzzyFlag'] = True
            update_context(self.session_id, self.session)
            return response
        else:
            response = self.build_response("string",
                                           gettext('Sorry we don\'t have an answer for that. Could you try again?'),
                                           "text", "")
            return response

    # If FuzzyFlag is True, then execute this
    # check if the fuzzysearch came from a query or code
    # if not, then check if the suggestion picked comes from a next state
    # if not what they are looking for, return to start
    def fuzzy_execute(self, message, bot_id):
        if str(message).lower() == gettext('yes'):
            self.session['fuzzyFlag'] = False
            get_next_state = self.get_task_definition('Task Name', self.session['suggestion'], bot_id)
            response = self.execute(get_next_state)
            update_context(self.session_id, self.session)
            return response
            pass
        elif message.lower() == gettext('no'):
            self.session['fuzzyFlag'] = False
            update_context(self.session_id, self.session)
            next_state = self.get_task_definition('Is Start', 'true',
                                                  bot_id)  # self.get_task_definition("Is Start","true", bot_id)
            response = self.execute(next_state)
            return response
        else:
            response = self.build_response("list", gettext('Please choose yes or no.'), "button_horizontal",
                                           [gettext("Yes"), gettext("No")])
            return response

    def input_execute(self, message, next_possible_state_names):
        if self.session['name'] == 'None':  # todo store these in conversation logger
            self.session['name'] = message
        elif 'contact' not in self.session.keys():
            self.session['contact'] = message
        elif 'profession' not in self.session.keys():
            self.session['profession'] = message
        else:
            pass  # add something here later if all fields are input
        update_context(self.session_id, self.session)
        get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
        response = self.execute(get_next_state)
        return response

    def navigate_character_execute(self, message, state=None):
        if 'product_search' not in self.session.keys() or self.session["product_search"] == False:
            msg = message

            product_collection = get_context(self.session_id)['collection_info'][0]
            prod_search_result = [i['product'] for i in get_all_prod(product_collection, "initial character", message)]

            if prod_search_result == None or len(prod_search_result) == 0:
                get_state = self.get_task_definition("Task ID", "1", self.session['bot_id'])
                update('status', self.session_id, {"current_state": get_state['Task Name']})
                response = self.url_execute(get_state, get_state['Task Text'])
                return response
            else:
                if not self.session['Wa'] and msg.lower().strip() not in ["wa", "ワ"]:
                    next_state = self.get_task_definition('Task ID', str(int(float(state['Next Task IDs'][0]))),
                                                          self.session['bot_id'])
                    response = self.build_response('list', next_state['Task Text'], 'button_vertical',
                                                   prod_search_result)  # interaction_values)
                else:
                    response = self.build_response('list', state['Task Text'], 'button_vertical',
                                                   prod_search_result)  # interaction_values)

                self.session["product_search"] = True
                update_context(self.session_id, self.session)
                return response
        else:

            task_def = {"Interaction Values": []}
            current_state = {}

            data_collection = get_context(self.session_id)['collection_info'][1]

            search_key = {
                "drug_master": "product",
                "dosage_form": "form",
                "stability_data": "title"
            }

            next_state = self.get_task_definition('Task ID', str(int(float(state['Next Task IDs'][0]))),
                                                  self.session['bot_id'])

            if not self.session['Wa']:
                next_state = self.get_task_definition('Task ID', str(int(float(next_state['Next Task IDs'][0]))),
                                                      self.session['bot_id'])
            update('status', self.session_id, {"current_state": next_state['Task Name']})
            result = get_all_prod(data_collection, "product", message)
            if result == None or len(result) == 0:
                no_result_state_id = str(int(next_state['Task ID']) + 1)
                no_result_state = self.get_task_definition('Task ID', no_result_state_id, self.session['bot_id'])
                response = self.url_execute(no_result_state, no_result_state['Task Text'])

            else:
                prod_search_result = [
                    task_def["Interaction Values"].append(str(i[search_key[data_collection]]) + '|' + str(i['url'])) for
                    i in result]
                task_text = next_state['Task Text']
                task_def["Action Type"] = next_state["Action Type"]
                response = self.url_execute(task_def, task_text)
            self.session['Wa'] = False

            update_context(self.session_id, self.session)
            self.session["product_search"] = False
            update_context(self.session_id, self.session)
            return response

    def navigate_execute(self, message, next_possible_states_names, bot_id):
        found = False
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                get_next_state = self.get_task_definition('Task Name', state, bot_id)

                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    if get_next_state['Action Type'] == 'navigate_character' and message.lower().strip() in ['wa', 'ワ']:
                        response = self.navigate_character_execute(message, get_next_state)
                        update('status', self.session_id, {"current_state": get_next_state['Task Name']})
                        self.session['Wa'] = True
                        update_context(self.session_id, self.session)


                    else:
                        self.session['Wa'] = False
                        update_context(self.session_id, self.session)
                        response = self.execute(get_next_state)

                        # check if possible next state is 'navigate_character' then set the interaction as button_horizontal
                        possible_next_state = self.get_task_definition('Task ID', str(
                            int(float(get_next_state['Next Task IDs'][0]))), bot_id)
                        if possible_next_state["Action Type"] == "navigate_character":
                            response["message"][0]["interaction"] = "button_horizontal"
                    return response

        if found == False:
            current_state = get_by_id('status', self.session_id)['prior_state']
            current_state = self.get_task_definition('Task Name', current_state, self.session['bot_id'])

            try:
                next_state = self.get_task_definition('Task ID', str(int(float(current_state['Next Task IDs'][0]))),
                                                      self.session['bot_id'])
            except Exception as e:
                print("exception is........", e)

            response = self.navigate_character_execute(message, next_state)
            # do Fuzzy search here
            # response = self.fuzzy_search(next_possible_states_names, message)
            return response
            pass

    def restart_execute(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            get_start = self.get_task_definition("Is Start", "true", bot_id)
            start_response = self.execute(get_start)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon!'),
                                                 "text", "", False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def restart_execute_FAQ(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            start_response = self.build_response("string", gettext("What other questions can I help you with?"), "text",
                                                 "", False)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon'),
                                                 "text", "", False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def restart_next_task_execute(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            current_state = get_by_id('status', self.session_id)['current_state']

            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = state_definition['Next Task IDs'][0]
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            start_response = self.execute(next_state_def)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for using the Pfizer Bot!'), "text", "",
                                                 False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    # if action type is hybrid, execute this
    # first check if the message is a 1:1 match with taskNames
    # if it is, execute the corresponding state
    # if it is not, check message against keywords. For each keyword matching in the message, add a point
    # return taskNames with highest keyword scores for using to pick from
    def hybrid_guided_execute(self, message, next_possible_states_names, bot_id):
        # global guidedFlag
        found = False

        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                # get_next_state = get('state', 'state_name', state)
                get_next_state = self.get_task_definition('Task Name', state, bot_id)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    self.conv_log.updateIntentFoundCounter(message)
                    interaction = get_next_state['Interaction Type']
                    response = self.execute(get_next_state)
                    print('interaction is ', interaction)
                    if 'handler' not in interaction.lower():
                        response2 = self.restart_next_task(response)
                        return response2
                    else:
                        return response
        if found == False:  # so here instead of a fuzzysearch, do the keyword search
            stateScores = []
            finalStates = []
            possibleNonAliasStates = []
            for state in next_possible_states_names:
                score = 0
                stateinfo = self.get_task_definition('Task Name', state, bot_id)
                # print('STATE INFO HYBRID - ', stateinfo)
                alias = stateinfo['Alias']
                if alias.lower() == 'false':
                    keywords = stateinfo['Keywords']
                    if len(keywords) > 0:
                        print('list of keywords ', keywords)
                        for key in keywords:
                            if key.lower().strip() in message.lower().strip() and key != '':
                                score += 1
                                print('KEY IS -', key, '-')
                                # if key.lower().strip() in savedKey.lower().strip() -> score+=1
                        stateScores.append(score)
                        possibleNonAliasStates.append(state)
            print('STATE SCORES - ', stateScores)
            print('STATE ALIASES - ', possibleNonAliasStates)
            # pick the state names with the highest scores, return those as options
            highScore = max(stateScores)
            print('HIGH SCORE - ', highScore)
            if highScore != 0:
                for i in np.arange(0, len(stateScores)):
                    if stateScores[i] == highScore:
                        finalStates.append(possibleNonAliasStates[i])
            print('FINAL STATES - ', finalStates)
            if len(finalStates) != 0:
                if len(finalStates) == 1:
                    get_next_state = self.get_task_definition('Task Name', finalStates[0], bot_id)
                    self.conv_log.updateIntentFoundCounter(
                        message)  # todo check what the interaction type was. if it was normal or custom
                    response = self.execute(get_next_state)
                    interaction = get_next_state['Interaction Type']
                    print('interaction is ', interaction)
                    if 'handler' not in interaction.lower():
                        response2 = self.restart_next_task(response)
                        return response2
                    else:
                        return response
                else:
                    print('babel test- ', gettext(hybrid_intent_not_found))
                    finalStates.append(gettext(hybrid_intent_not_found))
                    response = self.build_response("list", gettext('Are you interested in any of these options?'),
                                                   "button_vertical", finalStates)
                    self.session['guidedFlag'] = True
                    self.session['hybrid_user_intent'] = message
                    session_id = update_context(self.session_id, self.session)
                    print('RESPONSE - ', response)
                    return response
            else:
                response = self.build_response("string", gettext(
                    'Sorry, we may not have information for that. Can you try again?'), "text", "")
                return response

    # if expecting a response from Hybrid guided, check here if it leads to a valid state
    def guided_flag_execute(self, message, bot_id):
        get_next_state = self.get_task_definition('Task Name', message, bot_id)
        if str(get_next_state) != 'None':
            response = self.execute(get_next_state)
            self.session['guidedFlag'] = False
            session_id = update_context(self.session_id, self.session)
            self.conv_log.updateIntentFoundCounter(self.session['hybrid_user_intent'])
            return response
        elif message == gettext(hybrid_intent_not_found):
            self.conv_log.updateIntentNotFound(self.session['hybrid_user_intent'])
            response = self.build_response("string", gettext(
                'I\'m sorry none of those options were helpful. Would you like to try rephrasing your question?'),
                                           "text", "")
            response_json_2 = self.restart(response)
            self.session['guidedFlag'] = False
            update_context(self.session_id, self.session)
            return response_json_2
        else:
            response = self.build_response("string", gettext('Please choose one of the options'), "text", "")
            return response

    def faq_execute(self, message, bot_id, asr_flag, es_analyzer):
        search = SearchES(self.session_id, bot_id)
        self.session['ES_UsedFlag'] = True
        update_context(self.session_id, self.session)
        response_flag, response = search.execute(message, bot_id, asr_flag, es_analyzer)
        if response_flag == 0:
            self.session['ASRFlag'] = False
            response_json_2 = self.restart_FAQ(response)
            update_context(self.session_id, self.session)
            return response_json_2
        # self.restart()
        elif response_flag == 1:
            # display the ES answer
            logger.debug('RESPONSE TO RETURN FROM FAQ EXECUTE - ', response)
            return response
        elif response_flag == 2:
            state = self.get_task_definition('Is Start', 'true', bot_id)
            response = self.execute(state)
            return response
        pass

    def location_execute(self, message):
        try:
            # message should be a 5 digit zip code
            pharmacy_data = retrieve_location(message, "pharmacy", 5)
            if pharmacy_data['status'] == 'Success':
                pharmacy_list = pharmacy_data['message']
                response = self.build_response("list", gettext("Please select any of the stores to view it on a map"),
                                               "location_maps", pharmacy_list)  # [[name, address],[name, address]]

            else:
                response = self.build_response("string", gettext(
                    "Sorry, we do not have pharmacies within 50 miles of your area."), "text", "")

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = state_definition['Next Task IDs'][0]
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response
        except Exception as e:
            logger.error("Location Execute Error:", str(e))

    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlag'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Error: ", str(e))

    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart_FAQ(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlagFAQ'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart FAQ Error: ", str(e))

    def restart_next_task(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlagNextTask'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Next Task Error: ", str(e))
