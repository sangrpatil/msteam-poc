import xlsxwriter

from utility.mongo_dao import *


def bot_excel(bot_id):
    bot = get_by_id('bot', bot_id)
    file_name = 'static/reports/' + str(bot_id) + '.xlsx'

    header = ['Bot Name', 'Task ID', 'Is Start', 'Is Reserved', 'Task Name',
              'Task Text', 'Answers ES',
              'Next Task IDs', 'Interaction Type', 'Interaction Values', 'Product Store Data', 'Keywords', 'Alias',
              'Action Type', 'Interaction Fetch From DB', 'Interaction DB Host',
              'Interaction Connection String', 'Interaction DB Port', 'Interaction DB Type',
              'Interaction DB Table Name', 'Interaction DB Select Column', 'Interaction DB Filter Column',
              'Interaction DB Filter Value', 'Use Code', 'Function Code']

    file_path = os.path.dirname(os.path.dirname(__file__))
    file_path = os.path.join(file_path, "static", "reports")
    file_path = os.path.join(file_path, str(bot_id) + '.xlsx')

    workbook = xlsxwriter.Workbook(filename=file_path)
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    bot_sheet = workbook.add_worksheet('bot_sheet')
    bot_sheet.write_row('A1', header, cell_format=formatting)
    j = 0
    for i in bot['mapping']:
        j += 1
        row = []
        for name in header:
            if isinstance(i[name], (list,)):
                row.append(str(';'.join(map(str, i[name]))))
            else:
                row.append(str(i[name]))
        bot_sheet.write_row(j, 0, row)
    workbook.close()
    return file_name
