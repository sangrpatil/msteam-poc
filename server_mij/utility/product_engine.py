import json
import os

import xlsxwriter
from flask import jsonify

from utility.excel_parser import convert
from utility.logger import logger
from utility.mongo_dao import insert as dao_insert, delete_all as dao_delete_all, get_collection as dao_get_collection


# save the file in specified location
def save_file(file_handle, directory):
    try:
        # check if directory exist
        if not os.path.isdir(directory):
            os.mkdir(directory)

        # save the file
        filename = os.path.join(directory, file_handle.filename)
        file_handle.save(filename)
        return filename
    except Exception as e:
        logger.error(str(e))


# upload the product data
def upload_product_data(request, collection, product_store_directory):
    try:
        file_handle = request.files['file']

        # save the file in product_store directory
        filename = save_file(file_handle, product_store_directory)

        # Read data from saved file
        product_data = json.loads(convert(filename))

        # check number of sheets in uploaded file
        sheet_count = len(product_data.keys())
        if sheet_count == 1:
            for key, value in product_data.items():
                product_data = value
        else:
            message = "Uploaded data file must contain only one sheet"
            logger.error(message)
            return jsonify(
                status="Failure",
                msg=message
            )

    except Exception as e:
        logger.error("Upload data product engine error: ", str(e))
        product_data = request.get_json(force=True)

    is_saved, message = store_product_data(collection, product_data)
    if is_saved == 0:
        collection = collection.replace("_", " ")
        return jsonify(
            status='Success',
            msg="{0} {1}".format(collection, "data saved successfully")
        )
    else:
        return jsonify(
            status='Failure',
            msg=message
        )


# saves the data into database
def store_product_data(collection, product_data):
    try:
        # processing the data
        for index in range(len(product_data)):
            try:
                row = {key.lower(): value.strip() for key, value in product_data[index].items()}
                product_data[index] = row
            except Exception as e:
                logger.error(str(e))
        # removing collection from database
        dao_delete_all(collection)

        # inserting the drug master data in database
        dao_insert(collection, product_data)

        return 0, 'Success'
    except Exception as e:
        logger.error(str(e))
        return 1, 'Failure'


# create excel file using the data stored in database
def download_product_data(collection, directory):
    try:
        # fetch the data from database
        product_data = dao_get_collection(collection)

        # check if directory exist
        directory = os.path.join(directory, "download")
        if not os.path.isdir(directory):
            os.mkdir(directory)

        # file that will have all the data
        filename = "{0}{1}".format(collection, ".xlsx")
        file_path = os.path.join(directory, filename)

        # sheet headers
        header = list(product_data[0].keys())
        if "_id" in header:
            header.remove("_id")

        workbook = xlsxwriter.Workbook(filename=file_path)

        # adding formatting for workbook
        formatting = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': 'white',
            'fg_color': '#007DC6'})
        product_data_sheet = workbook.add_worksheet('data')
        product_data_sheet.write_row('A1', header, cell_format=formatting)
        row_number = 0
        for i in product_data:
            row_number += 1
            row = []
            for name in header:
                if isinstance(i[name], (list,)):
                    row.append(str(';'.join(map(str, i[name]))))
                else:
                    row.append(str(i[name]))
            product_data_sheet.write_row(row_number, 0, row)
        workbook.close()
        return directory, filename
    except Exception as e:
        logger.error(str(e))
