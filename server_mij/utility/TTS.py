import base64

from google.cloud import texttospeech

from utility.logger import logger


# use google cloud text to speech API to turn text into base64 encoded audio string
def synthesize_text(text):
    try:
        """Synthesizes speech from the input string of text."""
        client = texttospeech.TextToSpeechClient()
        input_text = texttospeech.types.SynthesisInput(text=text)
        # Note: the voice can also be specified by name.
        # Names of voices can be retrieved with client.list_voices().
        voice = texttospeech.types.VoiceSelectionParams(
            language_code='en-US',  # en-US for English, es-MX for Spanish(Mexico)
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)
        audio_config = texttospeech.types.AudioConfig(
            audio_encoding=texttospeech.enums.AudioEncoding.MP3, pitch=-5.0)
        response = client.synthesize_speech(input_text, voice, audio_config)

        # The response's audio_content is binary.
        audio = response.audio_content
        convertedAudio = base64.b64encode(audio)
        convertedAudio = str(convertedAudio)[2:-1]
        return str(convertedAudio)
    except Exception as e:
        logger.error(str(e))
