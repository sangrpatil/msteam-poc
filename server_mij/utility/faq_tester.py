import datetime
import json
import logging
import smtplib
import time
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import requests
import xlrd
import xlsxwriter

server_host = 'localhost'
server_port = '8000'
bot_id = '5b58d0c5e0923346e8a8d380'

"""
Helper functions
"""


def getColNames(sheet):
    rowSize = sheet.row_len(0)
    colValues = sheet.row_values(0, 0, rowSize)
    columnNames = []

    for value in colValues:
        columnNames.append(value)

    return columnNames


def getRowData(row, columnNames):
    rowData = {}
    counter = 0

    for cell in row:
        # check if it is of date type print in iso format
        if cell.ctype == xlrd.XL_CELL_DATE:
            rowData[columnNames[counter].lower().replace(' ', '_')] = datetime.datetime(
                *xlrd.xldate_as_tuple(cell.value, 0)).isoformat()
        else:
            rowData[columnNames[counter]] = str(cell.value)
        counter += 1

    return rowData


def getSheetData(sheet, columnNames):
    nRows = sheet.nrows
    sheetData = []
    counter = 1

    for idx in range(1, nRows):
        row = sheet.row(idx)
        rowData = getRowData(row, columnNames)
        sheetData.append(rowData)

    return sheetData


def getWorkBookData(workbook):
    nsheets = workbook.nsheets
    counter = 0
    workbookdata = {}

    for idx in range(0, nsheets):
        worksheet = workbook.sheet_by_index(idx)
        columnNames = getColNames(worksheet)
        sheetdata = getSheetData(worksheet, columnNames)
        workbookdata[worksheet.name.lower().replace(' ', '_')] = sheetdata

    return workbookdata


def convert(file_location):
    workbook = xlrd.open_workbook(file_location)
    workbookdata = getWorkBookData(workbook)
    return json.dumps(workbookdata)


logger = logging.getLogger()


def send_email(report_filename, emails):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'paco@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "PACO Bot Report for " + str(today_date)
        body = "Hello " + ",\n\nThank you for using BOT. Please find attached the Report for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)


    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.".format(e))
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        # server.login(username, password)
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.".format(e))
        return ('Failure')


'''
Tester Code
'''

test = convert(r'../mapping/SAMM_test_v10.xlsx')
questionanswer_list = []
# answer_list = []

mapping_result = json.loads(test)
for key, value in mapping_result.items():
    for item in value:
        if item['Task Name'] == 'FAQ':
            questionanswer_list.append([item['Task Text'], item['Answers ES']])

get_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/init'
response = json.loads(requests.get(get_request_url).text)

session_id = str(response['session_id'])

post_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/' + session_id + '/chat'
post_request_data = json.dumps({"message": "", "state": "", "ASR": False})

headers = {'content-type': "application/json"}
response = requests.request("POST", post_request_url, data=post_request_data, headers=headers)
response_json = json.loads(response.text)

print(response_json)
counter = 1

fileTitle = r'../reports/SAMM_Test_072518' + str(time.time()) + '.xlsx'
workbook = xlsxwriter.Workbook(filename=fileTitle)
testLog = workbook.add_worksheet('TestResults')
testLog.write_row('A1', ['QUESTION', 'GIVEN ANSWER', 'EXPECTED ANSWER', 'CORRECT?'])
testLog.autofilter('A1:D1')
for q_a_tuple in questionanswer_list:
    post_request_data = json.dumps({"message": q_a_tuple[0], "state": "", "ASR": False})
    r = requests.request("POST", post_request_url, data=post_request_data, headers=headers)
    r_json = json.loads(r.text)
    if r_json['result']['message'][0]['text'] != q_a_tuple[1]:
        dataRow = [q_a_tuple[0], r_json['result']['message'][0]['text'], q_a_tuple[1], 'INCORRECT']
        testLog.write_row(counter, 0, dataRow)
    else:
        dataRow = [q_a_tuple[0], r_json['result']['message'][0]['text'], q_a_tuple[1], 'CORRECT']
        testLog.write_row(counter, 0, dataRow)
    counter += 1
    print(counter)

workbook.close()
# print(answer_list[0])
# send_email(fileTitle, ['robert.xu@pfizer.com'])
