import datetime
from datetime import timedelta

import xlsxwriter
from dateutil import tz

from utility.mongo_dao import *

to_zone = tz.gettz('UTC')


def get_dashboard(start, end, bot_id=None, bot_name='', download=False):
    try:
        ##################### Number of Conversations By Day ####################
        data = get_DashData(start, end, bot_id)

        dateKeyList = []
        dateValuesList = []
        for key, value in sorted(data["dateDict"].items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)

        ######################## Peak Engagement Hours #########################
        timeKeyList = []
        timeValueList = []
        for key, value in sorted(data["timeDict"].items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)

        ########################## LATENCY GRAPH ##############################
        latencyKeyList = []
        latencyValueList = []
        for key, value in sorted(data["latencyDict"].items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum'] / value['numConvos']
                else:
                    finalLatencyAvg = 0
            latencyValueList.append(finalLatencyAvg)

        final_json = {
            "bot_name": bot_name,
            "graph_data": [
                {
                    "graph_name": "Number of Conversations By Day",
                    "x_values": dateKeyList,
                    "y_values": dateValuesList,
                    "x_axis_title": "Days",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "Peak Engagement Hours",
                    "x_values": timeKeyList,
                    "y_values": timeValueList,
                    "x_axis_title": "Time of Day (JST)",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "Bot Response Latency",
                    "x_values": latencyKeyList,
                    "y_values": latencyValueList,
                    "x_axis_title": "Time of Day (JST)",
                    "y_axis_title": "Average Response Latency (sec)"
                }
            ], "table_data": {}
        }

        if download == True:
            return {
                "dateKeyList": dateKeyList,
                "dateValuesList": dateValuesList,
                "timeKeyList": timeKeyList,
                "timeValueList": timeValueList,
                "latencyKeyList": latencyKeyList,
                "latencyValueList": latencyValueList,
                "num_convo": data["num_convo"]}
        return final_json
    except Exception as e:
        print('failure due to ', e)
        return {"bot_name": bot_name,
                "graph_data": [],
                "table_data": {}}
        return "Failure"


def get_ConvoLog(start, end, bot_id, offset=0, next=50):
    try:
        start_date, end_date = start_end(start, end)
        res = client[database]["status"].aggregate([
            {
                "$match": {"$and": [

                    {"bot_id": {"$eq": bot_id}},
                    {"conversations": {
                        "$exists": "true"
                    }},
                    {"conversations.1": {
                        "$exists": "true"
                    }}
                ]
                }

            },
            {
                "$project": {
                    "wordsAsKeyValuePairs": {
                        "$objectToArray": "$conversations"
                    },
                    "id": "$_id"
                }
            },
            {
                "$unwind": "$wordsAsKeyValuePairs"
            },
            {"$sort": {'wordsAsKeyValuePairs.v.time': -1}},
            {
                "$match": {"$and": [

                    {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                    {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
                }

            },
            {
                "$project": {
                    "CONVERSATION ID": "$id",
                    "FROM": {
                        "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                                 "bot message"]}, "then": "ChatBot", "else": "User"}}
                    ,
                    "MESSAGE": "$wordsAsKeyValuePairs.v.message",
                    "DATE": {"$concat": [
                        {"$substr": [{"$year": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0, 4]}, "-",
                        {"$substr": [{"$month": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0, 2]}, "-",
                        {"$substr": [{"$dayOfMonth": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0, 2]}
                    ]},
                    "TIME (JST)": {"$concat": [
                        {"$substr": [{"$hour": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0, 2]}, ":",
                        {"$substr": [{"$minute": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0, 2]}, ":",
                        {"$substr": [{"$second": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0, 4]}
                    ]}
                }}
        ], allowDiskUse = True)
        result = [i for i in res]
        count = 0

        result = [
            {
                "total": len(result),
                "intent": result[offset:offset + next]
            }
        ]

        id_dic = {}
        type_dic = {}
        # loop to check user message
        for typ in result[0]["intent"]:
            if typ['CONVERSATION ID'] in type_dic:
                type_dic[typ['CONVERSATION ID']].add(typ['FROM'])
            else:
                type_dic[typ['CONVERSATION ID']] = {typ['FROM']}
            if "_id" in typ:
                del typ["_id"]

        result[0]["intent"] = list(
            filter(lambda item: 'User' in type_dic[item['CONVERSATION ID']], result[0]["intent"]))
        for j in result[0]["intent"]:
            if j['CONVERSATION ID'] in id_dic:
                j['CONVERSATION ID'] = id_dic[j['CONVERSATION ID']]
            else:
                count += 1
                id_dic[j['CONVERSATION ID']] = count
                j['CONVERSATION ID'] = id_dic[j['CONVERSATION ID']]
        return result[0]["intent"], result[0]["total"]
    except Exception as e:
        print(e)
        return [], 0


def get_DashData(start, end, bot_id, group=None):
    try:
        resconvo, num_convo = convo_complete(start, end, bot_id)

        dateDict = {}
        timeDict = {}
        latencyDict = {}

        for k in range(0, 24):
            timeDict[k] = []
            latencyDict[k] = []

        convo = resconvo
        for z in convo:
            if z["DATE"] in dateDict:
                dateDict[z["DATE"]].append(z["CONVERSATION ID"])
            else:
                dateDict[z["DATE"]] = [z["CONVERSATION ID"]]

        botJustSentMessage = True
        userReplyTime = datetime.datetime.utcnow()
        for j in convo:
            hour = int(j["TIME (JST)"].split(":")[0])
            if hour in timeDict:
                timeDict[hour].append(j["CONVERSATION ID"])
            else:
                timeDict[hour] = [j["CONVERSATION ID"]]
            if hour in latencyDict:
                if latencyDict[hour] == []:
                    # only want to calculate "bot message timestamp" - "user message timestamp"
                    if j['FROM'] == 'User':
                        userReplyTime = j['time']

                    latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                elif j['FROM'] == 'User':
                    userReplyTime = j['time']
                    botJustSentMessage = False
                elif j['FROM'] == 'ChatBot' and botJustSentMessage == False:
                    latency = (j['time'] - userReplyTime).total_seconds()
                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                    latencySum = latencyDict[hour]['latencySum'] + latency
                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                    botJustSentMessage = True
        return {
            "dateDict": dateDict,
            "timeDict": timeDict,
            "latencyDict": latencyDict,
            "num_convo": num_convo
        }
    except Exception as e:
        print("dash data is - ", e)
        return {}


def generateReports_convo(start, end, bot_id=None):
    fileTitle = 'static/reports/Chatbot_Report_convo' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    fileTitle = os.path.join(directory_path, fileTitle)

    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)

    convoLog = workbook.add_worksheet('ConversationLog')
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    headers = ['CONVERSATION ID', 'FROM', 'MESSAGE', 'DATE', 'TIME (JST)']

    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:E1')

    tempconvodata, num_convo = convo_complete(start, end, bot_id)
    conversation = tempconvodata
    count = 1
    for cnv in conversation:
        dataRow = [cnv['CONVERSATION ID'],
                   cnv['FROM'],
                   str(cnv['MESSAGE']),
                   cnv['DATE'],
                   cnv['TIME (JST)']
                   ]
        convoLog.write_row(count, 0, dataRow)
        count += 1
    workbook.close()
    print('Total report generation completed.')
    return fileTitle


def convo_complete(start, end, bot_id=None):
    start_date, end_date = start_end(start, end)

    res = client[database]["status"].aggregate([
        {
            "$match": {"$and": [

                {"bot_id": {"$eq": bot_id}},
                {"conversations": {
                    "$exists": "true"
                }}, {"conversations.1": {
                    "$exists": "true"
                }}]
            }
        },
        {
            "$project": {
                "wordsAsKeyValuePairs": {
                    "$objectToArray": "$conversations"
                },
                "id": "$_id"
            }
        },
        {
            "$unwind": "$wordsAsKeyValuePairs"
        },
        {
            "$match": {"$and": [

                {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
            }

        },
        {
            "$project": {
                "CONVERSATION ID": "$id",
                "FROM": {
                    "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                             "bot message"]}, "then": "ChatBot", "else": "User"}}
                ,
                "MESSAGE": "$wordsAsKeyValuePairs.v.message",
                "DATE": {"$concat": [
                    {"$substr": [{"$year": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 4]}, "-",
                    {"$substr": [{"$month": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 2]}, "-",
                    {"$substr": [{"$dayOfMonth": {"date": "$wordsAsKeyValuePairs.v.time"}},
                                 0, 2]}
                ]},
                "TIME (JST)": {"$concat": [
                    {"$substr": [{"$hour": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 2]}, ":",
                    {"$substr": [{"$minute": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 2]}, ":",
                    {"$substr": [{"$second": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 4]}
                ]},
                "time": "$wordsAsKeyValuePairs.v.time"
            }}
    ])
    result = [i for i in res]
    count = 0
    id_dic = {}
    type_dic = {}
    # loop to check user message
    for typ in result:
        if typ['CONVERSATION ID'] in type_dic:
            type_dic[typ['CONVERSATION ID']].add(typ['FROM'])
        else:
            type_dic[typ['CONVERSATION ID']] = {typ['FROM']}

    result = list(filter(lambda item: 'User' in type_dic[item['CONVERSATION ID']], result))

    for j in result:

        if j['CONVERSATION ID'] in id_dic:
            j['CONVERSATION ID'] = id_dic[j['CONVERSATION ID']]
        else:
            count += 1
            id_dic[j['CONVERSATION ID']] = count
            j['CONVERSATION ID'] = id_dic[j['CONVERSATION ID']]

    count = len(result)
    return result, count


def graph_download(bot_id, bot_name, start, end):
    response = get_dashboard(start, end, bot_id, bot_name, True)
    number_conv_counter = response['num_convo']
    fileTitle = 'static/reports/Chatbot_Report_Graph' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'

    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)

    fileTitle = os.path.join(directory_path, fileTitle)
    with open(fileTitle, "wb") as f:
        pass
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    dashboard = workbook.add_worksheet('Dashboard')
    dataSheet = workbook.add_worksheet('DataSheet')
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})
    title = 'Chatbot Dashboard: ' + datetime.datetime.today().strftime('%m-%d-%Y')
    formatting.set_font_size(20)
    dashboard.merge_range('A1:S2', title, formatting)
    formatting.set_font_size(11)

    dashboard.write(3, 1, 'Summary Data')
    dashboard.merge_range('B4:C4', 'Summary Data', formatting)
    dashboard.write_row(4, 1, ['Number of Conversations', number_conv_counter])
    dashboard.set_column(1, 1, 50)

    # initialize charts
    barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
    lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
    lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})

    dataSheet.write_row(0, 0, response['dateKeyList'])
    dataSheet.write_row(1, 0, response['dateValuesList'])
    dataSheet.write_row(2, 0, response['timeKeyList'])
    dataSheet.write_row(3, 0, response['timeValueList'])
    dataSheet.write_row(6, 0, response['latencyKeyList'])
    dataSheet.write_row(7, 0, response['latencyValueList'])

    barGraph_ConvByDay.add_series({
        'name': 'Number of Conversations by Day',
        'categories': ['DataSheet', 0, 0, 0, len(response['dateKeyList']) - 1],
        'values': ['DataSheet', 1, 0, 1, len(response['dateKeyList']) - 1]
    })

    barGraph_ConvByDay.set_x_axis({'name': 'Days'})
    barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
    barGraph_ConvByDay.set_legend({'none': True})

    lineGraph_ConvByHour.add_series({
        'name': 'Peak Engagement Hours',
        'categories': '=DataSheet!$A$3:$Y$3',
        'values': '=DataSheet!$A$4:$Y$4'
    })
    lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day (JST)'})
    lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
    lineGraph_ConvByHour.set_legend({'none': True})

    lineGraph_LatencyByHour.add_series({
        'name': 'Bot Response Latency',
        'categories': ['DataSheet', 6, 0, 6, len(response['latencyKeyList']) - 1],
        'values': ['DataSheet', 7, 0, 7, len(response['latencyValueList']) - 1]
    })

    lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day (JST)'})
    lineGraph_LatencyByHour.set_y_axis({'name': 'Aversage Response Latency (sec)'})
    lineGraph_LatencyByHour.set_legend({'none': True})

    # insert the charts into the dashboard
    dashboard.insert_chart('B11', barGraph_ConvByDay)
    dashboard.insert_chart('F11', lineGraph_ConvByHour)
    dashboard.insert_chart('B27', lineGraph_LatencyByHour)

    # Hide the datasheet used to create the charts
    dataSheet.hide()
    workbook.close()
    print('Total report generation completed.')
    return fileTitle


def start_end(start, end):
    # jpn = pytz.timezone('Asia/Tokyo')
    if start == '':
        # start = "1000-01-01"
        start = str(datetime.datetime.utcnow() + timedelta(days=-90)).split(' ')[0]
        start_date = datetime.datetime.strptime(start, '%Y-%m-%d')
        # start_date = start_date.astimezone(to_zone).strftime('%Y-%m-%d')
    else:
        start_date = datetime.datetime.strptime(start, '%Y-%m-%d')
        # start_date = jpn.localize(start_date).astimezone(to_zone)

    if end == '':
        end = str(datetime.datetime.utcnow() + timedelta(days=2)).split(' ')[0]
        # end_date = str(datetime.datetime.utcnow()+timedelta(days=1)).split(' ')[0]
        end_date = datetime.datetime.strptime(end, '%Y-%m-%d')
        # print(end_date)
        # end_date = jpn.localize(end_date).astimezone(to_zone).strftime('%Y-%m-%d')
    else:
        end_date = datetime.datetime.strptime(end, '%Y-%m-%d') + timedelta(days=1)
        # end_date = (jpn.localize(end_date).astimezone(to_zone) + timedelta(days=1))  # .strftime('%Y-%m-%d')

    return start_date, end_date
