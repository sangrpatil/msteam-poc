from utility.de_coupled_report import start_end
import os

from bson.objectid import ObjectId
from pymongo import MongoClient

host = os.environ.get('MONGO_HOST') or 'amrvpce1a02014'
port = os.environ.get('MONGO_PORT') or '47017'
username = os.environ.get('MONGO_USERNAME') or 'admin'
password = os.environ.get('MONGO_PASSWORD') or 'admin'
database = os.environ.get('MONGO_DB') or 'mijdb'

client = MongoClient('mongodb://' + username + ':' + password + '@' + host + ':' + str(port) + '/admin')

def convo_complete(start, end, bot_id=None):
    start_date, end_date = start_end(start, end)

    res = client[database]["status"].aggregate([
        {
            "$match": {"$and": [

                {"bot_id": {"$eq": bot_id}},
                {"conversations": {
                    "$exists": "true"
                }}, {"conversations.1": {
                    "$exists": "true"
                }}]
            }

        },
        {
            "$project": {
                "wordsAsKeyValuePairs": {
                    "$objectToArray": "$conversations"
                },
                "id": "$_id"
            }
        },
        {
            "$unwind": "$wordsAsKeyValuePairs"
        },
        {
            "$match": {"$and": [

                {"wordsAsKeyValuePairs.v.time": {"$gte": start_date}},
                {"wordsAsKeyValuePairs.v.time": {"$lte": end_date}}]
            }

        },
        {
            "$project": {
                "CONVERSATION ID": "$id",
                "FROM": {
                    "$cond": {"if": {"$eq": ["$wordsAsKeyValuePairs.v.type",
                                             "bot message"]}, "then": "ChatBot", "else": "User"}}
                ,
                "MESSAGE": "$wordsAsKeyValuePairs.v.message",
                "DATE": {"$concat": [
                    {"$substr": [{"$year": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 4]}, "-",
                    {"$substr": [{"$month": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 2]}, "-",
                    {"$substr": [{"$dayOfMonth": {"date": "$wordsAsKeyValuePairs.v.time"}},
                                 0, 2]}
                ]},
                "TIME (JST)": {"$concat": [
                    {"$substr": [{"$hour": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 2]}, ":",
                    {"$substr": [{"$minute": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 2]}, ":",
                    {"$substr": [{"$second": {"date": "$wordsAsKeyValuePairs.v.time"}}, 0,
                                 4]}
                ]},
                "time": "$wordsAsKeyValuePairs.v.time"
            }} ,
        # {"$group": {"_id": None, "myCount": {"$sum": 1}, "convo": {"$push": "$row"}}},

    ])
    result = [i for i in res]
    count = 0
    id_dic = {}
    type_dic = {}
    # loop to check user message
    for typ in result:
        if typ['CONVERSATION ID'] in type_dic:
            type_dic[typ['CONVERSATION ID']].add(typ['FROM'])
        else:
            type_dic[typ['CONVERSATION ID']] = {typ['FROM']}

    result = list(filter(lambda item: 'User' in type_dic[item['CONVERSATION ID']], result))

    for j in result:

        if j['CONVERSATION ID'] in id_dic:
            j['CONVERSATION ID'] = id_dic[j['CONVERSATION ID']]
        else:
            count += 1
            id_dic[j['CONVERSATION ID']] = count
            j['CONVERSATION ID'] = id_dic[j['CONVERSATION ID']]

    # result[0]['myCount'] = len(result[0]["convo"])
    count = len(result)
    return result, count

convo_complete("2019-11-15", "2019-12-02", "5d63e87461414f0008cf2506")