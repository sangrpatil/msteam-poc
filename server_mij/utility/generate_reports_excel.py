import configparser
import datetime
import re

import xlsxwriter

from utility.logger import logger
from utility.mongo_dao import get_collection

app_config = configparser.ConfigParser()


# app_config.read_file(open(r'../config/app_settings.ini'))


def generateReportsDaily(bot_id=None):
    cursor = get_collection('status')  # todo - read for only particular bot
    if bot_id == None:
        bot_name = 'All_Bots'
    else:
        bot_name = cursor = get_collection('bot')
        for document in cursor:
            if str(bot_id) == str(document['_id']).replace('ObjectId(', '').replace(')', ''):
                bot_name = document['bot name']
    if bot_name == '':
        bot_name = 'Bot'
    fileTitle = 'reports/_' + bot_name + '_Chatbot_Report_Daily_' + datetime.datetime.today().strftime(
        '%m-%d-%Y') + '.xlsx'
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    convoLog = workbook.add_worksheet('ConversationLog')

    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['CONVERSATION ID', 'FROM', 'MESSAGE', 'DATE', 'TIME (JST)']
    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:I1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    ID = 0
    number_conv_counter = 0
    dateDict = {}
    timeDict = {}
    latencyDict = {}
    final_conv_log = []

    final_conv_log.append(headers)
    found_bot_id = False

    # create hour dict for conversations over time of day and latency
    for k in range(0, 24):
        timeDict[k] = []
        latencyDict[k] = []

    for document in cursor:
        try:
            # determine the converation's rating and comments, if any

            if bot_id == None:
                number_conv_counter += 1
                found_bot_id = True

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    print('bot ID is none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        dataRow = [ID, type, message, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ", str(e))

            elif str(document['bot_id']) == str(bot_id):
                number_conv_counter += 1
                found_bot_id = True

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    print('bot ID is not none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        dataRow = [ID, type, message, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ", str(e))

            else:
                continue
        except Exception as e:
            logger.error("Generate Report Excel DOCUMENT CURSOR ERROR - : ", str(e))

    try:
        if found_bot_id == False:
            return 'Failure'
    except Exception as e:
        logger.error("Generate Report Excel error: ", str(e))
        pass

    # Hide the datasheet used to create the charts

    workbook.close()
    return fileTitle


def generateReportsWeekly(bot_id=None):  # todo pass bot ID
    try:
        cursor = get_collection('status')  # todo - read for only particular bot

        if bot_id == None:
            bot_name = 'All_Bots'
        else:
            cursor_bot = get_collection('bot')
            for document in cursor_bot:
                if str(bot_id) == str(document['_id']).replace('ObjectId(', '').replace(')', ''):
                    bot_name = document['bot name']
        if bot_name == '':
            bot_name = 'Bot'
        fileTitle = 'static/reports/' + str(bot_id) + '.xlsx'

        workbook = xlsxwriter.Workbook(filename=fileTitle)
        dashboard = workbook.add_worksheet('Dashboard')
        convoLog = workbook.add_worksheet('ConversationLog')
        dataSheet = workbook.add_worksheet('DataSheet')

        # master formatting style
        formatting = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': 'white',
            'fg_color': '#007DC6'})

        # write column headers for conversation data log
        headers = ['CONVERSATION ID', 'FROM', 'MESSAGE', 'DATE', 'TIME (JST)']
        convoLog.write_row('A1', headers, cell_format=formatting)
        convoLog.set_column(0, 8, 20)
        convoLog.autofilter('A1:E1')

        # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
        i = 1
        ID = 0
        number_conv_counter = 0
        dateDict = {}
        timeDict = {}
        latencyDict = {}
        final_conv_log = []

        final_conv_log.append(headers)

        # create hour dict for conversations over time of day and latency
        for k in range(0, 24):
            timeDict[k] = []
            latencyDict[k] = []

        for document in cursor:

            try:
                # determine the converation's rating and comments, if any

                if len(document["conversations"]) == 1:
                    continue

                if bot_id == None:
                    number_conv_counter += 1
                    # found_bot_id = True

                    # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                    ID += 1
                    userReplyTime = datetime.datetime.now()

                    # now iterate through each individual conversation log
                    try:
                        conversations = document['conversations']

                        botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                        for item in conversations:
                            message = conversations[item]['message']
                            type = conversations[item]['type']
                            time = conversations[item]['time']

                            # process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            # process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            # process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            # create bar graph information for dates vs #conversations
                            if date in dateDict:
                                dateDict[date].append(ID)
                            else:
                                dateDict[date] = [ID]

                            # create line graph information for # conversations by hour of day
                            if hour in timeDict:
                                timeDict[hour].append(ID)
                            else:
                                timeDict[hour] = [ID]

                            # create line graph info for latency by hour of day
                            if hour in latencyDict:
                                if latencyDict[hour] == []:
                                    # only want to calculate "bot message timestamp" - "user message timestamp"
                                    if conversations[item]['type'] == 'user message':
                                        userReplyTime = time
                                    latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                                elif conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                    botJustSentMessage = False
                                else:
                                    if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                        latency = (time - userReplyTime).total_seconds()
                                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                                        latencySum = latencyDict[hour]['latencySum'] + latency
                                        latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                        botJustSentMessage = True
                            final_conv_log.append([ID, type, message, date, min_hour_sec])

                            dataRow = [ID, type, message, date, min_hour_sec]
                            convoLog.write_row(i, 0, dataRow)
                            i += 1
                    except Exception as e:
                        logger.error("Generate Report Excel error: ", str(e))

                elif str(document['bot_id']) == str(bot_id):
                    number_conv_counter += 1

                    # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                    ID += 1
                    userReplyTime = datetime.datetime.now()

                    # now iterate through each individual conversation log
                    try:
                        conversations = document['conversations']

                        botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                        for item in conversations:
                            message = conversations[item]['message']
                            type = conversations[item]['type']
                            time = conversations[item]['time']

                            # process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            # process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            # process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            # create bar graph information for dates vs #conversations
                            if date in dateDict:
                                dateDict[date].append(ID)
                            else:
                                dateDict[date] = [ID]

                            # create line graph information for # conversations by hour of day
                            if hour in timeDict:
                                timeDict[hour].append(ID)
                            else:
                                timeDict[hour] = [ID]

                            # create line graph info for latency by hour of day
                            if hour in latencyDict:
                                if latencyDict[hour] == []:
                                    # only want to calculate "bot message timestamp" - "user message timestamp"
                                    if conversations[item]['type'] == 'user message':
                                        userReplyTime = time
                                    latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                                elif conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                    botJustSentMessage = False
                                else:
                                    if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                        latency = (time - userReplyTime).total_seconds()
                                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                                        latencySum = latencyDict[hour]['latencySum'] + latency
                                        latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                        botJustSentMessage = True

                            dataRow = [ID, type, message, date, min_hour_sec]
                            convoLog.write_row(i, 0, dataRow)
                            i += 1
                    except Exception as e:
                        logger.error("Generate Report Excel error: ", str(e))

                else:
                    continue
            except Exception as e:
                logger.error('DOCUMENT CURSOR ERROR - ', e)
    except Exception as e:
        logger.error('Weekly Notifications Failure of  - ', e)
    try:

        # format main dashboard
        title = 'Chatbot Dashboard: ' + datetime.datetime.today().strftime('%m-%d-%Y')
        formatting.set_font_size(20)
        dashboard.merge_range('A1:S2', title, formatting)
        formatting.set_font_size(11)

        # create summary data table
        dashboard.write(3, 1, 'Summary Data')
        dashboard.merge_range('B4:C4', 'Summary Data', formatting)
        dashboard.write_row(4, 1, ['Number of Conversations', number_conv_counter])
        dashboard.set_column(1, 1, 50)

        # initialize charts
        barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
        lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
        lineGraph_RatingByDay = workbook.add_chart({'type': 'line'})
        lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})
        barGraph_RatingCount = workbook.add_chart({'type': 'column'})

        # initialize lists for data sheet
        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        latencyKeyList = []
        latencyValueList = []

        for key, value in sorted(latencyDict.items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum'] / value['numConvos']
                else:
                    finalLatencyAvg = 0
            latencyValueList.append(finalLatencyAvg)

        for key, value in sorted(dateDict.items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)

        for key, value in sorted(timeDict.items()):
            if key == 12:
                key = '12 pm'
            elif key == 24 or key == 0:
                key = '12 am'
            elif (key - 12) > 0:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)

        dataSheet.write_row(0, 0, dateKeyList)
        dataSheet.write_row(1, 0, dateValuesList)
        dataSheet.write_row(2, 0, timeKeyList)
        dataSheet.write_row(3, 0, timeValueList)
        dataSheet.write_row(6, 0, latencyKeyList)
        dataSheet.write_row(7, 0, latencyValueList)

        # create the charts and format them

        barGraph_ConvByDay.add_series({
            'name': 'Number of Conversations by Day',
            'categories': ['DataSheet', 0, 0, 0, len(dateKeyList) - 1],
            'values': ['DataSheet', 1, 0, 1, len(dateKeyList) - 1]
        })

        barGraph_ConvByDay.set_x_axis({'name': 'Days'})
        barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
        barGraph_ConvByDay.set_legend({'none': True})

        lineGraph_ConvByHour.add_series({
            'name': 'Peak Engagement Hours',
            'categories': '=DataSheet!$A$3:$Y$3',
            'values': '=DataSheet!$A$4:$Y$4'
        })
        lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day (JST)'})
        lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
        lineGraph_ConvByHour.set_legend({'none': True})

        lineGraph_LatencyByHour.add_series({
            'name': 'Bot Response Latency',
            'categories': ['DataSheet', 6, 0, 6, len(latencyKeyList) - 1],
            'values': ['DataSheet', 7, 0, 7, len(latencyValueList) - 1]
        })

        lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day (JST)'})
        lineGraph_LatencyByHour.set_y_axis({'name': 'Average Response Latency (sec)'})
        lineGraph_LatencyByHour.set_legend({'none': True})

        # insert the charts into the dashboard
        dashboard.insert_chart('B11', barGraph_ConvByDay)
        dashboard.insert_chart('F11', lineGraph_ConvByHour)
        dashboard.insert_chart('B27', lineGraph_LatencyByHour)

        # Hide the datasheet used to create the charts
        dataSheet.hide()
        workbook.close()
        return fileTitle
    except Exception as e:
        logger.error('Weekly Sheet Generation Failure due to  - ', e)
        return 'Failure due to ' + str(e)
