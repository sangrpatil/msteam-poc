import configparser
import datetime
import os
import re

import xlsxwriter

from utility.mongo_dao import get_collection

app_config = configparser.ConfigParser()


def generateDashboardExcel(bot_id=None, begin=None, end=None):  # todo pass bot ID
    cursor = get_collection('status')  # todo - read for only particular bot

    fileTitle = 'Chatbot_Report_' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'

    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    file_uri = os.path.join(directory_path, 'static/reports', fileTitle)

    workbook = xlsxwriter.Workbook(filename=file_uri)
    convoLog = workbook.add_worksheet('ConversationLog')

    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['CONVERSATION ID', 'FROM', 'MESSAGE', 'DATE', 'TIME (JST)']

    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 5, 20)
    convoLog.autofilter('A1:E1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    ID = 0
    final_conv_log = []
    final_conv_log.append(headers)
    found_bot_id = False

    for document in cursor:

        if bot_id == None:
            found_bot_id = True

            # initialize and increment necessary counters.
            ID += 1

            # now iterate through each individual conversation log
            try:
                conversations = document['conversations']

                for item in conversations:
                    message = conversations[item]['message']
                    type = conversations[item]['type']
                    time = conversations[item]['time']

                    # process the message to remove html elements
                    message = re.sub(r'<br>', ' ', message)
                    message = re.sub(r'<button .*', '', message)
                    message = re.sub(r'<[^>]+>', '', message)

                    # process message type to read as "Chatbot" or "User"
                    if type == 'bot message':
                        type = 'ChatBot'
                    else:
                        type = 'User'

                    # process the time into manageable numbers
                    date, min_hour = str(time).split(' ')
                    min_hour_sec = min_hour[:8]
                    min_hour = min_hour[:5]
                    hour = min_hour[:2]
                    hour = int(hour)

                    final_conv_log.append([ID, type, message, date, min_hour_sec])
                    i += 1
            except Exception as e:
                print(e)

        elif "bot_id" in document:
            if str(document['bot_id']) == str(bot_id):
                found_bot_id = True

                # initialize and increment necessary counters.
                ID += 1

                # now iterate through each individual conversation log
                try:
                    conversations = document['conversations']

                    for item in conversations:
                        message = conversations[item]['message']
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        if not begin:
                            begin_time = time
                        else:
                            begin_time = begin
                        if not end:
                            end_time = time
                        else:
                            end_time = end

                        if time.date() >= begin_time.date() and time.date() <= end_time.date():

                            # process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            # process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            # process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            # write row of dataset to conversation log
                            dataRow = [ID, type, message, date, min_hour_sec]
                            convoLog.write_row(i, 0, dataRow)
                            i += 1
                except Exception as e:
                    print(e)

        else:
            continue

    try:
        if found_bot_id == False:
            return 'Failure, Bot Id not found'
    except:
        pass

    workbook.close()
    return fileTitle
