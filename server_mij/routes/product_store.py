from flask import Blueprint, request, send_from_directory

from utility.logger import logger
from utility.product_engine import upload_product_data, download_product_data

# create blueprint
product_store_blueprint = Blueprint('productstore', __name__, template_folder='templates')

# directory where all product store files will be stored
product_store_directory = r"./product_store"

# collection names
drug_master_collection = "drug_master"
dosage_form_collection = "dosage_form"
stability_data_collection = "stability_data"


@product_store_blueprint.route('/api/productstore/drugmaster', methods=['GET', 'POST'])
def product_create_drug_master():
    if request.method == 'GET':
        try:
            directory, filename = download_product_data(drug_master_collection, product_store_directory)
            return send_from_directory(directory=directory, filename=filename)
        except Exception as e:
            logger.error("Drug master download error: ", str(e))

    elif request.method == 'POST':
        try:
            return upload_product_data(request, drug_master_collection, product_store_directory)
        except Exception as e:
            logger.error("Drug master upload error: ", str(e))


@product_store_blueprint.route('/api/productstore/dosageform', methods=['GET', 'POST'])
def product_create_dosage_form():
    if request.method == 'GET':
        try:
            directory, filename = download_product_data(dosage_form_collection, product_store_directory)
            return send_from_directory(directory=directory, filename=filename)
        except Exception as e:
            logger.error("Dosage form download error: ", str(e))

    elif request.method == 'POST':
        try:
            return upload_product_data(request, dosage_form_collection, product_store_directory)
        except Exception as e:
            logger.error("Dosage form upload error: ", str(e))


@product_store_blueprint.route('/api/productstore/stabilitydata', methods=['GET', 'POST'])
def product_create_stability_data():
    if request.method == 'GET':
        try:
            directory, filename = download_product_data(stability_data_collection, product_store_directory)
            return send_from_directory(directory=directory, filename=filename)
        except Exception as e:
            logger.error("Stability data download error: ", str(e))

    elif request.method == 'POST':
        try:
            return upload_product_data(request, stability_data_collection, product_store_directory)
        except Exception as e:
            logger.error("Stability data upload error: ", str(e))
