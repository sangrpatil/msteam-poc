from flask import Blueprint, request, jsonify, send_from_directory

from utility.de_coupled_report import *
from utility.generate_dashboard_excel import generateDashboardExcel
from utility.generate_reports_excel import generateReportsWeekly
from utility.generate_reports_json import generateReports

metrics_blueprint = Blueprint('metrics', __name__, template_folder='templates')


# Return metrics of all bots
@metrics_blueprint.route('/api/metrics', methods=['GET'])
def metrics_all_bots():
    if request.method == 'GET':
        # Call the function with not bot_id passed
        returnedjson = generateReports()
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        pass


# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>', methods=['GET'])
def metrics_bot(bot_id):
    if request.method == 'GET':
        # Call the function with bot_id passed
        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            logger.error("metrics error: ", str(e))
            return jsonify(
                status='Failure',
                message=e
            )


# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>/download', methods=['GET'])
def metrics_download(bot_id):
    if request.method == 'GET':
        try:
            filename1 = generateReportsWeekly(bot_id)
            file_name = str(bot_id) + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            logger.error("metrics error: ", str(e))
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/api/metrics/<bot_id>/generateFilteredReports', methods=['POST'])
def dashboard_download(bot_id):
    if request.method == 'POST':
        try:
            dates = request.json

            # check if from date and end date is existing
            from_date = dates.get('from_date', None)
            end_date = dates.get('end_date', None)

            if not 'from_date' in dates or not "end_date" in dates:
                return jsonify(
                    status="Failure",
                    msg="Both from date and end date is required"
                )

            try:
                from_date = datetime.datetime.strptime(dates['from_date'], '%Y-%m-%d')
                end_date = datetime.datetime.strptime(dates['end_date'], '%Y-%m-%d')
            except Exception as e:
                logger.error(str(e))
                return jsonify(
                    status="Failure",
                    msg="Dates should be in format yyyy-mm-dd"
                )

            if from_date > end_date:
                return jsonify(
                    status="Failure",
                    msg="End date should be greater than or equal to From date"
                )

            filename = generateDashboardExcel(bot_id, from_date, end_date)
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=filename)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/api/metrics/<bot_id>/convo', methods=['POST'])
def metrics_convo(bot_id):
    if request.method == 'POST':
        offset = request.json['offset']
        next = request.json['next']
        start = request.json['start_date']
        end = request.json['end_date']
        try:
            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.error(e)
            print('user info read error - ', e)

        try:
            returnedjson, total = get_ConvoLog(start, end, bot_id, offset, next)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    msg='Exception'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson,
                    total=total,
                    bot_name=bot_name
                )
        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/api/metrics/<bot_id>/graph', methods=['POST'])
def metrics_graph_data(bot_id):
    if request.method == 'POST':
        start = request.json['start_date']
        end = request.json['end_date']
        try:
            bot_name = get_by_id('bot', bot_id).get('bot name')
        except Exception as e:
            logger.error(e)
            print('user info read error - ', e)

        try:
            returnedjson = get_dashboard(start, end, bot_id, bot_name)
            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    bot_name=returnedjson['bot_name'],
                    graph_data=returnedjson['graph_data'],
                    table_data=returnedjson['table_data']
                )
        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )


@metrics_blueprint.route('/api/metrics/<bot_id>/convo/download', methods=['GET', 'POST'])
def metrics_download_convo(bot_id):
    if request.method == 'GET':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = generateReports_convo(start, end, bot_id)
            file_name = 'Chatbot_Report_convo' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = generateReports_convo(start, end, bot_id)
            file_name = 'Chatbot_Report_convo' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure


@metrics_blueprint.route('/api/metrics/<bot_id>/graph_download', methods=['POST'])
def metrics_download_graph(bot_id):
    if request.method == 'POST':
        try:
            start = request.json['start_date']
            end = request.json['end_date']
            filename = graph_download(bot_id, '', start, end)
            file_name = 'Chatbot_Report_Graph' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure
