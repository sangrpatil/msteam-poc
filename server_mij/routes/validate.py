from flask import Blueprint, jsonify

from utility.mongo_dao import get_by_id

validate_blueprint = Blueprint('validate', __name__, template_folder='templates')
import bson


@validate_blueprint.route('/validate/<string:type>/<string:id>', methods=['GET'])
def validate(type, id):
    if type == 'session':
        if not (bson.objectid.ObjectId.is_valid(id) and get_by_id('context', id)):
            return jsonify(result=1, msg='Existing Session ID is invalid')
        else:
            try:
                current_status = get_by_id('status', id)
                current_state = current_status['date_created']
                return jsonify(result=0, msg='Success')
            except Exception as e:
                print('TIME EXCEPTION - ', e)
                # existing_session = False
                return jsonify(result=1, msg='Existing Session is False')
    elif type == 'bot':
        if not (bson.objectid.ObjectId.is_valid(id) and get_by_id('bot', id)):
            return jsonify(result=1, msg='Bot ID is invalid')

        return jsonify(result=0, msg='Success')
    else:
        return jsonify(result=1, msg='Must send bot id or session id')
