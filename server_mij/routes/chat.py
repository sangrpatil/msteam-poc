from flask import Blueprint

chat_blueprint = Blueprint('chat', __name__, template_folder='templates')


@chat_blueprint.route('/chat/init')
def chat_init():
    return 'Chat initialized'


@chat_blueprint.route('/chat/<bot_id>/', methods=['GET', 'POST'])
def chat(bot_id):
    pass
