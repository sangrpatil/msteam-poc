from utility.mongo_dao import get_one as get, get_by_id, update,get_collection
from utility.conversation_logger import ConversationLogger
from fuzzywuzzy import fuzz
from operator import itemgetter
import datetime
import re
import numpy as np
import random
import os
import configparser
from flask_babel import gettext, refresh
from flask import current_app as app

#from utility.ElasticSearchExecute import SearchES
#from utility.TTS import synthesize_text
from utility.session_manager import get as get_context, modify as update_context, set as set_context
from utility.elasticsearch_query import SearchES
from utility.logger import logger
from utility.TTS import synthesize_text
from utility.gcp_places import retrieve_location
from utility.Sharepoint_API import *
"""
Has three main functions: init, execute, resume
The rest are utility functions
@init: initializes the session with an id and starts logging
@execute: executes bot tasks, i.e. displaying a message or buttons
@resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
"""
print("in execution engn")
app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))
# app.config['BABEL_DEFAULT_LOCALE'] = 'de'
# refresh()
survey_count = 4
msg_group=''

hybrid_intent_not_found = 'I am not interested in any of these options'
class ExecutionEngine():
    def __init__(self,session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id)
        #self.survey_count = 4

    """
    Function: executes bot actions by giving user prompts (buttons/text/query outputs)
    This will check whether the output should be text, buttons, etc
    @:param self
    @:param task_definition: this is the current state json
    """
    def get_task_definition(self, search_key, search_value, bot_id):
        global msg_group
        state_return = ''
        state_definition_all = get_by_id('bot', bot_id)['mapping']
        for state in state_definition_all:
            for key, value in state.items():
                if key == search_key and value == search_value:
                    state_return = state
                    msg_group=state['Group']
                    print("get task**", msg_group, search_key,search_value)
                    return state
        if state_return == '':
            return('None')

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json={},group=msg_group):
        guidedRestart = False
        if self.session['restartFlag'] == True and self.session['ES_UsedFlag'] == False:
            guidedRestart = True
        print("bot message",static_text)
        print('ASR FLAG VALUE IN BUILD RESPONSE = {}'.format(self.session['ASRFlag']))
        self.conv_log.bot_log(static_text,self.session['ASRFlag'],self.session['endSurveyFlag'],guidedRestart,group, self.session['user_id'])
        #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        if self.session['ASRFlag'] == True:
            static_text1 = re.sub(r'<[^<]+?>',' ',static_text)
            print('STATIC TEXT - ', static_text)
            tts_text = synthesize_text(static_text1)
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False
        if is_multi_message == True:
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio":tts_text,
                "disable_response":self.session['disable_input']
            })
            existing_json['is_multi'] = True
            return existing_json
        else:
            json_return = {
                "is_multi":False,
                "message":[{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": self.session['disable_input']
                }]
            }
            return json_return

    def execute(self, task_definition):
        print("IN EXECUTE FUNCTION** = ",task_definition['Group'])
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = False
        else:
            self.session['disable_input'] = False
        session_id = update_context(self.session_id,self.session)

        # check what the information about the current state is.
        # depending on what the interaction type is for the current state is, execute accordingly
        global msg_group
        msg_group=task_definition['Group']
        try:
            self.conv_log.update(task_definition['Task Name'],msg_group)  # To update the current state in status
            #self.session['botName'] = task_definition['bot_name']
            response_json = ''

            task_text = task_definition['Task Text']
            interaction = task_definition['Interaction Type']

            #dbData = task_definition['interaction']['database_details']
            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text,msg_group)#, dbData)

            # if it is url, then show the hyperlink to the user
            elif interaction == 'url':
                response_json = self.url_execute(task_definition, task_text,msg_group)#, dbData)

            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text,msg_group)#, dbData)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                print('INTERACTION TYPE WAS BUTTON')
                self.session['menuResult'] = False
                update_context(self.session_id, self.session)
                response_json = self.button_execute(task_definition, task_text,msg_group)#, dbData, interactionObj)

            # if it is a zipcode input, give the user an input to type in a zipcode
            elif interaction == 'zipcode':
                response_json = self.zip_execute(task_definition, task_text,msg_group)

        except Exception as e:
            print('Execute Method Error - ', str(e))
            response_json = 'Sorry we weren''t able to understand your intent. - EXECUTE Error'

        return response_json

    def resume(self, message, ASR, bot_id,group):
        print('IN RESUME FUNCTION** WITH GROUP = ',group)
        global msg_group
        msg_group=group
        #todo - should I make the returns uniform
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            if message.lower().strip() == 'nein' and self.session['disable_input'] == True:
                self.session['disable_input'] = True
            else:
                self.session['disable_input'] = False
        self.session['ASRFlag'] = ASR
        print('THE ASR VALUE IS = ', self.session['ASRFlag'], ASR)
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.session['ES_UsedFlag'] = False
        update_context(self.session_id,self.session)
        print("MESSAGE RECEIVED IN RESUME FUNCTION = {}".format(message))
        self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'],guidedRestartFlag=False,questionCategory=msg_group, user_id=self.session['user_id'])
        try:
            # first check if the message is small talk
            small_talk_response = self.small_talk_module(message)
            keyword_response = self.keyword_handler(message, bot_id)
            switch_group_response,grp_name = self.switch_group(message)
            if small_talk_response != 0:
                return small_talk_response
                #self.send_msg(smallTalkMsg)
                #self.send_interaction_msg(smallTalkMsg, "text")
            elif keyword_response != 0:
                print('KEYWORD FOUND')
                return keyword_response

            elif switch_group_response == 0:
                return self.check_group_content_support(grp_name)
            # next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restart_execute(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagFAQ'] is True:
                return self.restart_execute_FAQ(message, self.session_id, self.session, bot_id)
            elif self.session['endSurveyFlag'] is True:
                return self.survey_execute(message)
            # next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                return self.fuzzy_execute(message,bot_id)
            # next check if we're expecting a response from a hybrid guided
            elif self.session['guidedFlag'] is True:
                return self.guided_flag_execute(message, bot_id)
            # if it was none of the above, then it is a normal message
            else:

                # check if group is to be displayed
                print('COMPLEMENTRAY ACTION CHCK = {}'.format(self.get_task_definition('Task Name', get_by_id("status", self.session_id)["current_state"], bot_id)))
                if self.get_task_definition('Task Name', get_by_id("status", self.session_id)["current_state"], bot_id)['Complementary Action'] == 'group' and self.session['menuSelection'] == False:
                    self.session['menuSelection'] = True
                    self.session['menuResult'] = message
                    print('MENU RESULT SESSION = {}'.format(self.session['menuResult']))
                    update_context(self.session_id, self.session)
                    return self.check_group_content_support(message)
                    print("--in comp action---")

                elif self.session['subMenuSelection'] == False and self.session['menuSelection'] == True:
                    print('MESSAGE RECIEVED = {}'.format(message))

                    self.session['subMenuSelection'] = True
                    self.session['menuSelection'] == False
                    if message == "Sharepoint Document Search":
                        print(self.session['subMenuResult'])
                        self.session['subMenuResult'] = 'Sharepoint Document Search'
                        response = self.build_response("string",
                                                       "Escribe el tema sobre el cual quieres buscar una política.", "text",
                                                       "", group=msg_group)
                    else:
                        self.session['subMenuResult'] = 'Free-form FAQ'
                        response = self.build_response("string",
                                                       "¿en que te puedo ayudar?", "text",
                                                       "", group=msg_group)
                    update_context(self.session_id, self.session)
                    return response

                else:
                    print(self.session['subMenuResult'])
                    if self.session['subMenuResult'] == 'Sharepoint Document Search':
                        print("GOING TO EXECUTE SHAREPOINT")
                        response = self.sharepoint(message)
                        return response
                    else:
                        print("GOING TO DO FAQ EXECUTE")
                        es_analyzer = get_by_id('bot', bot_id)['es_analyzer']
                        response = self.faq_execute(message, bot_id, self.session['ASRFlag'], es_analyzer)
                        return response

                # language and analyzer
                #language = get_by_id('bot', bot_id)['language']
                es_analyzer = get_by_id('bot', bot_id)['es_analyzer']

                # first determine what the state is in the conversation, and what states can lead from here
                current_state = get_by_id('status', self.session_id)['current_state']

                state_definition = self.get_task_definition('Task Name',current_state,bot_id)

                action_type = state_definition['Action Type']
                #self.session['botName'] = state_definition['bot_name']
                next_task_ids = state_definition['Next Task IDs']
                next_possible_states_json = []
                next_possible_states_names = []
                try:
                    for id in next_task_ids:
                        state_option = self.get_task_definition('Task ID', id, bot_id)
                        next_possible_states_json.append(state_option)
                    for state_json in next_possible_states_json:
                        next_possible_states_names.append(state_json['Task Name'])
                except Exception as e:
                    next_possible_states_json = self.get_task_definition('Is Start', 'true', bot_id)
                    next_possible_states_names = next_possible_states_json['Task Name']
                # if the action type is code, take the user's message and apply it to the code
                if action_type == 'code': #todo
                    self.codeExecute(message, state_definition)
                # if the action if FAQ, then take the user's message and query elasticsearch with it
                elif action_type == 'FAQ':
                    response = self.faq_execute(message,bot_id, self.session['ASRFlag'],es_analyzer)
                    return response
                # if the action is hybrid guided, take the user's message and check the hybrid guided process
                elif action_type == 'hybridGuided':
                    response = self.hybrid_guided_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action is query, query the user's message against a database using DAO.py
                elif action_type == 'query': #todo
                    self.queryExecute(message, current_state)
                # if the action type is navigate, we are navigating from one branch to another in the conversation
                elif action_type == 'navigate':
                    response = self.navigate_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action type is location, we are taking in the user's zip code input
                elif action_type == 'location':
                    response = self.location_execute(message)
                    return response
        except Exception as e:
            print('Resume Method Error - ', str(e))
            logger.error(str(e)) #logger.info(string)
            return 'Sorry, we weren''t able to understand your intent - RESUME Error'



    #####################################################################################################################
    #####################################################################################################################
    """
    Execute Functions
    """
    #####################################################################################################################
    #####################################################################################################################

    def text_execute(self, task_definition, task_text, group):
        # static_values = task_definition['interaction']['values']
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            # self.send_msg(task_text + '<br>' + static_values[0])
            response_task_text = self.build_response('string',task_text,'text','',group=group)
            return response_task_text


    def button_execute(self, task_definition, task_text, group):
        """
        # type, static_text, interaction, interaction_elements
        # static_values = task_definition['interaction']['values']
        :param task_definition:
        :param task_text:
        :return:
        """
        currentState = task_definition['Task Name']
        task_text = re.sub(r'<>', currentState, task_text)

        if task_definition['Interaction Fetch From DB'] != "True":
            # self.send_msg(task_text + '<br>' + static_values[0])
            interaction_values = task_definition['Interaction Values']
            print("OLDER INTERACION VALUE = {}".format(interaction_values))
            if self.session['menuSelection'] == False:
                group_list = []
                groups = get_collection("groups")
                for index, group_detail in enumerate(groups):
                    group_list.append(group_detail['name'])
            else:
                print('MENU SELECTION WAS TRUE')
                group_list = []


            interaction_values = group_list
            task_text = self.build_response('list', task_text, 'button_vertical', interaction_values,group=group)
            return task_text

    # if interaction is none, check if query database or not
    # just show text and then restart
    def none_execute(self, task_definition, task_text,group): #, dbData):
        # global results
        static_values = task_definition['Interaction Values']
        if task_definition['Interaction Fetch From DB'].lower() != "true":  # print from flashmessage if interaction = none
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)

            response = self.build_response("string",message,"text","",group=group)
            response_json_2 = self.restart(response)
            return response_json_2



    # if interaction is url, check if query database or not
    # then display url and restart
    def url_execute(self, task_definition, task_text,group):  # , dbData):
        interaction_url = task_definition['Interaction Values'][0]
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)
            # response_json = self.build_response("string", message, "text", "")
            response = self.build_response("hyperlink", gettext("Click Here"), "url", interaction_url,group=group)

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)

            next_state = state_definition['Next Task IDs'][0]
            next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            # response_json_2 = self.restart_execute("yes",self.session_id,self.session,self.session['bot_id'])

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response


    def zip_execute(self, task_definition, task_text,group):
        response = self.build_response("string",task_text, "location_zip","",group=group)
        return response

    #####################################################################################################################
    #####################################################################################################################
    """
    Resume Functions
    """
    #####################################################################################################################
    #####################################################################################################################

    def switch_group(self,message):

        switch_keywords = ['cambiar a','Switch']
        grp_list = []
        groups = get_collection("groups")
        for index, group_detail in enumerate(groups):
            grp_list.append(group_detail['name'])
        for keywords in switch_keywords:
            if keywords.lower() in message.lower():
                for grp_names in grp_list:
                    if grp_names.lower() in message.lower():
                        return 0,grp_names

        return 1,'No grp'
        pass

    def small_talk_fuzzy(self, msg, search_list):
        for item in search_list:
            if fuzz.ratio(msg, item) > 65:
                return 1
        return 0

    def small_talk_module(self,msg):
        msg = re.sub(r'[^\w\s]','',msg)
        # greetings = [gettext('hello'), gettext('hi'), gettext('what''s up'), gettext('good afternoon'),
        #              gettext('good morning'), gettext('good day'), gettext('good evening')]
        # question_questions = [gettext('I would like to ask a question'), gettext('I want to ask you a question'),
        #                       gettext('I have a doubt'), gettext('I want to ask you something'),
        #                       gettext('I want to ask you questions'), gettext('I want to consult you')]
        # inquiries = [gettext('how are you'), gettext('how are you doing'), gettext('how is your day')]
        # greeting_response = [gettext('Hi there! How can I help you')]
        # question_question_responses = [gettext('With pleasure, tell me how I can help you.')]
        # inquiriesResponse = [gettext('I\'m doing well! How can I help you today?'),
        #                      gettext('I\'m feeling great! How can I help you today?')]

        greetings = ['hello', 'hola', 'que tal', 'buenas tardes',
                     'buenos días', 'buen día', 'buenas noches','hola cobi','hola cobi!']
        question_questions = ['quiero hacer una pregunta', 'quero hacerte una pregunta',
                              'tengo una duda', 'quiero preguntarte',
                              'deseo hacerte una pregunta', 'quiero consultarte']
        inquiries = ['cómo estás', 'cómo estás', 'cómo va su día']
        greeting_response = ['Hola, espero estés muy bien, ¿en qué puedo servirte?']
        question_question_responses = ['Con gusto, dime en qué puedo ayudarte']
        inquiriesResponse = ['¡Lo estoy haciendo bien! ¿Cómo puedo ayudarte hoy?',
                             '¡Me siento muy bien! ¿Cómo puedo ayudarte hoy?']
        global msg_group
        if msg.lower().strip() in greetings or self.small_talk_fuzzy(msg,greetings) == 1:
            response = self.build_response("string", random.choice(greeting_response), "text", "",group=msg_group)
            return response
        elif msg.lower().strip() in question_questions:
            response = self.build_response("string", random.choice(question_question_responses), "text", "",group=msg_group)
            return response
        elif msg.lower().strip() in inquiries:
            response = self.build_response("string",random.choice(inquiriesResponse),"text","",group=msg_group)
            return response
        else:
            return 0
    def keyword_handler(self, message, bot_id):
        global msg_group
        help_config = app_config.get('GENERAL', 'help_keywords')
        restart_config = app_config.get('GENERAL', 'restart_keywords')
        goback_config = app_config.get('GENERAL', 'goback_keywords')

        help_keywords = help_config
        restart_keywords = restart_config
        goback_keywords = goback_config

        if message.lower().strip() in help_keywords:
            help_task = self.get_task_definition("Task Name","help",bot_id)
            help_text = help_task['Task Text']
            return self.build_response("string",help_text,"text","",group=msg_group)
        elif message.lower().strip() in restart_keywords:
            
            print("RESTART KEYWORD FOUND")
            restart = self.get_task_definition("Is Start","true",bot_id)
            self.session['menuSelection'] = False
            update_context(self.session_id, self.session)
            return self.execute(restart)
        elif message.lower().strip() in goback_keywords:
            current_state = get_by_id('status', self.session_id)['current_state']
            prior_state = get_by_id('status', self.session_id)['prior_state']
            history = get_by_id('status', self.session_id)['history']
            history = history[:-1] #pop out the last element
            if (len(history) == 0):
                return self.build_response("string","Lo siento, no hay nada para volver.", "text", "",group=msg_group)
            previous_state = history[-1] #get the last element
            history = history[:-1] #pop out again because will be replaced in conversation logger
            update('status', self.session_id, {'current_state': previous_state, 'prior_state': current_state, 'Group':msg_group,'history': history,"date_created": datetime.datetime.now()})
            previous_state_json = self.get_task_definition('Task Name', previous_state, bot_id)
            return self.execute(previous_state_json)
        else:
            return 0


    def survey_execute(self, message):

        # global surveyFlagNum
        # global surveyFlagComm
        # global endSurveyFlag
        print('Executing Survey Execute Method with Message = {}'.format(message))
        if self.session['surveyFlagNum'] is True:
            if message in '12345':
                # log survey
                self.session['endSurveyFlag'] = True
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.survey_create(2, '')
                return response
            else:
                # self.send_msg(gettext('Please choose an option 1-5.'))
                # self.send_interaction_msg(gettext('Please choose an option 1-5.'), "text")
                response = self.build_response("string", "Por favor, elija una opción 1-5.", "text", "",
                                               group=msg_group)
                response_json_2 = self.survey_create(1, response)
                return response_json_2
        elif self.session['surveyFlagComm'] is True:
            self.session['endSurveyFlag'] = False
            self.session['surveyFlagComm'] = False
            self.session['surveyFlagNum'] = False
            update_context(self.session_id, self.session)
            response = self.build_response("string", "¡Gracias por su participación!", "text", "", group=msg_group)
            # self.send_msg(gettext('Thank you for your participation!'))
            # self.send_interaction_msg(gettext('Thank you for your participation!'), "text")
            # self.send_endsignal() #todo - how do we disable buttons to end
            return response

        ##############
        
    def survey_create(self,state,json):
        # global endSurveyFlag
        # global surveyFlagNum
        # global surveyFlagComm
        global msg_group
        self.session['endSurveyFlag'] = True
        update_context(self.session_id, self.session)
        if state ==1:
            self.session['surveyFlagNum'] = True
            update_context(self.session_id, self.session)
            options = [1,2,3,4,5]
            response = self.build_response("list","En una escala de 1 a 5, ¿qué tan satisfecho estaba con su servicio hoy?","button_horizontal",options,True,json,group=msg_group)
            return response
        elif state == 2:
            self.session['surveyFlagComm'] = True
            update_context(self.session_id,self.session)
            response = self.build_response("string", "¡Gracias por su participación!", "text", "", group=msg_group)
            #response = self.build_response("list","¿Te gustaría dejar algún comentario?","button_horizontal",[gettext("Yes"), gettext("No")])
            return response


    # use Fuzzywuzzy to check for fuzzysearch options for Navigate options
    # if the match is > 45% similar, return as option for user to pick from
    def fuzzy_search(self, nextStates, message):
        # global fuzzyFlag
        # global botName
        # global suggestion
        global msg_group
        options = []
        #interactionObj = Interaction()
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]
        if ratio > int(os.environ.get('FUZZY_RATIO') or '45'):
            # check if task is a navigate task, if yes, check message value, see if pass
            response = self.build_response("list",gettext('We couldn\'t find that. Do you mean: ') + suggestionOutput,"button_vertical",[gettext("Yes"), gettext("No")],group=msg_group)
            self.session['fuzzyFlag'] = True
            update_context(self.session_id, self.session)
            return response
        else:
            # self.send_msg(gettext('Sorry we don\'t have an answer for that. Could you try again?'))
            response = self.build_response("string",gettext('Sorry we don\'t have an answer for that. Could you try again?'),"text","",group=msg_group)
            return response

    # If FuzzyFlag is True, then execute this
    # check if the fuzzysearch came from a query or code
    # if not, then check if the suggestion picked comes from a next state
    # if not what they are looking for, return to start
    def fuzzy_execute(self, message, bot_id):
        global msg_group
        if str(message).lower() == 'si':

            self.session['fuzzyFlag'] = False
            get_next_state = self.get_task_definition('Task Name',self.session['suggestion'],bot_id)
            response = self.execute(get_next_state)
            update_context(self.session_id,self.session)
            return response
            pass
        elif message.lower() == gettext('no'):
            self.session['fuzzyFlag'] = False
            update_context(self.session_id,self.session)
            next_state = self.get_task_definition('Is Start','true',bot_id) #self.get_task_definition("Is Start","true", bot_id)
            response = self.execute(next_state)
            return response
        else:
            response = self.build_response("list",gettext('Please choose yes or no.'),"button_horizontal",["Si", gettext("No")],group=msg_group)
            return response
    def navigate_execute(self, message, next_possible_states_names, bot_id):
        found = False
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                # get_next_state = get('state', 'state_name', state) #get by Task ID
                get_next_state = self.get_task_definition('Task Name', state, bot_id)
                print('NAVIGATE NEXT STATE - ', get_next_state)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True

                    response = self.execute(get_next_state)
                    return response
                    # self.execute(get_next_state)
                    # pass
        if found == False:
            # do Fuzzy search here
            response = self.fuzzy_search(next_possible_states_names, message)
            return response
            pass

    def restart_execute(self, message, session_id, session, bot_id):
        # global restartFlag
        # global queryFlag

        if message.strip().lower() == 'si':
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            get_start = self.get_task_definition("Is Start","true", bot_id)
            start_response = self.execute(get_start)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon!'), "text", "", False,group=msg_group)
            # ,["list",gettext('On a scale of 1-5, how satisfied were you with your service today?'),"button",[gettext('Yes'),gettext('No')]])
            #response_final = self.build_response("list", gettext('On a scale of 1-5, how satisfied were you with your service today?'), "button",[gettext('Yes'), gettext('No')], True, response_first)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            # interactionObj = Interaction()
            # html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
            # self.send_msg(gettext('Would you like to continue? Please choose yes or no.') + '<br>' + html)
            # self.send_interaction_msg([gettext('Would you like to continue? Please choose yes or no.'), gettext("Yes"), gettext("No")],"button")
            response = self.build_response("list", "¿Te gustaria continuar? Por favor, elija sí o no.", "button_horizontal",["Si", gettext("No")],group=msg_group)
            return response

    def restart_execute_FAQ(self, message, session_id, session, bot_id):
        # global restartFlag
        # global queryFlag
        if message.strip().lower() == 'si':
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            # get_start = self.get_task_definition("Is Start", "true", bot_id)
            # start_response = self.execute(get_start)
            start_response = self.build_response("string","¿Con qué otras preguntas puedo ayudarte?","text","",False,group=msg_group)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", "Gracias por chatear, espero volver a hablar pronto", "text", "",False,group=msg_group)
            # ,["list",gettext('On a scale of 1-5, how satisfied were you with your service today?'),"button",[gettext('Yes'),gettext('No')]])
            # response_final = self.build_response("list", gettext('On a scale of 1-5, how satisfied were you with your service today?'), "button",[gettext('Yes'), gettext('No')], True, response_first)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            # interactionObj = Interaction()
            # html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
            # self.send_msg(gettext('Would you like to continue? Please choose yes or no.') + '<br>' + html)
            # self.send_interaction_msg([gettext('Would you like to continue? Please choose yes or no.'), gettext("Yes"), gettext("No")],"button")
            response = self.build_response("list", "¿Te gustaria continuar? Por favor, elija sí o no.",
                                           "button_horizontal", ["Si", gettext("No")],group=msg_group)
            return response

    # if action type is hybrid, execute this
    # first check if the message is a 1:1 match with taskNames
    # if it is, execute the corresponding state
    # if it is not, check message against keywords. For each keyword matching in the message, add a point
    # return taskNames with highest keyword scores for using to pick from
    def hybrid_guided_execute(self, message, next_possible_states_names, bot_id):
        # global guidedFlag
        found = False

        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                #get_next_state = get('state', 'state_name', state)
                get_next_state = self.get_task_definition('Task Name',state, bot_id)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    self.conv_log.updateIntentFoundCounter(message)
                    response = self.execute(get_next_state)
                    return response
        if found == False:  # so here instead of a fuzzysearch, do the keyword search
            stateScores = []
            finalStates = []
            possibleNonAliasStates = []
            for state in next_possible_states_names:
                score = 0
                stateinfo = self.get_task_definition('Task Name', state, bot_id)
                # print('STATE INFO HYBRID - ', stateinfo)
                alias = stateinfo['Alias']
                if alias.lower() == 'false':
                    keywords = stateinfo['Keywords']
                    if len(keywords) > 0:
                        for key in keywords:
                            if key.lower().strip() in message.lower().strip():
                                score += 1
                                # if key.lower().strip() in savedKey.lower().strip() -> score+=1
                        stateScores.append(score)
                        possibleNonAliasStates.append(state)
            print('STATE SCORES - ', stateScores)
            print('STATE ALIASES - ', possibleNonAliasStates)
            # pick the state names with the highest scores, return those as options
            highScore = max(stateScores)
            print('HIGH SCORE - ', highScore)
            if highScore != 0:
                for i in np.arange(0, len(stateScores)):
                    if stateScores[i] == highScore:
                        finalStates.append(possibleNonAliasStates[i])
            print('FINAL STATES - ', finalStates)
            if len(finalStates) != 0:
                print('babel test- ', gettext(hybrid_intent_not_found))
                finalStates.append(gettext(hybrid_intent_not_found))
                response = self.build_response("list",gettext('Are you interested in any of these options?'),"button_vertical",finalStates,group=msg_group)
                self.session['guidedFlag'] = True
                self.session['hybrid_user_intent'] = message
                session_id = update_context(self.session_id, self.session)
                print('RESPONSE - ', response)
                return response
            else:
                response = self.build_response("string",gettext('Sorry, we may not have information for that. Can you try again?'),"text","",group=msg_group)
                return response

    # if expecting a response from Hybrid guided, check here if it leads to a valid state
    def guided_flag_execute(self, message, bot_id):
        get_next_state = self.get_task_definition('Task Name', message, bot_id)
        if str(get_next_state) != 'None':
            response = self.execute(get_next_state)
            self.session['guidedFlag'] = False
            session_id = update_context(self.session_id, self.session)
            self.conv_log.updateIntentFoundCounter(self.session['hybrid_user_intent'])
            return response
        elif message == gettext(hybrid_intent_not_found):
            self.conv_log.updateIntentNotFound(self.session['hybrid_user_intent'])
            response = self.build_response("string", gettext('I\'m sorry none of those options were helpful. Would you like to try rephrasing your question?'), "text", "",group=msg_group)
            response_json_2 = self.restart(response)
            self.session['guidedFlag'] = False
            update_context(self.session_id, self.session)
            return response_json_2
        else:
            response = self.build_response("string",gettext('Please choose one of the options'),"text","",group=msg_group)
            return response

    def faq_execute(self, message,bot_id, asr_flag, es_analyzer):
        search = SearchES(self.session_id, bot_id)
        self.session['ES_UsedFlag'] = True
        print('ASF FLAG VALUE IN FAQ EXECUTE FUNCTION = {} and ASR FALG VALUE IN SESSION = {}'.format(asr_flag, self.session['ASRFlag']))
        update_context(self.session_id, self.session)
        response_flag, response = search.execute(message, bot_id, asr_flag,es_analyzer)
        print('RESPONSE RECEIVED FROM ES EXECUTE = {}'.format(response))
        print('ES RESTART COUNTER IN EXECUTION ENGINE = {}'.format(self.session['es_restart_counter'])) 
        if response_flag == 0:
            print('FAQ RESPONSE FROM ES FILE WAS 0. GOING TO RESTART')
            self.session['ASRFlag'] = False
            self.session['es_restart_counter'] = 0
            response_json_2 = response #self.restart_FAQ(response)
            update_context(self.session_id, self.session)
            print('ES RESTART COUNTER IN EXECUTION ENGINE AFTER COUNTER RESET= {}'.format(self.session['es_restart_counter']))
            return response_json_2
        # self.restart()
        elif response_flag == 1:
            #display the ES answer
            print('FAQ RESPONSE FROM ES FILE WAS 1. NOT RESTARTING. RESPONSE TO RETURN FROM FAQ EXECUTE - ', response)
            return response
        elif response_flag == 2:
            state = self.get_task_definition('Is Start', 'true',bot_id)
            response = self.execute(state)
            return response
        pass

    def location_execute(self, message):
        #message should be a 5 digit zip code
        pharmacy_data = retrieve_location(message,"pharmacy",5)
        if pharmacy_data['status'] == 'Success':
            pharmacy_list = pharmacy_data['message']
            response = self.build_response("list", gettext("Please select any of the stores to view it on a map"),"location_maps", pharmacy_list,group=msg_group)  # [[name, address],[name, address]]
            # response_2 = self.restart(response)

            response_json_2 = self.restart_execute("yes",self.session_id,self.session,self.session['bot_id'])
            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response
        else:
            response = self.build_response("string", gettext("Sorry, we do not have pharmacies within 50 miles of your area."),"text", "",group=msg_group)
            response_json_2 = self.restart_execute("yes", self.session_id, self.session, self.session['bot_id'])
            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response


    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart(self, json):
        # global restartFlag
        # global botName
        #interactionObj = Interaction()
        #html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
        response = self.build_response("list", "¿Te gustaria continuar? Por favor, elija Sí o No.","button_horizontal",["Si",gettext("No")], True, json,group=msg_group)
        self.session['restartFlag'] = True
        session_id = update_context(self.session_id, self.session)
        return response

    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart_FAQ(self, json):
        # global restartFlag
        # global botName
        # interactionObj = Interaction()
        # html = interactionObj.getYesNoButtonHTML([gettext("Yes"), gettext("No")])
        response = self.build_response("list", "¿Te gustaria continuar? Por favor, elija Sí o No.", "button_horizontal",
                                       ["Si", gettext("No")], True, json,group=msg_group)
        self.session['restartFlagFAQ'] = True
        session_id = update_context(self.session_id, self.session)
        return response

    def sharepoint(self, message):
        doc_list = sharepoint(message)

        print('DOCUEMNT LIST FROM SHAREPOINT = {}'.format(doc_list))
        if len(doc_list) == 0:
            response = self.build_response("string", "No hay resultados de sharepoint para su consulta.",
                                           "text", "", group=msg_group)

        else:
            # response = self.build_response('list', gettext('Please select one of the content support'),
            #                                'button_vertical', doc_list, group=msg_group)
            # response = self.build_response("list", gettext("Please select any of the stores to view it on a map"),"location_maps", pharmacy_list,group=msg_group)  # [[name, address],[name, address]]
            response = self.build_response('list', 'Los siguientes son los documentos de Sharepoint que contienen su consulta. Haga clic en el botón para ir al documento.',
                                           'location_maps', doc_list, group=msg_group)

        return response

    def check_group_content_support(self, group):
        print('CHECKING CONTENT SUPPORT for GROUP = {}'.format(group))
        content = get('groups', 'name', group)['content_support']
        if ["Free-form FAQ", "Sharepoint Document Search"] == content:
            response = self.build_response('list', gettext('Please select one of the content support'),
                                           'button_vertical', content, group=msg_group)
        elif len(content) == 1 and content[0] == "Free-form FAQ":
            self.session['subMenuSelection'] = True
            # response = self.faq_execute(self.message, self.bot_id, self.session['ASRFlag'], self.es_analyzer)
            self.session['subMenuResult'] = 'Free-form FAQ'
            self.session['menuResult'] = group
            response = self.build_response("string", "¿en que te puedo ayudar?", "text", "",
                                           group=group)
        elif len(content) == 1 and content[0] == "Sharepoint Document Search":
            self.session['subMenuSelection'] = True
            self.session['subMenuResult'] = 'Sharepoint Document Search'
            self.session['menuResult'] = group
            # response=self.sharepoint(self)
            response = self.build_response("string", "Escribe el tema sobre el cual quieres buscar una política.", "text",
                                           "", group=group)
        update_context(self.session_id, self.session)
        print('RETURNING CONTENT SUPPORT for GROUP = {}'.format(group))
        return response








