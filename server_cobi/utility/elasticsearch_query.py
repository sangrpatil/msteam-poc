from flask_socketio import SocketIO, send, emit
from flask_babel import gettext
from elasticsearch import Elasticsearch
#from utility.Interaction import Interaction
#from google.cloud import translate
from flask import session
import json
from utility.conversation_logger import ConversationLogger
from utility.TTS import synthesize_text
from utility.session_manager import get as get_context, modify as update_context
import os
import re
import math
import configparser
from utility.logger import logger

#GLobal variables
msg_grp = ''

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))
restart_config_counter = app_config.get('ELASTICSEARCH', 'response_count')
restart_config_counter = int(restart_config_counter)
elasticsearch_host = os.environ.get('ES_HOST') or 'localhost'
elasticsearch_port = os.environ.get('ES_PORT') or '9200'
elasticsearch_threshold = os.environ.get('ES_THRESHOLD') or '0.94'

class SearchES():

    def __init__(self, session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id)

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json={},group=msg_grp):
        #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        print('Going to Log the message for group = {}'.format(group))
        self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], guidedRestartFlag=False,questionCategory=group, user_id=None)
        print('ASR FLAG VALUE IN BUILD RESPONSE IN ELASTICSEARCH FILE = {}'.format(self.session['ASRFlag']))
        if self.session['ASRFlag'] == True:
            static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
            tts_text = synthesize_text(static_text1)
            # tts_text = "I am sorry but I am not able to play the audio right now"
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False
        if is_multi_message == True:
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio":tts_text
            })
            existing_json['is_multi'] = True
            return existing_json
        else:
            json_return = {
                "is_multi":False,
                "message":[{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text
                }]
            }
            return json_return

    #based off feedback, update weight of elasticsearch answers
    def update_votes(self,score_answer, botName):

        updated_score_answer = score_answer
        new_source = json.loads(json.dumps(updated_score_answer[2]))

        l = {"doc": {"qid": updated_score_answer[1], "q": new_source['q'], "a": updated_score_answer[3],
                     "votes": int(new_source['votes']) + 1}}

        # print('BODY OF PARTIAL DATA UPDATE')
        # print(l)

        # l = {"qid": score_answer[0], "q": score_answer[1]['q'], "a":  score_answer[1]['a'], "votes":  int(score_answer[1]['votes']) + 1}
        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])
        es.update(index=str(botName).lower()+self.session['menuResult'].replace(" ", "").lower(), doc_type=str(botName).lower()+self.session['menuResult'].replace(" ", "").lower(), id=score_answer[1],body=l)

    #search for answer within english elasticsearch
    def search_es(self,question,botName):
        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

        data = {
            "query": {
                "function_score": {
                    "query": {

                        "match": {
                            'q': question
                        }
                    },
                    "field_value_factor": {
                        "field": "votes",
                        "modifier": "log2p"
                    }
                }
            }
        }
        response = es.search(index=str(botName).lower(), body=data)
        score = ''
        answer = ''
        source = ''
        id = ''
        score_answer = []
        for items in response['hits']['hits']:
            score = items['_score']
            source = items['_source']
            id = items['_id']
            answer = source['a']
            votes = source['votes']
            # print('UPDATED SCORE = {}'.format(score))
            # print('VOTES = {}'.format(votes))
            # print('LOG VALUE = {}'.format(math.log10(votes + 2)))
            # print('ORIGINAL SCORE VALUE = {} '.format(((score) / (math.log10(votes + 2)))))
            if ((score) / (math.log10(votes + 2))) < float(elasticsearch_threshold):
                print('This answer score is below threshold - Skipping this answer = {}'.format(answer))
                continue
            else:
                print('APPENDING THIS ANSWER = {}'.format(answer))
                score_answer.append([score, id, source, answer, votes])
        # print(response['hits']['hits'])
        # print(len(score_answer))
        # print(score_answer)
        return score_answer

    #search for answers within Spanish elasticsearch
    def search_es_non_english(self, question, botName, elasticsearch_analyzer):

        # print('BotName received from execute function = ' + botName)
        print('GOING TO EXECUTE NON-ENGLISH ES SEARCH. CURRENT GROUP = {}'.format(self.session['menuResult']))
        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

        # data = {
        #     "query": {
        #         "function_score": {
        #
        #             "query": {
        #
        #                 "multi_match": {
        #                     "type": "most_fields",
        #                     "query": question,
        #                     "fields": ["q", str(elasticsearch_analyzer)]
        #
        #                 }
        #             },
        #
        #             "field_value_factor": {
        #                 "field": "votes",
        #                 "modifier": "log2p"
        #             }
        #
        #         }
        #     }
        # }

        data = {
            "query": {
                "function_score": {

                    "query": {

                        "multi_match": {
                            "type": "phrase",
                            "query": question,
                            "fields": ["q"]

                        }
                    },

                    "field_value_factor": {
                        "field": "votes",
                        "modifier": "log2p"
                    }

                }
            }
        }



        response = es.search(index=str(botName).lower()+self.session['menuResult'].replace(" ", "").lower(), body=data)
        score = ''
        answer = ''
        source = ''
        id = ''

        print('RESPONSE FROM ELASTICSEARCH AFTER EXACT MATCH= {}'.format(response))

        if response['hits']['total'] == 0:
            print('No Exact match found. Going to execute Best Match')
            data = {
                "query": {
                    "function_score": {

                        "query": {

                            "multi_match": {
                                "type": "best_fields",
                                "query": question,
                                "fields": ["q"]

                            }
                        },

                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p"
                        }

                    }
                }
            }
            response = es.search(index=str(botName).lower() + self.session['menuResult'].replace(" ", "").lower(),body=data)
            print('RESPONSE FROM ELASTICSEARCH AFTER BEST FIELDS MATCH= {}'.format(response))
        # score_answer is - [score,id,source,answer]
        score_answer = []
        for items in response['hits']['hits']:
            score = items['_score']
            source = items['_source']
            id = items['_id']
            answer = source['a']
            votes = source['votes']
            print('UPDATED SCORE = {}'.format(score))
            print('VOTES = {}'.format(votes))
            # print('LOG VALUE = {}'.format(math.log10(votes + 2)))
            print('ORIGINAL SCORE VALUE = {} '.format(((score) / (math.log10(votes + 2)))))
            if ((score) / (math.log10(votes + 2))) < float(elasticsearch_threshold):
                print('This answer score is below threshold - Skipping this answer = {}'.format(answer))
                continue
            else:
                print('APPENDING THIS ANSWER = {}'.format(answer))
                score_answer.append([score,id, source, answer, votes])
        print("RESPONSE ELASTICSEARCH = {}".format(response['hits']['hits']))
        # print(len(score_answer))
        # print('SCORE ANSWER LIST')
        # print(score_answer)
        # score_answer.sort(key=lambda x: int(x[0]), reverse=True)
        return (score_answer)


    #take users message and determine what to do with it
    #will either be feedback yes/no or will be a message to query
    def execute(self,message,BotName, ASR, es_analyzer): #message,bot_id, self.session['ASRFlag'],es_analyzer

        print('ASR FLAG VALUE IN ES EXECUTE FUNCTION = {}'.format(self.session['ASRFlag']))
        print("INSIDE ES EXECUTE FUNCTION. SESSION VARIABLES ARE = {}".format(self.session))
        if message == "Go Back":
            return 2, ''
        if self.session['feedbackFlag_ES'] == True:
            if message.lower().strip() == 'si':
                print('USER PRESSED YES - ANSWER WAS HELPFUL. GOING TO UPDATE VOTE')
                updated_score_answer = self.session['es_reponse'][0]

                self.update_votes(updated_score_answer,BotName)

                print('SCORE ANSWER AFTER VOTE UPDATE')
                response = self.build_response("string","¡Gracias! Su comentario ha sido añadido.","text","",group=self.session['menuResult'])
                # self.send_msg(gettext('Thank you! Your feedback has added.'))
                self.session['suggestCounter_ES'] = 0
                self.session['feedbackFlag_ES'] = False
                print('ES RESTART COUNTER = {}'.format(self.session['es_restart_counter']))
                print('restart_config_counter = {}'.format(restart_config_counter))
                if self.session['es_restart_counter'] > restart_config_counter:
                    print('MAXIMUMUM COUNTER REACHED. ASKING TO RESTART')
                    self.session['es_restart_counter']=0
                    update_context(self.session_id, self.session)
                    return 0,response
                print('MAXIMUM COUNTER NOT REACHED. SENDING 1 to EXECUTION ENGINE')
                update_context(self.session_id, self.session)
                self.intentFoundRecord(self.session['currentQuestion'],self.session['menuResult'])
                return 1, response
            elif message.lower().strip() == gettext('no'):
                print('USER PRESSED NO - ANSWER WAS NOT HELPFUL. GET NEXT ANSWER')
                self.session['suggestCounter_ES'] += 1
                update_context(self.session_id, self.session)
                if self.session['suggestCounter_ES'] < 3:
                    try:
                        if self.session['es_reponse']:
                            del self.session['es_reponse'][0]
                            response = self.build_response("string",self.session['es_reponse'][0][3],"text","",group=self.session['menuResult'])
                            response_json_2 = self.feedback(response)
                            self.session['feedbackFlag_ES'] = True
                            update_context(self.session_id, self.session)
                            return 1, response_json_2
                            # return code,message
                    except Exception as e:
                        self.session['feedbackFlag_ES'] = False
                        self.intentNotFoundRecord(self.session['currentQuestion'],self.session['menuResult'])
                        self.session['suggestCounter_ES'] = 0
                        response_json = self.build_response("string",
                                                            "Lo sentimos, no pudimos encontrar lo que buscabas. Su comentario ha sido observado.",
                                                            "text", "",group=self.session['menuResult'])
                        if self.session['es_restart_counter'] > restart_config_counter:
                            print('MAXIMUMUM COUNTER REACHED IN EXCEPT BLOCK OF NO. ASKING TO RESTART')
                            self.session['es_restart_counter'] = 0
                            update_context(self.session_id, self.session)
                            return 0, response
                        print('MAXIMUM COUNTER NOT REACHED IN EXCEPT BLOCK OF NO. SENDING 1 to EXECUTION ENGINE')
                        update_context(self.session_id, self.session)

                        return 1, response_json
                else:
                    # self.send_msg(gettext('Sorry we weren''t able to find what you were looking for. Your feedback has been noted.'))
                    self.intentNotFoundRecord(self.session['currentQuestion'],self.session['menuResult'])
                    self.session['suggestCounter_ES'] = 0
                    self.session['feedbackFlag_ES'] = False
                    response_json = self.build_response("string",
                                                        "Lo sentimos, no pudimos encontrar lo que buscabas. Su comentario ha sido observado.",
                                                        "text", "",group=self.session['menuResult'])
                    if self.session['es_restart_counter'] > restart_config_counter:
                        self.session['es_restart_counter'] = 0
                        update_context(self.session_id, self.session)
                        return 0,response_json
                    update_context(self.session_id, self.session)

                    return 1, response_json
            else:
                self.session['currentQuestion'] = message
                self.session['feedbackFlag_ES'] = False
                update_context(self.session_id, self.session)
                if es_analyzer == 'English':
                    response_json = self.checkResponse(message, BotName)
                    return 1, response_json
                else:
                    response_json = self.checkResponse_non_english(message, BotName, es_analyzer)
                    #if self.session['es_restart_counter'] > restart_config_counter:
                        #self.session['es_restart_counter'] = 0
                        #update_context(self.session_id, self.session)
                        #return 0,response_json
                    return 1, response_json

        else:
            print("RECCIEVED USER QUESTION")
            self.session['currentQuestion'] = message
            update_context(self.session_id, self.session)
            if es_analyzer == 'English':
                response_query = self.checkResponse(message, BotName)
                return 1, response_query
            else:
                response_query = self.checkResponse_non_english(message, BotName, es_analyzer)
                #if self.session['es_restart_counter'] > restart_config_counter:
                    #self.session['es_restart_counter'] = 0
                    #update_context(self.session_id, self.session)
                    #return 0,response_query

                return 1, response_query
        # else:
        #     self.send_msg(gettext('Sorry, could you try again?'))
        #     #record the user input that could not be found
        #     pass

    #send message and send TTS audio
    def send_msg(self, msg):
        # global ASRFlag
        endSurveyFlag = False
        self.conv_log.bot_log(msg, self.session['ASRFlag'], endSurveyFlag)
        emit('message', msg)
        # msg = re.sub(r'<br>', ' ', msg)
        # msg = re.sub(r'<button .*', '', msg)
        # self.sendAudio(msg)

    # take message and send to TTS function from TTS.synthesize_text()
    # will only do this if ASRFlag is true, meaning ASR was called, and if it is the 1st message to be sent (to avoid sending multiple messages)
    # def sendAudio(self, msg):
    #     # global ASRFlag
    #     # global audioSentFlag
    #     if session['ASRFlag'] == True and session['audioSentFlag_ES'] == False:
    #         audio = synthesize_text(msg) #msg = 'how can i help you'
    #         emit('audioTTSOutput', audio)
    #         session['audioSentFlag_ES'] = True

    #check feedback, whether or not user was satisfied with the answer returned by elasticsearch
    def feedback(self, input_json):
        json = self.build_response("list","¿Fue útil esto?","button_horizontal",['Si',gettext('No')],True,input_json)
        self.session['feedbackFlag_ES'] = True
        update_context(self.session_id, self.session)
        return json

    #if the user is actually
    def checkResponse(self, message, BotName):
        # global es_reponse
        # global feedbackFlag
        # print('USER ASKED A QUESTION')
        print('USER ASKED A QUESTION ', self.session['es_restart_counter'])
        self.session['es_restart_counter'] += 1
        self.session['es_reponse'] = self.search_es(message, BotName)
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        if len(self.session['es_reponse']) > 0:
            # print('ANSWER')
            # print(self.session['es_reponse'][0][3])
            #
            # print('SCORE')
            # print(self.session['es_reponse'][0][0])
            #
            # print('ID')
            # print(self.session['es_reponse'][0][1])
            #
            # print('SOURCE')
            # print(self.session['es_reponse'][0][2])

            response_json = self.build_response("string",self.session['es_reponse'][0][3],"text","",group=self.session['menuResult'])
            response_json_2 = self.feedback(response_json)

            # self.feedback()
            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)
            # return (self.session['es_reponse'][0][1])
            return response_json_2

        else:
            response_json = self.build_response("string",gettext('Sorry, I can''t find an answer for that. Could you try again?'),"text","",group=self.session['menuResult'])
            # return 1
            self.intentNotFoundRecord(message,self.session['menuResult'])
            return response_json

    def checkResponse_non_english(self, message, BotName, es_analyzer):
        # global es_reponse
        # global feedbackFlag
        print('USER ASKED A QUESTION')
        print('USER ASKED A QUESTION ', self.session['es_restart_counter'])
        self.session['es_restart_counter'] += 1
        print('UPDATED ES COUNTER = {}'.format(self.session['es_restart_counter']))
        self.session['es_reponse'] = self.search_es_non_english(message, BotName, es_analyzer)
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        # if len(session['es_reponse']) > 0 and session['es_reponse'][0][0] > 0.29:
        if len(self.session['es_reponse']) > 0:
            # print('ANSWER')
            # print(self.session['es_reponse'][0][3])
            #
            # print('SCORE')
            # print(self.session['es_reponse'][0][0])
            #
            # print('ID')
            # print(self.session['es_reponse'][0][1])
            #
            # print('SOURCE')
            # print(self.session['es_reponse'][0][2])

            response_json = self.build_response("string", self.session['es_reponse'][0][3], "text", "",group=self.session['menuResult'])
            response_json_2 = self.feedback(response_json)

            #self.send_msg(self.session['es_reponse'][0][3])
            #self.feedback()
            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)
            #return (self.session['es_reponse'][0][1])
            return response_json_2
        else:
            response_json = self.build_response("string", "Lo siento, no puedo encontrar una respuesta para eso. ¿Podrías intentarlo de nuevo?","text","",group=self.session['menuResult'])
            #self.send_msg(gettext('Sorry, I can''t find an answer for that. Could you try again?'))
            # return 1
            self.intentNotFoundRecord(message,self.session['menuResult'])
            return response_json
    def intentNotFoundRecord(self, msg,group):
        self.conv_log.updateIntentNotFound(msg,group, self.session["user_id"])
        pass
    def intentFoundRecord(self, msg,group):
        self.conv_log.updateIntentFoundCounter(msg,group, self.session["user_id"])
