# Sets and gets context of an user
from utility.mongo_dao import insert,get_by_id, update

# returns context_id
def set(context_json): #todo insert timestamp
    context_id = insert('context',context_json)
    return context_id

def modify(context_id, context_json):
    context_id = update('context', context_id, context_json)
    return context_id

# returns context_json
def get(context_id):
    context_json = get_by_id('context',context_id)
    return context_json

