# Logger
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# Create a file handler
handler = logging.FileHandler('logs/Cobibot-prod-server.log')
# Create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
# Add the handlers to the logger
logger.addHandler(handler)
