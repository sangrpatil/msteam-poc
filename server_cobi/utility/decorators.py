from flask import Blueprint, request, jsonify, current_app,  render_template, session, redirect, Markup, url_for, flash
from functools import wraps
from utility.mongo_dao import *

# Decorators
from utility.session_manager import get as get_context, set as set_context, modify as update_context
def is_logged_in():
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if 'loggedIn' in session and session["loggedIn"]:
                return f(*args, **kwargs)
            else:
                return render_template('login.html')
        return wrapped
    return wrapper

def admin_check(*session):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            #session=get_context(session_id)
            if 'username' in session and session["username"]=='admin':
                return f(*args, **kwargs)
            else:
                return jsonify(
                    status='Error',
                    msg="You are not allowed to access this page."
                )
        return wrapped
    return wrapper

def requires_roles_groups(*roles):
    """
    Decorator for checking loggin, allowed roles ,group and allowed bot ids
    :param roles: dictionary containing allowed roles , groups and bot ids
    :return:
    """
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            print(roles)
            if 'loggedIn' in session and session["loggedIn"]:
                if 'username' in session:
                    user=get_one('user','username',session['username'])
                    group=[i['_id'] for i in user.get('group')]
                    print(group, "*****")
                    role_list=list_query('role','groups',group)
                    print(role_list,"role list")
                    role_list=[j['name'] for j in role_list[0].get('role')]
                    # for j in group:
                    #     for k in j:
                    #         role.append((k.get('name')).lower())
                    #
                    #
                    if 'role' in roles[0]:
                        allowed_roles=[x.lower() for x in roles[0].get('role')]
                        role_flag=False
                        for t in allowed_roles:
                            if t in role_list:
                                 role_flag=True
                                 break
                    permission_flag=False
                    if 'permission' in roles[0]:
                        allowed_permissions = [x.lower() for x in roles[0].get('permission')]
                        for t in allowed_permissions:
                            if t in user.get('permission'):
                                permission_flag = True
                                break
                        if not permission_flag:#not role_flag or
                            return jsonify(
                                    status='Error',
                                    msg="You are not allowed to access this page."
                                )
                    else:
                        if 'bot_id' in roles[0]:
                            print("here")
                            bot_id=kwargs.get('bot_id')
                            if bot_id in roles[0].get('bot_id'):
                                return f(*args, **kwargs)
                            else:
                                return jsonify(
                                    status='Error',
                                    msg="You are not allowed to access this page."
                                )

                        return f(*args, **kwargs)
            else:
                return render_template('login.html')
        return wrapped
    return wrapper


def is_admin(username):
    if username=='admin':
        return True
    else:
        return False