from flask import Blueprint, request, jsonify, current_app,  render_template, session, redirect, Markup, url_for, flash
from utility.mongo_dao import *
from utility.decorators import *

assign_blueprint = Blueprint('assignment', __name__, template_folder='templates')


@assign_blueprint.route('/api/assigngroup', methods = ['POST'])
def group_assign():
        msg=request.json
        print(msg)
        try:
            existing_user = get_all('user', 'username', msg['username'])
            existing_group=get_all('groups', 'name', msg['group'])
            if len(existing_user) > 0 and len(existing_group)>0:
                user = eval(existing_user[0])
                grp=eval(existing_group[0])
                if "group" in user.keys():
                    g=[]
                    for i in user.get('group'):
                        g.append(i)
                    g.append(grp)
                    update('user', user.get('_id'),{'group':g})
                else:
                    update('user', user.get('_id'), {'group': [grp]})
                return jsonify(
                    status='Success',
                    msg="Group assigned successfully"
                )
            else:
                return jsonify(
                    status='Failed',
                    msg="No record found"
                )
        except Exception as e:
            return jsonify(status='Exception',
                    msg=str(e))

@assign_blueprint.route('/api/assignrole', methods = ['POST'])
@requires_roles_groups({'role':['admin','compliance']})
def role_assign():
        msg=request.json
        print(msg)
        try:
            existing_group=get_all('groups', 'name', msg['group'])
            existing_role=get_all('role', 'name', msg['role'])
            print(existing_group)
            if len(existing_group) > 0 and len(existing_group) > 0:
                grp=eval(existing_group[0])
                role=eval(existing_role[0])
                #update_role=[{'_id':role.get('_id'),"name":msg['role']}]
                if "role" in grp.keys():
                    r=[]
                    for i in grp.get('role'):
                        r.append(i)
                    r.append(role)
                    update('groups', grp.get('_id'),{'role':r})
                else:
                    update('groups', grp.get('_id'), {'role': [role]})

                return jsonify(
                    status='Success',
                    msg="Role assigned to group successfully"
                )
            else:
                return jsonify(
                    status='Failed',
                    msg="No record found"
                )
        except Exception as e:
            return jsonify(status='Exception',
                    msg=e)
