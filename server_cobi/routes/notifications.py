from flask import Blueprint, render_template, request, jsonify, session, Markup, json, Response
from datetime import *
import calendar
import sys
from utility.mongo_dao import insert,update,get_one as get, get_by_id, delete
from utility.mail import Mail
from utility.mail_pfizer import send_email
from routes.validate import validate
import bson
import json
import requests
from utility.generate_reports_excel import generateReportsWeekly, generateReportsDaily
from utility.decorators import *
notifications_blueprint = Blueprint('notifications', __name__, template_folder='templates')
g_notifications = {}
# Return metrics of all bots
@notifications_blueprint.route('/scheduler/<bot_id>/create', methods = ['POST'])
@requires_roles_groups({'role':['admin']})
def set_notification(bot_id): #,emails,notification_day,notification_time

    # Validate bot_id

    response_validate = validate('bot',bot_id)
    print(bot_id)
    print('REPONSE from Crete Notification = {}'.format(Response(response_validate)))
    # print(response.is_json())
    print('RESPONSE - ', type(response_validate))

    response_json = response_validate.get_data(as_text=True)
    response_json = json.loads(response_json)
    print(response_json)
    response_status = response_json['result']
    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:
        try:
            # Schedule notifications in memory
            configuration = request.get_json(force=True)

            # Save it to mongoDB
            insert('notifications',
                   {"emails": configuration['emails'],
                    "bot_id":bot_id,
                    "notification_time": configuration['notification_time'],
                    "notification_day": configuration['notification_day'],
                    "notification_frequency": configuration['notification_frequency'],
                    "sent": False})

            global g_notifications
            print('Current Global Notification Variable in notification.py = {}'.format(g_notifications))
            g_notifications[bson.ObjectId(bot_id)]={"emails": configuration['emails'],
                                                    "bot_id":bot_id,
                                                    "notification_time": configuration['notification_time'],
                                                    "notification_day": configuration['notification_day'],
                                                    "notification_frequency": configuration['notification_frequency'],
                                                    "sent": False}


            return jsonify(
                status = "Success",
                msg = "Notification Set"
            )
        except Exception as e:
            return jsonify(
                status="Failed",
                msg=e
            )

@notifications_blueprint.route('/scheduler/<bot_id>', methods=['GET'])
@requires_roles_groups({'role':['admin','compliance']})
def get_notification(bot_id):
    # Validate bot_id
    response_validate = validate('bot', bot_id)
    response_json = response_validate.get_data(as_text=True)
    response_json = json.loads(response_json)
    print('REPONSE from Crete Notification = {}'.format(response_json))
    # response_json = response.get_json()
    print(response_json)
    response_status = response_json['result']

    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:

        global g_notifications
        return jsonify(status="Success",msg=g_notifications[bson.ObjectId(bot_id)])

# @metrics_blueprint.route('/scheduler')
# def get_all_notification():
#     global g_notifications
#     return g_notifications

@notifications_blueprint.route('/scheduler/<bot_id>/delete', methods=['GET'])
@requires_roles_groups({'role':['admin','compliance']})
def delete_notification(bot_id):
    # Validate bot_id
    response_validate = validate('bot', bot_id)
    response_json = response_validate.get_data(as_text=True)
    response_json = json.loads(response_json)
    print('REPONSE from Crete Notification = {}'.format(response_json))
    # response_json = response.get_json()
    print(response_json)
    response_status = response_json['result']
    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:
        try:
            # Delete it from memory
            global g_notifications
            g_notifications.pop(bson.ObjectId(bot_id))
            # Delete it to mongoDB
            delete('notifications', bot_id)
            return jsonify(
                status="Success",
                msg="Notification deleted"
            )
        except Exception as e:
            return jsonify(
                status="Failed",
                msg=e
            )

@notifications_blueprint.route('/scheduler/<bot_id>/update', methods = ['POST'])
@requires_roles_groups({'role':['admin','compliance']})
def update_notification(bot_id):
    # Validate bot_id
    response_validate = validate('bot', bot_id)
    response_json = response_validate.get_data(as_text=True)
    response_json = json.loads(response_json)
    print('REPONSE from Crete Notification = {}'.format(response_json))
    # response_json = response.get_json()
    print(response_json)
    response_status = response_json['result']
    if response_status == 1:
        return jsonify(
            status="Failed",
            msg=response_json['msg']
        )
    else:
        try:
            # Schedule notifications in memory
            configuration = request.get_json(force=True)
            # Schedule notifications in memory
            global g_notifications
            g_notifications[bson.ObjectId(bot_id)]={"emails": configuration['emails'],
                                                    "bot_id":bot_id,
                                                    "notification_time": configuration['notification_time'],
                                                    "notification_day": configuration['notification_day'],
                                                    "notification_frequency": configuration['notification_frequency'],
                                                    "sent": False}
            # Save it to mongoDB
            update('notifications', bot_id, {"emails": configuration['emails'],
                                                    "bot_id":bot_id,
                                                    "notification_time": configuration['notification_time'],
                                                    "notification_day": configuration['notification_day'],
                                                    "notification_frequency": configuration['notification_frequency'],
                                                    "sent": False})
            return jsonify(
                status="Success",
                msg="Notification Updated"
            )
        except Exception as e:
            return jsonify(
                status="Failed",
                msg=e
            )
    pass



# Function to prepare and send notification
def send_notification_weekly(bot_id,emails):
    global g_notifications
    notification = g_notifications[bot_id]
    if notification['sent']==False:
        try:
            print('sending notification for bot',bot_id,'to users ',emails)
            # Prepare notification (Generate your reports and store it in some common location)
            # print('hello')
            file_loc = generateReportsWeekly(bot_id)

            email_list = []
            for email_name in emails:
                email_list.append(email_name['email'])
            try:
                send_email(file_loc, email_list)
            except Exception as e:
                print('email send error - ', e)
            g_notifications[bot_id]['sent']=True
        except Exception as e:
            print('SEND NOTIFICATION ERROR - ', e)
            g_notifications[bot_id]['sent'] = True
    pass

# Function to prepare and send notification
def send_notification_daily(bot_id,emails):
    global g_notifications
    notification = g_notifications[bot_id]
    if notification['sent']==False:
        try:
            print('sending notification for bot',bot_id,'to users ',emails)
            # Prepare notification (Generate your reports and store it in some common location)
            # print('hello')
            file_loc = generateReportsDaily(bot_id)

            email_list = []
            for email_name in emails:
                email_list.append(email_name['email'])
            try:
                send_email(file_loc, email_list)
            except Exception as e:
                print('email send error - ', e)
            g_notifications[bot_id]['sent']=True
        except Exception as e:
            print('SEND NOTIFICATION ERROR - ', e)
            g_notifications[bot_id]['sent'] = True
    pass

# Asynchronous function to listen for notifications
def listen_notification():

    print('Staring to listen....')
    global g_notifications
    print('Current Global Notification Variable in listen notification.py = {}'.format(g_notifications))
    while True:
        for bot_id,notification in g_notifications.items():
            current_day = calendar.day_name[date.today().weekday()]
            current_time = datetime.now().strftime('%H:%M')
            notification_day =notification['notification_day']
            notification_time =notification['notification_time']
            notification_frequency = notification['notification_frequency']
            emails = notification['emails']
            if (notification_frequency.lower() == 'weekly' and current_day==notification_day and current_time==notification_time):
                send_notification_weekly(bot_id,emails)
                # break; #Just for testing
            elif (notification_frequency.lower() == 'daily' and current_time==notification_time):
                send_notification_daily(bot_id,emails)
                # break; #Just for testing
            elif notification_frequency.lower() == 'weekly&daily':
                if (current_day == notification_day and current_time == notification_time):
                    send_notification_weekly(bot_id, emails)
                    # break; #Just for testing
                elif (current_time == notification_time):
                    send_notification_daily(bot_id, emails)
            else:
                # Update the notification sent to False
                g_notifications[bot_id]['sent']=False