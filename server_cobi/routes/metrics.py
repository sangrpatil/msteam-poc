from flask import Blueprint, render_template, request, jsonify, session, Markup, send_from_directory
from utility.logger import logger
from utility.generate_reports_json import generateReports
from utility.generate_reports_excel import generateReportsWeekly
import json
import os
from utility.decorators import *
metrics_blueprint = Blueprint('metrics', __name__, template_folder='templates')


# Return metrics of all bots
@metrics_blueprint.route('/api/metrics', methods=['GET','POST'])
#@requires_roles_groups({'role':['admin']})
def metrics_all_bots():
    if request.method == 'GET':
        # Call the function with not bot_id passed
        group = ['Flotilla', 'Pfizer Conmigo', 'Actividades PLEM FV', 'Compliance']
        group = [str(i) for i in list(request.json['group_name'])]
        returnedjson = generateReports(None,group)
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        # return returnedjson
        pass
    elif request.method == 'POST':
        # Call the function with not bot_id passed
        #group = ['Flotilla', 'Pfizer Conmigo', 'Actividades PLEM FV', 'Compliance']
        group = [str(i) for i in list(request.json['group_name'])]
        returnedjson = generateReports(None,group)
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        # return returnedjson
        pass

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>', methods=['GET','POST'])
# @requires_roles_groups({'role':['admin','compliance']})
def metrics_bot(bot_id):
    if request.method == 'GET':
        print('INSIDE BOT METRICE METHOD')
        # Call the function with bot_id passed
        group = ['Flotilla','Pfizer Conmigo','Actividades PLEM FV','Compliance']
        group = [str(i) for i in list(request.json['group_name'])]
        try:
            # Call the function with not bot_id passed
            returnedjson = generateReports(bot_id,group)
            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )
    elif request.method == 'POST':
        print("REQUEST JSON IN METRICS = {}".format(request.json))
        group = [str(i) for i in list(request.json['group_name'])]
        try:
            # Call the function with not bot_id passed
            returnedjson = generateReports(bot_id,group)
            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            return jsonify(
                status='Failure',
                message=e
            )
# # Return metrics of a particular bot
# @metrics_blueprint.route('/metrics/<bot_id>/<category>', methods=['GET'])
# #@requires_roles_groups({'role':['admin','compliance']})
# def metrics_bot(bot_id,category):
#     if request.method == 'GET':
#         #Call the function with bot_id passed
#         try:
#             returnedjson = generateReports(bot_id,category)
#
#             if returnedjson == 'Failure':
#                 return jsonify(
#                     status='Failure',
#                     metrics='Bot ID not found'
#                 )
#             else:
#                 return jsonify(
#                     status='Success',
#                     metrics=returnedjson
#                 )
#
#         except Exception as e:
#             return jsonify(
#                 status='Failure',
#                 message=e
#             )

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>/download', methods=['GET','POST'])
#@requires_roles_groups({'role':['admin','compliance']})
def metrics_download(bot_id):
    if request.method == 'GET':
        print('INITIATING REPORT DOWNLOAD')
        category = ['Flotilla', 'Pfizer Conmigo', 'Actividades PLEM FV', 'Compliance']
        category = [str(i) for i in list(request.json['group_name'])]
        try:
            filename1 = generateReportsWeekly(bot_id,category)
            file_name = 'Cobi_Report_'+str(bot_id) + '.xlsx'
            print('FILENAME RECIVED = {}'.format(filename1))
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure
    elif request.method == 'POST':
        print('INITIATING POST REPORT DOWNLOAD')
        category = [str(i) for i in list(request.json['group_name'])]
        #category = ['Flotilla', 'Pfizer Conmigo', 'Actividades PLEM FV', 'Compliance']
        try:
            filename1 = generateReportsWeekly(bot_id, category)
            file_name = 'Cobi_Report_' + str(bot_id) + '.xlsx'
            print('FILENAME RECIVED = {}'.format(filename1))
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            failure = 'Failure as ' + str(e)
            return failure

