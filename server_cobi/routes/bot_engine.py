from utility.mongo_dao import insert as dao_insert, update as dao_update, get_by_id as dao_get_by_id, delete as dao_delete,get_one
from utility.logger import logger
import configparser
import os
import uuid
import wave
from utility.session_manager import get as get_context, modify as update_context
from elasticsearch import Elasticsearch
from flask import session, current_app, jsonify
from flask_socketio import emit
from flask_babel import gettext
from utility.ASR import transcribe_file
import bson
import datetime
import pytz
# Function to validate and store the JSON
def validate_bot(configuration):
    print("in bot engn validate")
    app_config = configparser.ConfigParser()
    app_config.read_file(open(r'config/app_settings.ini'))
    print("validate bot1*****")
    print("configuration--->",configuration)
    #configuration=configuration['configuration']
    try:
        is_start_true_counter = 0
        task_id_list = []
        task_name_list = []
        interaction_value_list = []
        hybrid_type_flag = False
        hybrid_dict_cell_entries = {}
        navigate_dict_cell_entries = {}
        hybrid_dict_check = {}
        navigate_dict_check = {}
        bot_name_list = []
        # Validate the configuration file
        if len(configuration['mapping']) == 0:
            return 0, 'Success'
        else:
            # newmap=[]
            # for grp,grp_map in configuration['mapping']:
            #     for val in grp_map:
            #         newmap.append(val)
            # print("new map---->",newmap)
            #for row in configuration['mapping']:
            for row in configuration['mapping']:
                #Validate Bot Name
                if str(row['Is Reserved']).strip().lower() == 'false':
                    # if str(row['Bot Name']).strip() != configuration['bot name']:
                    #     return 1, 'Bot names must all be the same'
                    # bot_name_list.append(str(row['Bot Name']).strip())

                    # validate task ID - must be unique if Task Name and Action Type are NOT FAQ
                    #                   must be an integer, must not be null/empty string
                    try:
                        test=int(float(row['Task ID']))
                    except:
                        return 1, 'Task ID must be an integer'
                    if str(row['Task ID']).strip() == '' or row['Task ID'] is None:
                        return 1, 'Task ID must not be empty'
                    if str(row['Task Name']).strip() != 'FAQ':
                        task_id_list.append(row['Task ID'])
                        navigate_dict_cell_entries[str(int(float(row['Task ID'])))] = str(row['Task Name']).strip()

                    #Validate Is Start - must be "true" or "false, and can only have 1 value as true
                    if str(row['Is Start']).strip().lower() != 'true' and str(row['Is Start']).strip().lower() != 'false':
                        return 1, 'Is Start must be either ''true'' or ''false''.'
                    print("IS start: ---->",str(row['Is Start']).strip().lower())
                    print("IS start task: ---->",row)
                    if str(row['Is Start']).strip().lower() == 'true':
                        is_start_true_counter += 1

                    #Validate Task Name - should not be empty or null. also if Task Name is FAQ, then Action Type must be FAQ
                    #if validation passes, then append the task name to a list to validate later against interaction values
                    if str(row['Task Name']).strip() == '' or row['Task Name'] is None:
                        return 1, 'Task Name should not be empty'
                    if str(row['Task Name']).strip() == 'FAQ':
                        if str(row['Action Type']) != 'FAQ':
                            return 1, 'If Task Name is marked as FAQ, then the Action Type must also be marked as FAQ'
                    else:
                        task_name_list.append(str(row['Task Name']).strip())

                    #Validate Task Text - should not be empty or null
                    if str(row['Task Text']).strip() == '' or row['Task Text'] is None:
                        return 1, 'Task Text should not be empty'

                    #Validate Next Task IDs - must be numeric, semi-colon separated, but CAN be empty if Task Name and Action Type are FAQ
                    if str(row['Task Name']).strip() != 'FAQ' and str(row['Action Type']).strip() != 'FAQ':
                        if str(row['Next Task IDs']).strip() == '' or row['Next Task IDs'] is None:
                            return 1, 'Next Task IDs should not be empty'
                        if isinstance(row['Next Task IDs'], str):
                            next_task_list = str(row['Next Task IDs']).split(';')
                        elif isinstance(row['Next Task IDs'], list):
                            next_task_list = row['Next Task IDs']
                        for next_ID in next_task_list:
                            try:
                                test = int(str(next_ID))
                            except:
                                return 1, 'Next Task IDs must be integers'

                    #Validate Interaction Type - needs to be text/button/url/none. shouldn't be empty/null. If FAQ type, then should be marked as Text
                    if str(row['Interaction Type']).strip() == '' or row['Interaction Type'] is None:
                        return 1, 'Interaction Type should not be empty'
                    interaction_type_config = app_config.get('GENERAL', 'interaction_type_config')
                    if str(row['Interaction Type']).strip() not in interaction_type_config:
                        return 1, 'The Interaction Type must either be ''text'', ''button'', ''url'', or ''none''.'
                    if str(row['Task Name']).strip() == 'FAQ':
                        if str(row['Interaction Type']).strip() != 'text':
                            return 1, 'If the Task Name is FAQ, then the Interaction Type must be set as ''text''.'

                    #Validate Interaction Values - can be empty if Interaction Type is FAQ, text, or none,
                    #                               should NOT be empty if url or button
                    #                               else should be strings separated by semi-colons
                    try:
                        interaction_type_interaction_values_none = app_config.get('GENERAL', 'interaction_type_interaction_values_none')
                        interaction_type_interaction_values_not_none = app_config.get('GENERAL','interaction_type_interaction_values_not_none')
                        if str(row['Interaction Type']).strip() in interaction_type_interaction_values_none:
                            if str(row['Interaction Values']).strip() == '' or row['Interaction Values'] is None:
                                return 1, 'Interaction Values should not be empty if Task Name is button or url'

                            if isinstance(row['Interaction Values'], str):
                                interaction_value_row_list = str(row['Interaction Values']).split(';')
                            elif isinstance(row['Interaction Values'], list):
                                interaction_value_row_list = row['Interaction Values']

                            interaction_value_row_list = [y.strip() for y in interaction_value_row_list]
                            for item in interaction_value_row_list:
                                interaction_value_list.append(item)
                        elif str(row['Interaction Type']).strip() in interaction_type_interaction_values_not_none:
                            # if str(row['Interaction Values']).strip() != '' and row['Interaction Values'] is not None:
                            if row['Interaction Values'] != [] and row['Interaction Values'] != [''] and row['Interaction Values'] is not None and str(row['Interaction Values']) != '':
                                return 1, 'Interaction Values should be empty if the Interaction Type is None, Text, or FAQ'
                    except:
                        pass
                    #Validate Keywords - must not be empty if there exists a Hybrid Guided Action Type somewhere
                    #                   keywords should be semi-colon separated
                    try:
                        if str(row['Keywords']).strip() != '' and row['Keywords'] is not None:
                            if isinstance(row['Keywords'], str):
                                keywords_split = str(row['Keywords']).split(';')
                            elif isinstance(row['Keywords'], list):
                                keywords_split = row['Keywords']

                            keywords_split = [y.strip() for y in keywords_split]
                            if str(int(float(row['Task ID']))) in hybrid_dict_cell_entries:
                                hybrid_dict_cell_entries[str(int(float(row['Task ID'])))]['Keywords'] = keywords_split
                            else:
                                hybrid_dict_cell_entries[str(int(float(row['Task ID'])))]={"Keywords": keywords_split, "Alias":""}
                    except:
                        pass
                    #Validate Alias - must not be empty if there exists a Hybrid Guided Action Type somewhere
                    #               needs to either be true/false
                    if str(row['Alias']).strip() != '' and row['Alias'] is not None:
                        if str(row['Alias']).strip().lower() != 'true' and str(row['Alias']).strip().lower() != 'false':
                            return 1, 'Alias must be either ''true'' or ''false''.'
                        else:
                            if str(int(float(row['Task ID']))) in hybrid_dict_cell_entries:
                                hybrid_dict_cell_entries[str(int(float(row['Task ID'])))]['Alias'] = str(row['Alias'])
                            else:
                                hybrid_dict_cell_entries[str(int(float(row['Task ID'])))] = {"Keywords": "", "Alias": str(row['Alias'])}

                    #Validate Action Type - needs to either be FAQ, navigate, code, query, hybrid guided
                    action_type_config = app_config.get('GENERAL','action_type_config')
                    if str(row['Action Type']) not in action_type_config:
                        return 1, 'Action Type must either be ''FAQ'', ''navigate'', ''code'', ''query'', or ''hybridGuided''.'
                    if str(row['Action Type']) == 'hybridGuided':
                        hybrid_type_flag = True
                        # next_id_values = str(row['Next Task IDs']).split(';') #######3
                        if isinstance(row['Next Task IDs'], str):
                            next_id_values = str(row['Next Task IDs']).split(';')
                        elif isinstance(row['Next Task IDs'], list):
                            next_id_values = row['Next Task IDs']

                        next_id_values = [y.strip() for y in next_id_values]
                        hybrid_dict_check[row['Task ID']] = {'Next Task IDs':next_id_values}
                    if str(row['Action Type']) == 'navigate' and str(row['Interaction Type'])== 'button':
                        # next_id_values = str(row['Next Task IDs']).split(';')
                        if isinstance(row['Next Task IDs'], str):
                            next_id_values = str(row['Next Task IDs']).split(';')
                        elif isinstance(row['Next Task IDs'], list):
                            next_id_values = row['Next Task IDs']
                        next_id_values = [y.strip() for y in next_id_values]
                        # int_values = str(row['Interaction Values']).split(';')

                        if isinstance(row['Interaction Values'], str):
                            int_values = str(row['Interaction Values']).split(';')
                        elif isinstance(row['Interaction Values'], list):
                            int_values = row['Interaction Values']

                        int_values = [y.strip() for y in int_values]
                        navigate_dict_check[row['Task ID']] = {
                            'Next Task IDs': next_id_values,
                            'Interaction Values': int_values
                        }
                    #Validate Interaction Fetch from DB - cannot be empty, must be true or false
                    #                                       if it is true, then database information must not be empty
                    if str(row['Interaction Fetch From DB']).strip().lower() != 'true' and str(row['Interaction Fetch From DB']).strip().lower() != 'false':
                        return 1, 'Interaction Fetch From DB must either be ''true'' or ''false''.'
                    if str(row['Interaction Fetch From DB']).strip() == 'true':
                        if str(row['Interaction DB Host']).strip() == '' or row['Interaction DB Host'] is None:
                            return 1, 'Interaction DB Host must not be empty if using an external database'
                        if str(row['Interaction Connection String']).strip() == '' or row['Interaction Connection String'] is None:
                            return 1, 'Interaction Connection String must not be empty if using an external database'
                        if str(row['Interaction DB Port']).strip() == '' or row['Interaction DB Port'] is None:
                            return 1, 'Interaction DB Port must not be empty if using an external database'
                        if str(row['Interaction DB Type']).strip() == '' or row['Interaction DB Type'] is None:
                            return 1, 'Interaction DB Type must not be empty if using an external database'
                        if str(row['Interaction DB Table Name']).strip() == '' or row['Interaction DB Table Name'] is None:
                            return 1, 'Interaction DB Table Name must not be empty if using an external database'
                        if str(row['Interaction DB Select Column']).strip() == '' or row['Interaction DB Select Column'] is None:
                            return 1, 'Interaction DB Select Column must not be empty if using an external database'
                        if str(row['Interaction DB Filter Column']).strip() == '' or row['Interaction DB Filter Column'] is None:
                            return 1, 'Interaction DB Filter Column must not be empty if using an external database'
                        if str(row['Interaction DB Filter Value']).strip() == '' or row['Interaction DB Filter Value'] is None:
                            return 1, 'Interaction DB Filter Value must not be empty if using an external database'

                    #Validate Use Code - if it isn't empty, then it needs to be "true" or "false", and function code needs to not be empty
                    if 'Use Code' in row:
                        if str(row['Use Code']).strip() != '' and row['Use Code'] is not None:
                            if str(row['Use Code']).strip().lower() != 'true' and str(row['Use Code']).strip().lower() != 'false':
                                return 1, 'Use Code must be ''true'' or ''false'' if it is going to be used.'
                            if str(row['Use Code']).strip() == 'true':
                                if str(row['Function Code']).strip() == '' or row['Function Code'] is None:
                                    return 1, 'Function Code must not be empty if Use Code is true'
                    #Validate the Answers - can be empty, but if Task Name is FAQ, then it cannot
                    if str(row['Task Name']).strip() == 'FAQ':
                        if str(row['Answers ES']).strip() == '' or row['Answers ES'] is None:
                            return 1, 'Answers ES cannot be empty for FAQ questions'

            # #validate only 1 bot name
            # if len(set(bot_name_list)) != 1:
            #     return 1, 'There can only be 1 Bot Name'
            #Validate Is Start after checking all rows - can only have 1 true value
            print("cofigu---->",configuration)
            if is_start_true_counter != 1:
                return 1, 'Is Start must contain one ''true'' value.'

            #Validate Task ID's - must be unique for rows not associated with an FAQ
            if len(task_id_list) != len(set(task_id_list)):
                return 1, 'Task ID''s must be unique'

            #if there is a hybrid guided, there must be keywords
            #the next tasks need to lead to task ID's with alias' and keywords
            if hybrid_type_flag == True:
                for key, value in hybrid_dict_check.items():
                    for ID in value['Next Task IDs']:
                        if ID in hybrid_dict_cell_entries:
                            try:
                                if len(hybrid_dict_cell_entries[ID]['Keywords']) < 1:
                                    return 1, 'Please enter keywords for rows associated with a hybrid guided conversation'
                            except:
                                return 1, 'Please enter keywords for rows associated with a hybrid guided conversation'
                            try:
                                if len(hybrid_dict_cell_entries[ID]['Alias']) < 1:
                                    return 1, 'Please enter a true or false value for an Alias associated with a hybrid conversation'
                            except:
                                return 1, 'Please enter a true or false value for an Alias associated with a hybrid conversation'
                        else:
                            return 1, 'The Hybrid Guided ID''s in a given cell need associated ID''s in a given row. Please check to make sure they all match.'

            #if there is a navigate type with buttons, the ID's and interaction values need to point to something in the check
            for key, value in sorted(navigate_dict_check.items()):
                #value will have next task id's and interaction values
                #break up the next task id list -> check if that id is in the navigate_dict_cell_entries
                #if that number IS in the navigate_dict_cell_entries, then look at the Task Name
                #take that task name, and check if it is contained in the list of interaction values
                task_name_check = ''
                for next_id in value['Next Task IDs']:
                    if str(int(float(next_id))) in navigate_dict_cell_entries:
                        task_name_check = navigate_dict_cell_entries[next_id]
                    else:
                        message = 'For ID ' + key + ', check the next task IDs and ensure they point to existing Task IDs.'
                        return 1, message
                for interaction_value in value['Interaction Values']:
                    help_config = app_config.get('GENERAL', 'help_keywords')
                    restart_config = app_config.get('GENERAL', 'restart_keywords')
                    goback_config = app_config.get('GENERAL', 'goback_keywords')
                    if (interaction_value.lower().strip() not in help_config) and (interaction_value.lower().strip() not in restart_config) and (interaction_value.lower().strip() not in goback_config):
                        if interaction_value.strip() not in task_name_list:
                            message = 'For ID ' + key + ', check that the interaction value for ' + interaction_value + ' matches a corresponding Task Name.'
                            return 1, message
                pass

            return 0, 'Success'
    except Exception as e:
        logger.error(str(e))
        message = 'Failure relating to ' + str(e)
        return 1, message

def store_bot(configuration):
    print("in bot engn store bot")
    try:
        # Store the configuration file
        #print(configuration)
        final_bot_json = {
            "bot name": configuration['bot name'],
            "language":configuration['language'],
            "es_analyzer":configuration['es_analyzer'],
            "mapping":[],
            "update_time":pytz.utc.localize(datetime.datetime.utcnow())

        }
        #configuration['group_name']="Car Fleet"
        for row in configuration['mapping']:
            row['Bot Name']+=row['Group']
            row['Task Name'] = row['Task Name'].strip()
            try:
                row['Task ID'] = str(int(float(row['Task ID']))).strip()
            except:
                row['Task ID'] = str(row['Task ID']).strip() #to account for reserved words

            next_task_ids = row['Next Task IDs']
            row['Next Task IDs'] = [y.strip() for y in next_task_ids.split(';')]
            if row['Next Task IDs'][0] == '':
                row['Next Task IDs'] = []

            keywords = row['Keywords']
            # print('KEYWORDS -- ', keywords)
            row['Keywords'] = [y.strip() for y in keywords.split(';')]
            if row['Keywords'][0] == '':
                row['Keywords'] = []


            interaction_values = row['Interaction Values']
            row['Interaction Values'] = [y.strip() for y in interaction_values.split(';')]
            if row['Interaction Values'][0] == '':
                row['Interaction Values'] = []
            final_bot_json['mapping'].append(row)

        bot_id = dao_insert('bot', final_bot_json)

        elasticsearch_result = process_elasticsearch(configuration, bot_id)
        if elasticsearch_result[0] != 0:
            return 1, '', 'Elasticsearch Creation Failure'

        return 0, bot_id,'Success'
    except Exception as e:
        logger.error(str(e))
        return 1,'', 'Failure'

def process_elasticsearch(configuration, bot_id):
    print("in bot engn elasticsearch")

    final_es_json = {
        str(bot_id).lower():[]
    }

    mapping_json = {}
    for row in configuration['mapping']:
        if row['Task Name'] == 'FAQ':

            current_id = int(float(row['Task ID']))

            if current_id in mapping_json:
                mapping_json[current_id]['q'].append(row['Task Text'])
            else:
                mapping_json[current_id] = {
                    'qid':current_id,
                    'q':[row['Task Text']],
                    'a':row['Answers ES'],
                    'votes':0
                }
    print("*****")
    for key, value in sorted(mapping_json.items()):
        final_es_json[str(bot_id).lower()].append(value)
    print('FINAL JSON - ', final_es_json)
    if configuration['language'] == 'en':
        result = create_es_knowledgebase(final_es_json, bot_id,configuration['group_name'].replace(" ", ""))
        if result == 'Failure':
            return 1, 'Failure'
    else:
        result = create_es_knowledgebase_non_english(final_es_json, bot_id, configuration['es_analyzer'],configuration['group_name'].replace(" ", ""))
        if result == 'Failure':
            return 1, 'Failure'
    pass
    return 0, 'Success'


def create_es_knowledgebase(json_input, bot_name,group_name):
    print("in bot engn create knowledge base")
    try:
        es_host = os.environ.get('ES_HOST') or 'localhost'
        es_port = os.environ.get('ES_PORT') or '9200'
        es = Elasticsearch([{'host': str(es_host), 'port': int(es_port)}])
    except Exception as e:
        print('Error in connecting to Elasticsearch server - {}'.format(e))
        return ('Failure')

    es.indices.delete(index=str(bot_name).lower()+group_name.lower(), ignore=[400, 404])
    try:
        qna = json_input[str(bot_name).lower()]
        for items in qna:
            es.index(index=str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'], body=items)
    except Exception as e:
        print('Error in creating knowledgebase in Elasticsearch - {}'.format(e))
        return ('Failure')

# def create_es_knowledgebase_non_english(json_input, bot_name, es_analyzer,group_name):
#     print("in bot engn create neng base")
#     try:
#         elasticsearch_host = os.environ.get('ES_HOST') or 'localhost'
#         elasticsearch_port = os.environ.get('ES_PORT') or '9200'
#         es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])
#     except Exception as e:
#         print('Error in connecting to Elasticsearch server - {}'.format(e))
#         return ('Failure')
#
#     #todo - needs documentation on what this is
#     data_mapping = {
#         "mappings": {
#             str(bot_name).lower() + group_name.lower(): {
#                 "properties": {
#                     "qid": {
#                         "type": "string",
#                         "fields": {
#                             "stemmed": {
#                                 "type": "string",
#                                 "analyzer": es_analyzer
#                             }
#                         }
#                     },
#                     "q":{
#                         "type": "array",
#                         "fields": {
#                             "stemmed": {
#                                 "type": "string",
#                                 "analyzer": es_analyzer
#                             }
#                         }
#                     },
#                     "a": {
#                         "type": "string",
#                         "fields": {
#                             "stemmed": {
#                                 "type": "string",
#                                 "analyzer": es_analyzer
#                             }
#                         }
#                     },
#                     "votes": {
#                         "type": "integer",
#                         "fields": {
#                             "stemmed": {
#                                 "type": "integer",
#                                 "analyzer": es_analyzer
#                             }
#                         }
#                     }
#
#                 }
#             }
#         }
#     }
#
#     # Delete previous documents at the same index
#     try:
#         if es.indices.exists(index=str(bot_name).lower()):
#             print('Spanish Bot index already exists - going to delete it')
#             qna = json_input[str(bot_name).lower()]
#             for items in qna:
#                 es.delete(index = str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'])
#         else:
#             print('Spanish Bot index does not exists - Going to add index')
#     except Exception as e:
#         print('Error in deleting knowledgebase index in Elasticsearch - es - {}'.format(e))
#         #return ('Failure')
#
#     # Create mapping index
#     try:
#         qna = json_input[str(bot_name).lower()]
#         for items in qna:
#             es.create(index = str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'], body= data_mapping)
#     except Exception as e:
#         print('Error in creating mapping index knowledgebase in Elasticsearch - {}'.format(e))
#         return ('Failure')
#
#     # index documents
#     try:
#         qna = json_input[str(bot_name).lower()]
#         for items in qna:
#             es.index(index=str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'], body=items)
#     except Exception as e:
#         print(e)
#         print('Error in creating knowledgebase in Elasticsearch - {}'.format(e))
#         return ('Failure')
#
#     print('Successfuly created the knowledgebase for Spanish content')

def create_es_knowledgebase_non_english(json_input, bot_name, es_analyzer,group_name):
    print("in bot engn create neng base for index = {}".format(str(bot_name).lower()+group_name.lower()))
    try:
        elasticsearch_host = os.environ.get('ES_HOST') or 'localhost'
        elasticsearch_port = os.environ.get('ES_PORT') or '9200'
        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])
    except Exception as e:
        print('Error in connecting to Elasticsearch server - {}'.format(e))
        return ('Failure')

    #todo - needs documentation on what this is
    data_mapping = {
        "mappings": {
            str(bot_name).lower()+group_name.lower(): {
                "properties": {
                    "qid": {
                        "type": "string",
                        "fields": {
                            "stemmed": {
                                "type": "string",
                                "analyzer": es_analyzer
                            }
                        }
                    },
                    "q":{
                        "type": "array",
                        "fields": {
                            "stemmed": {
                                "type": "string",
                                "analyzer": es_analyzer
                            }
                        }
                    },
                    "a": {
                        "type": "string",
                        "fields": {
                            "stemmed": {
                                "type": "string",
                                "analyzer": es_analyzer
                            }
                        }
                    },
                    "votes": {
                        "type": "integer",
                        "fields": {
                            "stemmed": {
                                "type": "integer",
                                "analyzer": es_analyzer
                            }
                        }
                    }

                }
            }
        }
    }

    # Delete previous documents at the same index
    es.indices.delete(index = str(bot_name).lower()+group_name.lower(), ignore=[400,404])
    # try:
    #     if es.indices.exists(index=str(bot_name).lower()+group_name.lower()):
    #         print('Spanish Bot index already exists - going to delete it')
    #         qna = json_input[str(bot_name).lower()]
    #         for items in qna:
    #             es.delete(index = str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'])
    #     else:
    #         print('Spanish Bot index does not exists - Going to add index')
    # except Exception as e:
    #     print('Error in deleting knowledgebase index in Elasticsearch - es - {}'.format(e))
    #     #return ('Failure')

    # Create mapping index
    try:
        qna = json_input[str(bot_name).lower()]
        for items in qna:
            es.create(index = str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'], body= data_mapping)
    except Exception as e:
        print('Error in creating mapping index knowledgebase in Elasticsearch - {}'.format(e))
        return ('Failure')

    # index documents
    try:
        qna = json_input[str(bot_name).lower()]
        for items in qna:
            es.index(index=str(bot_name).lower()+group_name.lower(), doc_type=str(bot_name).lower()+group_name.lower(), id = items['qid'], body=items)
    except Exception as e:
        print('Error in creating knowledgebase in Elasticsearch - {}'.format(e))
        return ('Failure')

    print('Successfuly created the knowledgebase for Spanish content')

def update_bot(configuration,bot_id,group_list=None):
    print("in bot engn update bot")
    try:
            # Store the configuration file
            # is_valid, message = validate_bot(configuration) #configuration is the whole json now, from the mongoDB
            # if is_valid == 0:
            #     final_bot_json = {
            #         "bot name": configuration['bot name'],
            #         "language": configuration['language'],
            #         "es_analyzer": configuration['es_analyzer'],
            #         "mapping": [],
            #         "update_time": pytz.utc.localize(datetime.datetime.utcnow())
            #     }
            #     for row in configuration['mapping']:
            #         row['Task Name'] = row['Task Name'].strip()
            #         try:
            #             row['Task ID'] = str(int(float(row['Task ID']))).strip()
            #         except:
            #             row['Task ID'] = str(row['Task ID']).strip()  # to account for reserved words
            #
            #         next_task_ids = row['Next Task IDs']
            #         row['Next Task IDs'] = [y.strip() for y in next_task_ids.split(';')]
            #
            #         keywords = row['Keywords']
            #         row['Keywords'] = [y.strip() for y in keywords.split(';')]
            #
            #         interaction_values = row['Interaction Values']
            #         row['Interaction Values'] = [y.strip() for y in interaction_values.split(';')]
            #         final_bot_json['mapping'].append(row)
        configuration1 = dao_get_by_id('bot', bot_id)
        print("configuration1--**", configuration1)
        # user ='admin'#session['username']
        # usr = get_one('user', 'username', user)
        group = group_list#[str(i.get('name')) for i in usr.get('group')]
        print("user **** group", group)
        task_list_na = []
        group_list_mapping={}
        for i in group:
            group_list_mapping[str(i)]=[]
        print(group_list_mapping,"&&&&&")
        newmap = []
        for grp_map in configuration['mapping']:
            #for val in configuration['mapping']:
                newmap.append(grp_map)
        print("new map---->", newmap)
        for row in newmap:
            try:
                # row.update((k, str(int(float(v))).strip()) for k, v in row.items() if k == 'Task ID')
                row.update((k, v) for k, v in row.items() if k == 'Task ID')
                if not(str(row['Group']) ==''):
                    group_list_mapping[str(row['Group'])].append(row)

                # row['Task ID'] = str(int(float(row['Task ID']))).strip()
            except Exception as e:
                print("in update bot exception:", e)
                return 1, "", "Came in exception " + str(row)
                # row['Task ID'] = str(row['Task ID']).strip()  # to account for reserved words
        #for i in configuration['mapping']:
        # for i in newmap:
        #     if i['Task Name'] == 'Start':
        #         configuration['mapping'].remove(i)
        #         #newmap.remove(i)
        #configuration['mapping'] = configuration['mapping'] + configuration1['mapping']
        #newmap[0]=newmap[0]+ configuration1['mapping']
        configuration['mapping']=newmap
        configuration['mapping'] = configuration['mapping'] + configuration1['mapping']
        configuration['update_time'] = pytz.utc.localize(datetime.datetime.utcnow())
        if '_id' in configuration:
            del configuration['_id']

        dao_update('bot', bot_id, configuration)
        for gp,gm in group_list_mapping.items():
            conf=configuration
            conf['mapping']=gm
            conf['group_name']=str(gp).replace(" ","")
            #elasticsearch_result = process_elasticsearch(conf, ''.join((str(bot_id)+str(gp)).split()))
            elasticsearch_result = process_elasticsearch(conf, bot_id)
            if elasticsearch_result[0] != 0:
                return 1, '', 'Elasticsearch Creation Failure'
        # elasticsearch_result = process_elasticsearch(configuration, str(bot_id)+str(gp))
        # if elasticsearch_result[0] != 0:
        #     return 1, '', 'Elasticsearch Creation Failure'

        return 0, bot_id, 'Success'

    except Exception as e:
        logger.error(str(e))
        return 1,'', 'Failure'



def read_bot(bot_id):
    try:
        configuration = dao_get_by_id('bot',bot_id)
        return 0, configuration,'Success'
    except Exception as e:
        logger.error(str(e))
        print(e,"***")
        return 1, '', 'Failure'


def delete_bot(bot_id):
    try:
        bot_id = bson.ObjectId(bot_id)
        dao_delete('bot',bot_id)
        return 0, 'Success'
    except Exception as e:
        logger.error(str(e))
        return 1, 'Failure'


