from flask import Blueprint, request, jsonify, current_app,  render_template, session, redirect, Markup, url_for, flash
from utility.mongo_dao import *
from utility.decorators import *
#from flask import json
import json
from bson import json_util

permission_blueprint = Blueprint('permission', __name__, template_folder='templates')

@permission_blueprint.route('/api/permission/create', methods=['POST','GET'])
def role_create():
        message = request.json
        existing_user = get_all('role','name',message['name'])
        print(existing_user)
        if len(existing_user)>0:
           return jsonify(
                Status='Error',
                Message="Role creation failed"
            )
        else:
            insert('role',{"name":message['name']})
            return jsonify(
                        Status='Success',
                        Message="Role created successfully"
                    )


@permission_blueprint.route('/api/permission/update', methods=['POST'])
def role_update():
        message = request.json
        existing_user =get_all('role','name',message['name'])
        print(existing_user)
        if len(existing_user)>0:
            user = eval(existing_user[0])
            update('role',user.get('_id'),{"name":message['newname']})
            return jsonify(
                    Status='Success',
                    Message="Role updated successfully"
                )
        else:
            return jsonify(
                Status='Error',
                Message="Role updation failed"
            )

@permission_blueprint.route('/api/permission/read', methods=['GET'])
def role_read():
        role_list =get_collection('role')
        json_docs = []
        #print(len(user_list))
        for doc in role_list:
            json_doc = json.dumps(doc, default=json_util.default)
            json_docs.append(json_doc)
        return jsonify(
                Status='Success',
                User_List=json_docs
            )



@permission_blueprint.route('/api/permission/delete', methods=['GET'])
def role_delete():
        message = request.json
        existing_role = get_all('role', 'name', message['name'])

        if len(existing_role) > 0:
            user = eval(existing_role[0])
            delete('role',user.get('_id'))
            return jsonify(
                Status='Success',
                Message="Role deleted successfully"
            )
        else:
            return jsonify(
                Status='Error',
                Message="No record found"
            )
