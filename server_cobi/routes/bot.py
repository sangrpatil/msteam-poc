from flask import Blueprint, request, jsonify, render_template, session as flask_session, redirect, Markup
from utility.mongo_dao import insert, get_by_id, get_one, update, get_collection,get_all
from utility.bot_engine import validate_bot, store_bot, update_bot, read_bot, delete_bot
from utility.logger import logger
from utility.execution_engine import ExecutionEngine
from utility.session_manager import get as get_context, set as set_context, modify as update_context
import time
from bson import ObjectId
from .user import *
import bson
import json
from utility.excel_parser import convert
import os
from flask_babel import refresh, gettext
from flask import current_app as app,flash, url_for
from utility.decorators import *
import datetime
import configparser
bot_blueprint = Blueprint('bot', __name__, template_folder='templates')

question_group={}
group=''


@bot_blueprint.route('/check',methods = ['GET', 'POST'])
#@admin_check()
def check():
    return jsonify(
        status='Running'
    )

@bot_blueprint.route('/api/login', methods = ['GET', 'POST'])
def login():
    if request.method=='GET':
        return render_template('login.html')

    elif request.method == 'POST':
        logger.debug("in logins")
        admin_username = os.environ.get('ADMIN_USERNAME') or 'admin'
        admin_password = os.environ.get('ADMIN_PASSWORD') or 'admin'
        logindata = request.json
        if logindata == None:
            username = request.form.get('username')
            password = request.form.get('password')
        else:
            username = logindata['username']
            password = logindata['password']
        logger.info("Username received = {}".format(username))
        config = configparser.ConfigParser()
        config.read(r'config/session.ini')
        config['SESSION']['username']=username
        with open(r'config/session.ini', 'w') as configfile:
            config.write(configfile)

        now = datetime.datetime.now()
        dic = {"lastlogin": str(now.strftime("%Y-%m-%d %H:%M"))}
        user= get_one('user', 'username', username)
        # if username == admin_username and password == admin_password:
        #     session['loggedIn'] = True
        #     session['username']=username
        #     id = get_one('user', 'username', username).get('_id')
        #     update('user', id, dic)
        #     #return render_template('create_bot_excel.html')
        #     return jsonify(
        #         status = 'Success',
        #         msg = 'User logged in successfully'
        #     )
        if not(user==None) and password==user.get('password') :
            flask_session['username']=username
            print(flask_session,"---flask session----")
            session['loggedIn'] = True
            session['username'] = username
            print(now.strftime("%Y-%m-%d %H:%M"),"***")
            id=get_one('user','username',username).get('_id')
            update('user',id,dic)
            print("session---", type(session), session)
            logger.info('Following User legged in successfully = {}'.format(username))
            return jsonify(
                status='Success',
                msg=str(id).replace('ObjectId(', '').replace(')', '')
            )
            # try:
            #     print("in admin bot update")
            #     if session['update_login_attempt'] == True:
            #         string_update = '/bot/' + str(session['update_bot_id']) + '/update'
            #         return redirect(string_update)
            #     else:
            #         return redirect('/bot/create')
            # except Exception as e:
            #     print(e)
            #     return redirect('/bot/create')
        # elif len(get_one('user','username',username))>0:
        #     session['loggedIn'] = True
        #     session['username'] = username
        #     try:
        #         if session['update_login_attempt'] == True:
        #             string_update = '/bot/' + str(session['update_bot_id']) + '/update'
        #             return redirect(string_update)
        #         else:
        #             return redirect('/bot/create')
        #     except:
        #
        #         return redirect('/bot/create')
        else:
            logger.info('Login Failed for username = {}'.format(username))
            session['loggedIn'] = False
            #return render_template('login.html')
            return jsonify(
                status='Failure',
                msg='User login failed'
            )

@bot_blueprint.route('/api/logout', methods = ['GET'])
def logout():
    session['loggedIn'] = False
    return render_template('login.html')

@bot_blueprint.route('/api/bot/create',methods=['GET','POST'])
#@admin_check()
#@requires_roles_groups({'role':['admin','compliance']})
def bot_create():
    session['update_login_attempt'] = False
    if request.method == 'GET':
        try:
            if session['loggedIn'] == True:
                print("bot create excel")
                return render_template('create_bot_excel.html')
            else:
                return redirect('/api/login')
        except:
            return render_template('create_bot_excel.html')
            return redirect('/api/login')
    elif request.method == 'POST':
        print('INSIDE POST')
        try:
            f = request.files['file']
            print('DID WE GET THE FILE')
            f.save(r'./mapping/' + f.filename)
            configuration = {"bot name":"Lorlatinib Bot","language":"es","es_analyzer":"Spanish","mapping":[]} #todo un-hardcode this
            mapping_result = json.loads(convert(r'./mapping/' + f.filename))
            print('RECIEVED MAPPING', mapping_result, type(mapping_result))
            for key, value in mapping_result.items():
                configuration["mapping"] = value
                configuration["bot name"]=value[1]['Bot Name']
                configuration["group_name"]=value[1]['Group']
        except Exception as e:
            configuration = request.get_json(force=True)

        print('CONFIGURATION - ', configuration)
        is_valid,message = validate_bot(configuration)
        if is_valid == 0:
            is_saved, bot_id, message = store_bot(configuration)
            if is_saved == 0:
                return jsonify(
                    status='Success',
                    id=bot_id,
                    msg='Bot created successfully'
                )
            else:
                return jsonify(
                    status='Error',
                    msg=message
                )
        else:
            return jsonify(
                status='Error',
                msg=message
            )

@bot_blueprint.route('/api/bot/<bot_id>/update',methods=['GET','POST'])
#@requires_roles_groups({'role':['admin','compliance']})
def bot_update(bot_id):
    update_flag = ''
    if request.method == 'GET':
        logger.debug('GET method called in bot update method')
        session['update_login_attempt'] = True
        session['update_bot_id'] = bot_id
        try:
            if session['loggedIn'] == True:
                return render_template('update_bot.html', bot_id=bot_id)
            else:
                return redirect('/login')
        except:
            return redirect('/login')


    elif request.method == 'POST':
        logger.debug('POST method called in Update method')
        try:
            f = request.files['file']
            # print(f)
            logger.debug('WE GOT THE EXCEL FILE FOR UPDATE')
            f.save(r'./mapping/' + f.filename)

            configuration = {"bot name":"Lorlatinib Bot","language":"es","es_analyzer":"Spanish","mapping":[]} #todo un-hardcode this
            mapping_result = json.loads(convert(r'./mapping/' + f.filename))
            for key, value in mapping_result.items():
                configuration["mapping"] = value
                configuration["bot name"] = value[1]['Bot Name']
                configuration["group_name"]=value[1]['Group']
                grp_list = [configuration["group_name"]]
        except Exception as e:
            popup_flag = False  # for showing error messages to user
            configuration = request.get_json(force=True)

            # Code changes related to "Is start must have oe true value"
            for key, value in configuration["botObj"]["mapping"].items():
                if len(value) == 1:
                    if max([len(v) for k, v in configuration["botObj"]["mapping"].items() if k != key], default = 0) == 0: # checking if is_start is present in other groups
                        if value[0]["Task Name"].lower() == "start" and value[0]["Is Start"] == False :
                             configuration["botObj"]["mapping"][key][0]['Is Start'] = True
                             configuration["botObj"]["mapping"][key][0]["Task Name"] = "Start"
                             configuration["botObj"]["mapping"][key][0]["Complementary Action"] = "group"

                        elif value[0]["Task Name"].lower() == "start" and value[0]["Is Start"] == True : # if there are 2 questions and only start is left
                            continue
                        else:
                             print("raise an error message asking user to upload start question for group ", key)
                             popup_flag = True
                    else:
                        if value[0]["Task Name"].lower() != "start":
                            popup_flag = True
                            print("raise an error asking message asking user to upload start question for group ", key)
                        elif value[0]["Task Name"].lower() == "start":
                            configuration["botObj"]["mapping"][key][0]['Is Start'] = True
                            configuration["botObj"]["mapping"][key][0]["Task Name"] = "Start"
                            configuration["botObj"]["mapping"][key][0]["Complementary Action"] = "group"
            # Code changes related to "Is start must have oe true value" ends here

            # print("config from ui-->",configuration)
            grp_list=configuration['sessionObj']['group_name']
            update_flag = 'True'
            configuration=configuration['botObj']

        #modify configuration
        new_mapping = []

        if update_flag != '':
            logger.debug('SESSION OBJECT WAS IN CONFIGURATION. ADMIN UI CALLED')
            # print(configuration['mapping'], "<-----configuration mapping")
            # print(isinstance(configuration['mapping'][0], (list,)))
            testconf = [v for k, v in configuration['mapping'].items() if len(v) > 0]
            # if isinstance(configuration['mapping'][next(iter(configuration['mapping']))][0]['Task Text'], (list,)):
            # print("TESTCONF = {}".format(testconf))
            # print("TYPE OF TESTCONF = {}".format(type(testconf)))
            # if isinstance(configuration['mapping'][0]['Task Text'],(list,)):
            #for item in configuration['mapping']:
            if not testconf:
                configuration["mapping"] = [] # if the group has only one item and that is being deleted
            elif isinstance(testconf[0][0]['Task Text'], (list,)):

                for grp, grp_val in configuration['mapping'].items():
                    for item in grp_val:
                        # for item in configuration['mapping']:

                        task_text_list = item['Task Text']
                        if len(item['Task Text']) == 1:
                            item['Task Text'] = item['Task Text'][0]
                            new_mapping.append(item)
                        else:
                            if item['Is Start']=='true' or item['Is Start']==True:
                                task_var = item['Task Text']
                                item['Task Text'] = item['Task Text'][0]
                                new_mapping.append(item)

                                for i in task_var[1:]:
                                    item_copy = item.copy()
                                    item_copy['Is Start']='false'
                                    item_copy['Task Text'] = i
                                    new_mapping.append(item_copy)
                            else:
                                for text in task_text_list:
                                    item_copy = item.copy()
                                    for k,v in item_copy.items():
                                        if v=='true':
                                            item_copy[k]=True
                                        elif v=='false':
                                            item_copy[k]=False


                                    # print('---text check---', text)
                                    item_copy['Task Text'] = text
                                    # print("--item copy--",item_copy)
                                    # print( "--new map copy--",new_mapping)
                                    # print("--item copy in--",item_copy in new_mapping )
                                    if item_copy not in new_mapping:
                                        new_mapping.append(item_copy)
                # print('---new mapping check---', new_mapping)
                configuration['mapping'] = new_mapping
        else:

            logger.debug('SESSION OBJECT WAS NOT IN CONFIGURATION. EXCEL UPDATE CALLED')
            print('\n')

            if isinstance(configuration['mapping'][0]['Task Text'],(list,)):

                for item in configuration['mapping']:

                    task_text_list = item['Task Text']
                    if len(item['Task Text']) == 1:
                        item['Task Text'] = item['Task Text'][0]
                        new_mapping.append(item)
                    else:
                        if item['Is Start'] == 'true' or item['Is Start'] == True:
                            task_var = item['Task Text']
                            item['Task Text'] = item['Task Text'][0]
                            new_mapping.append(item)

                            for i in task_var[1:]:
                                item_copy = item.copy()
                                item_copy['Is Start'] = 'false'
                                item_copy['Task Text'] = i
                                new_mapping.append(item_copy)
                        else:
                            for text in task_text_list:
                                item_copy = item.copy()
                                for k, v in item_copy.items():
                                    if v == 'true':
                                        item_copy[k] = True
                                    elif v == 'false':
                                        item_copy[k] = False

                                # print('---text check---', text)
                                item_copy['Task Text'] = text

                                if item_copy not in new_mapping:
                                    new_mapping.append(item_copy)
                # print('---new mapping check---', new_mapping)
                configuration['mapping'] = new_mapping
        if popup_flag:
            is_valid, message = validate_bot(configuration, True)
            # popup_flag = False  # for safety?
        else:
            is_valid, message = validate_bot(configuration)
        # print('\n')
        # print('\n')
        print('\n')
        logger.info('********************SENDING THIS MAPPING TO UPDATE BOT = {}*****************'.format(configuration['mapping']))
        if is_valid == 0:
            is_saved,bot_id,message = update_bot(configuration,bot_id,grp_list)
            success_msg = 'Your bot has been updated!'
            if is_saved == 0:
                logger.info('Bot Updated Successfully')

                return jsonify(
                    status='Success',
                    id=bot_id,
                    msg='Bot updated successfully'
                )
            else:
                logger.error('Error in Bot update = {}'.format(message))
                return jsonify(
                    status='Error',
                    msg=message
                )

        else:

            logger.error('Error in Bot update = {}'.format(message))
            return jsonify(
                status='Error',
                msg=message
            )

@bot_blueprint.route('/api/bot/<bot_id>',methods=['GET','POST'])
#@requires_roles_groups({'role':['admin','compliance']})
def bot_read(bot_id):
    if request.method == 'POST':
        print("REQUEST JSON IN BOT READ FOR SPECIFIC BOT = {}".format(request.json))
        group_list = [str(i) for i in list(request.json['group_name'])]
        print("GROUP LIST IN BOT READ FOR SPECIFIC BOT = {}".format(group_list))
        is_valid,configuration, message = read_bot(bot_id)
        # print('config - ',configuration)
        configuration['_id'] = str(configuration['_id']).replace('ObjectId(','').replace(')','')
        groups_collection = get_collection("groups")
        grp_list_empty = []
        print("GROUPS COLLECTION = {}".format(groups_collection))
        for documents in groups_collection:
            print('GROUPS IN GROUP COLLECTION = {}'.format(documents))
            grp_list_empty.append(documents['name'])

        print("EMPTY GROUP LIST = {}".format(grp_list_empty))
        grp_dict_empty = {k: [] for v, k in enumerate(grp_list_empty)}
        new_mapping = []
        if is_valid == 0:
            last_id = 0
            task_text_dict = {}
            grp_task_text_dict = {i: {} for i in group_list}
            grp_mapping = {i: [] for i in group_list}
            print('GROUP TASK TEXT DICT = {}'.format(grp_task_text_dict))
            # print("1***",configuration['mapping'])
            for item in configuration['mapping']:
                for grp, val in grp_task_text_dict.items():
                    print('CURRENT VAL = {} and GROUP = {}'.format(val, grp))
                    try:
                        if last_id < int(float(item['Task ID'])):
                            last_id = int(float(item['Task ID']))
                    except:
                        pass
                    try:
                        if item['Task ID'] == '':
                            item['Task ID'] = '0'
                        if (item['Task ID'] in val) and (str(item['Group']).lower() == str(grp).lower()):


                            grp_task_text_dict[grp][item['Task ID']].append(item['Task Text'])
                            # print('CURRENT STATUS OF GROUP TASK TEXT DICT AFTER APPENDING= {}. Going to Break out of loop'.format(grp_task_text_dict))
                            break
                        elif str(item['Group']).lower() == str(grp).lower():

                            grp_task_text_dict[grp][item['Task ID']] = [item['Task Text']]
                            # print(
                            #     'CURRENT STATUS OF GROUP TASK TEXT DICT AFTER APPENDING IN NEW TASK ID = {}. Going to Break out of loop'.format(
                            #         grp_task_text_dict))
                            break
                    except Exception as e:
                        print("THIS EXCEPTION CAME = {}".format(e))
            seen_ids = []
                # print('--TASK TEXT DICT---',task_text_dict)
            # print("1***", configuration['mapping'])

            for map in configuration['mapping']:

                if str(map['Group']) != '':
                    print("SEEN IDS SO FAR GROUP WISE = {}".format(grp_dict_empty[str(map['Group'])]))
                    # print('\n')
                    # print('\n')
                    print("SEEN IDS SO FAR = {}".format(grp_dict_empty))
                    if map['Task ID'] == '':
                        map['Task ID'] = '0'
                    # if map['Task ID'] in seen_ids and str(map['Group']) in seen_group:
                    if map['Task ID'] in grp_dict_empty[str(map['Group'])]:
                        print("{} TASK ID FOR GROUP {} ALREADY PRESENT IN SEEN ID. GOIN TO SKIP".format(map['Task ID'],
                                                                                                        map['Group']))
                        pass
                    else:

                        print("{} IS A NEW TASK ID FOR GROUP {}.GOIN TO ADD IT".format(map['Task ID'], map['Group']))

                        # seen_ids.append(map['Task ID'])
                        grp_dict_empty[str(map['Group'])].append(map['Task ID'])


                        print("MAP IN CONFIGURATION MAPPING IS = {}".format(map))

                        print('GROUP IN MAP = {}'.format(map['Group']))
                        if str(map['Group']) in group_list:
                            try:

                                map['Task Text'] = list(set(grp_task_text_dict[str(map['Group'])][map['Task ID']]))
                                
                                grp_mapping[str(map['Group'])].append(map)
                            except:
                                pass
                            new_mapping.append(map)
                else:
                    print("FOND EMPTY GROUP NAME")
                    continue
            print("group mapping----->", grp_mapping)

            # print(grp_mapping,"--------- group task text --------")
            configuration['max_id'] = last_id
            #configuration['mapping'] = new_mapping
            configuration['mapping'] =grp_mapping# [{k:v} for k,v in grp_mapping.items()]

            return jsonify(
                status='Success',
                configuration=configuration,
                msg=message
            )
        else:
            return jsonify(
                status='Failure',
                configuration='',
                msg=message
            )

@bot_blueprint.route('/api/bot',methods=['GET','POST'])
#@requires_roles_groups({'role':['admin','compliance']})
#@admin_check()
def bot_read_all():
    if request.method == 'POST':
        try:
            print("REQUEST JSON IN BOT READ ALL = {}".format(request.json))
            group_list = [str(i) for i in list(request.json['group_name'])]
            print("GROUP LIST IN BOT READ ALL = {}".format(group_list))
            bots = get_collection("bot")
            groups_collection = get_collection("groups")
            grp_list_empty = []
            print("GROUPS COLLECTION = {}".format(groups_collection))
            for documents in groups_collection:
                print('GROUPS IN GROUP COLLECTION = {}'.format(documents))
                grp_list_empty.append(documents['name'])

            print("EMPTY GROUP LIST = {}".format(grp_list_empty))
            grp_dict_empty = {k: [] for v, k in enumerate(grp_list_empty)}
            bot_list = []
            print("1***")
            for index, bot in enumerate(bots):
                bot['_id'] = str(bot['_id']).replace('ObjectId(','').replace(')','')
                new_mapping = []
                last_id = 0
                task_text_dict = {}
                # grp_task_text_dict = {i : task_text_dict for i in group_list}
                grp_task_text_dict = {i: {} for i in group_list}
                grp_mapping = {i: [] for i in group_list}
                # print("2***",bot['mapping'])
                print('GROUP TASK TEXT DICT = {}'.format(grp_task_text_dict))
                bot_mapping=bot['mapping']
                # print('\n')
                # print('\n')
                # print('\n')
                # print('BOT MAPPING SUBSET = {}'.format(bot_mapping[:10]))
                # print('\n')
                # print('\n')
                # print('\n')
                for item in bot['mapping']:
                    # print('\n')
                    # print('\n')
                    # print('\n')
                    # print('\n')
                    # print("ITEM IN BOT MAPPING = {}".format(item))
                    # print('\n')
                    # print('\n')
                    #
                    # print('\n')
                    # print('\n')
                    for grp, val in grp_task_text_dict.items():
                        print('CURRENT VAL = {} and GROUP = {}'.format(val,grp))
                        try:
                            if last_id < int(float(item['Task ID'])):
                                last_id = int(float(item['Task ID']))
                        except:
                            pass
                        try:
                            if item['Task ID'] == '':
                                item['Task ID'] = '0'
                            if (item['Task ID'] in val) and (str(item['Group']).lower() == str(grp).lower()):

                                # print('\n')
                                # print('\n')
                                # print('CURRENT STATUS OF GROUP TASK TEXT DICT BEFORE APPENDING= {}'.format(
                                #     grp_task_text_dict))
                                # print('\n')
                                # print('\n')
                                # print('GROUP AND TASK ID ALREADY IN ITEM AND TASK TEXT DICT IS SAME - {} and {}'.format(str(item['Group']).lower(),str(grp).lower()))
                                # print('\n')
                                # print('\n')
                                # print('APPENDING TO THIS GROUP IN GRP TASK DICT = {}'.format(grp_task_text_dict[grp][item['Task ID']]))

                                # val[item['Task ID']].append(item['Task Text'])
                                grp_task_text_dict[grp][item['Task ID']].append(item['Task Text'])
                                # print('CURRENT STATUS OF GROUP TASK TEXT DICT AFTER APPENDING= {}. Going to Break out of loop'.format(grp_task_text_dict))
                                break
                            elif str(item['Group']).lower()==str(grp).lower():
                                # print('CURRENT STATUS OF GROUP TASK TEXT DICT BEFORE APPENDING IN NEW TASK ID= {}'.format(
                                #     grp_task_text_dict))
                                # print('\n')
                                # print('NEW TASK ID BUT GROUP SAME = {} and {}'.format(str(item['Group']).lower(),str(grp).lower()))
                                # print('\n')
                                # print('\n')
                                # print('APPENDING TO THIS GROUP IN GRP TASK DICT IN NEW ID = {}'.format(grp_task_text_dict[grp]))
                                # print('\n')
                                # print('\n')
                                # val[item['Task ID']] = [item['Task Text']]
                                grp_task_text_dict[grp][item['Task ID']] = [item['Task Text']]
                                # print(
                                #     'CURRENT STATUS OF GROUP TASK TEXT DICT AFTER APPENDING IN NEW TASK ID = {}. Going to Break out of loop'.format(
                                #         grp_task_text_dict))
                                break
                        except Exception as e:
                            print("THIS EXCEPTION CAME = {}".format(e))
                        # print('--iteration check--', task_text_dict)
                # print("3***",bot_mapping)
                # print('CHECKING GROUP TASK TEXT DICT 2 = {}'.format(grp_task_text_dict))
                # print('\n')
                # print('\n')
                # print("EMPTY GROUP DICT = {}".format(grp_dict_empty))
                # print('\n')
                # print('\n')
                # print('BOT MAPPING SUBSET = {}'.format(bot_mapping[:10]))
                seen_ids = []
                seen_group = []
                    # print('--TASK TEXT DICT---', task_text_dict)
                for map in bot_mapping:
                    if str(map['Group']) != '':
                        print("SEEN IDS SO FAR GROUP WISE = {}".format(grp_dict_empty[str(map['Group'])]))
                        # print('\n')
                        # print('\n')
                        print("SEEN IDS SO FAR = {}".format(grp_dict_empty))
                        if map['Task ID'] == '':
                            map['Task ID'] = '0'
                        # if map['Task ID'] in seen_ids and str(map['Group']) in seen_group:
                        if map['Task ID'] in grp_dict_empty[str(map['Group'])]:
                            print("{} TASK ID FOR GROUP {} ALREADY PRESENT IN SEEN ID. GOIN TO SKIP".format(map['Task ID'],map['Group']))
                            pass
                        else:

                            print("{} IS A NEW TASK ID FOR GROUP {}.GOIN TO ADD IT".format(map['Task ID'],map['Group']))

                            # seen_ids.append(map['Task ID'])
                            grp_dict_empty[str(map['Group'])].append(map['Task ID'])

                            # print('\n')
                            # print('\n')
                            # print('\n')
                            # print('\n')
                            # print('GROUP LIST = {}'.format(group_list))
                            print("MAP IN CONFIGURATION MAPPING IS = {}".format(map))
                            # print('\n')
                            # print('\n')
                            # print('\n')
                            # print('\n')
                            #map['Task Text'] = grp_task_text_dict[str(map['Group']).lower()][map['Task ID']]
                            print('GROUP IN MAP = {}'.format(map['Group']))
                            if str(map['Group']) in group_list:
                                try:
                                    # print("APPENDING MAP TO GROUP = {}".format(str(map['Group'])))
                                    # print("GROUP TASK TEXT DICT = {}".format(grp_task_text_dict[str(map['Group'])]))
                                    # print('GROUP TASK TEXT WITH GROUP MAP')
                                    map['Task Text'] = list(set(grp_task_text_dict[str(map['Group'])][map['Task ID']]))
                                    # print('MAP TASK TEXT = {}'.format(map['Task Text']))
                                    grp_mapping[str(map['Group'])].append(map)
                                except:
                                    pass
                                new_mapping.append(map)
                    else:
                        print("FOND EMPTY GROUP NAME")
                        continue
                print("group mapping----->",grp_mapping)
                bot['max_id'] = last_id


                #bot['mapping'] = new_mapping
                bot['mapping'] = grp_mapping#[{k:v} for k,v in grp_mapping.items()]
                bot_list.append(bot)
            # print(grp_task_text_dict,"4***")
            return jsonify(
                bots= bot_list
            )
        except Exception as e:
            print("ERROR IN BOT READ ALL = {}".format(e))
            return jsonify(
                status='Failure',
                msg=str(e)
            )

@bot_blueprint.route('/api/bot/<bot_id>/delete',methods=['GET','POST'])
#@requires_roles_groups({'role':['admin','compliance']})
def bot_delete(bot_id):
    if request.method == 'GET':
        is_valid, message = delete_bot(bot_id)
        if is_valid == 0:
            return jsonify(
                status='Success',
                msg=message
            )
        else:
            return jsonify(
                status='Failure',
                msg=message
            )



################# Chat Events ####################

@bot_blueprint.route('/bot/<bot_id>/init',methods=['GET'])
def bot_init(bot_id):
    print('BOT ID: ',bot_id)
    try:

        if len(get_by_id('bot',bot_id))>0:
            logger.debug('Valid bot id - %s',bot_id)
            session = {}
            session['on_connect_start'] = True
            session['suggestion'] = ''
            session['fuzzyFlag'] = False
            session['restartFlag'] = False
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session['codeFlag'] = False
            session['guidedFlag'] = False
            session['audioSentFlag'] = False
            session['ASRFlag'] = False
            session['results'] = ''
            session['selectColumnQ'] = ''
            session['filterColumnQ'] = ''
            session['filterValueQ'] = ''
            session['surveyFlagNum'] = False
            session['surveyFlagComm'] = False
            session['endSurveyFlag'] = False
            session['feedbackFlag_ES'] = False
            session['suggestCounter_ES'] = 0
            session['audioSentFlag_ES'] = False
            session['bot_id'] = bot_id
            session['disable_input'] = False
            session['created_on'] = time.time()  # todo time zone
            session['menuSelection']=False
            session['menuResult'] = ''
            session['subMenuSelection'] = False
            session['subMenuResult'] = ''
            session['es_restart_counter'] = 0
            session['email'] = request.args.get("email_id")
            session['user_id'] = request.args.get("user_id")

            session_id = set_context(session)

            return jsonify(
                status='Success',
                session_id=session_id
            )

        else:
            return jsonify(
                status='Failure',
                msg='Invalid bot id'
            )

    except Exception as e:
        logger.error(str(e))
        return jsonify(
            status='Failure',
            msg='Invalid bot id'
        )


@bot_blueprint.route('/bot/<bot_id>/<session_id>/end',methods=['POST'])
def bot_end(bot_id,session_id):
    if (bson.objectid.ObjectId.is_valid(bot_id) and get_by_id('bot',bot_id)):
        if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context',session_id)):
            update('status', session_id, {'has_ended': True})
            return jsonify(
                status='Success',
                msg='Session ended'
            )
        else:
            return jsonify(
                status='Failure',
                msg='Not a valid Session ID'
            )
    else:
        return jsonify(
            status='Failure',
            msg='Not a valid Bot ID'
        )

@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat',methods=['POST'])
@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat/<user_id>', methods=['POST'])
def bot_chat(bot_id,session_id, user_id='None'):
    if (bson.objectid.ObjectId.is_valid(bot_id) and get_by_id('bot',bot_id)):
        if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context',session_id)):
            current_status = get_by_id('status', session_id)
            # try:
            #     ended_flag = current_status['has_ended']
            # except Exception as e:
            #     ended_flag = False
            # if ended_flag == True:
            #     return jsonify(
            #         status='Failure',
            #         result={"text":'Session has already ended'},
            #         current_state='None'
            #     )
            app.config['BABEL_DEFAULT_LOCALE'] = get_by_id('bot', bot_id)['language']
            refresh()
            message_request = request.get_json(force=True)
            message = message_request['message']
            current_state = message_request['state']
            asr_flag = message_request['ASR']
            print('ASR FLAG VALUE IN  BOT CHAT FUNCTION = {}'.format(asr_flag))
            session = get_context(session_id)

            if not session['user_id'] and message_request['user_id']:
                session['user_id'] = message_request['user_id']

            print('ON CONNECT- ',session['on_connect_start'])

            global question_group
            global group
            if session['on_connect_start'] == True:
                execution_engine = ExecutionEngine(session_id, bot_id)
                state_definition = get_by_id('bot', bot_id)['mapping']
                for state in state_definition:
                    for key, value in state.items():
                        if key=='Task Text':
                            ques=value.lower()
                        if key=='Group':
                            grp=value.lower()
                    question_group.update({ques:grp})
                #group list
                group_name_list=get_all('groups','name',not None)
                for state in state_definition:
                    for key, value in state.items():
                        if key == 'Is Start' and value == 'true':
                            response = execution_engine.execute(state)
                            return jsonify(
                                status='Success',
                                result=response,
                                current_state=get_by_id("status",session_id)["current_state"]
                            )
            else:
                #if condition - if msg is empty and on connet start is FALSE then just re-execute the current state
                #if we refresh, then just re-execute the current state.
                #pop out the last state in history because it will be added back in via conversation logger
                if message == '':
                    execution_engine = ExecutionEngine(session_id, bot_id)
                    current_state = get_by_id("status", session_id)["current_state"]
                    state_definition = get_by_id('bot', bot_id)['mapping']
                    for state in state_definition:
                        group=state['Group']
                        for key, value in state.items():
                            if key == 'Task Name' and value == current_state:
                                history = get_by_id('status', session_id)['history']
                                history = history[:-1]  # pop out the last element
                                update('status',session_id, {'history': history,'Group':group})
                                response = execution_engine.execute(state)
                                return jsonify(
                                    status='Success',
                                    result=response,
                                    current_state=get_by_id("status", session_id)["current_state"]
                                )

                else:
                    if message.lower() in question_group.keys():
                        group=question_group[message.lower()]

                    execution_engine = ExecutionEngine(session_id, bot_id)
                    response = execution_engine.resume(message, asr_flag, bot_id,group)
                    if response is None:
                        return jsonify(
                            status='Failure',
                            result=response,
                            current_state=get_by_id("status", session_id)["current_state"]
                        )
                    else:
                        return jsonify(
                            status='Success',
                            result=response,
                            current_state = get_by_id("status",session_id)["current_state"]
                        )
        else:
            return jsonify(
                status='Failure',
                result='Invalid session id'
            )
    else:
        return jsonify(
            status='Failure',
            result='Invalid bot id'
        )



@bot_blueprint.route('/dbqsearch/<bot_id>',methods=['POST'])
def database_question_search(bot_id):
    search_key=request.json['search_key']
    bot = get_by_id('bot', bot_id)
    search_result=[]
    for i in bot['mapping']:
        if search_key in i['Task Name']:
            search_result.append(i['Task Name'])

    return jsonify(
        status='Success',
        msg={search_key:search_result}
    )

