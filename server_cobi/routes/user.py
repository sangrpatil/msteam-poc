from flask import Blueprint, request, jsonify, current_app, render_template, session, redirect, Markup, url_for, flash
from utility.mongo_dao import *

user_blueprint = Blueprint('user', __name__, template_folder='templates')
# from flask import json
from utility.decorators import *
import json
from bson import json_util, ObjectId
from utility.session_manager import get as get_context, set as set_context, modify as update_context


#@user_blueprint.route('/user/create/<session>', methods=['POST', 'GET'])
@user_blueprint.route('/api/user/create', methods=['POST', 'GET'])
#@admin_check()
# @requires_roles_groups({'role':['admin']})
def user_create():
    try:
        message = request.json
        # print(message)
        # username=message['sessionObj']['user_name']
        # if not is_admin(username):
        #     return jsonify(
        #         status='Error',
        #         msg="You are not allowed to access this page."
        #     )
        # del message['sessionObj']
        if "_id" in message.keys():
            del message['_id']
        existing_user = get_all('user', 'username', message['username'])
        print(existing_user)
        message.update({"lastlogin": "Never logged in"})
        message.update({"group": []})
        if len(existing_user) > 0:
            return jsonify(
                status='Failed',
                msg="User creation failed"
            )
        else:
            insert('user', message)
            # insert('user',{"username":message['username'],'password':message['password'],"type":message['type'],"email":message["email"],"lastlogin":"Never logged in","group":[]})
            return jsonify(
                status='Success',
                msg="User created successfully"
            )
    except Exception as e:
        return jsonify(
            status='Failed',
            msg=str(e)
        )


@user_blueprint.route('/api/user/update', methods=['POST'])
# @requires_roles_groups({'role':['admin']})
def user_update():
    # try:
    message = request.json
    # admin_check
    # username = message['sessionObj']['user_name']
    # if not is_admin(username):
    #     return jsonify(
    #         status='Error',
    #         msg="You are not allowed to access this page."
    #     )
    # # del message['sessionObj']
    existing_user = get_by_id('user', message['_id'])
    print(existing_user)
    if len(existing_user) > 0:
        # user = eval(existing_user[0])
        # update('user',message('_id'),{"username":message['username'],'password':message['password'],"type":message['type']})
        id = message['_id']
        del message['_id']
        # message['permission']=[]
        if "lastlogin" in message.keys():
            del message['lastlogin']

        if len(existing_user.get("group")) > 0:
            existing_grp = [str(i["_id"]).replace('ObjectId(', '').replace(')', '') for i in
                            existing_user.get("group")]

            if len(message['group']) == 0:

                for j in existing_user.get("group"):
                    group_user_update(j['_id'], id, existing_user.get("username"), message['username'], False)
            else:
                new_grp = [i["_id"] for i in message['group']]
                for k in existing_grp:
                    if k not in new_grp:
                        group_user_update(k, id, existing_user.get("username"), message['username'], False)

        if "group" in message.keys() and len(message['group']) > 0:
            message['permission'] = ['read', 'write']
            print("permission updatetd")
            for i in message['group']:
                # update('groups',i['_id'],{"user_list"})
                group_user_update(ObjectId(i['_id']), id, existing_user.get("username"), message['username'], True)
                i['_id'] = ObjectId(i['_id'])
        elif len(message['group']) == 0:
            print("0 group")
            message['permission'] = []
        updated_user = update('user', id, message)

        return jsonify(
            status='Success',
            msg="User updated successfully"
        )
    else:
        return jsonify(
            status='Failed',
            msg="User updation failed"
        )


# except Exception as e:
#    return jsonify(
#        status='Failed',
#        msg=str(e)
#    )

@user_blueprint.route('/api/user/read', methods=['GET', 'POST'])
# @requires_roles_groups({'role':['admin']})
def user_read():
    #try:
    #admin_check
    message = request.json
    username = message['sessionObj']['user_name']
    if not is_admin(username):
        return jsonify(
            status='Error',
            msg="You are not allowed to access this page."
        )
    del message['sessionObj']
    users = get_collection("user")
    user_list = []
    for index, user in enumerate(users):
        usr = {}
        usr['_id'] = str(user['_id']).replace('ObjectId(', '').replace(')', '')
        usr['username'] = str(user['username'])
        usr['lastlogin'] = str(user['lastlogin'])
        usr['email'] = str(user['email'])
        usr['password'] = str(user['password'])
        if 'permission' in user.keys():
            usr['permission'] = user['permission']
        grp_list = []
        role_list = []
        for grp in user['group']:
            grp_list.append({"_id": str(grp['_id']).replace('ObjectId(', '').replace(')', ''), "name": grp['name']})
        '''
       for role in user['role']:
           role_list.append({str(role['_id']).replace('ObjectId(', '').replace(')', ''): role['name']})

       if 'group' in user.keys():
           for grp in user['group']:
               g={}
               g['_id']=str(grp['_id']).replace('ObjectId(', '').replace(')', '')
               g['name']=grp['name']
               try:
                   if 'role' in grp.keys():
                       for role in grp["role"]:
                           role_list.append({'name':role['name'],'_id':str(role['_id']).replace('ObjectId(', '').replace(')', '')})
                       g['role']=role_list

               except:
                   pass
               grp_list.append(g)'''
        usr['group'] = grp_list
        # usr['role']=role_list
        user_list.append(usr)

    return jsonify(
        status="Success",
        msg=user_list)


# except Exception as e:
#    return jsonify(
#     status='Failed',
#      msg=str(e)
#   )


@user_blueprint.route('/api/user/delete', methods=['GET', 'POST'])
# @requires_roles_groups({'role':['admin']})
def user_delete():
    # try:
    message = request.json
    # admin_check
    # username = message['sessionObj']['user_name']
    # if not is_admin(username):
    #     return jsonify(
    #         status='Error',
    #         msg="You are not allowed to access this page."
    #     )
    # del message['sessionObj']
    print(message)
    # grp_map={}
    existing_user = get_by_id('user', message['_id'])
    print(existing_user)
    if len(existing_user) > 0:
        for i in existing_user.get('group'):
            group_user_update(ObjectId(i.get('_id')), message['_id'], existing_user.get('username'),
                              existing_user.get('username'), False)
            # grp_map[i.get('_id')] = i.get('user_list')
            # usrlst=i.get('user_list').remove(message['_id'])
            # update('groups',i.get('_id'),{'user_list':usrlst})

        delete('user', message['_id'])
        return jsonify(
            status='Success',
            msg="User deleted successfully"
        )
    else:
        return jsonify(
            status='Failed',
            msg="No record found"
        )


# except Exception as e:
#     return jsonify(
#         status='Failed',
#         msg=str(e)
#     )

@user_blueprint.route('/api/user/join', methods=['GET', 'POST'])
# @requires_roles_groups({'role':['admin']})
def user_join():
    return jsonify(
        msg=json.dumps(join('user', 'groups', 'id', 'b')),
        status='Success'
    )


def group_user_update(id, user_id, prev_name, new_name, add=True):
    group = get_by_id('groups', id)
    usr_list = group.get("user_list")
    usr = {"_id": ObjectId(user_id), "name": prev_name}
    print(usr)
    print("usr_list", usr_list)
    if add:
        if usr not in usr_list:
            usr["name"] = new_name
            usr_list.append(usr)
        elif not (prev_name == new_name):
            usr_list.remove(usr)
            usr["name"] = new_name
            print(usr, "***")
            usr_list.append(usr)
    else:

        usr_list.remove(usr)
    update("groups", id, {"user_list": usr_list})


@user_blueprint.route('/api/user/specific', methods=['GET', 'POST'])
def user_specific_data():
    message = request.json
    print(message)
    # admin_check
    # username = message['sessionObj']['user_name']
    # if not is_admin(username):
    #     return jsonify(
    #         status='Error',
    #         msg="You are not allowed to access this page."
    #     )
    # del message['sessionObj']
    user = get_by_id('user', message['_id'])
    usr = {}
    usr['_id'] = str(user.get('_id')).replace('ObjectId(', '').replace(')', '')
    usr['username'] = str(user.get('username'))
    usr['lastlogin'] = str(user.get('lastlogin'))
    usr['email'] = str(user.get('email'))
    usr['password'] = str(user.get('password'))
    if 'permission' in user.keys():
        usr['permission'] = user.get('permission')
    grp_list = []
    role_list = []
    for grp in user.get('group'):
        print('USER_GROUP = {}'.format(grp))
        grp_list.append({"_id": str(grp['_id']).replace('ObjectId(', '').replace(')', ''), "name": grp['name']})
    usr['group'] = grp_list
    return jsonify(
        status="Success",
        msg=usr)












