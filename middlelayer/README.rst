=====================
Messenger Interface
=====================


Provides Interface to Facebook, Line  and Web chat Interafaces

Concept Architecture
=========================

.. image:: resources/static/images/Architecture.png
  :width: 400
  :alt: Unified Architecture

Inheritance Architecture
===========================

.. image:: resources/static/images/Besponsa.png
  :width: 400
  :height: 300
  :alt: Besponsa Image

Directory Structure
====================

.. image:: resources/static/images/directory_structure.png
  :alt: Directory Structure

Base Package (Base)
====================
::


  MessengerMixin   (MessengerMixin.py)
  MessageMixin     (MessageMixin.py)
  TranscriberMixin (TranscriberMixin.py)


Child Package (Facebook)
===========================
::


  FacebookMessenger (FacebookMessenger.py)
  FacebookMessage   (FacebookMessage.py)
  settings.py       (contains settings localized to Facebook Channel like Access token, secret etc)



SETTINGS.py

::

 DEFAULT_ENTRYPOINT_METHOD = "process_message"

 MESSENGER_WEBHOOK_MAPPING = [
	    {
	        "package_name": "Line",
	        "url_rule": "/line_webhook",
	        "METHODS": ['POST','GET']
	    }
	]

 On application initialization, the url "/line_webhook" will be attached to method "process_message" of the class
 "LineMessenger" present in LineMessenger.py in package "Line". The allowed Methods will be "GET" and "POST".


*********************
Adding a new Channel
*********************

To add a new Channel (Lets say Kakao) , Follow the below steps
::

 1. Add new package in messenger pagakge will channel_name
 	- messenger
 	  - messenger
 		- Base           (Already present)
 		- Facebook       (ALready Present)
 		- Line           (Already Present)
 		- Kakao          (newly added)
 
 2. Under Kakao directory add __init__.py
  	- messenger
 	  - messenger
 	    - Kakao
 	      - __init__.py

 3. Under Kako Directory add files KakaoMessenger.py and KakaoMessage.py
  	- messenger
 	  - messenger
 	    - Kakao
 	      - __init__.py
 	      - KakaoMessenger.py
 	      - KakaoMessage.py


 	Note: KakaoMessenger.py should have a class "KakakoMessenger". The class should inherit MessengerMixin from 
 	      "messenger/messenger/Base/MessengerMixin.py".
 	      KakaoMessage.py should have a class "KakaoMessage"

 	      MessengerMixin class from Base package will provide 
 	      		abstract methods (should be overwtitten)
 	      		 * _test_connection
 	      		 * process_message

 	      		concreate methods 
 	      		 * send_message_to_server

 	      		 (kindly see code documentation for functionlitites of thes methods)


 4. Add settings.py in the Kakao package
   	- messenger
 	  - messenger
 	    - Kakao
 	      - __init__.py
 	      - KakaoMessenger.py
 	      - KakaoMessage.py
 	      - settings.py


 	Note: settings.py should contain the settings localised to kakao (Access token, secretkey, fucntions to process the messages)
 		  For more inormation, see settings in "messenger/Facebook/settings.py"


 5. Now go to SETTINGS.py in most parent directory in project and add a entry for KAKAO. in MESSENGER_WEBHOOK_MAPPING dictionary.
 		For example:
 		{
	        "package_name": "Kakao",
	        "url_rule": "/kakao_webhook",
	        "METHODS": ['POST','GET']
	    }

	    package_name: is the name of package that you added and in which KakaoMessenger class is existing
	    url_rule: The url will serve as webhook and will be attached to "process_message" function in KakaoMessenger class. Only POST and 
	    		  GET methods will be allowed. The URL has to updated in Kakao Website. So all the events will be reveived in 
	    		  "process_message" method


	
Precap
::


 Package Name: Kakako
 Under Package file name should be KakaoMessenger.py ( Messenger should be suffix)
 Class name in KakaoMessenger.py should be KakaoMessenger (Messenger should be suffix) and should inherit MessengerMixin Class
 KakaoMessenger should over ride "process_message" method of MessengerMixin class. This method will receive all the messages from
 Kakao












 






