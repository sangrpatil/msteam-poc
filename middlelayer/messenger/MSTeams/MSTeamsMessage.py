from copy import deepcopy

from messenger.Base.MessageMixin import MessageMixin
from .settings import SERVER_CLIENT_FUNCTIONS_MAPPING as FUNCTION_MAPPINGS, MESSAGE_CONFIG


class MSTeamsMessage(MessageMixin):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._logger.info("Initialized MSTeamsMessage Class")

    _get_sliced_text = lambda self, x: x[:self._max_text_length]

    def _url_message(self, item):
        """
        Process the response to be sent to bot for displaying urls
        :param item: response message from server
        :return: processed response to be sent to bot for url messages
        """
        try:
            self._logger.info(f"Processing item {item}")
            return {"text": f"[{item['text']}]({item['interaction elements']})"}
        except Exception as e:
            self._logger.error(e)

    def _string_message(self, item):
        """
        Process the response to be sent to bot for displaying text
        :param item: response message from server
        :return: processed response to be sent to bot for text messages
        """
        try:
            self._logger.info(f"Processing item {item}")
            return {"text": item["text"]}
        except Exception as e:
            self._logger.error(e)

    def _button_message(self, item):
        """
        Process the response to be sent to bot for displaying buttons on card
        :param item: response message from server
        :return: processed response to be sent to bot for messages with buttons
        """
        try:
            self._logger.info(f"Processing item {item}")

            # setting up variables with initial data
            response_message = {}
            interaction_elements = item["interaction elements"]
            number_of_buttons = len(interaction_elements)
            buttons_on_card = MESSAGE_CONFIG['MAX_BUTTON_ON_CARD']

            attachments = {
                "attachments": []
            }

            # adding buttons to card
            init = 0
            end = buttons_on_card
            while init < number_of_buttons:
                button_list = []

                if number_of_buttons < end or (number_of_buttons - init) <= buttons_on_card:
                    end = (number_of_buttons % buttons_on_card) + init
                elif number_of_buttons > buttons_on_card and init > 0:
                    end += buttons_on_card

                for i in range(init, end):
                    button_list.append({
                        "type": "messageBack",
                        "title": interaction_elements[i],
                        "displayText": interaction_elements[i],
                        "value": {"button_message": interaction_elements[i]}
                    })

                attachments["attachments"].append({
                    "contentType": "application/vnd.microsoft.card.hero",
                    "content": {
                        "text": item["text"],
                        "buttons": button_list
                    }
                })
                init += buttons_on_card
            response_message.update(attachments)
            return response_message
        except Exception as e:
            self._logger.error(e)

    def _image_message(self, item):
        """
        Process the response to be sent to bot for displaying images in card
        :param item: response message from server
        :return: processed response to be sent to bot for image messages
        """
        try:
            self._logger.info(f"Processing item {item}")

            # setting up variables with initial data
            response_message = {}
            interaction_elements = item["interaction elements"]
            number_of_images = len(interaction_elements)
            images_on_card = MESSAGE_CONFIG['MAX_IMAGES_ON_CARD']

            attachments = {
                "attachments": [{
                    "contentType": "application/vnd.microsoft.teams.card.o365connector",
                    "content": {
                        "type": "MessageCard",
                        "text": item["text"],
                        "sections": []
                    }
                }]
            }

            # adding images to card
            init = 0
            end = images_on_card
            while init < number_of_images:
                image_list = []

                if number_of_images < end or (number_of_images - init) <= images_on_card:
                    end = (number_of_images % images_on_card) + init
                elif number_of_images > images_on_card and init > 0:
                    end += images_on_card

                for i in range(init, end):
                    image_list.append({
                        "image": interaction_elements[i]
                    })

                attachments["attachments"][0]["content"]["sections"].append({"images": image_list})
                init += images_on_card
            response_message.update(attachments)
            return response_message
        except Exception as e:
            self._logger.error(e)

    def _base_response(self, item):
        """
        :param item: request message payload from the bot to middleware
        :return: dictionary with base response fields
        """
        try:
            return {
                "type": "message",
                "from": {
                    "id": item["recipient"]["id"],
                    "name": item["recipient"]["name"]
                },
                "conversation": {
                    "id": item["conversation"]["id"]
                },
                "recipient": {
                    "id": item["from"]["id"]
                },
                "replyToId": item["id"]
            }
        except Exception as e:
            self._logger.error(e)

    def process_message(self, message):
        """
        Processes the response from server and sent back to bot
        :param message: response from the server
        :return: processed response message to be sent back to bot
        """
        try:
            self._logger.info("Processing message ")

            # list to store final output
            final_output = []

            # extracting message request from the bot to use it in response
            message_request = message["message_request"]

            # base response message
            base_response = self._base_response(message_request)

            # processing the response message to be sent to bot
            for item in message["result"]["message"]:
                intermediate_response = deepcopy(base_response)
                response = getattr(self, FUNCTION_MAPPINGS.get(item.get('interaction')))(item)
                intermediate_response.update(response)
                final_output.append(intermediate_response)
            return final_output
        except Exception as e:
            self._logger.error(e)
