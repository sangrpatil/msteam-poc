import os

SERVER_CLIENT_FUNCTIONS_MAPPING = {
    'url': "_url_message",
    "text": "_string_message",
    "button_vertical": "_button_message",
    "button_horizontal": "_button_message",
    "images": "_image_message"
}

MESSAGE_CONFIG = {
    'MAX_TEXT_LENTGH': 10000,
    'MAX_BUTTON_LABEL_LENGTH': 50,
    'MAX_BUTTON_ON_CARD': 30,
    'MAX_IMAGES_ON_CARD': 5
}

AUTHORIZATION_TOKEN_CONFIG = {
    "URL": "https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token",
    "GRANT_TYPE": "client_credentials",
    "CLIENT_ID": os.environ.get("CLIENT_ID") or "3b1e063d-7506-4b22-9a4b-c62b1dfc52c5",
    "CLIENT_SECRET": os.environ.get("CLIENT_SECRET") or "MqpTejpO4G/ndJwH-b@l9kS:zu90.jOG",
    "SCOPE": "https://api.botframework.com/.default"
}
