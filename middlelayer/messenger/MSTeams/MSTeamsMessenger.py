import json
import time
from copy import deepcopy

import requests
from flask import request, Response

from messenger.Base.MessengerMixin import MessengerMixin
from messenger.MSTeams.MSTeamsMessage import MSTeamsMessage
from .settings import MESSAGE_CONFIG, AUTHORIZATION_TOKEN_CONFIG


def validate_token(func):
    """
    :param func: function to be executed in decorator
    :return: authorization token
    """

    def wrapper(*args):
        start_time = MSTeamsMessenger.token["generated_at"]
        end_time = time.time()
        if (end_time - start_time) > MSTeamsMessenger.token["expires_in"] - 5:
            return func(*args)
        return MSTeamsMessenger.token

    return wrapper


class MSTeamsMessenger(MessengerMixin):
    _type = 'msteams'
    token = {
        "generated_at": time.time(),
        "expires_in": 0
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._conn_obj = None
        self._init_app(**kwargs)

    def _init_app(self, **kwargs):
        """
        :param kwargs: keyword argument
        :return: MSTeams Message object
        """
        message_configs = deepcopy(MESSAGE_CONFIG)
        message_configs.update(dict(logger=self._logger))
        self._message_object = MSTeamsMessage(**message_configs)

    def _test_connection(self):
        """
        Test the connection with MSTeams Messenger service
        :return: Boolean True
        """
        self._logger.info("Test Connect Called in MSTeams Messenger")
        return True

    def _get_conn_obj(self):
        pass

    def _get_msg_object(self):
        return self._message_object

    @validate_token
    def _get_authorization_token(self):
        """
        Method to get the authorization token from microsoft online bot framework
        :return: Json having authorization token
        """
        try:
            headers = {
                "Content-Type": "application/x-www-form-urlencoded"
            }

            payload = {
                "grant_type": AUTHORIZATION_TOKEN_CONFIG["GRANT_TYPE"],
                "client_id": AUTHORIZATION_TOKEN_CONFIG["CLIENT_ID"],
                "client_secret": AUTHORIZATION_TOKEN_CONFIG["CLIENT_SECRET"],
                "scope": AUTHORIZATION_TOKEN_CONFIG["SCOPE"]
            }

            response = requests.post(
                url=AUTHORIZATION_TOKEN_CONFIG["URL"],
                data=payload,
                headers=headers
            )
            if response.status_code == 200:
                MSTeamsMessenger.token = response.json()
                MSTeamsMessenger.token.update({"generated_at": time.time()})
            return response.json()
        except Exception as e:
            self._logger.error(e)

    def get_email_id(self, service_url, conversation_id, token):
        """
        Extract email id from msteam request using converastions API
        :param service_url: url received from msteam request
        :param conversation_id: conversation id from msteam request
        :param token: authorization token
        :return: email_id of message sender
        """
        try:
            email_id = ""
            url = f"{service_url}v3/conversations/{conversation_id}/members"

            headers = {
                "Authorization": token,
                "Content-Type": "application/json"
            }

            response = requests.get(
                url=url,
                headers=headers
            )

            if response.status_code == 200:
                # In one to one chat, send first members email id
                email_id = response.json()[0]["email"]

            return email_id
        except Exception as e:
            self._logger.error(e)

    def _send_response_to_bot(self, bot_request, payload, token):
        """
        Method send the response to the bot from where the request initialized
        :param bot_request: request payload that was transferred with bot message to middleware
        :param payload: data to be sent from middleware to bot
        :param token: authorization token
        :return: json with response id
        """
        try:
            url = f"{bot_request['serviceUrl']}v3/conversations/{bot_request['conversation']['id']}/activities/{bot_request['id']}"

            headers = {
                "Authorization": f"{token['token_type']} {token['access_token']}",
                "Content-Type": "application/json"
            }

            response = requests.post(
                url=url,
                data=json.dumps(payload),
                headers=headers
            )
            return response.json()
        except Exception as e:
            self._logger.error(e)

    def process_message(self):
        """
        Processes the incoming message from bot
        :return: json with all the response ids for messages sent from middleware to bot
        """
        try:
            # storing request json
            message_request = request.json

            # check the message type from bot, parse and send the message to server
            if (message_request['type'] == 'message') and ('value' in message_request) and (
                    'button_message' in message_request['value']):
                message_request['text'] = message_request['value']['button_message']
            elif message_request['type'] == 'conversationUpdate':
                message_request['text'] = ""

            # sending message to the server
            token = self._get_authorization_token()
            token_string = f"{token['token_type']} {token['access_token']}"
            email_id = self.get_email_id(message_request['serviceUrl'], message_request["conversation"]["id"],
                                         token_string)

            response = self.send_message_to_server(
                type=MSTeamsMessenger._type,
                message=message_request['text'],
                user_id=message_request['from']['id'],
                email_id=email_id
            )

            # adding request to the response received from server
            response.update({"message_request": message_request})

            # processing response message to MS teams compatible
            processed_response = self._get_msg_object().process_message(response)

            # send the response to bot
            response_ids = []
            for response_message in processed_response:
                response_ids.append(self._send_response_to_bot(message_request, response_message, token))

            self._logger.info(f"Processed response for messenger.")
            return Response(response=json.dumps(response_ids), status=200)
        except Exception as e:
            self._logger.error(e)
