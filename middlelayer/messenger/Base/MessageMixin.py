from abc import abstractmethod


class MessageMixin():

    def __init__(self, **kwargs):
        print("Initializing parent MessageMixin Class with ", kwargs)
        self._max_text_length = kwargs['MAX_TEXT_LENTGH']
        self._max_button_label_length = kwargs['MAX_BUTTON_LABEL_LENGTH']
        self._no_of_buttons = kwargs.get("NO_OF_BUTTONS")
        self._logger = kwargs['logger']

    @abstractmethod
    def _get_sliced_text(self, text):
        pass

    @abstractmethod
    def _url_message(self, item):
        pass

    @abstractmethod
    def _string_message(self, item):
        pass

    @abstractmethod
    def _button_message(self, item):
        pass

    @abstractmethod
    def process_message(self, message):
        pass
