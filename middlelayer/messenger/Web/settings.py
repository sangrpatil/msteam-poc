SERVER_CLIENT_FUNCTIONS_MAPPING = {
    'url': "_url_message",
    "text": "_string_message",
    "button_vertical": "_button_message",
    "button_horizontal": "_button_message"
}

MESSAGE_CONFIG = {
    'MAX_TEXT_LENTGH': 10000,
    'MAX_BUTTON_LABEL_LENGTH': 10000
}
