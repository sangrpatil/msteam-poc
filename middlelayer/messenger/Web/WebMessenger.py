import json
from copy import deepcopy

from flask import request

from messenger.Base.MessengerMixin import MessengerMixin
from messenger.Web.WebMessage import WebMessage
from .settings import MESSAGE_CONFIG


class WebMessenger(MessengerMixin):
    _type = 'web'

    def __init__(self, **kwargs):
        print("Initializaing supper init")
        super().__init__(**kwargs)

        self._conn_obj = None
        self._init_app(**kwargs)

    def _init_app(self, **kwargs):
        message_configs = deepcopy(MESSAGE_CONFIG)
        message_configs.update(dict(logger=self._logger))
        print(message_configs)
        self._message_object = WebMessage(**message_configs)

    def _test_connection(self):
        print("Test Connect Called in Line Messenger")
        return True

    def get_email_id(self):
        pass

    def _get_conn_obj(self):
        pass

    def _get_msg_object(self):
        return self._message_object

    def process_message(self):
        message_request = request.json
        response = self.send_message_to_server(type=WebMessenger._type, message=message_request['message'],
                                               user_id=message_request.get('grv_id'),
                                               session_id=message_request.get('session_id'))
        print(response)

        processed_response = self._get_msg_object().process_message(response)

        self._logger.info(f"Processed response: {processed_response}")

        return json.dumps(processed_response)
