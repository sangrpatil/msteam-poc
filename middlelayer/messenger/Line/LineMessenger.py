import json
import os
from copy import deepcopy

from flask import abort, request
from linebot import LineBotApi
from linebot import (WebhookHandler)
from linebot.exceptions import InvalidSignatureError
from linebot.models import MessageEvent, PostbackEvent, TextMessage, AudioMessage

from messenger.Base.MessengerMixin import MessengerMixin
from messenger.Base.TranscriberMixin import TranscriberMixin
from messenger.Line.LineMessage import LineMessage

if os.environ.get("TEST"):
    from .settings import TEST_CONFIG as CONFIG, TEST_MESSAGE_CONFIG as MESSAGE_CONFIG
else:
    from .settings import CONFIG, MESSAGE_CONFIG


class LineMessenger(MessengerMixin, TranscriberMixin):
    _client_connectionn_config = CONFIG
    _handler = WebhookHandler(_client_connectionn_config['CHANNEL_SECRET'])
    _type = 'line'

    def __init__(self, **kwargs):

        print("Initializing super init", kwargs)
        super().__init__(**kwargs)
        self._logger.info("Initializing the Line Object")
        self._conn_obj = None
        self._init_app(**kwargs)

    def _init_app(self, **kwargs):

        self._logger.info("Initializing the Line application _init_app")

        print(MESSAGE_CONFIG)
        message_configs = deepcopy(MESSAGE_CONFIG)
        message_configs.update(dict(logger=self._logger))
        print(message_configs)
        self._message_object = LineMessage(**message_configs)

        # define handler
        LineMessenger._handler.add(MessageEvent, message=TextMessage)(self._handle_message)
        LineMessenger._handler.add(PostbackEvent)(self._handle_message)
        LineMessenger._handler.add(MessageEvent, message=AudioMessage)(self._handle_message)

    def _test_connection(self):
        print("Test Connect Called in Line Messenger")
        return True

    def get_email_id(self):
        pass

    def _get_conn_obj(self):

        """
        returns the LineBotApi connection object
        It is doesn't exists, creates one and returns

        :return: LineBotApi Object
        """

        if not self._conn_obj or not self._test_connection():
            self._conn_obj = LineBotApi(self._client_connectionn_config['ACCESS_TOKEN'])
        return self._conn_obj

    @property
    def msg_object(self):

        """
        @property: returns corresponding message object

        :return: LineMessage Class object
        """
        return self._message_object

    def _fetch_content(self, id):

        content = self._get_conn_obj().get_message_content(id)
        return content

    def _convert_audio_to_text(self, id):

        content = self._fetch_content(id)

        file_name = self._get_file_name(ext="wav")
        self._dump(file_name, content.content)

        self._logger.info(f" Content will be store in file: {file_name}")

        text = self._get_text_from_audio_file(file_name, ext="m4a")
        self._logger.info(f"Received text after stt conversion {text}")

        return text

    def process_message(self):

        signature = request.headers['X-Line-Signature']
        # get request body as text
        body = request.get_data(as_text=True)

        self._logger.debug(json.dumps(body))
        # handle webhook body
        try:
            LineMessenger._handler.handle(body, signature)
        except InvalidSignatureError as e:

            self._logger.exception(e)

            abort(400)
        print("Here")
        return 'OK'

    def _handle_message(self, event):

        """
        Acts as handler to handle the event received from Line
        Declaration Lines: 48-50

        Called when event is MessageEvent or PostbackEvent
        For Message Event, message type can be:
                                    - TextMessage
                                    - AudioMessage

        To handle more message types and events, Kindly add declaration after line 50
        and add it condition to handle message after block of line no. 159

        :param event: Event received from Line
        :return: Nothing
        """

        try:
            self._logger.info(f"Event received: {event}")
            if event.type == "message":
                if event.message.type == "text":
                    text = event.message.text.lower()
                    self._logger.info(f"Text received from event: {text}")

                elif event.message.type == "audio":
                    self._logger.info(f"Audio received")
                    id = event.message.id
                    self._logger.info(f"Id for the audio received: {id}")

                    # retrieve the content using id
                    # text = self._convert_audio_to_text(id)

                    content = self._fetch_content(id)

                    text = self._transcribe_audio(data=content.content, ext="m4a")

            elif event.type == "postback":
                text = event.postback.data

            response = self.send_message_to_server(type=LineMessenger._type, message=text, user_id=event.source.user_id)
            print(response)

            processed_response = self.msg_object.process_message(response)
            print(self._get_conn_obj())
            print(event.reply_token)
            print(processed_response)
            self._get_conn_obj().reply_message(event.reply_token, processed_response)

        except Exception as e:
            self._logger.exception(e)
