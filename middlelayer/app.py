import json

import requests
from flask import Flask, request, redirect, abort, url_for, send_file

import ObjectFactory.ObjectFactory as factory
from SETTINGS import APP_PORT, APP_HOST
from SETTINGS import MESSENGER_WEBHOOK_MAPPING, BOT_ID, TEST_BOT_ID, MIDDLE_LAYER_ENDPOINT, SERVER_PORT, SERVER_HOST, \
    IMAGES_PATH
from utility_universal.mongo_dao import *
from utility_universal.session_manager import create_server_session

app = Flask(__name__)
app.config['BOT_ID'] = TEST_BOT_ID if os.environ.get("TEST") else BOT_ID
app.config['MIDDLE_LAYER_ENDPOINT'] = MIDDLE_LAYER_ENDPOINT


class Create():
    def creat_through_class(self):
        """
        Create instances of all the messengers and add url rules
        :return: None
        """
        print("Running Once")
        factory.ObjectFactory.get_obj_and_add_url_rule(app, MESSENGER_WEBHOOK_MAPPING)


def validate_id(type, id):
    """
    :param type: collection name. ex: bot, status
    :param id: document id
    :return: tuple with status and response message from server
    """
    post_request_url = "http://{server_host}:{server_port}/validate/{type}/{id}".format(
        server_host=SERVER_HOST,
        server_port=str(SERVER_PORT),
        type=type,
        id=id
    )
    response = requests.get(post_request_url)
    response_json = json.loads(response.text)
    response_status = response_json['result']

    return response_status, response_json['msg']


def create_session_mapping(user_id, client, bot_id, session_id, email_id=None):
    """
    Checks if there is session mapping with user id
    if yes, returns the session id to calling point

    if not , creates the session mapping, backend session and returns the session id

    if user id is not present (None), the there is no user id or grv id
    backend session will only be created in that case

    :param user_id: unique user id
    :param client: type of messenger
    :param bot_id: document id of bot
    :param email_id: email id of corresponding user
    :return:
    """
    try:
        temp = {
            "user_id": user_id,
            "type": client,
            "state": True,
            "secret_key": None if True else 0,
            "bot_id": bot_id,
            "flag": 1,
            "email_id": email_id
        }

        # create session
        if not session_id:
            session = create_server_session(user_id, bot_id=bot_id, client=client, email_id=email_id)
            if session['status'] == 'Success':
                print("server side session created...")

            else:
                print("Session can't be created on server side")
                abort(500)

            temp.update({"session_id": session['session_id']})
            insert("session_mapping", temp)

            return session

        else:
            temp.update({"session_id": session_id})
            insert("session_mapping", temp)
            return {
                "status": "Success",
                "session_id": session_id
            }
    except Exception as e:
        print("Exception occured in create sesision: ", e)
        abort(500)


def check_if_mapping_is_existing(user_id, client, bot_id, **kwargs):
    """
    checks if mapping is existing in middle layer

    :param user_id: user_id that identifies user for the client
    :param client: which client user is using, line,kakao,browser
    :param bot_id: document id of bot
    :return: result
    """
    result = None
    print("Json to be compared for session mapping: ", {
        "user_id": user_id,
        "type": client,
        "bot_id": bot_id,
        "state": True
    })

    if user_id:
        result = search_one('session_mapping', {
            "user_id": user_id,
            "type": client,
            "bot_id": bot_id,
            "state": True
        })

        if not result:
            print("The mapping is not existing", kwargs.get("session_id"))

            # initialize mapping
            created = create_session_mapping(user_id, client, bot_id, kwargs.get('session_id'), kwargs.get('email_id'))
            print("Session creation success")
            if created['status'] == "Success":
                result = search_one('session_mapping', {
                    "user_id": user_id,
                    "type": client,
                    "state": True,
                    "bot_id": bot_id
                })
                return result
        return result

    else:
        print("Mapping cant be created since the grv id or user id is not provided")

        is_not_valid = 1
        print(kwargs.get("session_id"))
        if kwargs.get('session_id'):
            # validate session_id
            is_not_valid, msg = validate_id("session", kwargs['session_id'])

        is_not_valid = 0
        if not kwargs.get('session_id') or kwargs.get('session_id') == 'undefined':
            session = create_server_session(user_id, bot_id=bot_id, client=client)
            if session['status'] == 'Success':
                print("server side session created...")
            else:
                print("Session cant be created for web ui, aborting")
                abort(401)

            return session

        return {
            "status": "Success",
            "session_id": kwargs.get('session_id')
        }


@app.route("/callback", methods=['POST'])
def webhook_function():
    """
    Validates bot id
    Check if mapping is existing
    if yes: return the session ie
    if not, it will create the mapping , nad initialize the session at server end and return the session
    redirect to method() function of same file
    :return: url to redirect to server
    """
    data = request.json
    print("Need to forward this request to different server")
    is_not_valid, msg = validate_id("bot", data.get("bot_id"))

    if is_not_valid:
        print("Error in validating bot id: ", msg)
        abort(401)

    # data.get('user_id') or data.get('grv_id') : only one will be present at one time
    # is nothing is present , no mapping will be created , only session will be created at backend
    res = check_if_mapping_is_existing(
        data.get('user_id') or data.get('grv_id'),
        data.get('type'), data.get("bot_id"),
        session_id=data.get("session_id"),
        email_id=data.get("email_id")
    )

    if not res:
        abort(500)
    else:
        print("returned and created mapping")
        rediect_url = redirect(url_for('method', bot_id=data.get("bot_id"), session_id=res['session_id'],
                                       grv_id=data.get('grv_id', None) or data.get('user_id', None)), code=307)
        return rediect_url


@app.route("/redirected/<bot_id>/<session_id>/", methods=['POST'])
@app.route("/redirected/<bot_id>/<session_id>/<grv_id>", methods=['POST'])
def method(bot_id, session_id, grv_id=None):
    """
    Redirects users request to server to process the message
    Entry point for server chat
    grv_id will be used for webbrowser
    and will be replaced by user_id for line and other messengers
    :return:
    """
    post_request_url = "http://{server_host}:{server_port}/bot/{bot_id}/{session_id}/chat{grv_id}".format(
        server_host=SERVER_HOST,
        server_port=SERVER_PORT,
        bot_id=bot_id,
        session_id=session_id,
        grv_id="/{}".format(grv_id) if grv_id else ""

    )
    return redirect(post_request_url, code=307)


@app.route("/image", methods=['GET'])
def serve_image():
    image_name = request.args.get('image')
    image = os.path.join(IMAGES_PATH, image_name)

    return send_file(image, mimetype='image/png')


# creat_through_class()
if __name__ == "__main__":
    Create().creat_through_class()
    app.run(host=APP_HOST, port=int(APP_PORT), threaded=True)

