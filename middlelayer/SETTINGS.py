import os

MESSENGER_WEBHOOK_MAPPING = [
    {
        "package_name": "Line",
        "url_rule": "/line_webhook",
        "METHODS": ['POST', 'GET']
    },
    {
        "package_name": "Web",
        "url_rule": "/endpoint",
        "METHODS": ['POST', 'GET']
    },
    {
        "package_name": "Facebook",
        "url_rule": "/message",
        "METHODS": ['POST', 'GET']
    },
    {
        "package_name": "MSTeams",
        "url_rule": "/msteams_message",
        "METHODS": ['POST', 'GET']
    }
]

CLASS_NAME_SUFFIX = "Messenger"
FILE_NAME_SUFFIX = "Messenger"

DEFAULT_ENTRYPOINT_METHOD = "process_message"

BOT_ID = "5dfb1a1a8f2426432497d24c"
TEST_BOT_ID = "5dfb1a1a8f2426432497d24c"
MIDDLE_LAYER_ENDPOINT = "webhook_function"
SERVER_HOST = os.environ.get('SERVER_HOST') or 'localhost'
SERVER_PORT = os.environ.get('SERVER_PORT') or '8000'
APP_HOST = os.environ.get('APP_HOST') or 'localhost'
APP_PORT = os.environ.get('APP_PORT') or '5050'

RESOURCE_PATH = os.path.join(os.path.dirname(__file__), "resources")
STATIC_PATH = os.path.join(RESOURCE_PATH, "static")
DATA_PATH = os.path.join(RESOURCE_PATH, "data")
RECORDINGS_PATH = os.path.join(DATA_PATH, "recordings")
IMAGES_PATH = os.path.join(STATIC_PATH, 'images')

IMAGE_URL = os.environ.get("IMAGE_URL") or "https://e0d78cd2.ngrok.io/image?image="
