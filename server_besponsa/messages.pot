# Translations template for PROJECT.
# Copyright (C) 2018 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2018-09-13 13:26-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.3\n"

#: templates/chat.html:37
msgid "Message"
msgstr ""

#: templates/chat.html:45
msgid "Hit enter to send message"
msgstr ""

#: utility/conversation_logger.py:106 utility/elasticsearch_query.py:202
#: utility/execution_engine.py:510 utility/execution_engine.py:582
#: utility/execution_engine.py:1059 utility/execution_engine.py:1089
#: utility/execution_engine.py:1121 utility/execution_engine_backup.py:283
#: utility/execution_engine_backup.py:318
#: utility/execution_engine_backup.py:347
msgid "yes"
msgstr ""

#: utility/conversation_logger.py:106 utility/elasticsearch_query.py:216
#: utility/execution_engine.py:514 utility/execution_engine.py:614
#: utility/execution_engine.py:1068 utility/execution_engine.py:1099
#: utility/execution_engine.py:1137 utility/execution_engine_backup.py:290
#: utility/execution_engine_backup.py:321
#: utility/execution_engine_backup.py:377
msgid "no"
msgstr ""

#: utility/conversation_logger.py:143 utility/conversation_logger.py:165
#: utility/execution_engine.py:568 utility/execution_engine_backup.py:224
#: utility/execution_engine_backup.py:225
msgid "We couldn't find that. Do you mean: "
msgstr ""

#: utility/conversation_logger.py:143 utility/conversation_logger.py:165
#: utility/execution_engine.py:574 utility/execution_engine_backup.py:228
#: utility/execution_engine_backup.py:229
msgid "Sorry we don't have an answer for that. Could you try again?"
msgstr ""

#: utility/elasticsearch_query.py:209
msgid "Thank you! Your feedback has been added."
msgstr ""

#: utility/elasticsearch_query.py:235 utility/elasticsearch_query.py:243
msgid ""
"Sorry we werent able to find what you were looking for. Your feedback has"
" been noted."
msgstr ""

#: utility/elasticsearch_query.py:292
msgid "Was this helpful?"
msgstr ""

#: utility/elasticsearch_query.py:292 utility/execution_engine.py:549
#: utility/execution_engine.py:568 utility/execution_engine.py:621
#: utility/execution_engine.py:1083 utility/execution_engine.py:1115
#: utility/execution_engine.py:1154 utility/execution_engine.py:1305
#: utility/execution_engine.py:1317 utility/execution_engine.py:1324
#: utility/execution_engine_backup.py:223
#: utility/execution_engine_backup.py:225
#: utility/execution_engine_backup.py:236
#: utility/execution_engine_backup.py:238
#: utility/execution_engine_backup.py:270
#: utility/execution_engine_backup.py:272
#: utility/execution_engine_backup.py:299
#: utility/execution_engine_backup.py:301
#: utility/execution_engine_backup.py:390
#: utility/execution_engine_backup.py:392
msgid "Yes"
msgstr ""

#: utility/elasticsearch_query.py:292 utility/execution_engine.py:549
#: utility/execution_engine.py:568 utility/execution_engine.py:621
#: utility/execution_engine.py:1083 utility/execution_engine.py:1115
#: utility/execution_engine.py:1154 utility/execution_engine.py:1305
#: utility/execution_engine.py:1317 utility/execution_engine.py:1324
#: utility/execution_engine_backup.py:223
#: utility/execution_engine_backup.py:225
#: utility/execution_engine_backup.py:236
#: utility/execution_engine_backup.py:238
#: utility/execution_engine_backup.py:270
#: utility/execution_engine_backup.py:272
#: utility/execution_engine_backup.py:299
#: utility/execution_engine_backup.py:301
#: utility/execution_engine_backup.py:390
#: utility/execution_engine_backup.py:392
msgid "No"
msgstr ""

#: utility/elasticsearch_query.py:327 utility/elasticsearch_query.py:362
msgid "Sorry, I cant find an answer for that. Could you try again?"
msgstr ""

#: utility/execution_engine.py:362
msgid "Click Here"
msgstr ""

#: utility/execution_engine.py:421 utility/execution_engine_backup.py:243
msgid "hello"
msgstr ""

#: utility/execution_engine.py:421 utility/execution_engine_backup.py:243
msgid "hi"
msgstr ""

#: utility/execution_engine.py:421 utility/execution_engine_backup.py:243
msgid "whats up"
msgstr ""

#: utility/execution_engine.py:421 utility/execution_engine_backup.py:243
msgid "good afternoon"
msgstr ""

#: utility/execution_engine.py:422 utility/execution_engine_backup.py:243
msgid "good morning"
msgstr ""

#: utility/execution_engine.py:422 utility/execution_engine_backup.py:243
msgid "good day"
msgstr ""

#: utility/execution_engine.py:422 utility/execution_engine_backup.py:243
msgid "good evening"
msgstr ""

#: utility/execution_engine.py:423 utility/execution_engine_backup.py:244
msgid "I would like to ask a question"
msgstr ""

#: utility/execution_engine.py:423 utility/execution_engine_backup.py:244
msgid "I want to ask you a question"
msgstr ""

#: utility/execution_engine.py:424 utility/execution_engine_backup.py:244
msgid "I have a doubt"
msgstr ""

#: utility/execution_engine.py:424 utility/execution_engine_backup.py:244
msgid "I want to ask you something"
msgstr ""

#: utility/execution_engine.py:425 utility/execution_engine_backup.py:244
msgid "I want to ask you questions"
msgstr ""

#: utility/execution_engine.py:425 utility/execution_engine_backup.py:244
msgid "I want to consult you"
msgstr ""

#: utility/execution_engine.py:426 utility/execution_engine_backup.py:245
msgid "how are you"
msgstr ""

#: utility/execution_engine.py:426 utility/execution_engine_backup.py:245
msgid "how are you doing"
msgstr ""

#: utility/execution_engine.py:426 utility/execution_engine_backup.py:245
msgid "how is your day"
msgstr ""

#: utility/execution_engine.py:427 utility/execution_engine_backup.py:246
msgid "Hi there! How can I help you"
msgstr ""

#: utility/execution_engine.py:428 utility/execution_engine_backup.py:247
msgid "With pleasure, tell me how I can help you."
msgstr ""

#: utility/execution_engine.py:429
msgid "I'm doing well! How can I help you today?"
msgstr ""

#: utility/execution_engine.py:430
msgid "I'm feeling great! How can I help you today?"
msgstr ""

#: utility/execution_engine.py:482
msgid "Sorry, there is nothing to go back to."
msgstr ""

#: utility/execution_engine.py:506 utility/execution_engine_backup.py:314
#: utility/execution_engine_backup.py:315
msgid "Please choose an option 1-5."
msgstr ""

#: utility/execution_engine.py:512 utility/execution_engine_backup.py:319
#: utility/execution_engine_backup.py:320
msgid "Please leave your comment below:"
msgstr ""

#: utility/execution_engine.py:519 utility/execution_engine.py:529
#: utility/execution_engine_backup.py:325
#: utility/execution_engine_backup.py:326
#: utility/execution_engine_backup.py:332
#: utility/execution_engine_backup.py:333
msgid "Thank you for your participation!"
msgstr ""

#: utility/execution_engine.py:544 utility/execution_engine_backup.py:266
#: utility/execution_engine_backup.py:267
msgid "On a scale of 1-5, how satisfied were you with your service today?"
msgstr ""

#: utility/execution_engine.py:549 utility/execution_engine_backup.py:271
#: utility/execution_engine_backup.py:272
msgid "Would you like to leave any comments?"
msgstr ""

#: utility/execution_engine.py:621 utility/execution_engine_backup.py:391
#: utility/execution_engine_backup.py:392
msgid "Please choose yes or no."
msgstr ""

#: utility/execution_engine.py:673 utility/execution_engine.py:798
#: utility/execution_engine.py:895 utility/execution_engine.py:968
msgid "Which drug are you interested in?"
msgstr ""

#: utility/execution_engine.py:680 utility/execution_engine.py:806
#: utility/execution_engine.py:903 utility/execution_engine.py:976
msgid "Are you interested in these drugs?"
msgstr ""

#: utility/execution_engine.py:757
msgid "Ok, well the drug can be found here "
msgstr ""

#: utility/execution_engine.py:848
msgid "The price is "
msgstr ""

#: utility/execution_engine.py:855
msgid "Please type in a 2 Letter valid state "
msgstr ""

#: utility/execution_engine.py:860
msgid "Sorry, we don't have price information for this drug"
msgstr ""

#: utility/execution_engine.py:1028
msgid "Sorry, we don't have product information for this."
msgstr ""

#: utility/execution_engine.py:1072
msgid "Thanks for chatting, hope to talk again soon!"
msgstr ""

#: utility/execution_engine.py:1083 utility/execution_engine.py:1114
#: utility/execution_engine.py:1153 utility/execution_engine_backup.py:300
#: utility/execution_engine_backup.py:301
msgid "Would you like to continue? Please choose yes or no."
msgstr ""

#: utility/execution_engine.py:1096
msgid "What other questions can I help you with?"
msgstr ""

#: utility/execution_engine.py:1103
msgid "Thanks for chatting, hope to talk again soon"
msgstr ""

#: utility/execution_engine.py:1141 utility/execution_engine_backup.py:293
#: utility/execution_engine_backup.py:294
msgid "Thanks for using the Pfizer Bot!"
msgstr ""

#: utility/execution_engine.py:1217 utility/execution_engine_backup.py:491
#: utility/execution_engine_backup.py:494
msgid "Are you interested in any of these options?"
msgstr ""

#: utility/execution_engine.py:1224 utility/execution_engine_backup.py:498
#: utility/execution_engine_backup.py:499
msgid "Sorry, we may not have information for that. Can you try again?"
msgstr ""

#: utility/execution_engine.py:1238
msgid ""
"I'm sorry none of those options were helpful. Would you like to try "
"rephrasing your question?"
msgstr ""

#: utility/execution_engine.py:1244 utility/execution_engine_backup.py:402
#: utility/execution_engine_backup.py:403
msgid "Please choose one of the options"
msgstr ""

#: utility/execution_engine.py:1273
msgid "Please select any of the stores to view it on a map"
msgstr ""

#: utility/execution_engine.py:1281
msgid "Sorry, we do not have pharmacies within 50 miles of your area."
msgstr ""

#: utility/execution_engine.py:1305 utility/execution_engine.py:1316
#: utility/execution_engine.py:1323 utility/execution_engine_backup.py:237
#: utility/execution_engine_backup.py:238
msgid "Would you like to continue?"
msgstr ""

#: utility/execution_engine_backup.py:248
msgid "Im doing well! How can I help you today?"
msgstr ""

#: utility/execution_engine_backup.py:248
msgid "Im feeling great! How can I help you today?"
msgstr ""

#: utility/execution_engine_backup.py:380
#: utility/execution_engine_backup.py:381
msgid "Sorry about that. Lets try again from the start."
msgstr ""

#: utility/execution_engine_backup.py:569
#: utility/execution_engine_backup.py:599
#: utility/execution_engine_backup.py:618
msgid ""
"Sorry, we currently do not have access to this data. Please try again "
"later!"
msgstr ""

#: utility/execution_engine_backup.py:586
#: utility/execution_engine_backup.py:587
msgid "Click here to go to the link"
msgstr ""

#: utility/execution_engine_backup.py:662
msgid "Which category are you interested in?"
msgstr ""

#: utility/execution_engine_backup.py:670
msgid "Which category would you like to filter on?"
msgstr ""

#: utility/execution_engine_backup.py:677
msgid "Which of these do you want to know about?"
msgstr ""

#: utility/execution_engine_backup.py:682
msgid "Here is the information regarding "
msgstr ""

#: utility/execution_engine_backup.py:693
#: utility/execution_engine_backup.py:710
msgid ""
"Sorry we currently do not have access to this data. Please try again "
"later!"
msgstr ""

