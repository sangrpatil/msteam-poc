from flask_socketio import SocketIO, send, emit
# from flask_babel import gettext as get
from elasticsearch import Elasticsearch
import json
from utility.conversation_logger import ConversationLogger
from utility.TTS import synthesize_text
from utility.session_manager import get as get_context, modify as update_context
from utility.mongo_dao import get_one
import os
import re
import math
import configparser
from utility.logger import logger
from utility.mail_pfizer import generateemail
#GLobal variables

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))
restart_config_counter = app_config.get('ELASTICSEARCH', 'response_count')
restart_config_counter = int(restart_config_counter)
elasticsearch_host = os.environ.get('ES_HOST') or 'localhost'
elasticsearch_port = os.environ.get('ES_PORT') or '9200'
elasticsearch_threshold = os.environ.get('ES_THRESHOLD') or '1.0'

class SearchES():

    def __init__(self, session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id,bot_id)


    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json={},group=None):
        try:
            #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
            self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], guidedRestartFlag=False, group=self.session['group'], grv_id=self.session["grv_id"])
            if self.session['ASRFlag'] == True:
                static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
                tts_text = synthesize_text(static_text1)
                asr_use = True
                self.session['ASRFlag'] = False
                update_context(self.session_id, self.session)
            else:
                tts_text = ""
                asr_use = False
            if is_multi_message == True:
                existing_json['message'].append({
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio":tts_text,
                    "disable_response": self.session['disable_input']
                })
                existing_json['is_multi'] = True
                return existing_json
            else:
                json_return = {
                    "is_multi":False,
                    "message":[{
                        "interaction elements": interaction_elements,
                        "text": static_text,
                        "type": type,
                        "interaction": interaction,
                        "tts": asr_use,
                        "tts audio": tts_text,
                        "disable_response": self.session['disable_input']
                    }]
                }
                return json_return
        except Exception as e:
            logger.error("Elastic search Build response error: ")
            logger.exception(e)

    #based off feedback, update weight of elasticsearch answers
    def update_votes(self,score_answer, botName):
        try:
            updated_score_answer = score_answer
            new_source = json.loads(json.dumps(updated_score_answer[2]))

            l = {"doc": {"qid": updated_score_answer[1], "q": new_source['q'], "a": updated_score_answer[3],
                         "votes": int(new_source['votes']) + 1}}
            print(l)


            es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])
            es.update(index=str(botName).lower()+self.session['group'].lower().replace(" ", ""), doc_type=str(botName).lower()+self.session['group'].replace(" ", "").lower(), id=score_answer[1],body=l)
        except Exception as e:
            logger.error("Elastic search Build response error: ")
            logger.exception(e)

    #search for answer within english elasticsearch
    def search_es(self,question,botName):
        try:
            es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

            data = {
                "query": {
                    "function_score": {
                        "query": {

                            "match": {
                                'q': question
                            }
                        },
                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p"
                        }
                    }
                }
            }
            response = es.search(index=str(botName).lower(), body=data)
            score = ''
            answer = ''
            source = ''
            id = ''
            score_answer = []
            for items in response['hits']['hits']:
                score = items['_score']
                source = items['_source']
                id = items['_id']
                answer = source['a']
                votes = source['votes']
                if ((score) / (math.log10(votes + 2))) < float(elasticsearch_threshold):
                    print('This answer score is below threshold - Skipping this answer = {}'.format(answer))
                    continue
                else:
                    print('APPENDING THIS ANSWER = {}'.format(answer))
                    score_answer.append([score, id, source, answer, votes])
            return score_answer
        except Exception as e:
            logger.error("Elastic search searches error: ")
            logger.exception(e)

    #search for answers within Spanish elasticsearch
    def search_es_non_english(self, question, botName, elasticsearch_analyzer):

        # print('BotName received from execute function = ' + botName)
        print('GOING TO EXECUTE NON-ENGLISH ES SEARCH. CURRENT GROUP = {}'.format(self.session['group']))
        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

        data = {
            "query": {
                "function_score": {

                    "query": {

                        "multi_match": {
                            "type": "phrase",
                            "query": question,
                            "fields": ["q"]

                        }
                    },

                    "field_value_factor": {
                        "field": "votes",
                        "modifier": "log2p"
                    }

                }
            }
        }



        response = es.search(index=str(botName).lower()+self.session['group'].replace(" ", "").lower(), body=data)
        score = ''
        answer = ''
        source = ''
        id = ''

        print('RESPONSE FROM ELASTICSEARCH AFTER EXACT MATCH= {}'.format(response))

        if response['hits']['total'] == 0:
            print('No Exact match found. Going to execute Best Match')
            data = {
                "query": {
                    "function_score": {

                        "query": {

                            "multi_match": {
                                "type": "best_fields",
                                "query": question,
                                "fields": ["q"]

                            }
                        },

                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p"
                        }

                    }
                }
            }
            response = es.search(index=str(botName).lower() + self.session['group'].replace(" ", "").lower(),body=data)
            print('RESPONSE FROM ELASTICSEARCH AFTER BEST FIELDS MATCH= {}'.format(response))
        # score_answer is - [score,id,source,answer]
        score_answer = []
        for items in response['hits']['hits']:
            score = items['_score']
            source = items['_source']
            id = items['_id']
            answer = source['a']
            votes = source['votes']
            print('UPDATED SCORE = {}'.format(score))
            print('VOTES = {}'.format(votes))
            # print('LOG VALUE = {}'.format(math.log10(votes + 2)))
            print('ORIGINAL SCORE VALUE = {} '.format(((score) / (math.log10(votes + 2)))))
            if ((score) / (math.log10(votes + 2))) < float(elasticsearch_threshold):
                print('This answer score is below threshold - Skipping this answer = {}'.format(answer))
                continue
            else:
                print('APPENDING THIS ANSWER = {}'.format(answer))
                score_answer.append([score,id, source, answer, votes])
        print("RESPONSE ELASTICSEARCH = {}".format(response['hits']['hits']))
        # print(len(score_answer))
        # print('SCORE ANSWER LIST')
        # print(score_answer)
        # score_answer.sort(key=lambda x: int(x[0]), reverse=True)
        print("Score Answer: ", score_answer)
        return (score_answer)


    #take users message and determine what to do with it
    #will either be feedback yes/no or will be a message to query


    def execute(self,message,BotName, ASR, es_analyzer): #message,bot_id, self.session['ASRFlag'],es_analyzer
        print("Inside elasticsearch execute")
        messages_mappings_dictionary = {}
        messages_collection = get_one('messages', 'type', 'General')
        print("Messages collection: In execute - ", messages_collection)

        messages_mappings = messages_collection['mappings']

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside execute function: elasticsearch", messages_mappings_dictionary)

        print('ASR FLAG VALUE IN ES EXECUTE FUNCTION = {}'.format(self.session['ASRFlag']))
        print("INSIDE ES EXECUTE FUNCTION. SESSION VARIABLES ARE = {}".format(self.session))
        if message == "Go Back":
            return 2, ''
        if self.session['feedbackFlag_ES'] == True:
            if message.lower().strip() == messages_mappings_dictionary['yes']:
                print('USER PRESSED YES - ANSWER WAS HELPFUL. GOING TO UPDATE VOTE')
                updated_score_answer = self.session['es_reponse'][0]

                self.update_votes(updated_score_answer,BotName)

                print('SCORE ANSWER AFTER VOTE UPDATE')
                response = self.build_response("string",messages_mappings_dictionary["Thank you! Your feedback has been added."],"text","",group=self.session['group'])
                # self.send_msg(gettext('Thank you! Your feedback has added.'))
                self.session['suggestCounter_ES'] = 0
                self.session['noCounter_ES'] = 0
                self.session['feedbackFlag_ES'] = False
                print('ES RESTART COUNTER = {}'.format(self.session['es_restart_counter']))
                print('restart_config_counter = {}'.format(restart_config_counter))
                # if self.session['es_restart_counter'] > restart_config_counter:
                #     print('MAXIMUMUM COUNTER REACHED. ASKING TO RESTART')
                #     self.session['es_restart_counter'] = 0
                #     update_context(self.session_id, self.session)
                #     return 0 , response

                print('MAXIMUM COUNTER NOT REACHED. SENDING 1 to EXECUTION ENGINE')
                update_context(self.session_id, self.session)
                self.intentFoundRecord(self.session['currentQuestion'],self.session['group'])
                return 0, response

            elif message.lower().strip() == messages_mappings_dictionary['no']:
                print('USER PRESSED NO - ANSWER WAS NOT HELPFUL. GET NEXT ANSWER')
                self.session['suggestCounter_ES'] += 1
                self.session['noCounter_ES'] += 1

                update_context(self.session_id, self.session)

                if self.session['noCounter_ES'] == 2:
                    self.session['escalation'] = True
                    response_json = self.build_response("string",
                                                          messages_mappings_dictionary["Sorry we could not find an answer for what you were looking for. We will try to continuously improve based on your feedback. In the case that you would like to contact our iMSL or report an adverse event, please contact Pfizer at pfizerimsl@pfizer.com."],
                                                          "text", "", {})

                    self.session['noCounter_ES'] = 0
                    self.session['suggestCounter_ES'] = 0
                    update_context(self.session_id, self.session)
                    #generateemail(self.session_id, self.session, response_json)
                    return 0,  response_json

                if self.session['suggestCounter_ES'] < 3:
                    try:

                        if self.session['es_reponse']:
                            del self.session['es_reponse'][0]
                            print("printing response from elastic search ", self.session['es_reponse'])
                            response = self.build_response("string",self.session['es_reponse'][0][3],"text","",group=self.session['group'])
                            response_json_2 = self.feedback(response)
                            self.session['feedbackFlag_ES'] = True
                            update_context(self.session_id, self.session)
                            return 1, response_json_2

                            # return code,message
                    except Exception as e:
                        print(e)

                        self.session['feedbackFlag_ES'] = False
                        self.intentNotFoundRecord(self.session['currentQuestion'],self.session['group'])
                        self.session['suggestCounter_ES'] = 0
                        self.session['noCounter_ES'] = 0
                        response_json = self.build_response("string",
                                                            messages_mappings_dictionary["Sorry we weren''t able to find what you were looking for. Your feedback has been noted."],
                                                            "text", "",group=self.session['group'])
                        # if self.session['es_restart_counter'] > restart_config_counter:
                        #     print('MAXIMUMUM COUNTER REACHED IN EXCEPT BLOCK OF NO. ASKING TO RESTART')
                        #     self.session['es_restart_counter'] = 0
                        #     update_context(self.session_id, self.session)
                        #     return 0, response

                        print('MAXIMUM COUNTER NOT REACHED IN EXCEPT BLOCK OF NO. SENDING 1 to EXECUTION ENGINE')
                        print('ES RESTART COUNTER = {}'.format(self.session['es_restart_counter']))
                        print('restart_config_counter = {}'.format(restart_config_counter))
                        update_context(self.session_id, self.session)

                        return 3, response_json
                else:
                    # self.send_msg(gettext('Sorry we weren''t able to find what you were looking for. Your feedback has been noted.'))
                    self.intentNotFoundRecord(self.session['currentQuestion'],self.session['group'])
                    self.session['suggestCounter_ES'] = 0
                    self.session['noCounter_ES'] = 0
                    update_context(self.session_id, self.session)
                    self.session['feedbackFlag_ES'] = False
                    response_json = self.build_response("string",
                                                        messages_mappings_dictionary["Sorry we weren''t able to find what you were looking for. Your feedback has been noted."],
                                                        "text", "",group=self.session['group'])
                    # if self.session['es_restart_counter'] > restart_config_counter:
                    #     self.session['es_restart_counter'] = 0
                    #     update_context(self.session_id, self.session)
                    #     return 0,response_json
                    # update_context(self.session_id, self.session)

                    return 1, response_json
            else:
                self.session['currentQuestion'] = message
                self.session['feedbackFlag_ES'] = False
                update_context(self.session_id, self.session)
                if es_analyzer == 'English':
                    response_json = self.checkResponse_non_english(message, BotName, es_analyzer)
                    print("Response json: ,",response_json)
                    return 1, response_json
                else:
                    response_json = self.checkResponse_non_english(message, BotName, es_analyzer)
                    #if self.session['es_restart_counter'] > restart_config_counter:
                        #self.session['es_restart_counter'] = 0
                        #update_context(self.session_id, self.session)
                        #return 0,response_json
                    return 1, response_json

        else:
            print("RECCIEVED USER QUESTION")
            self.session['currentQuestion'] = message
            update_context(self.session_id, self.session)
            if es_analyzer == 'English':
                response_query = self.checkResponse_non_english(message, BotName, es_analyzer)
                print("Response query, ", response_query)
                return 1, response_query
            else:
                response_query = self.checkResponse_non_english(message, BotName, es_analyzer)
                #if self.session['es_restart_counter'] > restart_config_counter:
                    #self.session['es_restart_counter'] = 0
                    #update_context(self.session_id, self.session)
                    #return 0,response_query

                return 1, response_query
        # else:
        #     self.send_msg(gettext('Sorry, could you try again?'))
        #     #record the user input that could not be found
        #     pass


    # #send message and send TTS audio
    # def send_msg(self, msg):
    #     # global ASRFlag
    #     endSurveyFlag = False
    #     self.conv_log.bot_log(msg, self.session['ASRFlag'], endSurveyFlag)
    #     emit('message', msg)

    #check feedback, whether or not user was satisfied with the answer returned by elasticsearch
    def feedback(self, input_json):
        messages_collection = get_one('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}
        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside feedback function", messages_mappings_dictionary)

        json = self.build_response("list",messages_mappings_dictionary['Was this helpful?'],"button_horizontal",[messages_mappings_dictionary['Yes'],messages_mappings_dictionary['No']],True,input_json)
        self.session['feedbackFlag_ES'] = True
        update_context(self.session_id, self.session)
        return json

    #if the user is actually
    # def checkResponse(self, message, BotName):
    #     self.session['es_reponse'] = self.search_es(message, BotName)
    #     self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
    #     if len(self.session['es_reponse']) > 0:
    #         response_json = self.build_response("string",self.session['es_reponse'][0][3],"text","")
    #         response_json_2 = self.feedback(response_json)
    #
    #         self.session['feedbackFlag_ES'] = True
    #         update_context(self.session_id, self.session)
    #         return response_json_2
    #
    #     else:
    #         response_json = self.build_response("string",gettext('Sorry, I can''t find an answer for that. Could you try again?'),"text","")
    #         self.intentNotFoundRecord(message)
    #         return response_json

    def checkResponse_non_english(self, message, BotName, es_analyzer):

        messages_collection = get_one('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}
        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside checkResponse non-english function", messages_mappings_dictionary)

        # global es_reponse
        # global feedbackFlag
        print('USER ASKED A QUESTION')
        print('USER ASKED A QUESTION ', self.session['es_restart_counter'])

        self.session['es_restart_counter'] += 1
        print('UPDATED ES COUNTER = {}'.format(self.session['es_restart_counter']))
        print("Here")
        self.session['es_reponse'] = self.search_es_non_english(message, BotName, es_analyzer)
        print("After assigning to session: ",self.session['es_reponse'])
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        # if len(session['es_reponse']) > 0 and session['es_reponse'][0][0] > 0.29:
        print(len(self.session['es_reponse']))
        if len(self.session['es_reponse']) > 0:
            print('ANSWER')
            print(self.session['es_reponse'][0][3])
            #
            # print('SCORE')
            # print(self.session['es_reponse'][0][0])
            #
            # print('ID')
            # print(self.session['es_reponse'][0][1])
            #
            # print('SOURCE')
            # print(self.session['es_reponse'][0][2])

            response_json = self.build_response("string", self.session['es_reponse'][0][3], "text", "",group=self.session['group'])
            response_json_2 = self.feedback(response_json)
            print("Responses from checkResponse non-english \n")
            print("Response json: ", response_json)
            print("Response json_2: ", response_json_2)

            #self.send_msg(self.session['es_reponse'][0][3])
            #self.feedback()
            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)
            #return (self.session['es_reponse'][0][1])
            return response_json_2
        else:
            response_json = self.build_response("string", messages_mappings_dictionary["Sorry, I can''t find an answer for that. Could you try again?"],"text","",group=self.session['group'])
            #self.send_msg(gettext('Sorry, I can''t find an answer for that. Could you try again?'))
            # return 1
            self.intentNotFoundRecord(message,self.session['group'])
            return response_json
    def intentNotFoundRecord(self, msg,group):
        self.conv_log.updateIntentNotFound(msg,group, self.session["grv_id"])
        pass
    def intentFoundRecord(self, msg,group):
        self.conv_log.updateIntentFoundCounter(msg,group, self.session["grv_id"])