import xlsxwriter
import datetime
from utility.logger import logger
from utility.excel_parser import *
import os
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
import configparser
from utility.mongo_dao import get_all, get_collection

app_config = configparser.ConfigParser()



def generateReportsMonthly(from_date=None,to_date=None,bot_id = None):
    adverse_key_list = key_list_gen()
    cursor = get_collection('status')  # todo - read for only particular bot
    if bot_id == None:
        bot_name = 'All_Bots'
    else:
        bot_name = cursor = get_collection('bot')
        for document in cursor:
            if str(bot_id) == str(document['_id']).replace('ObjectId(','').replace(')',''):
                bot_name = document['bot name']
    if bot_name == '':
        bot_name = 'Bot'
    fileTitle = 'reports/_' + bot_name + '_Chatbot_Report_Monthly_' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    convoLog = workbook.add_worksheet('ConversationLog')


    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['ID', 'FROM', 'MESSAGE', 'ADVERSE EVENTS', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:I1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']
    headers = ['ID', 'FROM', 'MESSAGE','ADVERSE EVENTS',  'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    ID = 0
    numRated = 0
    sumScore = 0
    number_conv_counter = 0
    dateDict = {}
    timeDict = {}
    ratingsDict = {}
    latencyDict = {}
    intentFoundList = []
    intentNotFoundList = []
    final_conv_log = []

    final_conv_log.append(headers)
    found_bot_id = False

    # create hour dict for conversations over time of day and latency
    for k in range(0, 25):
        timeDict[k] = []
        latencyDict[k] = []
    ratingHistogramDict = {'No Rating': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
    for document in cursor:
        try:
            # determine the converation's rating and comments, if any

            if bot_id == None:
                number_conv_counter += 1
                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated += 1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)

                    rating = 'None'
                    ratingHistogramDict['No Rating'] += 1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)
                    comments = 'None'

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    print('bot ID is none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        Adverse_Events = word_check_list(message, adverse_key_list)
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID: int(rating)}

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'

                        dataRow = [ID, type, message, Adverse_Events, ASR, rating, comments, Survey, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ")
                    logger.exception(e)

            elif str(document['bot_id']) == str(bot_id):
                number_conv_counter += 1
                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated += 1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)
                    rating = 'None'
                    ratingHistogramDict['No Rating'] += 1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)
                    comments = 'None'

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()


                # now iterate through each individual conversation log
                try:
                    print('bot ID is not none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        Adverse_Events = word_check_list(message, adverse_key_list)
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID: int(rating)}

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'


                        dataRow = [ID, type, message, Adverse_Events, ASR, rating, comments, Survey, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ")
                    logger.exception(e)

            else:
                continue
        except Exception as e:
            logger.error("Generate Report Excel DOCUMENT CURSOR ERROR - : ")
            logger.exception(e)

    """
    Create sheets for intent found and intent not found
    """
    # #create header that will be used for "Intent Found" and "Intent not Found"

    headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']




    try:
        if found_bot_id == False:
            return 'Failure'
    except Exception as e:
        logger.error("Generate Report Excel error: ")
        logger.exception(e)
        pass

    # Hide the datasheet used to create the charts

    workbook.close()
    return fileTitle


def word_check_list(message ,key_list):
    stop = set(stopwords.words('english'))
    msg_tokens = word_tokenize(message)
    all_stops = stop | set(string.punctuation)
    tokens = [t for t in msg_tokens if t not in all_stops]
    for i in tokens:
        if i.lower() in key_list:
            return "Yes"
    return "No"

def key_list_gen():
    fileTitle = 'Besponsa_Adverse_Keywords_v1.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    print("Directory Path: ", directory_path)
    file_uri = os.path.join(directory_path, 'mapping', fileTitle)
    adverse_key_json = eval(convert(file_uri))
    print('Adverse key json :', adverse_key_json)
    # print(type(adverse_key_json), "***type of key file", adverse_key_json['sheet1'])
    adverse_key_list = [str(i['Keywords']).lower() for i in adverse_key_json['proposal_for_the_chatbot']]
    print('adverse_key_list', adverse_key_list)
    return adverse_key_list
