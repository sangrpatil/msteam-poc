from utility.excel_parser import *
import os
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import datetime
import re
import configparser
from utility.mongo_dao import get_all, get_collection, get_by_id
from utility.logger import logger
app_config = configparser.ConfigParser()
# app_config.read_file(open(r'../config/app_settings.ini'))

def generateReports(bot_id = None): #todo pass bot ID
    try:
        adverse_key_list = key_list_gen()
        cursor = get_collection('status') #todo - read for only particular bot
        #Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
        i = 1
        headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']
        headers = ['ID', 'FROM', 'MESSAGE', 'ADVERSE EVENTS', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
        ID = 0
        numRated = 0
        sumScore = 0
        dateDict = {}
        timeDict = {}
        ratingsDict = {}
        latencyDict = {}
        intentFoundList = []
        intentNotFoundList = []
        final_conv_log = []

        found_bot_id = False
        bot_name = ''
        #create hour dict for conversations over time of day and latency
        for k in range(0,25):
            timeDict[k] = []
            latencyDict[k] = []
        ratingHistogramDict ={'No Rating':0,'1':0,'2':0,'3':0,'4':0,'5':0}
        for document in cursor:
            #determine the converation's rating and comments, if any

            if bot_id == None:
                bot_name = 'None'
                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated +=1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report json error: ")
                    logger.exception(e)
                    rating = 'None'
                    ratingHistogramDict['No Rating'] +=1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report json error: ")
                    logger.exception(e)
                    comments = 'None'

                #initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime  = datetime.datetime.now()

                #look for user input intents that could be successfully answered (user said, "Yes" this was helpful)
                try:
                    intentFoundDocument = document['intentFound']
                    if intentFoundDocument != 'None':
                        for key, value in intentFoundDocument.items():
                            date, min_hour = str(value['time']).split(' ')
                            min_hour = min_hour[:5]
                            intentFoundList.append({"USER QUESTION":value['intentFound'],
                                                       "QUESTION ID": ID,
                                                       "DATE": date,
                                                       "TIME (UTC)": min_hour
                                                       })
                except Exception as e:
                    logger.error("Generate Repor Json error: ")
                    logger.exception(e)
                    pass

                #look for user input intents that couldn't be found (user said "No" not helpful, or bot said "sorry can't find it")
                try:
                    # headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']
                    intentNotFoundDocument = document['intentNotFound']
                    if intentNotFoundDocument != 'None':
                        for key, value in intentNotFoundDocument.items():
                            date, min_hour = str(value['time']).split(' ')
                            min_hour = min_hour[:5]
                            intentNotFoundList.append({"USER QUESTION":value['intentNotFound'],
                                                       "QUESTION ID": ID,
                                                       "DATE": date,
                                                       "TIME (UTC)": min_hour
                                                       })
                except Exception as e:
                    logger.error("Generate Repor Json error: ")
                    logger.exception(e)
                    pass

                #now iterate through each individual conversation log
                try:
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        Adverse_Events = word_check_list(message, adverse_key_list)
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        #process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        #process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        #process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        #create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        #create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        #create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum':0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos':numConvosLat,'latencySum': latencySum}
                                    botJustSentMessage = True

                        #rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID:int(rating)}

                        #check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'
                        final_conv_log.append({"ID": ID,
                                               "FROM": type,
                                               "MESSAGE": message,
                                               "ADVERSE EVENTS": Adverse_Events,
                                               "ASR/TTS USAGE": ASR,
                                               "RATING": rating,
                                               "COMMENTS": comments,
                                               "SURVEY RESPONSES": Survey,
                                               "DATE": date,
                                               "TIME (UTC)": min_hour_sec
                                               })

                except Exception as e:
                    logger.error("Generate Repor Json error: ")
                    logger.exception(e)

            elif str(document['bot_id']) == str(bot_id):
                bot_name = get_by_id('bot',bot_id)['bot name']

                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated +=1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report json error: ")
                    logger.exception(e)
                    rating = 'None'
                    ratingHistogramDict['No Rating'] +=1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report json error: ")
                    logger.exception(e)
                    comments = 'None'

                #initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime  = datetime.datetime.now()

                #look for user input intents that could be successfully answered (user said, "Yes" this was helpful)
                try:
                    intentFoundDocument = document['intentFound']
                    if intentFoundDocument != 'None':
                        for key, value in intentFoundDocument.items():
                            date, min_hour = str(value['time']).split(' ')
                            min_hour = min_hour[:5]
                            intentFoundList.append({"USER QUESTION":value['intentFound'],
                                                       "QUESTION ID": ID,
                                                       "DATE": date,
                                                       "TIME (UTC)": min_hour
                                                       })
                except Exception as e:
                    logger.error("Generate Repor Json error: ")
                    logger.exception(e)
                    pass

                #look for user input intents that couldn't be found (user said "No" not helpful, or bot said "sorry can't find it")
                try:
                    intentNotFoundDocument = document['intentNotFound']
                    if intentNotFoundDocument != 'None':
                        for key, value in intentNotFoundDocument.items():
                            date, min_hour = str(value['time']).split(' ')
                            min_hour = min_hour[:5]
                            intentNotFoundList.append({"USER QUESTION":value['intentNotFound'],
                                                       "QUESTION ID": ID,
                                                       "DATE": date,
                                                       "TIME (UTC)": min_hour
                                                       })
                except Exception as e:
                    logger.error("Generate Report Json error: ")
                    logger.exception(e)
                    pass

                #now iterate through each individual conversation log
                try:
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        Adverse_Events = word_check_list(message, adverse_key_list)
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        #process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        #process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        #process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        #create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        #create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        #create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum':0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos':numConvosLat,'latencySum': latencySum}
                                    botJustSentMessage = True

                        #rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID:int(rating)}

                        #check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'


                        final_conv_log.append({"ID":ID,
                                               "FROM":type,
                                               "MESSAGE":message,
                                               "ADVERSE EVENTS": Adverse_Events,
                                               "ASR/TTS USAGE":ASR,
                                               "RATING":rating,
                                               "COMMENTS":comments,
                                               "SURVEY RESPONSES":Survey,
                                               "DATE":date,
                                               "TIME (UTC)":min_hour_sec
                                               })

                except Exception as e:
                    logger.error("Generate Report Json error: ")
                    logger.exception(e)

            else:
                continue

        """
        Create sheets for intent found and intent not found
        """


        try:
            if found_bot_id == False:
                return 'Failure'
        except Exception as e:
            logger.error("Generate report json error: ")
            logger.exception(e)
            pass

        try:
            averageScore = sumScore/numRated
        except Exception as e:
            logger.error("Generate report json error: ")
            logger.exception(e)
            averageScore = 'N/A'


        #initialize lists for data sheet
        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        ratingKeyList = []
        ratingValueList = []
        latencyKeyList = []
        latencyValueList = []
        ratingCountKeyList = []
        ratingCountValueList = []

        #create the necessary lists for populating the charts
        for key, value in sorted(ratingHistogramDict.items()):
            ratingCountKeyList.append(key)
            ratingCountValueList.append(value)
        print(ratingCountKeyList)
        print(ratingCountValueList)

        for key, value in sorted(ratingsDict.items()):
            ratingKeyList.append(key)
            counter = 0
            ratingSum = 0
            for ratingKey, ratingValue in value.items():
                counter +=1
                ratingSum += ratingValue
            ratingValueList.append(ratingSum/counter)

        for key,value in sorted(latencyDict.items()):
            if key == 12:
                key = '12 pm'
            elif key ==24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum']/value['numConvos']
                else:
                    finalLatencyAvg =0
            latencyValueList.append(finalLatencyAvg)

        for key,value in sorted(dateDict.items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)

        for key,value in sorted(timeDict.items()):
            if key == 12:
                key = '12 pm'
            elif key ==24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)

        final_json = {
            "bot_name":bot_name,
            "chat_log":final_conv_log,
            "intent_not_found":intentNotFoundList,
            "intent_found":intentFoundList,
            "graph_data":[
                {
                    "graph_name":"Number of Conversations By Day",
                    "x_values":dateKeyList,
                    "y_values":dateValuesList,
                    "x_axis_title":"Days",
                    "y_axis_title":"Number of Conversations"
                },
                {
                    "graph_name": "Peak Engagement Hours",
                    "x_values": timeKeyList,
                    "y_values": timeValueList,
                    "x_axis_title": "Time of Day (UTC)",
                    "y_axis_title": "Number of Conversations"
                },
                {
                    "graph_name": "User Satisfaction",
                    "x_values": ratingKeyList,
                    "y_values": ratingValueList,
                    "x_axis_title": "Days",
                    "y_axis_title": "Average Satisfaction Rating"
                },
                {
                    "graph_name": "Bot Response Latency",
                    "x_values": latencyKeyList,
                    "y_values": latencyValueList,
                    "x_axis_title": "Time of Day (UTC)",
                    "y_axis_title": "Aversage Response Latency (sec)"
                },
                {
                    "graph_name": "User Satisfaction Distribution",
                    "x_values": ratingCountKeyList,
                    "y_values": ratingCountValueList,
                    "x_axis_title": "Rating Score",
                    "y_axis_title": "Number of Ratings"
                }
            ]
        }

        return final_json
    except Exception as e:
        logger.error("Generate Report Json error: ")
        logger.exception(e)
# print('THE JSON ',generateReports())

def word_check_list(message ,key_list):
    stop = set(stopwords.words('english'))
    msg_tokens = word_tokenize(message)
    all_stops = stop | set(string.punctuation)
    tokens = [t for t in msg_tokens if t not in all_stops]
    for i in tokens:
        if i.lower() in key_list:
            return "Yes"
    return "No"

def key_list_gen():
    fileTitle = 'Besponsa_Adverse_keywords_v1.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    print("Directory Path: ", directory_path)
    file_uri = os.path.join(directory_path, 'mapping', fileTitle)
    adverse_key_json = eval(convert(file_uri))
    print('Adverse key json :', adverse_key_json)
    # print(type(adverse_key_json), "***type of key file", adverse_key_json['sheet1'])
    adverse_key_list = [str(i['Keywords']).lower() for i in adverse_key_json['proposal_for_the_chatbot']]
    print('adverse_key_list', adverse_key_list)
    return adverse_key_list