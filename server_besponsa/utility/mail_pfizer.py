import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import logging
import xlsxwriter
from dateutil import tz
import re
import os
from utility.mongo_dao import get_all, get_collection,get_by_id
logger = logging.getLogger()



def send_email_escalation(report_filename, emails, ID):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'test@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "Besponsa Bot Escalation: ID - " + ID
        body = "Hello,\n\n Following user was not able to find an answer to his/her satisfaction from the HemaHub chatbot. Please find the conversation log attached."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)
        # with open(report_filename, "r",
        #           encoding='utf8') as fil:
        #     part = MIMEApplication(
        #         fil.read(),
        #         Name=basename(report_filename)
        #     )
        #
        # part['Content-Disposition'] = 'attachment; filename="%s"' % basename(report_filename)
        # msg.attach(part)

    except Exception as e:

        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.")
        logger.exception(e)
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        # server.login(username, password)
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:

        logger.error("ERROR: Unexpected error: Could not send email-{}.")
        logger.exception(e)
        return ('Failure')



####### Send email when user Security required is 'Yes' ######

def send_email_ysecurity(report_filename, emails):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'test@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "Besponsa Bot Appointment for Safety - " + str(today_date)
        body = "Hello " + ",\n\n A user of HemaHub chatbot wants to schedule an appointment with the Pfizer doctor for safety. Please find conversation log attached for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)


    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.")
        logger.exception(e)
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.")
        logger.exception(e)
        return ('Failure')

####### Send email when user Security required is 'No' ######


def send_email_nsecurity(report_filename, emails):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'test@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "Besponsa Bot Appointment for Non-Safety - " + str(today_date)
        body = "Hello " + ",\n\n A user of HemaHub chatbot wants to schedule an appointment with the Pfizer doctor which is not for safety. Please find conversation log attached for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)


    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.")
        logger.exception(e)
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.")
        logger.exception(e)
        return ('Failure')

def send_email(report_filename, emails):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = emails
    from_addr = 'paco@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "PACO Bot Report for " + str(today_date)
        body = "Hello " + ",\n\nThank you for using PACO. Please find attached the Report for " + str(
            today_date) + ". \n \n Thank you."
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)


    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.")
        logger.exception(e)
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.")
        logger.exception(e)
        return ('Failure')


def generateemail(session_id = None, session = None, input_json = None):
    print(input_json)
    bot_id = None
    #session_id = '5b2145965b2f7a1224db0455'
    cursor = get_by_id('status',session_id)  # todo - read for only particular bot

    bot_name = 'Besponsa'
    fileTitle = 'static/reports/' + bot_name + 'AP_Safety_Details' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    convoLog = workbook.add_worksheet('ConversationLog')

    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['ID', 'FROM', 'MESSAGE', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 14, 20)
    convoLog.autofilter('A1:O1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    headersIntent = ['USER EMAIL ADDRESS', 'DATE', 'TIME (UTC)']
    # headers = ['ID', 'FROM', 'MESSAGE', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    ID = session_id
    numRated = 0
    sumScore = 0
    number_conv_counter = 0
    dateDict = {}
    timeDict = {}
    ratingsDict = {}
    latencyDict = {}
    intentFoundList = []
    intentNotFoundList = []
    final_conv_log = []
    # intentFoundList.append(headersIntent)
    # intentNotFoundList.append(headersIntent)
    final_conv_log.append(headers)
    found_bot_id = False

    # create hour dict for conversations over time of day and latency
    for k in range(0, 25):
        timeDict[k] = []
        latencyDict[k] = []
    ratingHistogramDict = {'No Rating': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
    # for document in cursor:
    try:
        # match session_id
        print("DOCUMENT IS = {}".format(cursor))
        number_conv_counter += 1
        # found_bot_id = True
        try:
            rating = cursor['Rating']
            try:
                numRated += 1
                # sumScore += int(rating)
                ratingHistogramDict[rating] += 1
            except:
                numRated += 1
                sumScore = 0
                # ratingHistogramDict[rating] += 1
        except Exception as e:
            rating = 'None'
            ratingHistogramDict['No Rating'] += 1
        # print('we here and about to look for comments', list(comments.keys()))

        try:
            comments = cursor['comments']

        except Exception as e:
            comments = 'None'

        # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later

        userReplyTime = datetime.datetime.now()


        # now iterate through each individual conversation log
        try:
            # print('bot ID is not none ', cursor)
            conversations = cursor['conversations']
            # adverse_key_list=word_check_list()
            botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
            #adverse_key_list = key_list_gen()
            for item in conversations:
                message = conversations[item]['message']
                #Adverse_Events = word_check_list(message, adverse_key_list)
                type = conversations[item]['type']
                # time = conversations[item]['time']
                time = conversations[item]['time'].replace(tzinfo=tz.gettz('UTC'))

                # process the message to remove html elements
                message = re.sub(r'<br>', ' ', message)
                message = re.sub(r'<button .*', '', message)
                message = re.sub(r'<[^>]+>', '', message)

                # process message type to read as "Chatbot" or "User"
                if type == 'bot message':
                    type = 'ChatBot'
                else:
                    type = 'User'

                # process the time into manageable numbers
                date, min_hour = str(time).split(' ')
                min_hour_sec = min_hour[:8]
                min_hour = min_hour[:5]
                hour = min_hour[:2]
                hour = int(hour)

                # create bar graph information for dates vs #conversations
                if date in dateDict:
                    dateDict[date].append(ID)
                else:
                    dateDict[date] = [ID]

                # create line graph information for # conversations by hour of day
                if hour in timeDict:
                    timeDict[hour].append(ID)
                else:
                    timeDict[hour] = [ID]

                # create line graph info for latency by hour of day
                if hour in latencyDict:
                    if latencyDict[hour] == []:
                        # only want to calculate "bot message timestamp" - "user message timestamp"
                        if conversations[item]['type'] == 'user message':
                            userReplyTime = time
                        latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                    elif conversations[item]['type'] == 'user message':
                        userReplyTime = time
                        botJustSentMessage = False
                    else:
                        if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                            latency = (time - userReplyTime).total_seconds()
                            numConvosLat = 1 + latencyDict[hour]['numConvos']
                            latencySum = latencyDict[hour]['latencySum'] + latency
                            latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                            botJustSentMessage = True

                # rating over days
                if rating != 'None':
                    try:
                        if date in ratingsDict:
                            if ID in ratingsDict[date]:
                                pass
                            else:
                                ratingsDict[date][ID] = int(rating)
                        else:
                            ratingsDict[date] = {ID: int(rating)}
                    except:
                        pass

                # check if ASR was used or not
                if str(conversations[item]['ASR']) == 'False':
                    ASR = 'No'
                else:
                    ASR = 'Yes'

                if str(conversations[item]['endSurveyFlag']) == 'False':
                    Survey = 'No'
                else:
                    Survey = 'Yes'

                # write row of dataset to conversation log
                dataRow = [ID,type, message, ASR, rating, comments,Survey,date, min_hour_sec]
                convoLog.write_row(i, 0, dataRow)
                i += 1
        except Exception as e:
            print('Exception is ', e)
            workbook.close()

    except Exception as e:
        print('DOCUMENT CURSOR ERROR - ', e)
        workbook.close()

    """
    Create sheets for intent found and intent not found
    """
    # #create header that will be used for "Intent Found" and "Intent not Found"

    # Hide the datasheet used to create the charts

    workbook.close()
    print('we here?')

    emails = ['rsanghi@deloitte.com','tpotturu@deloitte.com']
    print('completed prior to sending email')

    try:

        if session['no_sec_app']:
            print('inside No Security email function')
            send_email_nsecurity(fileTitle, emails)
        elif session['security']:
            send_email_ysecurity(fileTitle, emails)
        elif session['escalation']:
            print('inside escalation')
            send_email_escalation(fileTitle, emails, ID)
        else:
            print('inside else statement')

    except Exception as e:
        print('email is not sending')
    return fileTitle
