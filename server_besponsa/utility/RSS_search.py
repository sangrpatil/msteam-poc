import feedparser
import re

RSS_FEEDS = {
    'onclive': 'https://www.onclive.com/articles-rss',
    # 'ash': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1290&id=426'
    'features': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1284&id=423',
    'diffusion': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1285&id=421',
    'ask the hematologist': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1286&id=427',
    'mini review': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1287&id=429',
    'clinical trials corner': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1288&id=431',
    'profiles': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1289&id=430',
    "president's column": 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1290&id=426',
    'op-ed': 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1291&id=445',
    "year's best": 'https://www.hematology.org/Hematologist/Templates/ListRss.aspx?t=cal&tid=1292&id=3562'
}

'''
    The user message will be received here(i.e. one of the news sources) 
    and the top five RSS feed regarding that news feed will be fetched
'''


def rss_news(message):
    publication = message.lower()
    feed = feedparser.parse(RSS_FEEDS[publication])
    publish = []
    news = {}
    final_news = []
    for i in range(5):
        publish.append(feed['entries'][i])
    for i in publish:
        news = i
        final_news.append({'title': news['title'], 'summary': cleanhtml(news['summary']), 'link': news['link']})
    return final_news


def get_publications():
    return RSS_FEEDS.keys()


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext