import xlsxwriter
import datetime
from utility.logger import logger
from utility.excel_parser import *
import os
import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import re
import configparser
from utility.mongo_dao import get_all, get_collection

app_config = configparser.ConfigParser()
# app_config.read_file(open(r'../config/app_settings.ini'))


def generateReportsDaily(bot_id = None):
    adverse_key_list = key_list_gen()
    cursor = get_collection('status')  # todo - read for only particular bot
    if bot_id == None:
        bot_name = 'All_Bots'
    else:
        bot_name = cursor = get_collection('bot')
        for document in cursor:
            if str(bot_id) == str(document['_id']).replace('ObjectId(','').replace(')',''):
                bot_name = document['bot name']
    if bot_name == '':
        bot_name = 'Bot'
    fileTitle = 'reports/_' + bot_name + '_Chatbot_Report_Daily_' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    convoLog = workbook.add_worksheet('ConversationLog')


    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['ID', 'FROM', 'MESSAGE','ADVERSE EVENTS', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 8, 20)
    convoLog.autofilter('A1:I1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']
    headers = ['ID', 'FROM', 'MESSAGE','ADVERSE EVENTS',  'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    ID = 0
    numRated = 0
    sumScore = 0
    number_conv_counter = 0
    dateDict = {}
    timeDict = {}
    ratingsDict = {}
    latencyDict = {}
    intentFoundList = []
    intentNotFoundList = []
    final_conv_log = []

    final_conv_log.append(headers)
    found_bot_id = False

    # create hour dict for conversations over time of day and latency
    for k in range(0, 25):
        timeDict[k] = []
        latencyDict[k] = []
    ratingHistogramDict = {'No Rating': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
    for document in cursor:
        try:
            # determine the converation's rating and comments, if any

            if bot_id == None:
                number_conv_counter += 1
                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated += 1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)

                    rating = 'None'
                    ratingHistogramDict['No Rating'] += 1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)
                    comments = 'None'

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()

                # now iterate through each individual conversation log
                try:
                    print('bot ID is none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        Adverse_Events = word_check_list(message, adverse_key_list)
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID: int(rating)}

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'

                        dataRow = [ID, type, message, Adverse_Events, ASR, rating, comments, Survey, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ")
                    logger.exception(e)

            elif str(document['bot_id']) == str(bot_id):
                number_conv_counter += 1
                found_bot_id = True
                try:
                    rating = document['Rating']
                    numRated += 1
                    sumScore += int(rating)
                    ratingHistogramDict[rating] += 1
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)
                    rating = 'None'
                    ratingHistogramDict['No Rating'] += 1
                try:
                    comments = document['Comments']
                except Exception as e:
                    logger.error("Generate report excel error: ")
                    logger.exception(e)
                    comments = 'None'

                # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                ID += 1
                userReplyTime = datetime.datetime.now()


                # now iterate through each individual conversation log
                try:
                    print('bot ID is not none ', document)
                    conversations = document['conversations']

                    botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                    for item in conversations:
                        message = conversations[item]['message']
                        Adverse_Events = word_check_list(message, adverse_key_list)
                        type = conversations[item]['type']
                        time = conversations[item]['time']

                        # process the message to remove html elements
                        message = re.sub(r'<br>', ' ', message)
                        message = re.sub(r'<button .*', '', message)
                        message = re.sub(r'<[^>]+>', '', message)

                        # process message type to read as "Chatbot" or "User"
                        if type == 'bot message':
                            type = 'ChatBot'
                        else:
                            type = 'User'

                        # process the time into manageable numbers
                        date, min_hour = str(time).split(' ')
                        min_hour_sec = min_hour[:8]
                        min_hour = min_hour[:5]
                        hour = min_hour[:2]
                        hour = int(hour)

                        # create bar graph information for dates vs #conversations
                        if date in dateDict:
                            dateDict[date].append(ID)
                        else:
                            dateDict[date] = [ID]

                        # create line graph information for # conversations by hour of day
                        if hour in timeDict:
                            timeDict[hour].append(ID)
                        else:
                            timeDict[hour] = [ID]

                        # create line graph info for latency by hour of day
                        if hour in latencyDict:
                            if latencyDict[hour] == []:
                                # only want to calculate "bot message timestamp" - "user message timestamp"
                                if conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                            elif conversations[item]['type'] == 'user message':
                                userReplyTime = time
                                botJustSentMessage = False
                            else:
                                if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                    latency = (time - userReplyTime).total_seconds()
                                    numConvosLat = 1 + latencyDict[hour]['numConvos']
                                    latencySum = latencyDict[hour]['latencySum'] + latency
                                    latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                                    botJustSentMessage = True

                        # rating over days
                        if rating != 'None':
                            if date in ratingsDict:
                                if ID in ratingsDict[date]:
                                    pass
                                else:
                                    ratingsDict[date][ID] = int(rating)
                            else:
                                ratingsDict[date] = {ID: int(rating)}

                        # check if ASR was used or not
                        if str(conversations[item]['ASR']) == 'False':
                            ASR = 'No'
                        else:
                            ASR = 'Yes'

                        if str(conversations[item]['endSurveyFlag']) == 'False':
                            Survey = 'No'
                        else:
                            Survey = 'Yes'


                        dataRow = [ID, type, message, Adverse_Events, ASR, rating, comments, Survey, date, min_hour_sec]
                        convoLog.write_row(i, 0, dataRow)
                        i += 1
                except Exception as e:
                    logger.error("Generate Report Excel error: ")
                    logger.exception(e)

            else:
                continue
        except Exception as e:
            logger.error("Generate Report Excel DOCUMENT CURSOR ERROR - : ")
            logger.exception(e)

    """
    Create sheets for intent found and intent not found
    """
    # #create header that will be used for "Intent Found" and "Intent not Found"

    headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']




    try:
        if found_bot_id == False:
            return 'Failure'
    except Exception as e:
        logger.error("Generate Report Excel error: ")
        logger.exception(e)
        pass

    # Hide the datasheet used to create the charts

    workbook.close()
    return fileTitle

def generateReportsWeekly(bot_id = "5cd0739587c6bd6dbc36f76a"): #todo pass bot ID
    try:
        adverse_key_list = key_list_gen()
        cursor = get_collection('status') #todo - read for only particular bot

        logger.info('---CHECKING BOT ID ---'+ str(bot_id))
        if bot_id == None:
            bot_name = 'All_Bots'
        else:
            cursor_bot = get_collection('bot')
            for document in cursor_bot:
                if str(bot_id) == str(document['_id']).replace('ObjectId(','').replace(')',''):
                    bot_name = document['bot name']
        if bot_name == '':
            bot_name = 'Bot'
        fileTitle = 'static/reports/'+str(bot_id) + '.xlsx'
        print(fileTitle)

        workbook = xlsxwriter.Workbook(filename=fileTitle)
        dashboard = workbook.add_worksheet('Dashboard')
        intentFound = workbook.add_worksheet('Intents Found')
        intentNotFound = workbook.add_worksheet('Intent Not Found')
        convoLog = workbook.add_worksheet('ConversationLog')
        dataSheet = workbook.add_worksheet('DataSheet')
        print("Errors before this")

        # master formatting style
        formatting = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': 'white',
            'fg_color': '#007DC6'})

        # write column headers for conversation data log
        headers = ['ID', 'FROM', 'MESSAGE', 'ADVERSE EVENTS','QUESTION CATEGORY', 'ASR/TTS USAGE', 'RATING', 'COMMENTS','SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
        convoLog.write_row('A1',headers, cell_format=formatting)
        convoLog.set_column(0, 8, 20)
        convoLog.autofilter('A1:I1')

        #Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
        i = 1
        headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']
        headers = ['ID', 'FROM', 'MESSAGE', 'ADVERSE EVENTS', 'QUESTION CATEGORY','ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
        ID = 0
        numRated = 0
        sumScore = 0
        number_conv_counter = 0
        dateDict = {}
        timeDict = {}
        ratingsDict = {}
        latencyDict = {}
        intentFoundList = []
        intentNotFoundList = []
        final_conv_log = []

        final_conv_log.append(headers)

        #create hour dict for conversations over time of day and latency
        for k in range(0,25):
            timeDict[k] = []
            latencyDict[k] = []
        ratingHistogramDict ={'No Rating':0,'1':0,'2':0,'3':0,'4':0,'5':0}
        for document in cursor:
            logger.info('---CHECKING BOT ID---' +  str(bot_id))
            try:
                #determine the converation's rating and comments, if any


                if bot_id == None:
                    number_conv_counter+=1
                    # found_bot_id = True
                    try:
                        rating = document['Rating']
                        numRated +=1
                        sumScore += int(rating)
                        ratingHistogramDict[rating] += 1
                    except Exception as e:
                        logger.error("Generate report excel error: ")
                        logger.exception(e)
                        rating = 'None'
                        ratingHistogramDict['No Rating'] +=1
                    try:
                        comments = document['Comments']
                    except Exception as e:
                        logger.error("Generate report excel error: "+ str(e))
                        logger.exception(e)
                        comments = 'None'

                    #initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                    ID += 1
                    userReplyTime  = datetime.datetime.now()

                    #look for user input intents that could be successfully answered (user said, "Yes" this was helpful)
                    try:
                        intentFoundDocument = document['intentFound']
                        if intentFoundDocument != 'None':
                            for key, value in intentFoundDocument.items():
                                date, min_hour = str(value['time']).split(' ')
                                min_hour = min_hour[:5]
                                intentFoundList.append([value['intentFound'],ID, date, min_hour])

                    except Exception as e:
                        logger.error("Generate Report Excel error: "+ str(e))
                        logger.exception(e)
                        pass

                    #look for user input intents that couldn't be found (user said "No" not helpful, or bot said "sorry can't find it")
                    try:
                        intentNotFoundDocument = document['intentNotFound']
                        if intentNotFoundDocument != 'None':
                            for key, value in intentNotFoundDocument.items():
                                date, min_hour = str(value['time']).split(' ')
                                min_hour = min_hour[:5]
                                intentNotFoundList.append([value['intentNotFound'],ID, date, min_hour])

                    except Exception as e:
                        logger.error("Generate Report Excel error: "+ str(e))
                        logger.exception(e)
                        pass

                    #now iterate through each individual conversation log
                    try:
                        conversations = document['conversations']

                        botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                        for item in conversations:
                            message = conversations[item]['message']
                            Adverse_Events = word_check_list(message, adverse_key_list)
                            type = conversations[item]['type']
                            time = conversations[item]['time']
                            groupIndex = conversations[item]['groupIndex']

                            #process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            #process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            #process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            #create bar graph information for dates vs #conversations
                            if date in dateDict:
                                dateDict[date].append(ID)
                            else:
                                dateDict[date] = [ID]

                            #create line graph information for # conversations by hour of day
                            if hour in timeDict:
                                timeDict[hour].append(ID)
                            else:
                                timeDict[hour] = [ID]

                            #create line graph info for latency by hour of day
                            if hour in latencyDict:
                                if latencyDict[hour] == []:
                                    # only want to calculate "bot message timestamp" - "user message timestamp"
                                    if conversations[item]['type'] == 'user message':
                                        userReplyTime = time
                                    latencyDict[hour] = {'numConvos': 0, 'latencySum':0}  # not time
                                elif conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                    botJustSentMessage = False
                                else:
                                    if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                        latency = (time - userReplyTime).total_seconds()
                                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                                        latencySum = latencyDict[hour]['latencySum'] + latency
                                        latencyDict[hour] = {'numConvos':numConvosLat,'latencySum': latencySum}
                                        botJustSentMessage = True

                            #rating over days
                            if rating != 'None':
                                if date in ratingsDict:
                                    if ID in ratingsDict[date]:
                                        pass
                                    else:
                                        ratingsDict[date][ID] = int(rating)
                                else:
                                    ratingsDict[date] = {ID:int(rating)}

                            #check if ASR was used or not
                            if str(conversations[item]['ASR']) == 'False':
                                ASR = 'No'
                            else:
                                ASR = 'Yes'

                            if str(conversations[item]['endSurveyFlag']) == 'False':
                                Survey = 'No'
                            else:
                                Survey = 'Yes'

                            # groupIndex  = conversations.get("item").get("groupIndex", "")
                            final_conv_log.append([ID,type, message, Adverse_Events, groupIndex,ASR, rating, comments,Survey, date, min_hour_sec])

                            dataRow = [ID,type, message, Adverse_Events, groupIndex,ASR, rating, comments,Survey, date, min_hour_sec]
                            convoLog.write_row(i,0, dataRow)
                            i += 1
                    except Exception as e:
                        logger.error("Generate Report Excel error: "+ str(e))
                        logger.exception(e)

                elif str(document['bot_id']) == str(bot_id):
                    number_conv_counter+=1
                    # found_bot_id = True
                    try:
                        rating = document['Rating']
                        numRated +=1
                        sumScore += int(rating)
                        ratingHistogramDict[rating] += 1
                    except Exception as e:
                        logger.error("Generate report excel error: "+ str(e))
                        logger.exception(e)
                        rating = 'None'
                        ratingHistogramDict['No Rating'] +=1
                    try:
                        comments = document['Comments']
                    except Exception as e:
                        logger.error("Generate report excel error: "+ str(e))
                        logger.exception(e)
                        comments = 'None'

                    #initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later
                    ID += 1
                    userReplyTime  = datetime.datetime.now()

                    #look for user input intents that could be successfully answered (user said, "Yes" this was helpful)
                    try:
                        intentFoundDocument = document['intentFound']
                        if intentFoundDocument != 'None':
                            for key, value in intentFoundDocument.items():
                                date, min_hour = str(value['time']).split(' ')
                                min_hour = min_hour[:5]
                                intentFoundList.append([value['intentFound'],ID, date, min_hour])
                    except Exception as e:
                        logger.error("Generate Report Excel error: "+ str(e))
                        logger.exception(e)
                        pass

                    #look for user input intents that couldn't be found (user said "No" not helpful, or bot said "sorry can't find it")
                    try:
                        intentNotFoundDocument = document['intentNotFound']
                        if intentNotFoundDocument != 'None':
                            for key, value in intentNotFoundDocument.items():
                                date, min_hour = str(value['time']).split(' ')
                                min_hour = min_hour[:5]
                                intentNotFoundList.append([value['intentNotFound'],ID, date, min_hour])
                    except Exception as e:
                        logger.error("Generate report excel error: "+ str(e))
                        logger.exception(e)
                        pass

                    #now iterate through each individual conversation log
                    try:
                        conversations = document['conversations']

                        botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
                        for item in conversations:
                            message = conversations[item]['message']
                            Adverse_Events = word_check_list(message, adverse_key_list)
                            type = conversations[item]['type']
                            time = conversations[item]['time']
                            groupIndex = conversations[item]['groupIndex']

                            #process the message to remove html elements
                            message = re.sub(r'<br>', ' ', message)
                            message = re.sub(r'<button .*', '', message)
                            message = re.sub(r'<[^>]+>', '', message)

                            #process message type to read as "Chatbot" or "User"
                            if type == 'bot message':
                                type = 'ChatBot'
                            else:
                                type = 'User'

                            #process the time into manageable numbers
                            date, min_hour = str(time).split(' ')
                            min_hour_sec = min_hour[:8]
                            min_hour = min_hour[:5]
                            hour = min_hour[:2]
                            hour = int(hour)

                            #create bar graph information for dates vs #conversations
                            if date in dateDict:
                                dateDict[date].append(ID)
                            else:
                                dateDict[date] = [ID]

                            #create line graph information for # conversations by hour of day
                            if hour in timeDict:
                                timeDict[hour].append(ID)
                            else:
                                timeDict[hour] = [ID]

                            #create line graph info for latency by hour of day
                            if hour in latencyDict:
                                if latencyDict[hour] == []:
                                    # only want to calculate "bot message timestamp" - "user message timestamp"
                                    if conversations[item]['type'] == 'user message':
                                        userReplyTime = time
                                    latencyDict[hour] = {'numConvos': 0, 'latencySum':0}  # not time
                                elif conversations[item]['type'] == 'user message':
                                    userReplyTime = time
                                    botJustSentMessage = False
                                else:
                                    if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                                        latency = (time - userReplyTime).total_seconds()
                                        numConvosLat = 1 + latencyDict[hour]['numConvos']
                                        latencySum = latencyDict[hour]['latencySum'] + latency
                                        latencyDict[hour] = {'numConvos':numConvosLat,'latencySum': latencySum}
                                        botJustSentMessage = True

                            #rating over days
                            if rating != 'None':
                                if date in ratingsDict:
                                    if ID in ratingsDict[date]:
                                        pass
                                    else:
                                        ratingsDict[date][ID] = int(rating)
                                else:
                                    ratingsDict[date] = {ID:int(rating)}

                            #check if ASR was used or not
                            if str(conversations[item]['ASR']) == 'False':
                                ASR = 'No'
                            else:
                                ASR = 'Yes'

                            if str(conversations[item]['endSurveyFlag']) == 'False':
                                Survey = 'No'
                            else:
                                Survey = 'Yes'

                            # groupIndex = conversations.get("item").get("groupIndex", "")
                            dataRow = [ID,type, message, Adverse_Events, groupIndex,ASR, rating, comments,Survey, date, min_hour_sec]
                            convoLog.write_row(i,0, dataRow)
                            i += 1
                    except Exception as e:
                        logger.error("Generate Report Excel error: "+ str(e))
                        logger.exception(e)

                else:
                    continue
            except Exception as e:
                logger.error('DOCUMENT CURSOR ERROR - '+ str(e))
                logger.exception(e)
    except Exception as e:
        logger.error('Weekly Notifications Failure of  - '+ str(e))
        logger.exception(e)
    try:
        """
        Create sheets for intent found and intent not found
        """
        print('---CHECKING 2nd HALF OF GEN REPORTS---')
        # #create header that will be used for "Intent Found" and "Intent not Found"
        headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']
        #
        # #create sheet for intent not found
        intentNotFound.write_row('A1', headersIntent, cell_format=formatting)
        intentNotFound.set_column(0, 7, 20)
        intentNotFound.autofilter('A1:D1')
        h = 1
        for item in intentNotFoundList:
            intentNotFound.write_row(h, 0, item)
            h+=1

        #create sheet for intent found
        intentFound.write_row('A1', headersIntent, cell_format=formatting)
        intentFound.set_column(0, 7, 20)
        intentFound.autofilter('A1:D1')
        h = 1
        for item in intentFoundList:
            intentFound.write_row(h, 0, item)
            h += 1


        #format main dashboard
        title = 'Chatbot Dashboard: ' + datetime.datetime.today().strftime('%m-%d-%Y')
        formatting.set_font_size(20)
        dashboard.merge_range('A1:S2', title, formatting)
        formatting.set_font_size(11)

        #create summary data table
        dashboard.write(3,1,'Summary Data')
        dashboard.merge_range('B4:C4', 'Summary Data', formatting)
        dashboard.write_row(4,1,['Number of Conversations', number_conv_counter])

        try:
            logger.info(f"Num rated: {numRated}")
            if not numRated:
                averageScore = 0
            else:
                averageScore = sumScore/numRated
        except Exception as e:
            logger.error("Generate report excel error: "+ str(e))
            logger.exception(e)
            averageScore = 'N/A'
        dashboard.write_row(5, 1, ['Average Satisfaction Rating', averageScore])
        dashboard.write_row(6, 1, ['Number of Questions Users Rated as Helpful', len(intentFoundList)])
        dashboard.write_row(7, 1, ['Number of Questions Rated as Unhelpful', len(intentNotFoundList)])
        dashboard.set_column(1, 1, 50)

        # initialize charts
        barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
        lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
        lineGraph_RatingByDay = workbook.add_chart({'type': 'line'})
        lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})
        barGraph_RatingCount = workbook.add_chart({'type': 'column'})

        #initialize lists for data sheet
        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        ratingKeyList = []
        ratingValueList = []
        latencyKeyList = []
        latencyValueList = []
        ratingCountKeyList = []
        ratingCountValueList = []
        #create the necessary lists for populating the charts
        for key, value in sorted(ratingHistogramDict.items()):
            ratingCountKeyList.append(key)
            ratingCountValueList.append(value)

        for key, value in sorted(ratingsDict.items()):
            ratingKeyList.append(key)
            counter = 0
            ratingSum = 0
            for ratingKey, ratingValue in value.items():
                counter +=1
                ratingSum += ratingValue
            ratingValueList.append(ratingSum/counter)

        for key,value in sorted(latencyDict.items()):
            if key == 12:
                key = '12 pm'
            elif key ==24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'
            latencyKeyList.append(key)
            if value == []:
                finalLatencyAvg = 0
            else:
                if value['numConvos'] != 0:
                    finalLatencyAvg = value['latencySum']/value['numConvos']
                else:
                    finalLatencyAvg =0
            latencyValueList.append(finalLatencyAvg)

        for key,value in sorted(dateDict.items()):
            dateKeyList.append(key)
            value = len(set(value))
            dateValuesList.append(value)

        for key,value in sorted(timeDict.items()):
            if key == 12:
                key = '12 pm'
            elif key ==24 or key == 0:
                key = '12 am'
            elif (key - 12) > 1:
                key = key - 12
                key = str(key) + ' pm'
            else:
                key = str(key) + ' am'

            timeKeyList.append(key)
            value = len(set(value))
            timeValueList.append(value)


        dataSheet.write_row(0,0,dateKeyList)
        dataSheet.write_row(1,0, dateValuesList)
        dataSheet.write_row(2, 0, timeKeyList)
        dataSheet.write_row(3, 0, timeValueList)
        dataSheet.write_row(4,0,ratingKeyList)
        dataSheet.write_row(5, 0, ratingValueList)
        dataSheet.write_row(6, 0, latencyKeyList)
        dataSheet.write_row(7, 0, latencyValueList)
        dataSheet.write_row(8, 0, ratingCountKeyList)
        dataSheet.write_row(9, 0, ratingCountValueList)

        # create the charts and format them


        barGraph_ConvByDay.add_series({
            'name':'Number of Conversations by Day',
            'categories':['DataSheet',0,0,0,len(dateKeyList)-1 if len(dateKeyList) else len(dateKeyList)],
            'values': ['DataSheet',1,0,1,len(dateKeyList)-1 if len(dateKeyList) else len(dateKeyList)]
        })

        barGraph_ConvByDay.set_x_axis({'name': 'Days'})
        barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
        barGraph_ConvByDay.set_legend({'none': True})

        lineGraph_ConvByHour.add_series({
            'name': 'Peak Engagement Hours',
            'categories': '=DataSheet!$A$3:$Y$3',
            'values': '=DataSheet!$A$4:$Y$4'
        })
        lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day (UTC)'})
        lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
        lineGraph_ConvByHour.set_legend({'none': True})

        lineGraph_RatingByDay.add_series({
            'name': 'User Satisfaction',
            'categories': ['DataSheet', 4, 0, 4, len(ratingKeyList) - 1 if len(ratingKeyList) else len(ratingKeyList) ],
            'values': ['DataSheet', 5, 0, 5, len(ratingValueList) - 1 if len(ratingValueList) else len(ratingValueList) ]
        })

        lineGraph_RatingByDay.set_x_axis({'name': 'Days'})
        lineGraph_RatingByDay.set_y_axis({'name': 'Average Satisfaction Rating', 'min': 0, 'max': 5})
        lineGraph_RatingByDay.set_legend({'none': True})

        lineGraph_LatencyByHour.add_series({
            'name': 'Bot Response Latency',
            'categories': ['DataSheet', 6, 0, 6, len(latencyKeyList) - 1 if  len(latencyKeyList) else  len(latencyKeyList) ],
            'values': ['DataSheet', 7, 0, 7, len(latencyValueList) - 1 if len(latencyValueList) else len(latencyValueList) ]
        })

        lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day (UTC)'})
        lineGraph_LatencyByHour.set_y_axis({'name': 'Aversage Response Latency (sec)'})
        lineGraph_LatencyByHour.set_legend({'none': True})

        barGraph_RatingCount.add_series({
            'name': 'User Satisfaction Distribution',
            'categories': ['DataSheet', 8, 0, 8, len(ratingCountKeyList) - 1 if len(ratingCountKeyList)  else len(ratingCountKeyList) ],
            'values': ['DataSheet', 9, 0, 9, len(ratingCountValueList) - 1 if len(ratingCountValueList) else len(ratingCountValueList) ]
        })

        barGraph_RatingCount.set_x_axis({'name': 'Rating Score'})
        barGraph_RatingCount.set_y_axis({'name': 'Number of Ratings'})
        barGraph_RatingCount.set_legend({'none': True})

        #insert the charts into the dashboard
        dashboard.insert_chart('B11', barGraph_ConvByDay)
        dashboard.insert_chart('F11', lineGraph_ConvByHour)
        dashboard.insert_chart('B27', lineGraph_RatingByDay)
        dashboard.insert_chart('F27', lineGraph_LatencyByHour)
        dashboard.insert_chart('B43', barGraph_RatingCount)

        #Hide the datasheet used to create the charts
        dataSheet.hide()
        workbook.close()
        print('we here?')
        return fileTitle
    except Exception as e:
        logger.error('Weekly Sheet Generation Failure due to  - '+ str(e))
        logger.exception(e)
        return 'Failure due to ' + str(e)
#print('THE JSON ',generateReportsWeekly('5b68a50ce092330ff894af7d'))

def word_check_list(message ,key_list):
    stop = set(stopwords.words('english'))
    msg_tokens = word_tokenize(message)
    all_stops = stop | set(string.punctuation)
    tokens = [t for t in msg_tokens if t not in all_stops]
    for i in tokens:
        if i.lower() in key_list:
            return "Yes"
    return "No"

def key_list_gen():
    fileTitle = 'Besponsa_Adverse_Keywords_v1.xlsx'
    file_path = os.path.dirname(__file__)
    directory_path = os.path.dirname(file_path)
    print("Directory Path: ", directory_path)
    file_uri = os.path.join(directory_path, 'mapping', fileTitle)
    adverse_key_json = eval(convert(file_uri))
    print('Adverse key json :', adverse_key_json)
    # print(type(adverse_key_json), "***type of key file", adverse_key_json['sheet1'])
    adverse_key_list = [str(i['Keywords']).lower() for i in adverse_key_json['proposal_for_the_chatbot']]
    print('adverse_key_list', adverse_key_list)
    return adverse_key_list
