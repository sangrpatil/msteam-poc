import xlsxwriter
import datetime
from pymongo import MongoClient
import re
import configparser
from utility.mongo_dao import get_all, get_collection,get_by_id
from bson.objectid import ObjectId
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import logging
import nltk
nltk.download('punkt')
nltk.download('stopwords')
logger = logging.getLogger()
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from utility.excel_parser import *
app_config = configparser.ConfigParser()
import string
# app_config.read_file(open(r'../config/app_settings.ini'))
from dateutil import tz


def generateReportsDaily(session_id = None, input_json = None):
    print(input_json)
    bot_id = None
    #session_id = '5b2145965b2f7a1224db0455'
    cursor = get_by_id('status',session_id)  # todo - read for only particular bot
    # client = MongoClient('mongodb://' + host + ':' + port + '/' + config_mongo_db)
    # print(cursor)
    # this name needs to match up with the app-config file ('chat_demo' needs to match)
    # db = client.chat_Conmigo

    # create worksheets within workbook
    # collection = db['status']
    # cursor = collection.find({})
    bot_name = 'Besponsa'
    fileTitle = 'static/reports/' + bot_name + 'AP_Safety_Details' + datetime.datetime.today().strftime('%m-%d-%Y') + '.xlsx'
    workbook = xlsxwriter.Workbook(filename=fileTitle)
    convoLog = workbook.add_worksheet('ConversationLog')


    # master formatting style
    formatting = workbook.add_format({
        'bold': 1,
        'border': 1,
        'align': 'center',
        'valign': 'vcenter',
        'font_color': 'white',
        'fg_color': '#007DC6'})

    # write column headers for conversation data log
    headers = ['ID', 'FROM', 'MESSAGE', 'EMAIL', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    convoLog.write_row('A1', headers, cell_format=formatting)
    convoLog.set_column(0, 14, 20)
    convoLog.autofilter('A1:O1')

    # Parse through the mongoDB and create the conversation data log. Also create all the datasets necessary for the graphs
    i = 1
    headersIntent = ['USER EMAIL ADDRESS', 'DATE', 'TIME (UTC)']
    # headers = ['ID', 'FROM', 'MESSAGE', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES', 'DATE', 'TIME (UTC)']
    ID = session_id
    numRated = 0
    sumScore = 0
    number_conv_counter = 0
    dateDict = {}
    timeDict = {}
    ratingsDict = {}
    latencyDict = {}
    intentFoundList = []
    intentNotFoundList = []
    final_conv_log = []
    # intentFoundList.append(headersIntent)
    # intentNotFoundList.append(headersIntent)
    final_conv_log.append(headers)
    found_bot_id = False

    # create hour dict for conversations over time of day and latency
    for k in range(0, 25):
        timeDict[k] = []
        latencyDict[k] = []
    ratingHistogramDict = {'No Rating': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0}
    # for document in cursor:
    try:
        # match session_id
        print("DOCUMENT IS = {}".format(cursor))
        number_conv_counter += 1
        # found_bot_id = True
        try:
            rating = cursor['Rating']
            try:
                numRated += 1
                # sumScore += int(rating)
                ratingHistogramDict[rating] += 1
            except:
                numRated += 1
                sumScore = 0
                # ratingHistogramDict[rating] += 1
        except Exception as e:
            rating = 'None'
            ratingHistogramDict['No Rating'] += 1
        # print('we here and about to look for comments', list(comments.keys()))

        try:
            comments = cursor['comments']

        except Exception as e:
            comments = 'None'



        # initialize and increment necessary counters. initialize userReplyTime object to be a datetime object, but will replace value later

        userReplyTime = datetime.datetime.now()

        # look for user input intents that could be successfully answered (user said, "Yes" this was helpful)
        # try:
        #     intentFoundDocument = cursor['intentFound']
        #     print('in INTENT FOUND')
        #     if intentFoundDocument != 'None':
        #         for key, value in intentFoundDocument.items():
        #             date, min_hour = str(value['time']).split(' ')
        #             min_hour = min_hour[:5]
        #             intentFoundList.append([value['intentFound'], ID, date, min_hour])
        #             print('APPENDED?')
        # except Exception as e:
        #     pass
        #
        # # look for user input intents that couldn't be found (user said "No" not helpful, or bot said "sorry can't find it")
        # try:
        #     intentNotFoundDocument = cursor['intentNotFound']
        #     if intentNotFoundDocument != 'None':
        #         for key, value in intentNotFoundDocument.items():
        #             date, min_hour = str(value['time']).split(' ')
        #             min_hour = min_hour[:5]
        #             intentNotFoundList.append([value['intentNotFound'], ID, date, min_hour])
        # except Exception as e:
        #     pass

        # now iterate through each individual conversation log
        try:
            # print('bot ID is not none ', cursor)
            conversations = cursor['conversations']
            # adverse_key_list=word_check_list()
            botJustSentMessage = True  # check whether or not bot just sent message, use for calculating latency
            #adverse_key_list = key_list_gen()
            for item in conversations:
                message = conversations[item]['message']
                #Adverse_Events = word_check_list(message, adverse_key_list)
                type = conversations[item]['type']
                # time = conversations[item]['time']
                time = conversations[item]['time'].replace(tzinfo=tz.gettz('UTC'))

                # process the message to remove html elements
                message = re.sub(r'<br>', ' ', message)
                message = re.sub(r'<button .*', '', message)
                message = re.sub(r'<[^>]+>', '', message)

                # process message type to read as "Chatbot" or "User"
                if type == 'bot message':
                    type = 'ChatBot'
                else:
                    type = 'User'

                # process the time into manageable numbers
                date, min_hour = str(time).split(' ')
                min_hour_sec = min_hour[:8]
                min_hour = min_hour[:5]
                hour = min_hour[:2]
                hour = int(hour)

                # create bar graph information for dates vs #conversations
                if date in dateDict:
                    dateDict[date].append(ID)
                else:
                    dateDict[date] = [ID]

                # create line graph information for # conversations by hour of day
                if hour in timeDict:
                    timeDict[hour].append(ID)
                else:
                    timeDict[hour] = [ID]

                # create line graph info for latency by hour of day
                if hour in latencyDict:
                    if latencyDict[hour] == []:
                        # only want to calculate "bot message timestamp" - "user message timestamp"
                        if conversations[item]['type'] == 'user message':
                            userReplyTime = time
                        latencyDict[hour] = {'numConvos': 0, 'latencySum': 0}  # not time
                    elif conversations[item]['type'] == 'user message':
                        userReplyTime = time
                        botJustSentMessage = False
                    else:
                        if conversations[item]['type'] == 'bot message' and botJustSentMessage == False:
                            latency = (time - userReplyTime).total_seconds()
                            numConvosLat = 1 + latencyDict[hour]['numConvos']
                            latencySum = latencyDict[hour]['latencySum'] + latency
                            latencyDict[hour] = {'numConvos': numConvosLat, 'latencySum': latencySum}
                            botJustSentMessage = True

                # rating over days
                if rating != 'None':
                    try:
                        if date in ratingsDict:
                            if ID in ratingsDict[date]:
                                pass
                            else:
                                ratingsDict[date][ID] = int(rating)
                        else:
                            ratingsDict[date] = {ID: int(rating)}
                    except:
                        pass

                # check if ASR was used or not
                if str(conversations[item]['ASR']) == 'False':
                    ASR = 'No'
                else:
                    ASR = 'Yes'

                if str(conversations[item]['endSurveyFlag']) == 'False':
                    Survey = 'No'
                else:
                    Survey = 'Yes'
                # headers = ['ID', 'FROM', 'MESSAGE', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES',
                #            'DATE', 'TIME (UTC)']

                # final_conv_log.append({"ID":ID,
                #                        "FROM":type,
                #                        "MESSAGE":message,
                #                        "ASR/TTS USAGE":ASR,
                #                        "RATING":rating,
                #                        "COMMENTS":comments,
                #                        "SURVEY RESPONSES":Survey,
                #                        "DATE":date,
                #                        "TIME (UTC)":min_hour_sec
                #                        })
                # write row of dataset to conversation log
                dataRow = [ID,type, message, ASR, rating, comments,Survey,  date, min_hour_sec]
                convoLog.write_row(i, 0, dataRow)
                i += 1
        except Exception as e:
            print('Exception is ', e)
            workbook.close()

    except Exception as e:
        print('DOCUMENT CURSOR ERROR - ', e)
        workbook.close()

    """
    Create sheets for intent found and intent not found
    """
    # #create header that will be used for "Intent Found" and "Intent not Found"

    # Hide the datasheet used to create the charts

    workbook.close()
    print('we here?')

    emails = ['rsanghi@deloitte.com','tpotturu@deloitte.com']
    print('completed prior to sending email')
    try:
        send_email(fileTitle,emails, session_id)
    except:
        print('email is not sending')
    return fileTitle

def send_email(report_filename, emails, ID):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    # to_addrs = ['ruth.darcy@pfizer.com','Elaine.Dock@pfizer.com','Erum.Farooqui@pfizer.com','Alicia.Gupta@pfizer.com','Metrics.4wmr1ekjft3bao0v@u.box.com','SaiBharath.Lella@pfizer.com','abhinav.mishra@pfizer.com','robert.xu@pfizer.com']
    to_addrs = emails
    from_addr = 'test@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = "SOP Bot Escalation: ID - " + ID
        body = "Please review this email from the SOP bot"
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(open(report_filename, "rb").read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + report_filename)
        msg.attach(part)

    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.")
        logger.exception(e)

        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        # server.login(username, password)
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.")
        logger.exception(e)
        return ('Failure')

def word_check_list(message ,key_list):
    stop = set(stopwords.words('english'))
    msg_tokens =word_tokenize(message)
    all_stops = stop | set(string.punctuation)
    tokens=[t for t in msg_tokens if t not in all_stops]
    for i in tokens:
        if i in key_list:
            return "Yes"
    return "No"





def key_list_gen():
    adverse_key_json = eval(convert(r'mapping/Adverse_Keywords.xlsx'))
    # print(type(adverse_key_json), "***type of key file", adverse_key_json['sheet1'])
    adverse_key_list = [str(i['Palavras Chaves']).lower() for i in adverse_key_json['sheet1']]
    return adverse_key_list

if __name__ == '__main__':

    generateReportsDaily()
