# Logger
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# Create a file handler
handler = logging.FileHandler('logs/bot-server.log')
# Create a logging format
formatter = logging.Formatter('%(asctime)s - %(funcName)s() - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
# Add the handlers to the logger
logger.addHandler(handler)
