import re, json
import os
import csv
import pandas as pd
import configparser
from utility.mongo_dao import get_one as get, get_by_id, update, search_one
from utility.mail_pfizer import generateemail
from utility.conversation_logger import ConversationLogger
from fuzzywuzzy import fuzz
from operator import itemgetter
from validate_email import validate_email
from flask_babel import gettext, refresh
from utility.RSS_search import get_publications, rss_news
from utility.session_manager import get as get_context, modify as update_context, set as set_context
from utility.elasticsearch_query import SearchES
from utility.logger import logger
from utility.TTS import synthesize_text
from utility.gcp_places import retrieve_location

"""
Has three main functions: init, execute, resume
The rest are utility functions
@init: initializes the session with an id and starts logging
@execute: executes bot tasks, i.e. displaying a message or buttons
@resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
"""
import math
import datetime
app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))


hybrid_intent_not_found = 'I am not interested in any of these options'
class ExecutionEngine():
    def __init__(self,session_id, bot_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        print(f"Got session id {session_id} with contents {self.session} ")
        self.conv_log = ConversationLogger(session_id, bot_id)

    """
    Function: executes bot actions by giving user prompts (buttons/text/query outputs)
    This will check whether the output should be text, buttons, etc
    @:param self
    @:param task_definition: this is the current state json
    """
    def get_task_definition(self, search_key, search_value, bot_id):
        state_return = 'None'
        print("Search key : search_value: ", search_key, search_value)
        if(search_key == "Is Start" and search_value == "true") or (search_value == "Start"):
            state_definition_all = get_by_id('bot', bot_id)['mapping']
            for state in state_definition_all:
                if search_key in state.keys():
                    if state[search_key] == search_value:
                        state_return = state
                        break
            return state_return
        else:
            current_group = self.session['group'].lower()
            state_definition_all = get_by_id('bot', bot_id)['mapping']
            for state in state_definition_all:
                if search_key in state.keys():
                    if state[search_key] == search_value and state['Group'].lower() == current_group:
                        state_return = state
                        break
            return state_return


    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json={}):
        print("Existing JSON , if any received : ", existing_json)
        guidedRestart = False
        if self.session['restartFlag'] == True and self.session['ES_UsedFlag'] == False:
            guidedRestart = True
        self.conv_log.bot_log(static_text,self.session['ASRFlag'],self.session['endSurveyFlag'],guidedRestart, self.session['group'], self.session['grv_id'])
        #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        static_text = static_text.replace("__NAME__",self.session['name'])
        disable_response = False
        if "button" in interaction:
            disable_response = False
        if self.session['ASRFlag'] == True:
            static_text1 = re.sub(r'<[^<]+?>',' ',static_text)
            print('STATIC TEXT - ', static_text)
            tts_text = synthesize_text(static_text1)
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False
        if is_multi_message == True:
            print("Multiple messages received.")
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio":tts_text,
                "disable_response":disable_response
            })
            existing_json['is_multi'] = True
            print("Existing json in build response: ", existing_json)
            return existing_json
        else:
            json_return = {
                "is_multi":False,
                "message":[{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": disable_response
                }]
            }
            print("Json return in build response ", json_return)
            return json_return

    def execute_specific_task(self, task_definition):
        # self.session['group'] = "Hong Kong"
        # session_id = update_context(self.session_id, self.session)
        try:
            self.conv_log.update(task_definition['Task Name'])  # To update the current state in status
            # self.session['botName'] = task_definition['bot_name']
            response_json = ''

            task_text = task_definition['Task Text']
            interaction = task_definition['Interaction Type']

            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text)  # , dbData)

            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text)  # , dbData)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                response_json = self.button_execute(task_definition, task_text)  # , dbData, interactionObj)

        except Exception as e:
            logger.error('Execute Method Error - ')
            logger.exception(e)
            response_json = 'Sorry we weren''t able to understand your intent. - EXECUTE Error'

        return response_json

    def execute_restart(self, state_definition, bot_id):
        message = state_definition['Task Text']
        start_state = self.get_task_definition("Task Name", "Start", bot_id)
        # interaction_values = state_definition['Interaction Values']
        response = self.build_response("list",message,"button_vertical",start_state['Interaction Values'],False)
        print("response from execute_restart: ", response)
        return response

    def execute(self, task_definition):

        self.session = get_context(self.session_id)
        print(f"In execute function,  session {self.session}")
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False
        # self.session['group'] = "Hong Kong"
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            self.session['disable_input'] = False
        session_id = update_context(self.session_id,self.session)
        # check what the information about the current state is.
        # depending on what the interaction type is for the current state is, execute accordingly

        try:
            self.conv_log.update(task_definition['Task Name'])  # To update the current state in status
            #self.session['botName'] = task_definition['bot_name']
            response_json = ''

            task_text = task_definition['Task Text']
            interaction = task_definition['Interaction Type']

            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text)#, dbData)

            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text)#, dbData)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                response_json = self.button_execute(task_definition, task_text)#, dbData, interactionObj)

            elif interaction == 'url':
                response_json = self.url_execute(task_definition, task_text)

        except Exception as e:
            logger.error('Execute Method Error - ')
            logger.exception(e)
            response_json = 'Sorry we weren''t able to understand your intent. - EXECUTE Error'

        return response_json

    def resume(self, message, ASR, bot_id):

        messages_mappings_dictionary = {}
        messages_collection = get('messages', 'type', 'General')
        #print("Messages collection: ", messages_collection)
        messages_mappings = messages_collection['mappings']

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        #print("Messages mapping dictionary: Inside resume function", messages_mappings_dictionary)


        self.session['message'] = message
        # self.session['group'] = "Hong Kong"
        #todo - should I make the returns uniform
        if self.session['on_connect_start'] == True:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            if message.lower().strip() == 'nein' and self.session['disable_input'] == True:
                self.session['disable_input'] = True
            else:
                self.session['disable_input'] = False
        self.session['ASRFlag'] = ASR
        logger.debug('THE ASR VALUE IS = ', self.session['ASRFlag'], ASR)
        self.session['message'] = message
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.session['ES_UsedFlag'] = False
        update_context(self.session_id,self.session)
        print("Logging the user message")
        self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'],guidedRestartFlag=False, group=self.session['group'], grv_id=self.session["grv_id"])
        print("Message inside resume: ", message)
        try:
            # first check if the message is small talk
            small_talk_response = self.small_talk_module(message)
            print("Small talk response: ", small_talk_response)
            keyword_response = self.keyword_handler(message, bot_id)
            print("session.session fuzzy flag", self.session['fuzzyFlag'])
            if small_talk_response != 0:
                response = self.build_response("list", small_talk_response['message'][0]['text'], "button_horizontal", [messages_mappings_dictionary["Answer Questions"], messages_mappings_dictionary["Main Menu"]], False) #small_talk_response)
                return response
            elif keyword_response != 0:
                return keyword_response
            # next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restart_execute(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagFAQ'] is True:
                return self.restart_execute_FAQ(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagNextTask'] is True:
                return self.restart_next_task_execute(message, self.session_id, self.session, bot_id)
            elif self.session['endSurveyFlag'] is True:
                return self.survey_execute(message)
            # next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                print("fuzzy flag is true on recipt of message" )
                return self.fuzzy_execute(message,bot_id)

            # if it was none of the above, then it is a normal message
            else:
                # language and analyzer
                print("inside else of resume function - checking for action types.")
                es_analyzer = get_by_id('bot', bot_id)['es_analyzer']

                # first determine what the state is in the conversation, and what states can lead from here
                current_state = get_by_id('status', self.session_id)['current_state']

                state_definition = self.get_task_definition('Task Name',current_state,bot_id)

                action_type = state_definition['Action Type']
                next_task_ids = state_definition['Next Task IDs']
                next_possible_states_json = []
                next_possible_states_names = []

                print("Current state :", current_state)
                print("State definition: ", state_definition)

                try:
                    for id in next_task_ids:
                        state_option = self.get_task_definition('Task ID', id, bot_id)
                        next_possible_states_json.append(state_option)
                    for state_json in next_possible_states_json:
                        next_possible_states_names.append(state_json['Task Name'])
                    print("next possible state names: ", next_possible_states_names)
                except Exception as e:
                    logger.error("Execution engine error: ")
                    logger.exception(e)
                    next_possible_states_json = self.get_task_definition('Is Start', 'true', bot_id)
                    next_possible_states_names = next_possible_states_json['Task Name']
                # if the action type is code, take the user's message and apply it to the code
                print(action_type, next_task_ids)

                # if the action if FAQ, then take the user's message and query elasticsearch with it
                if action_type == 'FAQ':
                    print("Action type = FAQ caught")
                    response = self.faq_execute(message, bot_id, self.session['ASRFlag'], es_analyzer)

                    # self.clear_current_state()
                    # update_context(self.session, self.session_id)
                    # print("PRESENT CURRENT STATE :", get_by_id("status", self.session_id)["current_state"])
                    return response

                elif action_type == 'navigate_start':
                    print("Inside action_type = navigate_start ")
                    self.clear_current_state()
                    print("region in navigate_start: ",self.session['group'])
                    current_state = get_by_id('status', self.session_id)['current_state']
                    state_definition = self.get_task_definition('Task Name', current_state, bot_id)
                    print("Current state definition: Inside navigate_start: ", state_definition)
                    response = self.execute_restart(state_definition, bot_id)
                    print("Response inside action type = navigate_start: ", response)
                    return response

                elif action_type == 'App_Calc':
                    response = self.ap_execute(message, self.session_id, bot_id, self.session['ASRFlag'])
                    return response
                elif action_type == 'email_next_task':
                    response = self.email_next_task(message, self.session_id, bot_id, self.session['ASRFlag'])
                    return response
                elif action_type == 'date_next_task':
                    response = self.date_next_task(message, next_possible_states_names)
                    return response
                elif action_type == 'time_next_task':
                    response = self.time_next_task(message, next_possible_states_names)
                    return response
                elif action_type == 'height_next_task':
                    response = self.height_next_task(message, next_possible_states_names)
                    return response
                elif action_type == 'weight_next_task':
                    response = self.weight_next_task(message, next_possible_states_names)
                    return response
                elif action_type == 'dosage_cycle_next_task':
                    response = self.dosage_cycle_next_task(message, next_possible_states_names)

                    self.clear_current_state()
                    update_context(self.session, self.session_id)
                    print("PRESENT CURRENT STATE :", get_by_id("status", self.session_id)["current_state"])
                    return response

                elif action_type == 'rssAction':
                    task_text = state_definition['Task Text']
                    print(task_text)
                    response = self.rss_news_execute(message, task_text)
                    print(response)
                    return response

                # if the action is query, query the user's message against a database using DAO.py
                elif action_type == 'query': #todo
                    self.queryExecute(message, current_state)
                # if the action type is navigate, we are navigating from one branch to another in the conversation
                elif action_type == 'navigate':
                    print("Navigating as action_type is NAVIGATE")
                    print(message, next_possible_states_names,current_state)
                    response = self.navigate_execute(message, next_possible_states_names, bot_id)
                    print("response of navigate_execute: ", response)
                    return response

                # if the action type is rss_navigate, we are navigating to a rss branch
                elif action_type == 'rss_navigate':
                    response = self.rss_navigate(message, next_possible_states_names, bot_id)
                    return response

                elif action_type == 'input':
                    response = self.input_execute(message,next_possible_states_names)
                    return response

        except Exception as e:

            # logger.error(str(e)) #logger.info(string)
            logger.exception(e)
            return 'Sorry, we weren''t able to understand your intent - RESUME Error'



    #####################################################################################################################
    #####################################################################################################################
    """
    Execute Functions
    """
    #####################################################################################################################
    #####################################################################################################################


    def text_execute(self, task_definition, task_text):
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            response_task_text = self.build_response('string',task_text,'text','')
            return response_task_text

    def button_execute(self, task_definition, task_text):  # type, static_text, interaction, interaction_elements
        currentState = task_definition['Task Name']
        print("Inside button_execute ==> Current State ", currentState)
        task_text = re.sub(r'<>', currentState, task_text)
        print("Task Text: ", task_text)

        if task_definition['Interaction Fetch From DB'] != "True":
            interaction_values = task_definition['Interaction Values']
            print("Interaction Values: ", interaction_values)
            task_text = self.build_response('list', task_text, 'button_vertical', interaction_values)
            print("Task text in button_execute that will be returned: ", task_text)
            return task_text

    # if interaction is none, check if query database or not
    # just show text and then restart
    def none_execute(self, task_definition, task_text): #, dbData):
        # global results
        static_values = task_definition['Interaction Values']
        if task_definition['Interaction Fetch From DB'].lower() != "true":  # print from flashmessage if interaction = none
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)
            print("Message in None execute: ", message)
            response = self.build_response("string",message,"text","")
            response_json_2 = self.restart(response)
            print("Response json 2 inside none execute: final response ", response_json_2)
            return response_json_2
            # return response

    # if interaction is url, check if query database or not
    # then display url and restart
    def url_execute(self, task_definition, task_text):  # , dbData
        interaction_url = task_definition['Interaction Values'][0]
        if task_definition['Interaction Fetch From DB'].lower() != "true":
            currentState = task_definition['Task Name']
            message = re.sub(r'<>', currentState, task_text)
            response = self.build_response("hyperlink", gettext("Click Here"), "url", interaction_url)
            print("Response of build response in url execute: ", response)
            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('Task Name', current_state, bot_id)
            print("After 1st response, state definition = ", state_definition)
            response_json_2 = self.restart(response)
            # next_state = state_definition['Next Task IDs'][0]
            # next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
            # print("next state definition: ", next_state_def)
            # response_json_2 = self.execute(next_state_def)
            print("Response json 2 in url execute: ", response_json_2)

            # response['is_multi'] = True
            # response['message'].append(response_json_2['message'][0])
            # print("Final response after appending that will be returned: ", response)
            return response_json_2


    #####################################################################################################################
    #####################################################################################################################
    """
    Resume Functions
    """
    #####################################################################################################################
    #####################################################################################################################

    def small_talk_fuzzy(self, msg, search_list):
        for item in search_list.keys():
            if fuzz.ratio(msg, item) > 65:
                return item
        return 0

    def small_talk_module(self,msg):
        msg = re.sub(r'[^\w\s]','',msg)
        messages_mappings_dictionary = {}
        print("!!msg in small_talk_module: ",msg)
        messages_collection = get('messages', 'type', 'Small Talk')
        print("Messages collection: ", messages_collection)
        messages_mappings = messages_collection['mappings']

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside small_talk_module function", messages_mappings_dictionary)
        if msg.lower().strip() in messages_mappings_dictionary.keys():  # or self.small_talk_fuzzy(msg,messages_collection) == 1:
            response = self.build_response("string", messages_mappings_dictionary[msg.lower().strip()], "text", "")
            print("Response from small talk ", response)
            return response
        elif self.small_talk_fuzzy(msg, messages_mappings_dictionary):
            key = self.small_talk_fuzzy(msg, messages_mappings_dictionary)
            response = self.build_response("string", messages_mappings_dictionary[key], "text", "")
            return response
        else:
            return 0
            # return 0

    def keyword_handler(self, message, bot_id):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside keyword handler", messages_mappings_dictionary)


        help_config = app_config.get('GENERAL', 'help_keywords')
        restart_config = app_config.get('GENERAL', 'restart_keywords')
        goback_config = app_config.get('GENERAL', 'goback_keywords')

        help_keywords = help_config
        restart_keywords = restart_config
        goback_keywords = goback_config

        if message.lower().strip() in help_keywords:
            help_task = self.get_task_definition("Task Name","help",bot_id)
            help_text = help_task['Task Text']

            response = self.build_response("string", help_text, "text", "")


            next_state = help_task['Next Task IDs']

            if next_state == '' or next_state == [] or next_state == [""]:
                return response

            next_state_def = self.get_task_definition('Task ID', next_state[0], bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

        elif message.lower().strip() in restart_keywords:
            restart = self.get_task_definition("Is Start","true",bot_id)
            self.session['custStatus'] = 'Done'
            self.session['productInformationHandler'] = 'Done'
            self.session['priceStatus'] = 'Done'
            self.session['prodStatus'] = 'Done'
            self.session['restartFlagFAQ'] = False
            update_context(self.session_id, self.session)
            return self.execute(restart)
        elif message.lower().strip() in goback_keywords:
            current_state = get_by_id('status', self.session_id)['current_state']
            prior_state = get_by_id('status', self.session_id)['prior_state']
            history = get_by_id('status', self.session_id)['history']
            history = history[:-1] #pop out the last element
            if (len(history) == 0):
                return self.build_response("string", messages_mappings_dictionary['Sorry, there is nothing to go back to'] , "text", "")
            previous_state = history[-1] #get the last element
            history = history[:-1] #pop out again because will be replaced in conversation logger
            update('status', self.session_id, {'current_state': previous_state, 'prior_state': current_state, 'history': history,"date_created": datetime.datetime.now()})
            previous_state_json = self.get_task_definition('Task Name', previous_state, bot_id)
            return self.execute(previous_state_json)
        else:
            return 0

    def survey_execute(self, message):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside survey_execute", messages_mappings_dictionary)


        if self.session['surveyFlagNum'] is True:
            if message in '12345':
                # log survey
                self.session['endSurveyFlag'] = True
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.survey_create(2, '')
                return response
            else:
                response = self.build_response("string",messages_mappings_dictionary['Please choose an option 1-5.'],"text","")
                response_json_2 = self.survey_create(1,response)
                return response_json_2
        elif self.session['surveyFlagComm'] is True:
            if message.strip().lower() == messages_mappings_dictionary['yes']:
                response = self.build_response("string",messages_mappings_dictionary['Please leave your comment below:'],"text","")
                return response
            elif message.strip().lower() == messages_mappings_dictionary['no']:
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.build_response("string",messages_mappings_dictionary['Thank you for your participation!'],"text","")
                return response

            else:
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                update_context(self.session_id,self.session)
                response = self.build_response("string",messages_mappings_dictionary['Thank you for your participation!'],"text","")
                return response

    def survey_create(self,state,json):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside survey create", messages_mappings_dictionary)

        self.session['endSurveyFlag'] = True
        update_context(self.session_id, self.session)
        if state ==1:
            self.session['surveyFlagNum'] = True
            update_context(self.session_id, self.session)
            options = ['1','2','3','4', 'Main Menu']
            response = self.build_response("list",messages_mappings_dictionary['On a scale of 1-4, how satisfied were you with our service today?'],"button_horizontal",options,True,json)
            return response
        elif state == 2:
            self.session['surveyFlagComm'] = True
            update_context(self.session_id,self.session)
            response = self.build_response("list",messages_mappings_dictionary['Would you like to leave any comments?'],"button_horizontal",[messages_mappings_dictionary['Yes'], messages_mappings_dictionary['No']])
            return response

    # use Fuzzywuzzy to check for fuzzysearch options for Navigate options
    # if the match is > 45% similar, return as option for user to pick from
    def fuzzy_search(self, nextStates, message):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        #print("Messages mapping dictionary: Inside fuzzy_search", messages_mappings_dictionary)

        options = []
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]
        print("Maximum ratio" , ratio)
        logger.info("Fuzzy logic Ratio " + str(ratio))
        if ratio > int(os.environ.get('FUZZY_RATIO') or '45'):
            print("ratio found, returning resposna to user")
            logger.info("ratio found, returning response to user")
            # check if task is a navigate task, if yes, check message value, see if pass
            response = self.build_response("list",messages_mappings_dictionary["We couldn't find that. Do you mean:"] + suggestionOutput,"button_vertical",[messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"]])
            self.session['fuzzyFlag'] = True
            update_context(self.session_id, self.session)
            return response
        else:
            print("resposna ot found, returning regret response to user")
            logger.info("respone not found, returning response touser")
            response = self.build_response("string",messages_mappings_dictionary["Sorry we don't have an answer for that. Could you try again?"],"text","")
            return response

    # If FuzzyFlag is True, then execute this
    # check if the fuzzysearch came from a query or code
    # if not, then check if the suggestion picked comes from a next state
    # if not what they are looking for, return to start
    def fuzzy_execute(self, message, bot_id):
        print("Inside the fuzzy execure---------- ")
        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        #print("Messages mapping dictionary: Inside fuzzy_execute", messages_mappings_dictionary)

        if str(message).lower() == messages_mappings_dictionary['yes']:
            #self.session['fuzzyFlag'] = False
            get_next_state = self.get_task_definition('Task Name',self.session['suggestion'],bot_id)
            response = self.execute(get_next_state)
            self.session['fuzzyFlag'] = False
            update_context(self.session_id,self.session)
            return response
            pass
        elif message.lower() == messages_mappings_dictionary['no']:
            self.session['fuzzyFlag'] = False
            update_context(self.session_id,self.session)
            next_state = self.get_task_definition('Is Start','true',bot_id) #self.get_task_definition("Is Start","true", bot_id)
            response = self.execute(next_state)
            return response
        else:
            response = self.build_response("list",messages_mappings_dictionary['Please choose yes or no.'],"button_horizontal",[messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"]])
            return response

    def input_execute(self, message, next_possible_state_names):
        if self.session['name'] == 'None': #todo store these in conversation logger
            self.session['name'] = message
        elif 'contact' not in self.session.keys():
            self.session['contact'] = message
        elif 'profession' not in self.session.keys():
            self.session['profession'] = message
        else:
            pass #add something here later if all fields are input
        update_context(self.session_id, self.session)
        get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
        response = self.execute(get_next_state)
        return response


    def navigate_execute(self, message, next_possible_states_names, bot_id):
        print("Inside Navigate execute \n")
        found = False
        # messages_collection = get('messages', 'type', 'General')
        print("Next possible state names: ", next_possible_states_names)
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                print("Message received and state to be compared: ", message, stateCompare)
                get_next_state = self.get_task_definition('Task Name', state, bot_id)
                print('NAVIGATE NEXT STATE - ', get_next_state)
                print("--------------------------------------------------------")
                task_text = get_next_state['Task Text']
                task_name = get_next_state['Task Name']
                task_id = get_next_state['Task ID']
                print("Task text to be sent as response", task_text)
                print("Task ID: ", task_id)
                print("Task name: ", task_name)
                if task_name in ["Yes", "yes","Yes<51>", "yes<51>"]:
                    self.session['dosage_yes_counter'] = self.session['dosage_yes_counter'] + 1
                    update_context(self.session_id, self.session)
                    print("inside if task name yes")
                    print("Yes counter value: ", self.session['dosage_yes_counter'])
                    print("No counter value: ", self.session['dosage_no_counter'])
                if task_name in ["no","No","No<51>","no<51>"]:
                    self.session['dosage_no_counter'] = self.session['dosage_no_counter'] + 1
                    update_context(self.session_id, self.session)
                    print("inside if task name no")
                    print("No counter value: ", self.session['dosage_no_counter'])
                    print("Yes counter value: ", self.session['dosage_yes_counter'])

                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    try:

                        if '{}' in task_text:
                            print("Format {} is present in next state task text")
                            response = self.execute(get_next_state)
                            print('response after running self.execute : ', response)
                            print("The message in this block is : ", message)



                            dosage = self.dosage_calc()
                            print("Dosage list returned", dosage)
                            dosage_values = dosage
                            try:
                                # Modifications made from here for {} formatting

                                if self.session['dosage_yes_counter'] == 1 and self.session['dosage_no_counter'] == 0:
                                    dosage_values = dosage[:3]
                                if self.session['dosage_yes_counter'] == 1 and self.session['dosage_no_counter'] == 1:
                                    dosage_values = dosage[3:6]
                                if self.session['dosage_yes_counter'] == 0 and self.session['dosage_no_counter'] == 2:
                                    dosage_values = dosage[6:9]



                            except Exception as e:
                                logger.exception(e)
                                print("Exception in dosage calculation :", e)



                            task_text_response = str(response['message'][0]['text'])
                            print("Task text response: ", task_text_response)
                            response['message'][0]['text'] = task_text_response.format(self.session['height'], self.session['weight'], *dosage_values)
                            print("Response to be sent ", response)

                            self.conv_log.update_last_conversation(response['message'][0]['text'])

                            print("Before resetting yes_no counters for dosage are: \n")
                            print("Yes counter value: ", self.session['dosage_yes_counter'])
                            print("No counter value: ", self.session['dosage_no_counter'])
                            self.session['dosage_yes_counter'] = 0
                            self.session['dosage_no_counter'] = 0
                            update_context(self.session_id, self.session)

                            return response

                        elif '{Adverse_Event}' in task_text:
                            print("Format {Adverse_Event} is present in next state task text")
                            response = self.execute(get_next_state)
                            print('response after running self.execute : ', response)
                            task_text_response = str(response['message'][0]['text'])
                            country = self.session['group']
                            print("Country is :", country)
                            country_dict = {}

                            filename = os.path.dirname(os.path.dirname(__file__))
                            filename = os.path.join(filename, "mapping")
                            filename = os.path.join(filename, "Adverse_Event.csv")
                            print("File =", filename)
                            # reader = csv.reader(open(filename))
                            # for row in reader:
                            #     if reader.line_num == 1:
                            #         headers = row[1:]
                            #     else:
                            #         country_dict[row[0]] = dict(zip(headers, row[1:]))

                            df = pd.read_csv(filename)
                            print("DF: ", df.head())

                            # result = df.to_dict(orient='records')
                            result = dict(zip(df['Country'], df['Message']))
                            print("Result: ", result)

                            task_text = result[country]

                            '''
                            phone = email = ""
                            for key, value in country_dict.items():
                                print(key,value)
                                print(value['Phone'])
                                print(value['Email'])
                                if key.lower() == country.lower():
                                    print("found a match")
                                    phone = value['Phone']
                                    email = value['Email']
                            print("Phone and email", phone, email)
                            '''

                            response['message'][0]['text'] = task_text_response.format(Adverse_Event=task_text)
                            # print("Response to be sent ", response)


                            print("Final task text response to be sent:", response)

                            self.conv_log.update_last_conversation(response['message'][0]['text'])


                            return response

                        else:
                            print("No formatting {} in task text")
                            print("Yes-No counter here: ")
                            print("Yes counter value: ", self.session['dosage_yes_counter'])
                            print("No counter value: ", self.session['dosage_no_counter'])
                            response = self.execute(get_next_state)
                            print("Response to be sent: ", response)

                            return response
                    except Exception as e:
                        logger.exception(e)

        if found == False:
            # do Fuzzy search here
            print("Inside found==False inside navigate execute")
            response = self.fuzzy_search(next_possible_states_names, message)
            print("Response of fuzzy search: ", response)
            return response
            pass

    def rss_navigate(self, message, next_possible_states_names, bot_id):
        found = False
        current_state = get_by_id('status', self.session_id)['current_state']
        state_definition = self.get_task_definition('Task Name', current_state, bot_id)

        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                get_next_state = self.get_task_definition('Task Name', state, bot_id)
                if message == 'ash':
                    response = self.execute(get_next_state)
                    return response
                if message == 'onclive':
                    task_text = state_definition['Task Text']
                    print(task_text)
                    response = self.rss_news_execute(message, task_text)
                    print(response)
                    return response


        if found == False:
            # do Fuzzy search here
            response = self.fuzzy_search(next_possible_states_names, message)
            return response
            pass




    def restart_execute(self, message, session_id, session, bot_id):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside restart_execute", messages_mappings_dictionary)

        if message.strip().lower() == messages_mappings_dictionary['yes']:
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            get_start = self.get_task_definition("Is Start","true", bot_id)
            start_response = self.execute(get_start)
            return start_response

        elif message.strip().lower() == messages_mappings_dictionary['no']:
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", messages_mappings_dictionary['Thanks for chatting, hope to talk again soon!'], "text", "", False)
            response_final = self.survey_create(1, response_first)
            # update('status', self.session_id, {'has_ended': True})
            return response_final
        elif message.strip().lower() == messages_mappings_dictionary['Main Menu'].strip().lower():
            print("Inside restart_execute option Main Menu")
            get_start = self.get_task_definition("Is Start", "true", bot_id)
            start_response = self.execute(get_start)
            return start_response
        else:
            print("no match in restart_execute")
            response = self.build_response("list", messages_mappings_dictionary['Would you like to continue? Please choose yes or no.'], "button_horizontal",[messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"]])
            return response

    def restart_execute_FAQ(self, message, session_id, session, bot_id):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside restart_execute_FAQ", messages_mappings_dictionary)

        if message.strip().lower() == messages_mappings_dictionary['yes']:
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session['noCounter_ES'] = 0
            session['suggestCounter_ES'] = 0
            session_id = update_context(session_id, session)
            task_definition = self.get_task_definition("Task Name", "Answer Questions", bot_id)
            start_response = self.execute(task_definition)
            return start_response
            # start_response = self.build_response("list",messages_mappings_dictionary["What would you like to do next?"],"button_horizontal", [messages_mappings_dictionary["Answer Questions"], messages_mappings_dictionary["Main Menu"]])
            # return start_response

        elif message.strip().lower() == messages_mappings_dictionary['no']:
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session['noCounter_ES'] = 0
            session['suggestCounter_ES'] = 0
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", messages_mappings_dictionary['Thanks for chatting, hope to talk again soon!'], "text", "",False)
            response_final = self.survey_create(1, response_first)
            # update('status', self.session_id, {'has_ended': True})
            return response_final

        elif message.strip().lower() == messages_mappings_dictionary['Main Menu'].strip().lower():
            print("Inside restart_execute_FAQ option Main Menu")
            self.clear_current_state()
            get_start = self.get_task_definition("Is Start", "true", bot_id)
            start_response = self.execute(get_start)
            session['restartFlagFAQ'] = False
            session['noCounter_ES'] = 0
            session['suggestCounter_ES'] = 0
            update_context(session_id, session)
            return start_response

        else:
            print("Inside else - will show continue intent")
            response = self.build_response("list", messages_mappings_dictionary['Would you like to continue? Please choose yes or no.'],
                                           "button_horizontal", [messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"]])
            return response

    def restart_next_task_execute(self, message, session_id, session, bot_id):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside restart next task execute ", messages_mappings_dictionary)
        try:

            if message.strip().lower() == messages_mappings_dictionary['yes']:
                session['restartFlagNextTask'] = False
                session['queryFlag'] = False
                session_id = update_context(session_id, session)
                current_state = get_by_id('status', self.session_id)['current_state']

                state_definition = self.get_task_definition('Task Name', current_state, bot_id)

                next_state = state_definition['Next Task IDs'][0]
                next_state_def = self.get_task_definition('Task ID', next_state, bot_id)
                start_response = self.execute(next_state_def)
                return start_response

            elif message.strip().lower() == messages_mappings_dictionary['no']:
                session['restartFlagNextTask'] = False
                session['queryFlag'] = False
                session_id = update_context(session_id, session)
                response_first = self.build_response("string", messages_mappings_dictionary['Thanks for using the Pfizer Bot!'], "text", "",
                                                     False)
                response_final = self.survey_create(1, response_first)
                # update('status', self.session_id, {'has_ended': True})
                return response_final
            else:
                response = self.build_response("list", messages_mappings_dictionary['Would you like to continue? Please choose yes or no.'],
                                               "button_horizontal", [messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"]])
                return response

        except Exception as e:
            logger.exception(e)

    # if action type is hybrid, execute this
    # first check if the message is a 1:1 match with taskNames
    # if it is, execute the corresponding state
    # if it is not, check message against keywords. For each keyword matching in the message, add a point
    # return taskNames with highest keyword scores for using to pick from



    def date_next_task(self, message, next_possible_state_names):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside date next task ", messages_mappings_dictionary)
        try:
            date = datetime.datetime.strptime(message, "%d/%m/%Y")
        except ValueError:
            response = self.build_response("string", messages_mappings_dictionary["Enter a valid date in dd/mm/yyyy format"],"text","")
            return response

        self.session['date_app'] = message.strip().lower()
        #self.conv_log.date_app(message)
        update_context(self.session_id, self.session)
        get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
        print('get_next_state', get_next_state)
        response = self.execute(get_next_state)
        print('response', response)
        return response


    def time_next_task(self, message, next_possible_state_names):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside time next task ", messages_mappings_dictionary)

        try:
            time = datetime.datetime.strptime(message, "%H:%M")
        except ValueError:
            response = self.build_response("string", messages_mappings_dictionary["Enter a valid time in hh:mm format"], "text", "")
            return response

        self.session['time_app'] = message.strip().lower()
        #self.conv_log.time_app(message)
        update_context(self.session_id, self.session)
        get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
        print('get_next_state', get_next_state)
        response = self.execute(get_next_state)
        print('response', response)
        return response

    def email_next_task(self, message, session_id, bot_id, asr_flag):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside email next task ", messages_mappings_dictionary)

        is_valid_email = validate_email(message)
        if is_valid_email is True:
            self.session['email_app'] = message.strip().lower()
            update_context(self.session_id, self.session)
            self.session['no_sec_app'] = True
            response_json_2 = self.ap_email(self.session['email_app'])
            print('response json 2 is ', response_json_2)
            state = self.get_task_definition('Is Start', 'true', bot_id)
            return response_json_2
        else:
            response = self.build_response("string", messages_mappings_dictionary["Please enter a valid email"],"text","")
            return response


    def ap_email(self, message):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside ap_email ", messages_mappings_dictionary)

        print('inside ap_email')
        is_valid_email = validate_email(message)
        if is_valid_email is True:
            print('input is ', message)
            json = self.build_response("list", messages_mappings_dictionary['Your request has been sent to concerned team. They will reach out to you shortly.'],"text",{})

            if self.session['no_sec_app']:
                print('inside No Security email function')
                #generateemail(self.session_id, self.session, json)

            # elif self.session['escalation']==True:
            #     print('inside esclataion email function')
            elif self.session['security']:
                print('inside security email function')
                #generateemail(self.session_id, self.session ,json)

            response = self.restart(json)
            update_context(self.session_id, self.session)

            return response
        else:
            response = self.build_response("string", messages_mappings_dictionary["Please enter a valid email"], "text", "")
            return response

    def ap_execute(self, message, session_id, bot_id, asr_flag):
        try:
            print('inside ap_execute')
            self.session['email_app'] = message.strip().lower()
            self.session['security'] = True
            response_json_2 = self.ap_email(self.session['email_app'])
            print('response json 2 is ', response_json_2)
            #self.restart_next_task_execute(self, message, session_id, self.session, bot_id)
            return response_json_2
        except Exception as e:
            logger.exception(e)

    def height_next_task(self, message, next_possible_state_names):
        print("Inside height next task")
        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside height next task ", messages_mappings_dictionary)

        try:
            if message.isnumeric():
                self.session['height'] = message.strip().lower()
                update_context(self.session_id, self.session)
                get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
                print('get_next_state', get_next_state)
                response = self.execute(get_next_state)
                print('response', response)

                # # Modifications made from here for {} formatting
                # task_text = get_next_state["Task Text"]
                # task_text = task_text.format(self.session['height'])
                # final_response = self.build_response("string",task_text, "text", "")
                # print("Final Response: ", final_response)
                # return final_response
                task_text_response = str(response['message'][0]['text'])
                response['message'][0]['text'] = task_text_response.format(self.session['height'])
                print("Response to be sent ", response)

                # Call custom update function in conversation logger to update last index
                # array last index of conversation

                self.conv_log.update_last_conversation(response['message'][0]['text'])

                return response
            else:
                response = self.build_response("string",messages_mappings_dictionary["Please enter a valid height(in cms)"], "text", "")
                return response

        except Exception as e:
            logger.exception(e)


    def weight_next_task(self, message, next_possible_state_names):
        print("Inside weight next task")
        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside weight next task ", messages_mappings_dictionary)
        try:

            if message.isnumeric():
                self.session['weight'] = message.strip().lower()
                update_context(self.session_id, self.session)
                get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
                print('get_next_state', get_next_state)
                response = self.execute(get_next_state)
                print('response', response)
                return response
            else:
                response = self.build_response("string", messages_mappings_dictionary["Please enter a valid weight(in kgs)"], "text", "")
                return response

        except Exception as e:
            logger.exception(e)

    '''
    VERSION 2 - WEIGHT NEXT TASK TO DISPLAY ALL CYCLES AT ONE RESPONSE
    
    def weight_next_task(self, message, next_possible_state_names):

        messages_collection = get('messages', 'type', 'General')
        if message.isnumeric():
            self.session['weight'] = message.strip().lower()
            update_context(self.session_id, self.session)
            get_next_state = self.get_task_definition('Task Name', next_possible_state_names[0], self.session['bot_id'])
            print('get_next_state', get_next_state)

            dosage = self.dosage_calc()
            print("Dosage list returned", dosage)
            task_text = get_next_state['Task Text']
            print("Task text to be sent as response", task_text)
            response = task_text.format(*dosage)
            print("Response to be sent ",  response)
            if response:
                return_response = self.build_response("string", response, "text", "")
            else:
                return_response = self.build_response("string", messages_collection['mappings']['Response not found'], "text", "")

            print('response', return_response)
            response1 = self.restart(return_response)
            update_context(self.session_id, self.session)
            return response1


        else:
            response = self.build_response("string", messages_collection['mappings']["Please enter a valid weight(in kgs)"], "text", "")
            return response

    '''

    # def dosage_cycle_next_task(self, message, next_possible_states_names):
    #     found = False
    #     print(next_possible_states_names)
    #     for state in next_possible_states_names:
    #         stateCompare = re.sub(r'<.*>', '', state)
    #         if message.strip().lower() == stateCompare.strip().lower():
    #             get_next_state = self.get_task_definition('Task Name', state, self.session['bot_id'])
    #             print('NAVIGATE NEXT STATE FROM DOSAGE CYCLE NEXT TASK - ', get_next_state)
    #             if 'None' in str(get_next_state):
    #                 pass
    #             else:
    #                 found = True
    #
    #                 response = self.execute(get_next_state)
    #                 print("response inside dosage_cycle_next_task ", response)
    #                 return response
    #
    #     if found == False:
    #         # do Fuzzy search here
    #         response = self.fuzzy_search(next_possible_states_names, message)
    #         print("Response inside if found==False: ", response)
    #         return response


    def dosage_calc(self):
        print('inside dosage_calc')

        try:

            height = int(self.session['height'])
            print('height is:', height)
            weight = int(self.session['weight'])
            print('weight is:', weight)
            # dosage_cycle = int(self.session['dosage_cycle'])
            # global dosage_cyclenew
            dosage_cyclenew = app_config.get('GENERAL', 'dosage_cyclenew')
            print("Raw from config file", dosage_cyclenew)
            list_dosage_cyclenew = list(dosage_cyclenew.split(","))
            list_dosage_cyclenew = [float(i) for i in list_dosage_cyclenew]
            print("Global cycle new and its type", list_dosage_cyclenew, " , ", type(list_dosage_cyclenew))
            body_surface_area = math.sqrt((height * weight) / 3600)
            print('body surface area =', body_surface_area)
            dosagenew = []
            print("list", list_dosage_cyclenew)
            # list_dosage_cyclenew = [int(i) for i in list_dosage_cyclenew]
            # print("After converting to int:", list_dosage_cyclenew)
            for day in list_dosage_cyclenew:
                print("day and type before after ", day, type(day), type(int(day)))
                newdose=(body_surface_area*day)
                dosagenew.append("{0:.2f}".format(newdose))
            print("DOSAGE : ", dosagenew)

            return dosagenew

        except Exception as e:
            logger.exception(e)



    def faq_execute(self, message,bot_id, asr_flag, es_analyzer):
        print("In FAQ execute")
        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}

        # Converting the list of Small Talk messages to a single dictionary
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: Inside faq execute", messages_mappings_dictionary)

        try:
            print("The message is : ", message)
            if message.strip().lower() == messages_mappings_dictionary["Answer Questions"].strip().lower():
                '''
                # Version 4 
                
                response = self.build_response("string",messages_mappings_dictionary["What would you like to know about?"],"text","")
                update_context(self.session_id, self.session)
                return response
                '''
                rerun_faq_state = self.get_task_definition("Task Name", "Answer Questions" or "answer questions",bot_id)
                print("Rerun faq state: ", rerun_faq_state)
                response = self.execute(rerun_faq_state)
                update_context(self.session_id, self.session)
                return response



            if message.strip().lower() == messages_mappings_dictionary["Main Menu"].strip().lower():
                self.clear_current_state()
                current_state = get_by_id('status', self.session_id)['current_state']
                state_definition = self.get_task_definition('Task Name', current_state, bot_id)
                print("Current state definition: Inside faq_execute after selecting Main Menu as option ", state_definition)
                response = self.execute(state_definition)
                print("Response inside message == Main Menu: ", response)
                return response

        except Exception as e:
            logger.exception(e)


        search = SearchES(self.session_id, bot_id)
        print("Search Inside FAQ Execute: ")
        self.session['ES_UsedFlag'] = True
        update_context(self.session_id, self.session)
        response_flag, response = search.execute(message, bot_id, asr_flag,es_analyzer)
        print("response flag, response", response_flag,response)
        if response_flag == 0:
            self.session['ASRFlag'] = False
            response_json_2 = self.restart_FAQ(response)
            print("When response flag=0 : response_json_2 after calling restart_FAQ --> ", response_json_2)
            update_context(self.session_id, self.session)
            return response_json_2
        # self.restart()
        elif response_flag == 1:
            #display the ES answer
            logger.debug('RESPONSE TO RETURN FROM FAQ EXECUTE - ', response)
            return response
        elif response_flag == 2:
            state = self.get_task_definition('Is Start', 'true', bot_id)
            response = self.execute(state)
            return response
        elif response_flag == 3:
            self.session['ASRFlag'] = False
            response_json_2 = self.restart_FAQ(response)
            print("When response flag=3 : response_json_2 after calling restart_FAQ --> ", response_json_2)
            update_context(self.session_id, self.session)
            return response_json_2

    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart(self, json):
        print("json received in restart function: ", json)
        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message'] : each_item['translation']})
        print("Messages mapping dictionary: ", messages_mappings_dictionary)

        try:
            response = self.build_response("list", messages_mappings_dictionary["Would you like to continue?"],"button_horizontal",[messages_mappings_dictionary["Yes"],messages_mappings_dictionary["No"]],True, json)
            self.session['restartFlag'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Error: ")
            logger.exception(e)

    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart_FAQ(self, json):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: ", messages_mappings_dictionary)

        try:
            response = self.build_response("list", messages_mappings_dictionary["Would you like to continue asking questions?"], "button_horizontal",
                                           [messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"], messages_mappings_dictionary["Main Menu"]], True, json)
            self.session['restartFlagFAQ'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart FAQ Error: ")
            logger.exception(e)

    def restart_next_task(self, json):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: ", messages_mappings_dictionary)
        try:
            response = self.build_response("list", messages_mappings_dictionary["Would you like to continue?"], "button_horizontal",
                                           [messages_mappings_dictionary["Yes"], messages_mappings_dictionary["No"]], True, json)
            self.session['restartFlagNextTask'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Next Task Error: ")
            logger.exception(e)

    def rss_news_execute(self, message, task_text):

        messages_collection = get('messages', 'type', 'General')
        messages_mappings = messages_collection['mappings']
        messages_mappings_dictionary = {}
        for each_item in messages_mappings:
            messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
        print("Messages mapping dictionary: ", messages_mappings_dictionary)

        print("Entered rss_news_execute")
        message = message.lower()
        # self.conv_log.update_user_msg('status', {
        #     'questionCategory': 'RSS'
        # })

        publications = get_publications()

        def matchWholeWord(w):
            return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

        word = ""

        for key in publications:
            print("key:messsage", key, message)
            if(bool(matchWholeWord(key)(message))):
                word = key
                break
        print("Message:", message)
        print("Word:", word)
        if word:
            news = rss_news(word)
            response = "Here are the links of top 5 news feeds: " + "\n"
            for key in news:
                print(key)
                if key['summary'] != '' or key['summary'] is not None:
                    # response += key['title'] + "\n" + key['summary'] + "\n" + key['link'] + "\n"
                    response += key['title'] + "\n" + key['link'] + "\n"
            return_response = self.build_response('string', response, 'text', '')
            print("Return response: ", return_response)

        else:
            return_response = self.build_response('string', messages_mappings_dictionary['Try again'], 'text', '')

        return return_response

    def clear_current_state(self):
        current_state = get_by_id('status', self.session_id)['current_state']
        update('status', self.session_id, {'current_state': "Start", 'prior_state': current_state})

