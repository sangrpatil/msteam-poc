from utility.de_coupled_report import get_ConvoLog,  get_intent_found, get_intent_NotFound, get_dashboard
import xlsxwriter
import json
from datetime import datetime
from utility.logger import logger


def generateReportsWeekly(bot_id=None, start_date=None, end_date=None): #todo pass bot ID
    try:
        # process and pass start date and end date here
        if start_date and end_date is None:
            start_date = ""
            end_date = ""
        data = get_ConvoLog(start_date,end_date, bot_id, offset=0, next=32767)
        intentFoundData = get_intent_found(start_date, end_date, bot_id, offset=0, next=32767)
        intentNotFoundData = get_intent_NotFound(start_date, end_date, bot_id, offset=0, next=32767)
        dashboardData = get_dashboard(bot_id, ["Hong Kong", "Singapore", "Taiwan", "India"])
        print("\n\n")

        fileTitle = 'static/reports/' + str(bot_id) +  '.xlsx'
        workbook = xlsxwriter.Workbook(filename=fileTitle)
        dashboard = workbook.add_worksheet('Dashboard')
        intentFound = workbook.add_worksheet('Intents Found')
        intentNotFound = workbook.add_worksheet('Intents Not Found')
        convoLog = workbook.add_worksheet('ConversationLog')
        dataSheet = workbook.add_worksheet('DataSheet')


        # master formatting style
        formatting = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': 'white',
            'fg_color': '#007DC6'})

        # write column headers for conversation data log
        headers = ['ID', 'FROM', 'MESSAGE', 'Adverse Event', 'ASR/TTS USAGE', 'RATING', 'COMMENTS', 'SURVEY RESPONSES',
                   'DATE', 'TIME (UTC)']


        print("Going here")
        convoLog.write_row('A1', headers, cell_format=formatting)
        convoLog.set_column(0, 8, 20)
        convoLog.autofilter('A1:I1')



        for row_num, row in enumerate(data[0]):
            print(row)
            data_row = [ row[i] for i in headers ]
            convoLog.write_row(row_num + 1,0 , data_row)

        headersIntent = ['USER QUESTION', 'QUESTION ID', 'DATE', 'TIME (UTC)']

        print("Going here")
        intentFound.write_row('A1', headersIntent, cell_format=formatting)
        intentFound.set_column(0, 7, 20)
        intentFound.autofilter('A1:D1')



        for row_num, row in enumerate(intentFoundData[0]):
            print(row)
            data_row = [ row[i] for i in headersIntent ]
            intentFound.write_row(row_num + 1,0 , data_row)


        print("Going here")
        intentNotFound.write_row('A1', headersIntent, cell_format=formatting)
        intentNotFound.set_column(0, 7, 20)
        intentNotFound.autofilter('A1:D1')



        for row_num, row in enumerate(intentNotFoundData[0]):
            print(row)
            data_row = [ row[i] for i in headersIntent ]
            intentNotFound.write_row(row_num + 1,0 , data_row)


        print(json.dumps(dashboardData, indent=4))

        barGraph_ConvByDay = workbook.add_chart({'type': 'column'})
        lineGraph_ConvByHour = workbook.add_chart({'type': 'line'})
        lineGraph_RatingByDay = workbook.add_chart({'type': 'line'})
        lineGraph_LatencyByHour = workbook.add_chart({'type': 'line'})
        barGraph_RatingCount = workbook.add_chart({'type': 'column'})

        dateKeyList = []
        dateValuesList = []
        timeKeyList = []
        timeValueList = []
        ratingKeyList = []
        ratingValueList = []
        latencyKeyList = []
        latencyValueList = []
        ratingCountKeyList = []
        ratingCountValueList = []


        graphs_dict = {}

        for i in dashboardData['graph_data']:
            graphs_dict[i["graph_name"]] = i

        # ratingCountKeyList =
        # for key, value in sorted():
        #     ratingCountKeyList.append(key)
        #     ratingCountValueList.append(value)

        dataSheet.write_row(0, 0, graphs_dict['Number of Questions By Day']['x_values'])
        dataSheet.write_row(1, 0, graphs_dict['Number of Questions By Day']['y_values'])
        dataSheet.write_row(2, 0, graphs_dict['Peak Engagement Hours']['x_values'])
        dataSheet.write_row(3, 0, graphs_dict['Peak Engagement Hours']['y_values'])
        dataSheet.write_row(4, 0, graphs_dict['User Satisfaction']['x_values'])
        dataSheet.write_row(5, 0, graphs_dict['User Satisfaction']['y_values'])
        dataSheet.write_row(6, 0, graphs_dict['Bot Response Latency']['x_values'])
        dataSheet.write_row(7, 0, graphs_dict['Bot Response Latency']['y_values'])
        dataSheet.write_row(8, 0, graphs_dict['User Satisfaction Distribution']['x_values'])
        dataSheet.write_row(9, 0, graphs_dict['User Satisfaction Distribution']['y_values'])

        dateKeyList =graphs_dict['Number of Questions By Day']['x_values']
        dateValuesList = graphs_dict['Number of Questions By Day']['y_values']
        timeKeyList = graphs_dict['Peak Engagement Hours']['x_values']
        timeValueList = graphs_dict['Peak Engagement Hours']['y_values']
        ratingKeyList = graphs_dict['User Satisfaction']['x_values']
        ratingValueList =graphs_dict['User Satisfaction']['y_values']
        latencyKeyList = graphs_dict['Bot Response Latency']['x_values']
        latencyValueList = graphs_dict['Bot Response Latency']['y_values']
        ratingCountKeyList = graphs_dict['User Satisfaction Distribution']['x_values']
        ratingCountValueList = graphs_dict['User Satisfaction Distribution']['y_values']

        barGraph_ConvByDay.add_series({
            'name': 'Number of Conversations by Day',
            'categories': ['DataSheet', 0, 0, 0, len(dateKeyList) - 1 if len(dateKeyList) else len(dateKeyList)],
            'values': ['DataSheet', 1, 0, 1, len(dateKeyList) - 1 if len(dateKeyList) else len(dateKeyList)]
        })

        barGraph_ConvByDay.set_x_axis({'name': 'Days'})
        barGraph_ConvByDay.set_y_axis({'name': 'Number of Conversations'})
        barGraph_ConvByDay.set_legend({'none': True})

        lineGraph_ConvByHour.add_series({
            'name': 'Peak Engagement Hours',
            'categories': '=DataSheet!$A$3:$Y$3',
            'values': '=DataSheet!$A$4:$Y$4'
        })
        lineGraph_ConvByHour.set_x_axis({'name': 'Time of Day (UTC)'})
        lineGraph_ConvByHour.set_y_axis({'name': 'Number of Conversations'})
        lineGraph_ConvByHour.set_legend({'none': True})

        lineGraph_RatingByDay.add_series({
            'name': 'User Satisfaction',
            'categories': ['DataSheet', 4, 0, 4, len(ratingKeyList) - 1 if len(ratingKeyList) else len(ratingKeyList)],
            'values': ['DataSheet', 5, 0, 5, len(ratingValueList) - 1 if len(ratingValueList) else len(ratingValueList)]
        })

        lineGraph_RatingByDay.set_x_axis({'name': 'Days'})
        lineGraph_RatingByDay.set_y_axis({'name': 'Average Satisfaction Rating', 'min': 0, 'max': 5})
        lineGraph_RatingByDay.set_legend({'none': True})

        lineGraph_LatencyByHour.add_series({
            'name': 'Bot Response Latency',
            'categories': ['DataSheet', 6, 0, 6, len(latencyKeyList) - 1 if len(latencyKeyList) else len(latencyKeyList)],
            'values': ['DataSheet', 7, 0, 7, len(latencyValueList) - 1 if len(latencyValueList) else len(latencyValueList)]
        })

        lineGraph_LatencyByHour.set_x_axis({'name': 'Time of Day (UTC)'})
        lineGraph_LatencyByHour.set_y_axis({'name': 'Aversage Response Latency (sec)'})
        lineGraph_LatencyByHour.set_legend({'none': True})

        barGraph_RatingCount.add_series({
            'name': 'User Satisfaction Distribution',
            'categories': ['DataSheet', 8, 0, 8,
                           len(ratingCountKeyList) - 1 if len(ratingCountKeyList) else len(ratingCountKeyList)],
            'values': ['DataSheet', 9, 0, 9,
                       len(ratingCountValueList) - 1 if len(ratingCountValueList) else len(ratingCountValueList)]
        })

        barGraph_RatingCount.set_x_axis({'name': 'Rating Score'})
        barGraph_RatingCount.set_y_axis({'name': 'Number of Ratings'})
        barGraph_RatingCount.set_legend({'none': True})

        # insert the charts into the dashboard
        dashboard.insert_chart('B11', barGraph_ConvByDay)
        dashboard.insert_chart('K11', lineGraph_ConvByHour)
        dashboard.insert_chart('B27', lineGraph_RatingByDay)
        dashboard.insert_chart('K27', lineGraph_LatencyByHour)
        dashboard.insert_chart('B43', barGraph_RatingCount)

        # Hide the datasheet used to create the charts
        dataSheet.hide()




        not_helpful = len(intentNotFoundData[0])
        helpful = len(intentFoundData[0])
        no_of_conversations = max(data[0], key=lambda x: x['ID']).get("ID", 0)
        zipped_values = zip(graphs_dict['User Satisfaction Distribution']['x_values'], graphs_dict['User Satisfaction Distribution']['y_values'])
        #print(list(zipped_values))
        sum_rating = 0
        total_conv_count = 0
        for rating, no_of_conv in list(zipped_values)[:-1]:
            sum_rating += int(rating) * no_of_conv
            print(sum_rating)
            total_conv_count += no_of_conv
            print("Total cCount: , ", total_conv_count)


        print("Total conv with rating:" ,total_conv_count)


        avg_rate_score = sum_rating/total_conv_count if total_conv_count else 0


        print(no_of_conversations, helpful, not_helpful, avg_rate_score)

        title = 'Chatbot Dashboard: ' + datetime.datetime.today().strftime('%m-%d-%Y')
        formatting.set_font_size(20)
        dashboard.merge_range('A1:S2', title, formatting)
        formatting.set_font_size(11)

        # create summary data table
        dashboard.write(3, 1, 'Summary Data')
        dashboard.merge_range('B4:C4', 'Summary Data', formatting)
        dashboard.write_row(4, 1, ['Number of Conversations', no_of_conversations])


        dashboard.write_row(5, 1, ['Average Satisfaction Rating', avg_rate_score])
        dashboard.write_row(6, 1, ['Number of Questions Users Rated as Helpful', helpful])
        dashboard.write_row(7, 1, ['Number of Questions Rated as Unhelpful', not_helpful])
        dashboard.set_column(1, 1, 50)
        workbook.close()

    except Exception as e:
        print("Exception occured in generating reports")
        print(e)
        logger.exception(e)

# generateReportsWeekly("5d023d1ebe88ea5be8907554")







