from flask import Blueprint, request, jsonify, render_template, session, redirect, Markup

from utility.mongo_dao import insert, get_by_id, get_one, update, get_collection, search_one, get_all, get_all_cursor, get_all_non_str, get_all_ids, execute_query
from utility.bot_engine import validate_bot, store_bot, update_bot, read_bot, delete_bot
from utility.logger import logger
import ast
from utility.execution_engine import ExecutionEngine
from utility.session_manager import get as get_context, set as set_context, modify as update_context
from utility.decorators import requires_admin
from utility.conversation_logger import ConversationLogger
from bson import ObjectId
import time
import datetime
import bson
import json
from utility.excel_parser import convert
import copy
import os
from flask_babel import refresh, gettext
from flask import current_app as app
import configparser

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))

bot_blueprint = Blueprint('bot', __name__, template_folder='templates')



@bot_blueprint.route('/check')
def check():
    return jsonify(
        status='Running'
    )

@bot_blueprint.route('/api/login', methods = ['GET', 'POST'])
def login():
    if request.method=='GET':
        return render_template('login.html')

    elif request.method == 'POST':
        logger.debug("in logins")
        admin_username = os.environ.get('ADMIN_USERNAME') or 'admin'
        admin_password = os.environ.get('ADMIN_PASSWORD') or 'admin'
        logindata = request.json
        if logindata == None:
            username = request.form.get('username')
            password = request.form.get('password')
            group = request.form.get('area')
        else:
            username = logindata['username']
            password = logindata['password']
            group = logindata.get('area')
        logger.info("Username received = {}".format(username))

        now = datetime.datetime.now()
        dic = {"lastlogin": str(now.strftime("%Y-%m-%d %H:%M"))}
        user= get_one('user', 'username', username)
        print(not(user==None) and password==user.get('password'))
        print(user)
        if not(user==None) and password==user.get('password') :

            #print(flask_session,"---flask session----")
            session['loggedIn'] = True
            session['username'] = username
            session['group'] = group
            print(now.strftime("%Y-%m-%d %H:%M"),"***")
            id=get_one('user','username',username).get('_id')
            update('user',id,dic)
            print("session---", type(session), session)
            logger.info('Following User legged in successfully = {}'.format(username))
            return jsonify(
                status='Success',
                msg=str(id).replace('ObjectId(', '').replace(')', '')
            )

        else:
            logger.info('Login Failed for username = {}'.format(username))
            session['loggedIn'] = False
            #return render_template('login.html')
            return jsonify(
                status='Failure',
                msg='User login failed'
            )

@bot_blueprint.route('/api/logout', methods = ['GET'])
def logout():
    session['loggedIn'] = False
    return render_template('login.html')

@bot_blueprint.route('/api/bot/create',methods=['GET','POST'])
def bot_create():
    session['update_login_attempt'] = False
    if request.method == 'GET':
        try:
            if session['loggedIn'] == True:
                return render_template('create_bot_excel.html')
            else:
                return redirect('/api/login')
        except:
            return render_template('create_bot_excel.html')
            # return redirect('/api/login')
    elif request.method == 'POST':

        try:
            f = request.files['file']
            f.save(r'./mapping/' + f.filename)
            configuration = {"bot name":"Lorlatinib Bot","language":"es","es_analyzer":"English","mapping":[]} #todo un-hardcode this
            mapping_result = json.loads(convert(r'./mapping/' + f.filename))
            for key, value in mapping_result.items():
                configuration["mapping"] = value
                configuration["bot name"]=value[1]['Bot Name']
                configuration["group_name"]=value[1]['Group']

        except Exception as e:
            configuration = request.get_json(force=True)

        print('CONFIGURATION - ', configuration)
        logger.info('CONFIGURATION',configuration)
        is_valid,message = validate_bot(configuration)
        row = next((item for item in configuration['mapping'] if item["Is Start"] == "true"), False)
        if row:
            row['Group'] = ""
        if is_valid == 0:
            is_saved, bot_id, message = store_bot(configuration)
            if is_saved == 0:
                return jsonify(
                    status='Success',
                    id=bot_id,
                    msg='Bot created successfully'
                )
            else:
                return jsonify(
                    status='Error',
                    msg=message
                )
        else:
            return jsonify(
                status='Error',
                msg=message
            )

@bot_blueprint.route('/api/bot/<bot_id>/update',methods=['GET','POST'])
@requires_admin({'role':['admin']})
def bot_update(bot_id):
    # excel update flag means whether update is being happening from excel CCT or Admin UI.
    excel_update_flag = ''
    start_required = True
    if request.method == 'GET':
        logger.debug('GET method called in bot update method')
        session['update_login_attempt'] = True
        session['update_bot_id'] = bot_id
        try:
            if session['loggedIn'] == True:
                return render_template('update_bot.html', bot_id=bot_id)
            else:
                return redirect('/login')
        except:
            return redirect('/login')


    elif request.method == 'POST':
        logger.debug('POST method called in Update method')
        try:
            f = request.files['file']
            # print(f)
            logger.debug('WE GOT THE EXCEL FILE FOR UPDATE')
            f.save(r'./mapping/' + f.filename)

            configuration = {"bot name":"Lorlatinib Bot","language":"es","es_analyzer":"English","mapping":[]} #todo un-hardcode this
            mapping_result = json.loads(convert(r'./mapping/' + f.filename))
            for key, value in mapping_result.items():
                configuration["mapping"] = value
                configuration["bot name"] = value[1]['Bot Name']
                configuration["group_name"]=value[1]['Group']
                row = next((item for item in configuration['mapping'] if item["Is Start"] == "true"), False)
                if row:
                    row['Group'] = ""
                grp_list = [configuration["group_name"]]
                start_required = False
        except Exception as e:
            configuration = request.get_json(force=True)
            # print("config from ui-->",configuration)
            grp_list=configuration['sessionObj']['group_name']
            excel_update_flag = 'True'
            configuration=configuration['botObj']

        # modify configuration
        new_mapping = []

        if excel_update_flag != '':
            logger.debug('-----------------SESSION OBJECT WAS IN CONFIGURATION. ADMIN UI CALLED')
            # print(configuration['mapping'], "<-----configuration mapping")
            # print(isinstance(configuration['mapping'][0], (list,)))
            testconf = [v for k, v in configuration['mapping'].items() if len(v) > 0]
            # if isinstance(configuration['mapping'][next(iter(configuration['mapping']))][0]['Task Text'], (list,)):
            # print("TESTCONF = {}".format(testconf))
            # print("TYPE OF TESTCONF = {}".format(type(testconf)))
            # if isinstance(configuration['mapping'][0]['Task Text'],(list,)):
            #for item in configuration['mapping']:
            if isinstance(testconf[0][0]['Task Text'], (list,)):
                logger.info("Updating state state. Merging it in mapping")
                configuration['mapping']['start'] = [configuration['start_state']]
                del configuration['start_state']
                logger.info(json.dumps(configuration, indent=4))

                for grp, grp_val in configuration['mapping'].items():
                    print(grp)
                    logger.info("Group is: " + str(grp))
                    for item in grp_val:
                        logger.info(grp)
                        logger.info(item)
                        # for item in configuration['mapping']:

                        task_text_list = item['Task Text']
                        if len(item['Task Text']) == 1:

                            item['Task Text'] = item['Task Text'][0]
                            new_mapping.append(item)
                            logger.info("Length < 1")
                            if item['Is Start']=='true' or item['Is Start'] == True:
                                logger.info("Received state is start")
                                item['Is Start']= "true"
                                logger.info("item: " + json.dumps(item, indent=4))
                        else:
                            logger.info("chekinf for start state")
                            logger.info(item['Is Start'])
                            if item['Is Start']=='true' or item['Is Start']:
                                logger.info("Reveived Start State")
                                logger.info(type(item['Task Text']))
                                logger.info(item['Task Text'])
                                logger.info(grp)
                                logger.info(json.dumps(grp_val))

                                if isinstance(item['Task Text'],list):
                                    logger.info("Item Task text is list, start")
                                    task_var = item['Task Text']
                                    item['Task Text'] = item['Task Text'][0]
                                    new_mapping.append(item)

                                    for i in task_var[1:]:
                                        item_copy = item.copy()
                                        item_copy['Is Start']='false'
                                        item_copy['Task Text'] = i
                                        new_mapping.append(item_copy)
                                else:
                                    logger.info("Item Task text is str, start")
                                    new_mapping.append(copy.deepcopy(item))
                            else:
                                for text in task_text_list:
                                    item_copy = copy.deepcopy(item)
                                    for k,v in item_copy.items():
                                        if v=='true':
                                            item_copy[k]=True
                                        elif v=='false':
                                            item_copy[k]=False


                                    # print('---text check---', text)
                                    item_copy['Task Text'] = text

                                    if item_copy not in new_mapping:
                                        new_mapping.append(item_copy)

                configuration['mapping'] = copy.deepcopy(new_mapping)
                logger.info("New mapping ------------------")
                logger.info(json.dumps(new_mapping, indent=4))
        else:

            logger.info('SESSION OBJECT WAS NOT IN CONFIGURATION. EXCEL UPDATE CALLED')
            print('\n')

            if isinstance(configuration['mapping'][0]['Task Text'],(list,)):

                for item in configuration['mapping']:

                    task_text_list = item['Task Text']
                    if len(item['Task Text']) == 1:
                        item['Task Text'] = item['Task Text'][0]
                        new_mapping.append(item)
                    else:
                        if item['Is Start'] == 'true' or item['Is Start'] == True:
                            logger.info("Task is start"+ str(isinstance(item['Task Text'], list)))

                            if isinstance(item['Task Text'], list):
                                task_var = item['Task Text']
                                item['Task Text'] = item['Task Text'][0]
                                new_mapping.append(item)

                                for i in task_var[1:]:
                                    item_copy = item.copy()
                                    item_copy['Is Start'] = 'false'
                                    item_copy['Task Text'] = i
                                    new_mapping.append(item_copy)
                            else:
                                print("Appending item")
                                new_mapping.append(item)

                        else:
                            if isinstance(task_text_list, list):
                                for text in task_text_list:
                                    item_copy = item.copy()
                                    for k, v in item_copy.items():
                                        if v == 'true':
                                            item_copy[k] = True
                                        elif v == 'false':
                                            item_copy[k] = False

                                    # print('---text check---', text)
                                    item_copy['Task Text'] = text

                                    if item_copy not in new_mapping:
                                        new_mapping.append(item_copy)
                            else:
                                item_copy = item.copy()
                                for k, v in item_copy.items():
                                    if v == 'true':
                                        item_copy[k] = True
                                    elif v == 'false':
                                        item_copy[k] = False

                                # print('---text check---', text)
                                item_copy['Task Text'] = text

                                if item_copy not in new_mapping:
                                    new_mapping.append(item_copy)

                # print('---new mapping check---', new_mapping)
                configuration['mapping'] = new_mapping

        logger.info("printing configuration again")
        logger.info(json.dumps(new_mapping, indent=4))
        is_valid, message = validate_bot(configuration,start_required)
        # print('\n')
        # print('\n')
        print('\n')
        logger.info('********************SENDING THIS MAPPING TO UPDATE BOT = {}*****************'.format(configuration['mapping']))
        if is_valid == 0:
            is_saved,bot_id,message = update_bot(configuration,bot_id,grp_list)
            success_msg = 'Your bot has been updated!'
            if is_saved == 0:
                logger.info('Bot Updated Successfully')

                return jsonify(
                    status='Success',
                    id=bot_id,
                    msg='Bot updated successfully'
                )
            else:
                logger.error('Error in Bot update = {}')
                logger.exception(message)
                return jsonify(
                    status='Error',
                    msg=message
                )

        else:
            print(message)
            logger.error('Error in Bot update = {}')
            logger.exception(message)
            return jsonify(
                status='Error',
                msg=message
            )


@bot_blueprint.route('/api/bot/<bot_id>', methods=['POST'])
@requires_admin({'role':['admin','general']})
def bot_read(bot_id):
    if request.method == 'POST':
        print("REQUEST JSON IN BOT READ FOR SPECIFIC BOT = {}".format(request.json))
        group_list = [str(i) for i in list(request.json['group_name'])]
        print("GROUP LIST IN BOT READ FOR SPECIFIC BOT = {}".format(group_list))
        is_valid, configuration, message = read_bot(bot_id)
        # print('config - ',configuration)
        configuration['_id'] = str(configuration['_id']).replace('ObjectId(', '').replace(')', '')
        groups_collection = get_collection("groups")
        grp_list_empty = []
        print("GROUPS COLLECTION = {}".format(groups_collection))
        for documents in groups_collection:
            grp_list_empty.append(documents['name'])


        grp_dict_empty = {k: [] for v, k in enumerate(grp_list_empty)}
        new_mapping = []
        if is_valid == 0:
            last_id = 0
            task_text_dict = {}
            grp_task_text_dict = {i: {} for i in group_list}
            grp_mapping = {i: [] for i in group_list}
            print('GROUP TASK TEXT DICT = {}'.format(grp_task_text_dict))
            # print("1***",configuration['mapping'])
            for item in configuration['mapping']:
                for grp, val in grp_task_text_dict.items():
                    print('CURRENT VAL = {} and GROUP = {}'.format(val, grp))
                    try:
                        if last_id < int(float(item['Task ID'])):
                            last_id = int(float(item['Task ID']))
                    except:
                        pass
                    try:
                        if item['Task ID'] == '':
                            item['Task ID'] = '0'
                        if (item['Task ID'] in val) and (str(item['Group']).lower() == str(grp).lower()):

                            grp_task_text_dict[grp][item['Task ID']].append(item['Task Text'])
                            break
                        elif str(item['Group']).lower() == str(grp).lower():

                            grp_task_text_dict[grp][item['Task ID']] = [item['Task Text']]
                            break
                    except Exception as e:
                        print("THIS EXCEPTION CAME = {}".format(e))


            logger.info("------- In read all after fist processing")
            start_state = None
            for map in configuration['mapping']:
                print(map)
                logger.info(json.dumps(map))
                if str(map['Group']) != '':
                    print("SEEN IDS SO FAR GROUP WISE = {}".format(grp_dict_empty[str(map['Group'])]))

                    if map['Task ID'] == '':
                        map['Task ID'] = '0'

                    if map['Task ID'] in grp_dict_empty[str(map['Group'])]:
                        print("{} TASK ID FOR GROUP {} ALREADY PRESENT IN SEEN ID. GOIN TO SKIP".format(map['Task ID'],
                                                                                                        map['Group']))
                        pass
                    else:

                        print("{} IS A NEW TASK ID FOR GROUP {}.GOIN TO ADD IT".format(map['Task ID'], map['Group']))

                        # seen_ids.append(map['Task ID'])
                        grp_dict_empty[str(map['Group'])].append(map['Task ID'])



                        print('GROUP IN MAP = {}'.format(map['Group']))
                        if str(map['Group']) in group_list:
                            try:

                                map['Task Text'] = list(set(grp_task_text_dict[str(map['Group'])][map['Task ID']]))

                                grp_mapping[str(map['Group'])].append(map)
                            except:
                                pass
                            new_mapping.append(map)
                else:
                    logger.info("Found empty Group, Checking if its start state")

                    if map['Is Start']:
                        new_map = copy.deepcopy(map)
                        new_map['Task Text'] = [ map['Task Text']]
                        start_state = new_map

            logger.info("Group mapping--> " , grp_mapping)


            configuration['max_id'] = last_id

            configuration['mapping'] = grp_mapping

            if start_state:
                configuration['start_state'] = start_state
            return jsonify(
                status='Success',
                configuration=configuration,
                msg=message
            )
        else:
            return jsonify(
                status='Failure',
                configuration='',
                msg=message
            )


@bot_blueprint.route('/api/bot', methods=['POST','GET'])
@requires_admin({'role':['admin','general']})
def bot_read_all():
    if request.method == 'POST':

        group_list = [str(i) for i in list(request.json['group_name'])]

        ids = bot_read_all_update()

        mappings = list()
        for id in ids:
            logger.info(id)
            bot_properties, max_task_id,start_state, result = bot_read_mongodb(id, group_list)


            configuration = {}
            configuration['bot name']= bot_properties['bot name']
            configuration['es_analyzer']= bot_properties['es_analyzer']
            configuration['language']= bot_properties['language']
            configuration['update_time'] = str(bot_properties['update_time'])
            configuration['max_id'] = max_task_id
            logger.info(max_task_id)

            configuration['mapping'] = result
            configuration['start_state'] = start_state[0] if start_state else start_state
            configuration['_id'] = id
            logger.info(json.dumps(configuration, indent=4))
            mappings.append(configuration)
            
        return jsonify(
            bots=mappings
        )

# Remove to be confirmed
# 514- 518
def bot_read_all_update():

    result = get_all_ids('bot')
    logger.info(json.dumps(result))
    return result


# To be moved to separate file
# bot_read_mongodb
def bot_read_mongodb(bot_id, groups_to_be_filtered):

    groups_to_be_filtered.append("")

    query =[ {
    "$match": {
        "_id": ObjectId(bot_id)
        }
},
{"$project": {
                    "mapping": "$mapping",
                    "id": "$mapping.Task ID",

                }
            },
            { "$unwind": "$mapping"},
            {"$redact": {
                "$cond": [
                    {"$in": [
                        {"$let": {
                            "vars": {
                                "item": "$mapping.Group"
                            },
                            "in": "$$item"
                        }},
                        groups_to_be_filtered
                    ]},
                    "$$KEEP",
                    "$$PRUNE"
                ]
            }},

{ "$group": {
       "_id": {
           "group": "$mapping.Group",
           "taskid": "$mapping.Task ID"
       },
       "values": {
           "$addToSet":
               "$mapping.Task Text"
            },
        "Is Start":{
           "$addToSet":"$mapping.Is Start"},
        "Interaction Values":{
           "$addToSet":"$mapping.Interaction Values"},
        "Action Type":{
               "$addToSet": "$mapping.Action Type"},
        "Next Task IDs": {
            "$addToSet": "$mapping.Next Task IDs"},
        "Alias": {
            "$addToSet": "$mapping.Alias"},
        "Answers ES": {
            "$addToSet": "$mapping.Answers ES"},
        "Function Code": {
            "$addToSet": "$mapping.Function Code"},
        "Interaction Connection String": {
            "$addToSet": "$mapping.Interaction Connection String"},
        "Interaction DB Filter Column": {
            "$addToSet": "$mapping.Interaction DB Filter Column"},
        "Interaction DB Filter Value": {
            "$addToSet": "$mapping.Interaction DB Filter Value"},
        "Interaction DB Host": {
            "$addToSet": "$mapping.Interaction DB Host"},
        "Interaction DB Port": {
            "$addToSet": "$mapping.Interaction DB Port"},
        "Interaction DB Select Column": {
            "$addToSet": "$mapping.Interaction DB Select Column"},
        "Interaction DB Table Name": {
            "$addToSet": "$mapping.Interaction DB Table Name"},
        "Interaction DB Type": {
            "$addToSet": "$mapping.Interaction DB Type"},
        "Interaction Fetch From DB": {
            "$addToSet": "$mapping.Interaction Fetch From DB"},
        "Interaction Type": {
            "$addToSet": "$mapping.Interaction Type"},
        "Is Reserved": {
            "$addToSet": "$mapping.Is Reserved"},
        "Keywords": {
            "$addToSet": "$mapping.Keywords"},
        "Task ID": {
            "$addToSet": "$mapping.Task ID"},
        "Task Name": {
            "$addToSet": "$mapping.Task Name"},
        "Use Code": {
            "$addToSet": "$mapping.Use Code"},
        "Bot Name": {
            "$addToSet": "$mapping.Bot Name"},
        "Interaction DB Select Column": {
            "$addToSet": "$mapping.Interaction DB Select Column"}


  }},
  {
                "$project": {"row": {
                    "Task ID": "$_id.taskid",
                    "Task Text": "$values",
                    "Group":"$_id.group",
                    "Is Start":{ "$arrayElemAt": [ "$Is Start", 0 ] },
                    "Interaction Values":{ "$arrayElemAt": [ "$Interaction Values", 0 ] },
                    "Action Type": { "$arrayElemAt": [ "$Action Type", 0 ] },
                    "Next Task IDs": {"$arrayElemAt": ["$Next Task IDs", 0]},
                    "Alias": {"$arrayElemAt": ["$Alias", 0]},
                    "Answers ES": {"$arrayElemAt": ["$Answers ES", 0]},
                    "Function Code": {"$arrayElemAt": ["$Function Code", 0]},
                    "Interaction Connection String": {"$arrayElemAt": ["$Interaction Connection String", 0]},
                    "Interaction DB Select Column": {"$arrayElemAt": ["$Interaction DB Select Column", 0]},

                    "Interaction DB Filter Column": {"$arrayElemAt": ["$Interaction DB Filter Column", 0]},
                    "Interaction DB Filter Value": {"$arrayElemAt": ["$Interaction DB Filter Value", 0]},
                    "Interaction DB Host": {"$arrayElemAt": ["$Interaction DB Host", 0]},
                    "Interaction DB Port": {"$arrayElemAt": ["$Interaction DB Port", 0]},
                    "Interaction DB Table Name": {"$arrayElemAt": ["$Interaction DB Table Name", 0]},
                    "Interaction DB Type": {"$arrayElemAt": ["$Interaction DB Type", 0]},
                    "Interaction Fetch From DB": {"$arrayElemAt": ["$Interaction Fetch From DB", 0]},
                    "Interaction Type": {"$arrayElemAt": ["$Interaction Type", 0]},
                    "Is Reserved": {"$arrayElemAt": ["$Is Reserved", 0]},
                    "Keywords": {"$arrayElemAt": ["$Keywords", 0]},
                    "Task Name": {"$arrayElemAt": ["$Task Name", 0]},
                    "Use Code": {"$arrayElemAt": ["$Use Code", 0]},
                    "Bot Name":  {"$arrayElemAt": ["$Bot Name", 0]}

                }
            }
        },
        { "$group": {
       "_id": "$_id.group",
       "values": {"$push": "$row"}
        }},
        { "$replaceRoot": {
    "newRoot": {
      "$arrayToObject": {
        "$concatArrays": [
          [
            { "k": "$_id", "v": "$values" }
          ]
        ]
      }
    }
  }}
        ]
    
    logger.info(f"query:{query} ")

    results = execute_query('bot', query)
    #logger.info("Results of executing query: " + json.dumps(results, indent=4))

    final_result = {}
    start_state = None
    for i in results:
        for k,v in i.items():
            if k:
                final_result[k] = v
            else:
                start_state = v



    max_task_id_query =   [
        {
            "$match": {
                "_id":ObjectId(bot_id)
            }
        },
        {
            "$unwind": {
                "path": "$mapping",

            }
        },
    {
        "$group": {
            "_id": "$_id",
            "minDelivered": { "$max": { '$toInt': '$mapping.Task ID' }},
        }
    },
    {
        "$project":{
            "_id": 0,
            "max_id": "$minDelivered"
        }
    }

    ]

    max_task_id = execute_query('bot', max_task_id_query)

    properties_query = [
        {
            "$match": {
                "_id": ObjectId(bot_id)
            }
        },
        {
            "$project" : {
                "_id": 0,
                "bot name": "$bot name",
                "language": "$language",
                "es_analyzer": "$es_analyzer",
                "update_time": "$update_time",

            }

        }
    ]
    
    bot_properties = execute_query('bot', properties_query)

    return bot_properties[0], max_task_id[0]['max_id'], start_state, final_result



@bot_blueprint.route('/bot/<bot_id>/delete',methods=['GET','POST'])
def bot_delete(bot_id):
    if request.method == 'GET':
        try:
            is_valid, message = delete_bot(bot_id)
        except Exception as e:
            logger.error("Bot error: ")
            logger.exception(e)
        if is_valid == 0:
            return jsonify(
                status='Success',
                msg=message
            )
        else:
            return jsonify(
                status='Failure',
                msg=message
            )


################# Chat Events ####################
@bot_blueprint.route('/bot/<bot_id>/init',methods=['GET'])
def bot_init(bot_id):
    logger.info('BOT ID: '+ str(bot_id))

    try:
        if len(get_by_id('bot',bot_id))>0:
            logger.debug('Valid bot id - %s',bot_id)
            session = {}
            session['on_connect_start'] = True
            session['suggestion'] = ''
            session['fuzzyFlag'] = False
            session['restartFlag'] = False
            session['restartFlagFAQ'] = False
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session['codeFlag'] = False
            session['guidedFlag'] = False
            session['audioSentFlag'] = False
            session['ASRFlag'] = False
            session['results'] = ''
            session['selectColumnQ'] = ''
            session['filterColumnQ'] = ''
            session['filterValueQ'] = ''
            session['surveyFlagNum'] = False
            session['surveyFlagComm'] = False
            session['endSurveyFlag'] = False
            session['feedbackFlag_ES'] = False
            session['suggestCounter_ES'] = 0
            session['audioSentFlag_ES'] = False
            session['bot_id'] = bot_id
            session['name'] = 'None'
            session['disable_input'] = False
            session['created_on'] = time.time()  # todo time zone
            session['group']= ""
            session['es_restart_counter'] = 0
            session['email_app'] = ''
            session['date_app'] = ''
            session['time_app'] = ''
            session['no_sec_app'] = False
            session['security'] = False
            session['escalation'] = False
            session['created_on'] = time.time()# todo time zone
            session['noCounter_ES'] = 0
            session['channel'] = ''  # we need to detect and set this.
            session['dosage_yes_counter'] = 0
            session['dosage_no_counter'] = 0
            session['grv_id'] = request.args.get('user_id')
            session_id = set_context(session)
            return jsonify(
                status='Success',
                session_id=session_id
            )

        else:
            return jsonify(
                status='Failure',
                msg='Invalid bot id'
            )

    except Exception as e:
        logger.exception(e)
        return jsonify(
            status='Failure',
            msg='Invalid bot id'
        )


@bot_blueprint.route('/bot/<bot_id>/<session_id>/end',methods=['POST'])
def bot_end(bot_id,session_id):
    try:
        if (bson.objectid.ObjectId.is_valid(bot_id) and get_by_id('bot',bot_id)):
            if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context',session_id)):
                update('status', session_id, {'has_ended': True})
                return jsonify(
                    status='Success',
                    msg='Session ended'
                )
            else:
                return jsonify(
                    status='Failure',
                    msg='Not a valid Session ID'
                )
        else:
            return jsonify(
                status='Failure',
                msg='Not a valid Bot ID'
            )
    except Exception as e:
        logger.error("Bot error: ")
        logger.exception(e)


@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat', methods=['POST'])
@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat/<grv>', methods=['POST'])
def bot_chat(bot_id,session_id, grv='None'):
    bot_data=get_by_id('bot',bot_id)
    conv_log = ConversationLogger(session_id, bot_id)
    if (bson.objectid.ObjectId.is_valid(bot_id) and bot_data):#get_by_id('bot',bot_id)):

        if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context',session_id)):



            app.config['BABEL_DEFAULT_LOCALE'] =bot_data['language']
            refresh()
            message_request = request.get_json(force=True)
            print("Message request: ", message_request)
            message = message_request['message']
            user_message = message
            current_state = message_request['state']
            asr_flag = message_request['ASR']

            # type to be saved from message_request
            channel_type = message_request['type']
            channels_list = []

            messages_collection = get_one('messages', 'type', 'General')
            messages_mappings = messages_collection['mappings']
            messages_mappings_dictionary = {}

            # Converting the list of Small Talk messages to a single dictionary
            for each_item in messages_mappings:

                messages_mappings_dictionary.update({each_item['message']: each_item['translation']})
            #print("Messages mapping dictionary: Inside restart_execute", messages_mappings_dictionary)

            session = get_context(session_id)
            session['channel'] = channel_type
            session['group'] = message_request['group']

            if not session['grv_id'] and message_request['user_id']:
                session['grv_id'] = message_request['user_id']
            update_context(session_id, session)
            print("Session: ", session)
            print("Message: ", message)
            response = ''

            user_region = session['group'].upper()
            if not user_region in app_config["CHANNELS"]:
                channels = []
                channels_string = ""
            else:
                channels = app_config.get('CHANNELS', user_region.replace(" ",""))
                channels = channels.split(',')
                channels_string = app_config.get('CHANNELS', user_region.replace(" ","")) or ""

            # channel validation
            if session['channel'] not in channels:
                json_return = {
                    "is_multi": False,
                    "message": [{
                        "interaction elements": "",
                        "text": messages_mappings_dictionary["For your country {current_channel_name} is not a valid interaction mode. Please use {channel}"].format(
                            current_channel_name=session['channel'], channel=channels_string.replace(',', ' or ')),
                        "type": "string",
                        "interaction": "text",
                        "tts": False,
                        "tts audio": "",
                        "disable_response": False
                    }]
                }
                conv_log.bot_log(json_return['message'][0]['text'], session['ASRFlag'], session['endSurveyFlag'], guidedRestartFlag=False, group=session['group'], grv_id=session["grv_id"])
                return jsonify(
                    status='Success',
                    result=json_return,
                    current_state=""
                )

            if session['on_connect_start'] is True:
                print("On connect start is true")
                execution_engine = ExecutionEngine(session_id, bot_id)
                state_definition = bot_data['mapping']

                for state in state_definition:
                    for key, value in state.items():
                        if key == 'Is Start' and value == 'true':
                            response = execution_engine.execute(state)

                            return jsonify(
                                status='Success',
                                result=response,
                                current_state=get_by_id("status",session_id)["current_state"]
                            )
            else:
                #if condition - if msg is empty and on connet start is FALSE then just re-execute the current state
                #if we refresh, then just re-execute the current state.
                #pop out the last state in history because it will be added back in via conversation logger
                if message == '':
                    execution_engine = ExecutionEngine(session_id, bot_id)
                    current_state = get_by_id("status", session_id)["current_state"]
                    state_definition = bot_data['mapping'] #get_by_id('bot', bot_id)['mapping']
                    # for state in state_definition:
                    #     for key, value in state.items():
                    #         if key == 'Task Name' and value == current_state:
                    #             history = get_by_id('status', session_id)['history']
                    #             history = history[:-1]  # pop out the last element
                    #             logger.info("History" , history)
                    #             update('status', session_id, {'history': history})
                    #             response = execution_engine.execute(state)
                    #
                    #             return jsonify(
                    #                 status='Success',
                    #                 result=response,
                    #                 current_state=get_by_id("status", session_id)["current_state"]
                    #             )
                    for state in state_definition:
                        for key, value in state.items():
                            if key == 'Is Start' and value == 'true':
                            # if key == 'Task Name' and value == current_state:
                            #     history = get_by_id('status', session_id)['history']
                            #     history = history[:-1]  # pop out the last element
                            #     logger.info("History", history)
                            #     update('status', session_id, {'history': history})
                                response = execution_engine.execute(state)

                                return jsonify(
                                    status='Success',
                                    result=response,
                                    current_state=get_by_id("status", session_id)["current_state"]
                                )
                else:
                    print("Session On connect is false ")
                    execution_engine = ExecutionEngine(session_id, bot_id)

                    response = execution_engine.resume(message, asr_flag, bot_id)
                        
                    print("----------------------------> Response from resume: ", response)
                    if response is None:
                        return jsonify(
                            status='Failure',
                            result=response,
                            current_state=get_by_id("status", session_id)["current_state"]
                        )
                    else:
                        current_state = get_by_id("status", session_id)["current_state"]
                        print("The current state is: ", current_state)
                        if (current_state == "Start"):
                            print("Current State s start -- error is supposd to be herer if you see this outpt")

                            state_definition = bot_data['mapping']
                            logger.info("Before for : ", response)

                            for state in state_definition:
                                # print("Each_state: ", state)
                                if state['Task Name'] == 'Start' and session['channel'] not in ['facebook','line', 'web'] and response['message'][0]['interaction'] != 'url':
                                    print("Executing the start state , this should not owrk")
                                    response = execution_engine.execute(state)

                                    # response["message"].append(execution_engine.execute(state)["message"][0])
                            logger.info("After for : ", response)
                        print("Returning this responsae -------------------------------", get_by_id("status", session_id)["current_state"])
                        return jsonify(
                            status='Success',
                            result=response,
                            current_state=get_by_id("status", session_id)["current_state"]
                        )

        else:
            return jsonify(
                status='Failure',
                result='Invalid session id'
            )
    else:
        return jsonify(
            status='Failure',
            result='Invalid bot id'
        )
