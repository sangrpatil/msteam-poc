from flask import Blueprint, render_template, request, jsonify, session, Markup, send_from_directory
from utility.logger import logger
from utility.generate_reports_json import generateReports
# from utility.generate_reports_excel import generateReportsWeekly
from utility.generate_repots_using_json import generateReportsWeekly
import os
from utility.mongo_dao import *
from utility.de_coupled_report import *
import json

metrics_blueprint = Blueprint('metrics', __name__, template_folder='templates')


# Return metrics of all bots
@metrics_blueprint.route('/api/metrics', methods=['GET','POST'])
def metrics_all_bots():
    if request.method == 'GET':
        # Call the function with not bot_id passed
        returnedjson = generateReports()
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )

    elif request.method == 'POST':
        # Call the function with not bot_id passed
        returnedjson = generateReports()
        if returnedjson == 'Failure':
            return jsonify(
                status='Failure',
                metrics='Bot ID not found'
            )
        else:
            return jsonify(
                status='Success',
                metrics=returnedjson
            )
        pass

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>', methods=['GET','POST'])
def metrics_bot(bot_id):
    if request.method == 'GET':
        #Call the function with bot_id passed
        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            logger.exception(e)
            return jsonify(
                status='Failure',
                message=e
            )
    if request.method == 'POST':
        #Call the function with bot_id passed
        try:
            returnedjson = generateReports(bot_id)

            if returnedjson == 'Failure':
                return jsonify(
                    status='Failure',
                    metrics='Bot ID not found'
                )
            else:
                return jsonify(
                    status='Success',
                    metrics=returnedjson
                )

        except Exception as e:
            logger.error("metrics error: ")
            logger.exception(e)
            return jsonify(
                status='Failure',
                message=e
            )

# Return metrics of a particular bot
@metrics_blueprint.route('/api/metrics/<bot_id>/download', methods=['GET','POST'])
def metrics_download(bot_id):
    start_date = datetime.datetime.today().replace(day=1)
    end_date = datetime.date.today()
    print("Start date and end date: ", start_date, end_date)
    if request.method == 'GET':
        try:
            filename1 = generateReportsWeekly(bot_id, start_date, end_date)
            file_name = str(bot_id) + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            logger.error("metrics error: ")
            logger.exception(e)
            failure = 'Failure as ' + str(e)
            return failure

    if request.method == 'POST':
        try:
            filename1 = generateReportsWeekly(bot_id)
            file_name = str(bot_id) + '.xlsx'
            path = os.path.abspath("static/reports/")
            return send_from_directory(directory=path, filename=file_name)
        except Exception as e:
            logger.error("metrics error: ")
            logger.exception(e)
            failure = 'Failure as ' + str(e)
            return failure


