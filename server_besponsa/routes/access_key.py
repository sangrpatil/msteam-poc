from flask import Blueprint, request, jsonify
import pandas as pd
from utility.mongo_dao import *
from utility.decorators import requires_admin
import json

access_key_blueprint = Blueprint('access_key', __name__, template_folder='templates')


@access_key_blueprint.route('/accesskeyapi/cb/uploadkeys', methods=['POST'])
@requires_admin({'role': ['admin']})
def upload_keys():
    '''
    This method takes file (.xlsx) as input and uploads the content in secret_keys collection
    :return: success on successful execution
    '''
    try:
        file = request.files['key_file']
        print(file)
    except:
        return jsonify(
            status='Failure',
            msg='File upload error'
        )

    # file.save("reports/key_file.xlsx")
    # xls = pd.ExcelFile("reports/key_file.xlsx")

    try:
        xls = pd.ExcelFile(file)
        sheetX = xls.parse(0)  # 2 is the sheet number'
        print("File parsed")
        print(sheetX)
        print(type(sheetX))
        var1 = list(sheetX)
        print("Pringing var")
        print(var1)
        print(type(var1))
        # if search_one('secret_keys', {'secret_key': var1[0]}) is None:
        #     insert('secret_keys', {'secret_key':var1[0], 'used': 0})
        print(sheetX[var1])
        print("Here")
        print(list(sheetX))

        if sheetX.isnull().values.any():
            return jsonify(
                status="Error",
                msg="Excel file cannot contain empty region or secret key"
            )


        var1 = list(set(map(tuple,sheetX.values)))

        print(var1)
        for i in var1:
            print(i[0])
            print(i[1])
            i = [ j.upper() for j in i]
            if search_one('secret_keys', {'secret_key': i[0], 'region':i[1]}) is None:
                insert('secret_keys', {'secret_key': i[0], 'used': 0, 'region':i[1]})
        return jsonify(
                status='Success',
                msg="secret keys created successfully."
            )
    except Exception as e:
        print(e)
        return jsonify(
            status='Error',
            msg="Error during key upload"
        )


@access_key_blueprint.route('/accesskeyapi/cb/display_excel/', methods=['POST'])
#@requires_admin({'role':['admin']})
def display_excel():
    '''
    This method takes file (.xlsx) as input and uploads the content in secret_keys collection
    :return: success on successful execution
    '''
    try:
        print(request.files)
        file = request.files['key_file']
        print(file)
    except:
        return jsonify(
            status='Failure',
            msg='File upload error'
        )
    # file.save("reports/key_file.xlsx")
    # xls = pd.ExcelFile("reports/key_file.xlsx")
    try:
        xls = pd.ExcelFile(file)
        sheetX = xls.parse(0)  # 2 is the sheet number



        columns_to_be_in_sheet = ['Key', 'Region']
        region_list = []
        list_to_return = list(set(map(tuple,sheetX.fillna('').values)))
        print("List to return before insert operation: ", list_to_return)

        for each_item in list_to_return:
            region_list.append(each_item[1])

        print("Region list to be sent for region check validation ", region_list)

        valid_region_flag = region_check(region_list)
        if valid_region_flag == 1:
            return jsonify(
                status="Error",
                msg="Excel file contains invalid region names. Check for special characters and whitespaces."
            )



        list_to_return.insert(0, sheetX.columns.tolist())

        print("List to return", list_to_return)

        return jsonify(
                status='Success',
                msg="secret keys read successfully",
                key_list=list_to_return
            )

    except Exception as e:
        print(e)
        return jsonify(
            status='Error',
            msg=e
        )


@access_key_blueprint.route('/accesskeyapi/cb/load_excel/', methods=['POST'])
@requires_admin({'role':['admin']})
def load_excel():

    # return jsonify(
    #     status="Success",
    #     msg="OK"
    # )
    try:
        message_request = request.json
        key_list = message_request['key_list']
        print("key list received in upload: ", key_list)
        valid_groups = message_request['sessionObj']['group_name']
        print("Valid groups for the admin: ", valid_groups)

        for each_item in key_list[1:]:
            print("Each_region: ", each_item[1])
            if each_item[1] not in valid_groups:
                return jsonify(
                    status='Failure',
                    msg="Sorry but you only have access to {} and {}. So you can upload access keys for these regions only. Please remove other groups from the excel file and try again".format(*valid_groups)
                )

        type = message_request['type']
        print("Message request in load_excel: ", message_request)
        print(key_list)
        # delete_result = delete_all("secret_keys")



        if type == "matched":

            columns = key_list[0]
            columns_to_be_in_sheet = ['Key', 'Region']
            is_correct_order = columns_to_be_in_sheet == columns
            is_reverse_order = columns == columns_to_be_in_sheet[::-1]

            print("Is_correct_order: ", is_correct_order)
            print("Is_reverse_order: ", is_reverse_order)

            if not is_correct_order:
                if is_reverse_order:
                    key_list = [i[::-1] for i in key_list]
                else:
                    return jsonify(
                        status='Failure',
                        msg="Columns should be of header 'Key' and 'Region'. Kindly update in excel sheet."
                    )

            if not all(item for sublist in key_list for item in sublist):
                return jsonify(
                        status="Error",
                        msg="Excel file cannot contain empty region or secret key"
                    )

            if len(key_list) < 2:
                return jsonify(
                    status="Failure",
                    msg="The file cannot contain empty values"
                )

            for secret_key in key_list[1:]:
                print("Secret key to be inserted is: ", secret_key[0])
                search_result = search_one("secret_keys", {'secret_key': secret_key[0].upper()})
                print("Search result is: ", search_result)
                if search_result is None:
                    print("New so will insert")
                    insert("secret_keys", {"secret_key": str(secret_key[0]).upper(), "used": 0,
                                           "region": str(secret_key[1]).upper()})
                    print(f" inserted Matched {secret_key[0]} for region {secret_key[1]}")

        else:
            columns = key_list[0]
            columns_to_be_in_sheet = ['Key']
            is_correct_order = columns_to_be_in_sheet == columns

            if not is_correct_order:
                    return jsonify(
                        status='Failure',
                        msg="Column should be of header 'Key'. Kindly update in excel sheet."
                    )

            if not all(item for item in key_list):
                return jsonify(
                    status="Error",
                    msg="Excel file cannot contain empty  secret key"
                )

            if len(key_list) < 2:
                return jsonify(
                    status="Success",
                    msg="The file cannot contain empyt values"
                )

            for secret_key in key_list[1:]:
                insert("secret_keys", {"secret_key": str(secret_key[0]).upper(), "used": 0})
                print(f" inserted Unmatched {secret_key[0]}")

        return jsonify(
                status='Success',
                msg="Keys inserted to database successfully"
            )
    except Exception as e:
        print(e)
        return jsonify(
            status='Failure',
            msg="Failed to insert Keys" + str(e)
        )


@access_key_blueprint.route('/accesskeyapi/cb/edit_key/', methods=['POST'])
@requires_admin({'role':['admin']})
def edit_keys():
    """
        unmatched
    :return:
    """
    try:
        message_request = request.get_json(force=True)['keyObj']
        key_id = message_request['_id']
        scrt_ky = message_request.get("secret_key")
        region = message_request.get('region') or None


        if not scrt_ky :
            return jsonify(
                status="Failure",
                msg="Secret Key cant be null"
            )


        secret_key = get_by_id("secret_keys", key_id)
        if secret_key is None:
            return jsonify(
                status='Failure',
                msg="Key with given id not available"

            )

        sec = get_one("secret_keys", "secret_key", scrt_ky.upper())
        print(sec)
        # if sec.count() > 1:
        #     return jsonify(
        #         status="Failure",
        #         msg="The secret key already exists in records as another record. Key cannot be updated"
        #
        #
        #     )

        if sec:
            if sec and str(sec['_id']) != key_id:
                return jsonify(
                        status="Failure",
                        msg="The secret key already exists in records as another record. Key cannot be updated"


                    )



        print("Updating the key")

        update("secret_keys", key_id, {"secret_key": scrt_ky.upper(), 'region': region.upper() if region else region })
        return jsonify(
                status='Success',
                msg="Key edited successfully"

        )
    except Exception as e:
        return jsonify(
            status='Error',
            msg="Exception "+str(e)

        )


@access_key_blueprint.route('/accesskeyapi/cb/edit_unmatched_key/', methods=['POST'])
@requires_admin({'role':['admin']})
def edit_unmatched_keys():
    try:
        message_request = request.get_json(force=True)['keyObj']
        key_id = message_request['_id']
        scrt_ky = message_request["secret_key"]

        if not scrt_ky:
            return jsonify(
                status="Error",
                msg="Secret Key can't be null"
            )


        secret_key = get_by_id("secret_keys", key_id)
        if secret_key is None:
            return jsonify(
                status='Error',
                msg="Key with given id not available"

            )
        else:
            if secret_key['used'] == 1:
                return jsonify(
                    status="Error",
                    msg="The key is already used and can't be edited."
                )

            sec =  get_one("secret_keys", "secret_key", scrt_ky.upper())
            if sec:
                print(sec['_id'], key_id)
                if str(sec['_id'])  != key_id:
                    return jsonify(
                        status="Error",
                        msg="The new key is already existing and can't be updated"
                    )
                else:
                    return jsonify(
                        status="Error",
                        msg="The present and entered key are same. Not Updated"
                    )


            update("secret_keys", key_id, {"secret_key": scrt_ky.upper()})
            return jsonify(
                    status='Success',
                    msg="Key edited successfully"

            )
    except Exception as e:
        return jsonify(
            status='Error',
            msg="Exception "+str(e)

        )




@access_key_blueprint.route('/accesskeyapi/cb/add_key/', methods=['POST'])
@requires_admin({'role':['admin']})
def add_keys():
    """
    unmatcned:
        region is not necessaru
    secret key:
        region is necessary
    :return:
    """
    try:
        message_request = request.get_json(force=True)['keyObj']
        print(message_request)

        type = message_request.get("type")

        if not type:
            return jsonify(
                status="Failure",
                msg="Kindly add type parameter to the request"
            )
        scrt_ky = message_request.get('secret_key')
        region = message_request.get("region")
        print("Secret Key and corresponding region: ", scrt_ky, region)

        if type == "unmatched":

            if not scrt_ky:
                return jsonify(
                    status= "Failure",
                    msg= "Kindly provide a valid secret key. Secret Key can't be empty while adding"

                )
            secret_key = get_one("secret_keys", "secret_key", scrt_ky.upper())
            if secret_key:
                return jsonify(
                    status="Failure",
                    msg= "Secret Key already exists. Kindly provide a valid secret Key"
                )

            if region:
                return jsonify(
                    status="Failure",
                    msg= "You can't add region wile adding secret key through unmathced page. Kindly add through Matched keys page"
                )

            insert("secret_keys", {"secret_key": scrt_ky.upper(), "used": 0})
            return jsonify(
                status='Success',
                msg="Key inserted successfully"

            )

        elif type=="matched":
            if not scrt_ky or not region:
                return jsonify(
                    status='Error',
                    msg= "Secret key or region can't be empty"
                )
            secret_key = get_one("secret_keys", "secret_key", scrt_ky.upper())
            if secret_key is not None:
                return jsonify(
                    status='Error',
                    msg="secret key already exists"

                )
            else:
                region_list = []
                region_list.append(region)
                valid_region_flag = region_check(region_list)
                if valid_region_flag == 1:
                    return jsonify(
                        status="Error",
                        msg="Invalid region name."
                    )

                insert("secret_keys", {"secret_key": scrt_ky.upper(), "used": 0, 'region': region.upper()})
                return jsonify(
                    status='Success',
                    msg="Key inserted successfully"

                )
    except Exception as e:
        return jsonify(
            status='Error',
            msg="Exception " + str(e)

        )

@access_key_blueprint.route('/accesskeyapi/cb/readkeys', methods=['GET','POST'])
@requires_admin({'role':['admin']})
def read_keys():

    """
    type: 0
    region: exist
    :return:
    """
    secret_keys = get_collection('secret_keys')
    secret_keys = get_all_with_multiple_conditions("secret_keys", {
        "region": {"$exists": True, "$ne": None},
        "used": 0
    })

    key_list = []
    for i in secret_keys:
        key_list.extend([{"_id": str(i["_id"]), "secret_key": i["secret_key"], "region": i['region']}])
        # str(i["_id"]).replace('ObjectId(', '').replace(')', ''):i['secret_key']} for i in secret_keys]

    return jsonify(
            status='Success',
            msg="Keys read successfully",
            Keys=key_list
        )


@access_key_blueprint.route('/accesskeyapi/cb/readusermapping', methods=['GET','POST'])
@requires_admin({'role':['admin']})
def read_usermapping():
    mapping = get_collection('besponsa_session_mapping')
    user_mapping = []
    # user_mapping=[{'User ID':i['user_id'],'Secret Key':i['secret_key'], '_id': str(i["_id"]).replace('ObjectId(', '').replace(')', '')}for i in mapping]
    for i in mapping:
        print(i)
        try:
            if i['secret_key'] is None:
                continue
            # user_mapping.append({'User ID': i['user_id'], 'Secret Key': None, '_id': None})
            else:
                try:
                    secret_key_search = search_one('secret_keys', {"secret_key": i['secret_key']})
                    object_id = str(secret_key_search["_id"]).replace('ObjectId(', '').replace(')', '')
                    user_mapping.append({'User ID': i['user_id'],
                                         'secret_key': secret_key_search['secret_key'],
                                         'region':secret_key_search['region'],
                                         '_id': object_id}
                                        )
                except Exception as e:
                    print('error in read user mapping is ', e)

        # if secret_key_search is None:
        #     user_mapping.extend([{'User ID': i['user_id'], 'Secret Key': None, '_id': object_id}])
        # else:
        #     user_mapping.extend([{'User ID': i['user_id'], 'Secret Key':i['secret_key'], '_id': object_id}])
        except Exception as e:
            print('read user mapping error is ', e)
    return jsonify(
            status='Success',
            msg="user mapping read successfully",
            Mapping=user_mapping)


# @access_key_blueprint.route('/korea/deletemapping', methods=['POST'])
# def delete_mapping():
#     message = request.json
#     usrid=message['User ID']
#     secretkey=message['Secret Key']
#     to_del=search_one('kakao_session_mapping',{'user_id':usrid,'secret_key':secretkey})
#     print(to_del)
#     update('kakao_session_mapping',to_del['_id'],{'secret_key':None,'flag':1})
#     return jsonify(
#             status='Success',
#         msg="mapping deleted successfully"
#         )

@access_key_blueprint.route('/accesskeyapi/cb/deletekeys', methods=['GET','POST'])
@requires_admin({'role':['admin']})
def delete_all_keys():
    try:
        result = delete_all('secret_keys')
        return jsonify(status='Success',
                       msg="secret keys deleted successfully"
                    )
    except Exception as e:
        return jsonify(status='Success',
                       msg=e
                       )


@access_key_blueprint.route('/accesskeyapi/cb/delete_key/', methods=['POST'])
@requires_admin({'role':['admin']})
def delete_key():
    try:
        key_id = request.get_json(force=True)['keyObj']['_id']
        print("Key ID of the key to be deleted: ", key_id)
        result = get_by_id('secret_keys', key_id)
        print(result)
        secret_key = result['secret_key']
        to_delete = search_one('besponsa_session_mapping', {'secret_key': secret_key})
        print("To delete: ", to_delete)
        channel_to_be_deleted = to_delete['type']
        print("Channel to be deleted:", channel_to_be_deleted)
        channels_in_secret_key = result['channels']
        channels_in_secret_key.remove(channel_to_be_deleted)
        print("Channels in secret key after deleting channel: ", channels_in_secret_key)

        if to_delete:

            update("secret_keys", key_id, {'channels': channels_in_secret_key})
            update('besponsa_session_mapping', to_delete['_id'], {'secret_key': None, 'flag': 1, "region": None})
            result = get_by_id('secret_keys', key_id)
            channels = result['channels']
            if len(channels) == 0:
                delete("secret_keys", key_id)
        else:
            delete("secret_keys", key_id)

        return jsonify(status='Success', msg="secret key deleted successfully")

    except Exception as e:
        return jsonify(
            status='Error',
            msg=e
           )


@access_key_blueprint.route('/accesskeyapi/cb/used_unused', methods=['GET','POST'])
@requires_admin({'role':['admin']})
def used_unused():
    try:
        cursor = get_collection('secret_keys')
        used = []
        unused = []
        for i in cursor:
            if i['used'] == 0:
                unused.append({"_id": str(i["_id"]).replace('ObjectId(', '').replace(')', ''), "secret_key": i['secret_key'], "region": i['region']})
            else:
                used.append({"_id": str(i["_id"]).replace('ObjectId(', '').replace(')', ''), "secret_key": i['secret_key'],"region": i['region']})
        return jsonify(msg='Success',
                       status="Success",
                       used=used,
                       unused=unused
                    )
    except Exception as e:
        return jsonify(status='Error',
                       msg=e
                       )



@access_key_blueprint.route('/accesskeyapi/cb/unmatched', methods=['GET','POST'])
@requires_admin({'role':['admin']})
def unmatched():
    print("Here")
    """
    type 0
    region: does not exist
    :return:
    """
    try:
        #cursor = get_collection('secret_keys')
        print("here")
        cursor = get_all_cursor('secret_keys','region', None)
        #print(cursor)

        unused = []
        for i in cursor:
            if i['used'] == 0:
                unused.append({"_id": str(i["_id"]).replace('ObjectId(', '').replace(')', ''), "secret_key": i['secret_key']})

        return jsonify(msg='Success',
                       status="Success",
                       unused=unused
                    )
    except Exception as e:
        return jsonify(status='Error',
                       msg=e
                       )



def region_check(regions):
    print("Regions list: ", regions)
    for region in regions:
        print("REgion to be searched for: ", region)
        groups_from_db = get_one("groups", "name", region)
        if not groups_from_db:
            return 1
    return 0

