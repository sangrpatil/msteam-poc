from flask import Blueprint, request, jsonify
from utility.mongo_dao import *
from utility.logger import logger
from utility.decorators import requires_admin
messages_blueprint = Blueprint('messages', __name__, template_folder='templates')


@messages_blueprint.route('/api/read_message', methods=['GET','POST'])
@requires_admin({'role':['admin']})
def read_message():
    try:
        messages = get_collection('messages')
        messages_list = []
        for i in messages:
            print(i)
            msg_list = []
            translation_list = []
            mappings = i["mappings"]
            mappings_list = []
            print("Mappings: ",mappings)
            for item in mappings:
                print("Msg: ", item["message"])
                print("Translation:", item["translation"])
                msg_list.append(item["message"])
                translation_list.append(item["translation"])

            for counter in range(len(mappings)):
                mappings_list.append({"message": msg_list[counter], "translation": translation_list[counter]})

            messages_list.extend([{"_id": str(i["_id"]), "type": i["type"], "mappings": mappings_list}])
            print(messages_list)

        return jsonify(
                status='Success',
                msg="Messages read successfully",
                messages=messages_list
        )
    except Exception as e:
        logger.error(str(e))
        logger.exception(e)
        return jsonify(
            status='Error',
            msg="Exception " + str(e)

        )


@messages_blueprint.route('/api/update_messages', methods=['POST'])
@requires_admin({'role':['admin']})
def update_messages():
    try:
        print("INSIDE UPDATE MSG")
        message_request = request.get_json(force=True)['msgListObj']
        print("Message request ", message_request)
        session_request = request.get_json(force=True)['sessionObj']
        admin_username = session_request['user_name']
        print(admin_username)
        logger.info("Admin Username updated mappings: " + admin_username)
        for mapping in message_request['messages']:

            message_type = mapping["type"]
            new_mapping = mapping["mappings"]
            # New mapping is the list of new mappings to be updated.
            # to_be_updated = get_one("messages", "type", message_type)
            _id = mapping['_id']
            print(_id)
            if _id is None:
                return jsonify(
                    status='Error',
                    msg="mapping with given type/id is not available"

                )
            update("messages", _id, {"mappings": new_mapping})
        return jsonify(
            status='Success',
            msg="Messages updated successfully"

        )

    except Exception as e:
        logger.error(str(e))
        logger.exception(e)
        return jsonify(
            status='Error',
            msg="Exception " + str(e)

        )
