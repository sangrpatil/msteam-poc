from flask import Blueprint, request, jsonify, current_app,  render_template, session, redirect, Markup, url_for, flash
from utility.mongo_dao import *
#from flask import json
import json
from utility.decorators import *
from bson import json_util,ObjectId

group_blueprint = Blueprint('group', __name__, template_folder='templates')

@group_blueprint.route('/api/group/create', methods=['POST','GET'])
#@requires_roles_groups({'role':['admin']})
@requires_admin({'role':['admin']})
def group_create():
        message = request.get_json(force=True)['groupObj']
        print(message)
        # admin_check
        # username = message['sessionObj']['user_name']
        # if not is_admin(username):
        #     return jsonify(
        #         status='Error',
        #         msg="You are not allowed to access this page."
        #     )
        #del message['sessionObj']
        existing_group = get_all('groups','name',message['name'])
        message.update({"user_list":[]})
        message.update({"role": []})
        if "content_support" not in message.keys():
            message.update({"content_support":[]})
        print(existing_group)
        if len(existing_group)>0:
           return jsonify(
                status='Failed',
                msg="Group creation failed"
            )
        else:
            insert('groups',message)
            return jsonify(
                        status='Success',
                        msg="Group created successfully"
                    )


@group_blueprint.route('/api/group/update', methods=['POST'])
#@requires_roles_groups({'role':['admin']})
@requires_admin({'role':['admin']})
def group_update():
    #try:
            message = request.get_json(force=True)['groupObj']
            print(message)
            # admin_check
            # username = message['sessionObj']['user_name']
            # if not is_admin(username):
            #     return jsonify(
            #         status='Error',
            #         msg="You are not allowed to access this page."
            #     )
            # del message['sessionObj']
            existing_group = get_by_id('groups', message['_id'])
            if len(existing_group) > 0:
                # user = eval(existing_user[0])
                # update('user',message('_id'),{"username":message['username'],'password':message['password'],"type":message['type']})
                id = message['_id']
                del message['_id']
                update('groups', id, message)
                updated_group=get_by_id('groups', id)
                for i in existing_group.get('user_list'):
                    #existing_group['_id'] = str(existing_group.get('_id')).replace('ObjectId(', '').replace(')', '')
                    #print(existing_group)
                    #usr=get_by_id('user',str(i["_id"]).replace('ObjectId(', '').replace(')', ''))
                    user_group_update(str(i["_id"]).replace('ObjectId(', '').replace(')', ''),id,existing_group.get("name"),message["name"],True)
                    # grplst = usr.get('group')
                    # grplst.remove(existing_group)
                    # print(grplst)
                    # updated_group['_id'] = str(existing_group.get('_id')).replace('ObjectId(', '').replace(')', '')
                    # grplst.append(updated_group)
                    # update('user', i, {'group': grplst})
                return jsonify(
                    status='Success',
                    msg="Group updated successfully"
                )
            else:
                return jsonify(
                    status='Failed',
                    msg="Group update failed"
                )

    #except Exception as e:
    #    return jsonify(
    #        status='Failed',
     #       msg=str(e)
     #   )

@group_blueprint.route('/api/group/read', methods=['GET','POST'])
#@requires_roles_groups({'role':['admin']})
@requires_admin({'role':['admin']})
def group_read():
    #try:
        message = request.get_json(force=True)
        print(message)
        # admin_check
        username = message['sessionObj']['user_name']
        # if not is_admin(username):
        #     return jsonify(
        #         status='Error',
        #         msg="You are not allowed to access this page."
        #     )
        del message['sessionObj']
        grps =get_collection('groups')
        grp_list=[]
        for doc in grps:
            print("***",doc)
            grp={}
            role_list = []
            grp['_id']=str(doc['_id'])
            grp['name']=doc['name']
            usr_lst=[]

            for i in doc['user_list']:
                print(i)
                # usr_lst.append(
                #     {"_id": str(i['_id']).replace('ObjectId(', '').replace(')', ''), "name": i['name']})
                usr_lst.append(
                    {
                        "_id": str(i['_id']).replace('ObjectId(', '').replace(')', ''),
                        "name": i['name']
                    })
            grp['user_list']=usr_lst
            # if "role" in doc.keys():
            #
            #     for role in doc['role']:
            #         print(role)
            #         #role_list.append({"_id":str(role['_id']).replace('ObjectId(', '').replace(')', ''),"name":role['name']})
            #         role_list.append(
            #             {
            #                 "_id": str(role['_id']).replace('ObjectId(', '').replace(')', ''),
            #                 "name": role['name']
            #             })
            # grp['role']=role_list
            grp_list.append(grp)
        print("group list :" ,grp_list)
        return jsonify(
                    status='Success',
                    msg=grp_list
                )
    # except Exception as e:
    #     return jsonify(
    #         status='Success',
    #         msg=str(e)
    #     )



@group_blueprint.route('/api/group/delete', methods=['GET','POST'])
#@requires_roles_groups({'role':['admin']})
@requires_admin({'role':['admin']})
def group_delete():
    #try:
        message = request.get_json(force=True)['groupObj']
        print(message)
        # admin_check
        # username = message['sessionObj']['user_name']
        # if not is_admin(username):
        #     return jsonify(
        #         status='Error',
        #         msg="You are not allowed to access this page."
        #     )
        # del message['sessionObj']
        existing_group = get_by_id('groups', message['_id'])
        if len(existing_group) > 0:
            for i in existing_group.get('user_list'):
                # existing_group['_id']=str(existing_group.get('_id')).replace('ObjectId(', '').replace(')', '')
                # print(existing_group)
                # print(get_by_id('user',i).get('group'),"***")
                # grplst=get_by_id('user',i).get('group').remove(existing_group)
                # print(grplst)
                # update('user',i,{'group':grplst})
                user_group_update(str(i["_id"]).replace('ObjectId(', '').replace(')', ''), message['_id'],
                                  existing_group.get("name"), message["name"], False)

            delete('groups', message['_id'])
            return jsonify(
                status='Success',
                msg="Group deleted successfully"
            )
        else:
            return jsonify(
                status='Failed',
                msg="No record found"
            )
    # except Exception as e:
    #     return jsonify(
    #         status='Failed',
    #         msg=str(e)
    #     )


def user_group_update(id,group_id,prev_name,new_name,add=True):
    user = get_by_id('user', id)
    grp_list = user.get("group")
    print("prev",prev_name,"new",new_name)
    grp = {"_id": ObjectId(group_id), "name": prev_name}
    print(grp)
    print("usr_list",grp_list)
    if add:
        if grp not in grp_list:
            grp["name"] = new_name
            grp_list.append(grp)
        elif not (prev_name == new_name):
            grp_list.remove(grp)
            grp["name"] = new_name
            print(grp, "***")
            grp_list.append(grp)
    else:
        grp_list.remove(grp)
    update("user",id,{"group":grp_list})


def role_group_update(id,group_id,prev_name,new_name,add=True):
    role = get_by_id('role', id)
    grp_list = role.get("group_list")
    print("prev",prev_name,"new",new_name)
    grp = {"_id": ObjectId(group_id), "name": prev_name}
    print(grp)
    print("usr_list",grp_list)
    if add:
        if grp not in grp_list:
            grp["name"] = new_name
            grp_list.append(grp)
        elif not (prev_name == new_name):
            grp_list.remove(grp)
            grp["name"] = new_name
            print(grp, "***")
            grp_list.append(grp)
    else:
        grp_list.remove(grp)
    update("role",id,{"group_list":grp_list})

@group_blueprint.route('/api/group/name', methods=['GET'])
def group_name():
    #try:
        message =[i['name'] for i in get_collection('groups')]
        print('***',type(message))
        return jsonify(message)


